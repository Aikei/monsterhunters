var uHttp = require('http')
var uFs = require('fs')

function run_cmd(cmd, args, cb, end)
{
   var spawn = require('child_process').spawn,
       child = spawn(cmd, args),
       me = this;
   child.stdout.on('data', function (buffer) { cb(me, buffer) });
   child.stdout.on('end', end);
}

function GetPolicyResponse()
{
  var xml = '<?xml version="1.0"?>\n<!DOCTYPE cross-domain-policy SYSTEM'
          + ' "http://www.macromedia.com/xml/dtds/cross-domain-policy.dtd">\n<cross-domain-policy>\n';
 
  xml += '<allow-access-from domain="*" to-ports="*"/>\n';
  xml += '<allow-http-request-headers-from-domain="*" headers="*"/>\n'
  xml += '<site-control permitted-cross-domain-policies="master-only"/>\n';
  xml += '</cross-domain-policy>\n\0';
 
  return xml;
}

var server = uHttp.createServer( function (request, response)
{
	//console.log('request received: ',request.url)
	if (request.url === '/crossdomain.xml')
	{
	   //console.log('responding with policy')
		var body = GetPolicyResponse();
		response.writeHead(200, {
		  'Content-Length': body.length,
		  'Content-Type': 'text/xml' });		
		response.end(body);
	}
	else
	{
		var strings = request.url.split('/')
		/*
		if (strings[1] == 'bin')
		   strings[1] += 's'
		if (strings[1] == 'bins')
		{
		  if (strings[2] === 'index.html')
		  {
			 var cmd = new run_cmd('upd-mh-bin', [],
				function(me, buffer) { me.stdout += buffer.toString() },
				function () 
				{ 
				   console.log(cmd.stdout); 
				   PipeData(response, strings.join('/')); 
				})  //request.url); })
		  }
		  else
		  {
			 PipeData(response, strings.join('/')) //request.url)
		  }
		  //var strm = uFs.createReadStream(__dirname+request.url)
		  //if (strm == undefined)
		  //   console.log("undefined file requested", request.url)
		  //else
		  //   strm.pipe(response)
		}
		else
		*/ 
		if(strings[1] == 'mediad')
		{
		  strings = strings.join('/')
		  PipeData(response,strings)
		}
		else if (strings[1] == 'data')
		{
		  PipeData(response,request.url)
		}
	}
})

function PipeData(response,data)
{
   //console.log('piping: ',data)
   var strm = uFs.createReadStream(__dirname+data)
   if (strm == undefined)
      console.log("undefined file requested", data)
   else
      strm.pipe(response)
}

server.listen(11500)

