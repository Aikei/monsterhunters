package 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class Preferences 
	{
		
		static public const SCREEN_WIDTH: Number = 900;
		static public const SCREEN_HEIGHT: Number = 690;
		static public const FULL_SCREEN_WIDTH: Number = 1100;
		static public const FULL_SCREEN_HEIGHT: Number = 690;		
		static public const NUMBER_OF_SKILL_PACKS: int = 32;
		static public var SCROLL_STEP: Number = 20;
		
		static public var REMOTE_SCRIPTS_LOAD: Boolean = true;
		static public var USE_TEST_PORT: Boolean = true;
		static public var TRY_LOCAL_HOST: Boolean = true;
		static public var TRY_REMOTE_HOST: Boolean = true;
		
		static public var TEST_PORT: int = 6600;
		static public var PRODUCTION_PORT: int = 8500;
		static public var REMOTE_HOST: String = "178.62.202.250";
		
		static public var clickingAllowed: Boolean = true;
		//static public var 
		public function Preferences() 
		{
			
		}
		
	}

}