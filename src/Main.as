package 
{
	
	
	import ads.Ads;
    import flash.display.Sprite;
    import flash.display.StageAlign;
	import flash.display.StageDisplayState;
    import flash.display.StageScaleMode;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.system.System;
	import starling.events.ResizeEvent;
	import system.HSounds;
 
    import starling.core.Starling;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;
	import flash.display.Shape;
	import system.Analytics;
	//import my.Preferences;
	
    //[SWF(width = "900", height = "690", framerate = "60", backgroundColor = "#FFFFFF")]
	//[SWF(width = "1200", height = "1200", backgroundColor = "#272d38")]
    public class Main extends Sprite
    {
        private var mStarling:Starling;
		private var _savePhysics: int = -1;
		private var _muteSaved: Boolean = false;
		private var _saveMuteSounds: Boolean;
		private var _saveMuteMusic: Boolean;
		
        public function Main()
        {
			if (stage)
				Init();
			else
				addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
        }
		
		public function OnAddedToStage(event: Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			Init();
		}
		
		public function Init():void 
		{
            // These settings are recommended to avoid problems with touch handling
			stage.align = StageAlign.TOP_LEFT;
            stage.scaleMode = StageScaleMode.NO_SCALE;

			//stage.stageWidth = 900;
			//stage.stageHeight = 690;
			//stage.root.loaderInfo.parameters;
            // Create a Starling instance that will run the "Game" class
			Starling.handleLostContext = true;	
			//var rect: Rectangle = new Rectangle(0, 0, Preferences.SCREEN_WIDTH, Preferences.SCREEN_HEIGHT);
			Analytics.Init(stage);
			//GoogleAnalytics.tracker.trackEvent("Screen", "Loading Screen");
			Analytics.TrackPage("/loading_screen");
            mStarling = new Starling(World, stage, null, null, "auto", "auto");
			//mStarling.viewPort.x = Ads.LEFT_BLOCK_WIDTH;
			//mStarling.antiAliasing = 8;
			//mStarling = new Starling(World, stage, null, null, "auto", "baseline");
			//mStarling = new Starling(World, stage, null, null, Context3DRenderMode.SOFTWARE);
			mStarling.enableErrorChecking = false;
            mStarling.start();

			addEventListener(Event.DEACTIVATE, OnDeactivate);
			addEventListener(Event.ACTIVATE, OnActivate);
			//stage.addEventListener(FocusEvent.FOCUS_OUT, OnClose);
			//addEventListener(Event.REMOVED_FROM_STAGE, OnClose);
			//Starling.current.nativeStage.addEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
			mStarling.stage.addEventListener(Event.RESIZE, OnResize);
		}		
		
		//private function OnClose(e:Event):void 
		//{
			//HSounds.StopTheme();
		//}
		
		//public function OnMouseDown(event: MouseEvent): void 
		//{
			//if (World.vkWindowIncreased)
			//{
				//Starling.current.nativeStage.removeEventListener(MouseEvent.MOUSE_DOWN, OnMouseDown);
				//Starling.current.viewPort = new Rectangle(0, 0, Preferences.SCREEN_WIDTH, Preferences.SCREEN_HEIGHT);
				//Starling.current.stage.stageWidth = Preferences.SCREEN_WIDTH;
				//Starling.current.stage.stageHeight = Preferences.SCREEN_HEIGHT;					
				////MConsole.Write("_ii", "Mouse clicked!");
				////Starling.current.viewPort = new Rectangle(0, 0, 900, 690);
				////mStarling.stage.stageWidth = Starling.current.nativeStage.stageWidth;
				////mStarling.stage.stageHeight = Starling.current.nativeStage.stageHeight;
			//}
		//}
		
		public function OnResize(event: ResizeEvent):void 
		{
			MConsole.Write("_ii", "Resize!");
			Starling.current.viewPort = new Rectangle(0, 0, Preferences.SCREEN_WIDTH, Preferences.SCREEN_HEIGHT);
			Starling.current.stage.stageWidth = Preferences.SCREEN_WIDTH;
			Starling.current.stage.stageHeight = Preferences.SCREEN_HEIGHT;
			Starling.current.nativeStage.stageWidth = Preferences.SCREEN_WIDTH+Ads.RIGHT_BLOCK_WIDTH;
			Starling.current.nativeStage.stageHeight = Preferences.SCREEN_HEIGHT+Ads.BOTTOM_BLOCK_HEIGHT;
		}
						
		public function OnDeactivate(event: Event): void 
		{
			
			if (World.gWorld)
			{
				if (!_muteSaved)
				{
					_muteSaved = true;
					_saveMuteSounds = HSounds.muteSounds;
					_saveMuteMusic = HSounds.muteMusic;
					HSounds.muteSounds = true;
					HSounds.muteMusic = true;	
				}
				if (World.gWorld.physics !== World.gWorld.PHYSICS_DISCRETE)
				{
					_savePhysics = World.gWorld.physics;			
					World.gWorld.physics = World.gWorld.PHYSICS_DISCRETE;
				}
			}
		}
		
		public function OnActivate(event: Event): void 
		{			
			if (World.gWorld)
			{
				if (_muteSaved)
				{
					_muteSaved = false;
					HSounds.muteSounds = _saveMuteSounds;
					HSounds.muteMusic = _saveMuteMusic;					
				}
				if (_savePhysics !== -1)
				{
					World.gWorld.physics = _savePhysics;
					_savePhysics = -1;
					World.gWorld.firstFrameAfterDiscrete = true;
				}
			}
		}		
    }
	
}