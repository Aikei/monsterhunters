package
{
	/**
	 * ...
	 * @author Aikei
	 */

	import controller.profiles.SocialProfile;
	import flash.utils.setTimeout;
	import logic.Dungeon;
	import logic.HeroData;
	import logic.MonsterData;
	import mx.resources.ResourceManager;
	import mx.utils.LoaderUtil;
	import starling.text.BitmapChar;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	import system.HSounds;
	import system.PNGDecoder;
	import system.ProgramData;
	import system.SharedObjectManager;
	import system.PNGDecoder2;
	import view.MonsterSprite;
	
	import flash.display.Loader;
	import flash.events.ProgressEvent;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.utils.Endian;		
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.ui.Mouse;
	import flash.ui.MouseCursorData;	
	import flash.geom.Point;
	import flash.net.URLRequest; 
	import flash.events.Event; 	
	import flash.display.Bitmap;
	import flash.utils.Dictionary;	
	import flash.events.SecurityErrorEvent;
	import flash.events.IOErrorEvent;
	import flash.utils.ByteArray;
	import flash.system.LoaderContext;
	import flash.system.Security;
	import flash.system.ApplicationDomain;
	import flash.system.SecurityDomain;
	import flash.system.System;
	
	//import my.system.Classes;
	
	public class Assets 
	{
		//GlobalEffectFreezingRain;
		public static const RARE_MODE_ALL: int = 0;
		public static const RARE_MODE_ONLY_RARE: int = 1;
		public static const RARE_MODE_WITHOUT_RARE: int = 2;
		
		public static const VIDEO_MEMORY_MAX: Number = 128 * 1024 * 1024;
		public static var TEST: Boolean = false;
		
		//private static var _videoMemoryUsed: Number = 0;
		private static var _monsterAtlases: Dictionary = new Dictionary;
		private static var _modelNames: Vector.<String> = new Vector.<String>;
		//private static var _modelNamesToModelNumbers: Dictionary = new Dictionary;
		
		private static var _rareModelNames: Vector.<String> = new Vector.<String>;

		private static var _monsterStateNames: Vector.<String> = new Vector.<String>;
		private static var _bitmapDataDictionary: Dictionary = new Dictionary;
		private static var _bitmapDataHolder: Dictionary = new Dictionary;
		private static var _atfDataHolder: Dictionary = new Dictionary;
		private static var _downloadResources: Dictionary = new Dictionary;
		
		private static var _lastLocationTextureNumber: uint;
		private static var _locationTextures: Dictionary = new Dictionary;
		private static var _textures : Dictionary = new Dictionary;
		private static var _permanentTextures: Dictionary = new Dictionary;
		//private static var _locationTextures : Array = new Array;
		private static var _loader: Loader = new Loader;
		private static var _urlLoader: URLLoader = new URLLoader;
		private static var _localLoad: Boolean = false;
		private static var _resourcesUrlBeginning: String = "http://178.62.202.250:11500/mediad/";
		private static var _localResourcesUrlBeginning: String = "../mediad/";
		private static var _loadIntoMemoryQueue: Array = new Array;		
		private static var _loadIntoMemoryCounter: int = 0;
		private static var _downloadQueue: Array = new Array;
		private static var _downloadCounter: int = 0;
		private static var _initEnded: Boolean = false;
		private static var _sharedObject: SharedObject;	
		private static var _lruList: Vector.<String> = new Vector.<String>;
		private static var _bitmapData: BitmapData;
		private static var _bytes: ByteArray;
		private static var _deflatedBytes: Dictionary = new Dictionary;
		private static var _pngDecoder: PNGDecoder = new PNGDecoder;
		private static var _idSharedObject: SharedObject;
		private static var _disconnectTime: Number = 0;
		private static var _asyncLoadDone: Boolean = false;
		private static var _syncLoadDone: Boolean = true;
		//private static var _asyncLoadCounter: int = 0;
		private static var _dungeonFilesToLoad: uint = 0;
		private static var _loadedDungeon: Dungeon;
		private static var _monsterDataForNewDungeon: MonsterData;
		private static var _downloadNumber: uint = 0;
		private static var _soNeedsFlush: Boolean = false;
		private static var _asyncLoad: Boolean = false;
		//public static const NUMBER_OF_MONSTER_MODELS: int = 6;
		
		public static function get idSharedObject(): SharedObject 
		{
			return _idSharedObject;
		}
		
		public static function get sharedObject(): SharedObject 
		{
			return _sharedObject
		}
		
		public static var loadOnTheFly: Boolean = false;
		public static var clearSharedObject: Boolean = false;
		
		public static function get monsterStateNames(): Vector.<String> 
		{
			return _monsterStateNames;
		}
		
		private static var _heroNames: Vector.<String> = new Vector.<String>;
		
		public static function get heroNames(): Vector.<String> 
		{
			return _heroNames;
		}
		
		public static var interfaceAtlas: TextureAtlas;
		public static var doorsAtlas: TextureAtlas;
		public static var currentBgAtlas: TextureAtlas;
		public static var loadingScreenAtlas: TextureAtlas;
		public static var miscAtlas: TextureAtlas;
		
		public static var bbold11: BitmapFont;
		public static var bbold12: BitmapFont;
		public static var bbold13: BitmapFont;
		public static var bbold14: BitmapFont;
		public static var bbold15: BitmapFont;
		public static var bbold16: BitmapFont;
		public static var bbold18: BitmapFont;
		public static var bbold24: BitmapFont;	
		
		public static var bregular12: BitmapFont;
		public static var bregular13: BitmapFont;		
		public static var bregular14: BitmapFont;
		
		public static var damageFont: BitmapFont;
		public static var bgLayoutData: Object = new Object;
		
		//public static var additionalClasses = new Classes;
		
		
		[Embed(source="../media/LORA-BOLD.ttf", embedAsCFF="false", fontFamily="Lora")]
		private static const Lora:Class;		
		
		[Embed(source = "../media/fontData/tutorialFont.fnt", mimeType = "application/octet-stream")] public static const TutorialFontXML:Class;
		
		[Embed(source = "../media/fontData/notoserif_bi.fnt", mimeType = "application/octet-stream")] public static const NotoSerifBi13XML:Class;
		
		[Embed(source = "../media/fontData/bold16buttons.fnt", mimeType = "application/octet-stream")] public static const BBold16ButtonsXml:Class;
		[Embed(source = "../media/fontData/bold24buttons.fnt", mimeType = "application/octet-stream")] public static const BBold24ButtonsXml:Class;
		[Embed(source = "../media/fontData/bold24.fnt", mimeType = "application/octet-stream")] public static const BBold24Xml:Class;
		[Embed(source = "../media/fontData/bold18.fnt", mimeType = "application/octet-stream")] public static const BBold18Xml:Class;
		[Embed(source = "../media/fontData/bold16.fnt", mimeType = "application/octet-stream")] public static const BBold16Xml:Class;	
		[Embed(source = "../media/fontData/bold15.fnt", mimeType = "application/octet-stream")] public static const BBold15Xml:Class;
		[Embed(source = "../media/fontData/bold15bgProgress.fnt", mimeType = "application/octet-stream")] public static const BBold15BgProgressXml:Class;
		[Embed(source = "../media/fontData/bold14.fnt", mimeType = "application/octet-stream")] public static const BBold14Xml:Class;	
		[Embed(source = "../media/fontData/bold13.fnt", mimeType = "application/octet-stream")] public static const BBold13Xml:Class;			
		[Embed(source = "../media/fontData/bold12.fnt", mimeType = "application/octet-stream")] public static const BBold12Xml:Class;
		[Embed(source = "../media/fontData/bold11.fnt", mimeType = "application/octet-stream")] public static const BBold11Xml:Class;	
		[Embed(source = "../media/fontData/bold11buttons.fnt", mimeType = "application/octet-stream")] public static const BBold11ButtonsXml:Class;	
		[Embed(source = "../media/fontData/bold12buttons.fnt", mimeType = "application/octet-stream")] public static const BBold12ButtonsXml:Class;
		[Embed(source = "../media/fontData/regular13quests.fnt", mimeType = "application/octet-stream")] public static const BRegular13QuestsXml:Class;
	
		[Embed(source = "../media/fontData/bold12o.fnt", mimeType = "application/octet-stream")] public static const BBold12oXml:Class;
		
		[Embed(source = "../media/fontData/regular12.fnt", mimeType = "application/octet-stream")] public static const BRegular12Xml:Class;
		[Embed(source = "../media/fontData/regular13.fnt", mimeType = "application/octet-stream")] public static const BRegular13Xml:Class;	
		[Embed(source = "../media/fontData/regular14.fnt", mimeType = "application/octet-stream")] public static const BRegular14Xml:Class;
		
		//[Embed(source = "../media/fontData/timerFont.fnt", mimeType = "application/octet-stream")] public static const TimerFontXml:Class;	
		[Embed(source = "../media/fontData/damageFont.xml", mimeType = "application/octet-stream")] public static const DamageFontXml:Class;
		[Embed(source = "../media/fontData/gloryFont.xml", mimeType = "application/octet-stream")] public static const GloryFontXml:Class;
		[Embed(source = "../media/fontData/goldFont.xml", mimeType = "application/octet-stream")] public static const GoldFontXml:Class;
		[Embed(source = "../media/fontData/oopsFont.fnt", mimeType = "application/octet-stream")] public static const OopsFontXML:Class;

		//[Embed(source = '../media/loading.png')] private static const LOADING_SCREEN_TEXTURE: Class;
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//DUNGEONS	
				
		//[Embed(source = '../media/d0/bg.png')] private static const BG_LOCATION_0: Class;
		//[Embed(source = '../media/d0/fg.png')] private static const FG_LOCATION_0: Class;
		//[Embed(source = '../media/d0/mid.png')] private static const MID_LOCATION_0: Class;		
		
		//MONSTERS
		
		//[Embed(source = '../media/dummy/stand.png')] private static const MONSTER_DUMMY_STAND: Class;
		//[Embed(source = '../media/dummy/harm.png')] private static const MONSTER_DUMMY_HARM: Class;		
		
		//[Embed(source = '../media/slime/stand.png')] private static const MONSTER_SLIME_STAND: Class;
		//[Embed(source = '../media/slime/harm.png')] private static const MONSTER_SLIME_HARM: Class;		
		//[Embed(source = '../media/sapling/stand.png')] private static const MONSTER_SAPLING_STAND: Class;	
		//[Embed(source = '../media/sapling/harm.png')] private static const MONSTER_SAPLING_HARM: Class;
		//[Embed(source = '../media/plant/stand.png')] private static const MONSTER_PLANT_STAND: Class;	
		//[Embed(source = '../media/plant/harm.png')] private static const MONSTER_PLANT_HARM: Class;
		
		//bg atlases
		
		[Embed(source = "../media/d0/bg_atlas.xml", mimeType = "application/octet-stream")] private static const BG_ATLAS_XML_0:Class;
		[Embed(source = "../media/d1/bg_atlas.xml", mimeType = "application/octet-stream")] private static const BG_ATLAS_XML_1:Class;
		[Embed(source = "../media/d2/bg_atlas.xml", mimeType = "application/octet-stream")] private static const BG_ATLAS_XML_2:Class;

		///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		[Embed(source = "../media/monsters/dummy/common/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_DUMMY_COMMON_STAND_XML:Class;
		[Embed(source = "../media/monsters/dummy/common/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_DUMMY_COMMON_HARM_XML:Class;		
		[Embed(source = "../media/monsters/dummy/rare/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_DUMMY_RARE_STAND_XML:Class;
		[Embed(source = "../media/monsters/dummy/rare/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_DUMMY_RARE_HARM_XML:Class;		
		[Embed(source = "../media/monsters/dummy/elite/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_DUMMY_ELITE_STAND_XML:Class;
		[Embed(source = "../media/monsters/dummy/elite/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_DUMMY_ELITE_HARM_XML:Class;		
		
		[Embed(source = "../media/monsters/slime/common/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SLIME_COMMON_STAND_XML:Class;
		[Embed(source = "../media/monsters/slime/common/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SLIME_COMMON_HARM_XML:Class;
		[Embed(source = "../media/monsters/slime/rare/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SLIME_RARE_STAND_XML:Class;
		[Embed(source = "../media/monsters/slime/rare/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SLIME_RARE_HARM_XML:Class;
		[Embed(source = "../media/monsters/slime/elite/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SLIME_ELITE_STAND_XML:Class;
		[Embed(source = "../media/monsters/slime/elite/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SLIME_ELITE_HARM_XML:Class;
		
		[Embed(source = "../media/monsters/sapling/common/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SAPLING_COMMON_STAND_XML:Class;
		[Embed(source = "../media/monsters/sapling/common/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SAPLING_COMMON_HARM_XML:Class;
		[Embed(source = "../media/monsters/sapling/rare/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SAPLING_RARE_STAND_XML:Class;
		[Embed(source = "../media/monsters/sapling/rare/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SAPLING_RARE_HARM_XML:Class;
		[Embed(source = "../media/monsters/sapling/elite/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SAPLING_ELITE_STAND_XML:Class;
		[Embed(source = "../media/monsters/sapling/elite/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SAPLING_ELITE_HARM_XML:Class;
		
		[Embed(source = "../media/monsters/plant/common/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_PLANT_COMMON_STAND_XML:Class;		
		[Embed(source = "../media/monsters/plant/common/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_PLANT_COMMON_HARM_XML:Class;				
		[Embed(source = "../media/monsters/plant/rare/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_PLANT_RARE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/plant/rare/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_PLANT_RARE_HARM_XML:Class;
		[Embed(source = "../media/monsters/plant/elite/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_PLANT_ELITE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/plant/elite/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_PLANT_ELITE_HARM_XML:Class;
						
		//graveyard
		
		[Embed(source = "../media/monsters/bat/common/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_BAT_COMMON_STAND_XML:Class;		
		[Embed(source = "../media/monsters/bat/common/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_BAT_COMMON_HARM_XML:Class;				
		[Embed(source = "../media/monsters/bat/rare/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_BAT_RARE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/bat/rare/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_BAT_RARE_HARM_XML:Class;
		[Embed(source = "../media/monsters/bat/elite/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_BAT_ELITE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/bat/elite/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_BAT_ELITE_HARM_XML:Class;	
		
		[Embed(source = "../media/monsters/shadow/common/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SHADOW_COMMON_STAND_XML:Class;		
		[Embed(source = "../media/monsters/shadow/common/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SHADOW_COMMON_HARM_XML:Class;				
		[Embed(source = "../media/monsters/shadow/rare/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SHADOW_RARE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/shadow/rare/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SHADOW_RARE_HARM_XML:Class;
		[Embed(source = "../media/monsters/shadow/elite/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SHADOW_ELITE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/shadow/elite/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SHADOW_ELITE_HARM_XML:Class;	
		
		[Embed(source = "../media/monsters/lurker/common/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_LURKER_COMMON_STAND_XML:Class;		
		[Embed(source = "../media/monsters/lurker/common/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_LURKER_COMMON_HARM_XML:Class;				
		[Embed(source = "../media/monsters/lurker/rare/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_LURKER_RARE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/lurker/rare/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_LURKER_RARE_HARM_XML:Class;
		[Embed(source = "../media/monsters/lurker/elite/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_LURKER_ELITE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/lurker/elite/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_LURKER_ELITE_HARM_XML:Class;
		
		//crypt
		
		[Embed(source = "../media/monsters/skeleton/common/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SKELETON_COMMON_STAND_XML:Class;		
		[Embed(source = "../media/monsters/skeleton/common/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SKELETON_COMMON_HARM_XML:Class;				
		[Embed(source = "../media/monsters/skeleton/rare/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SKELETON_RARE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/skeleton/rare/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SKELETON_RARE_HARM_XML:Class;
		[Embed(source = "../media/monsters/skeleton/elite/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SKELETON_ELITE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/skeleton/elite/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SKELETON_ELITE_HARM_XML:Class;	
		
		[Embed(source = "../media/monsters/goblin/common/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_GOBLIN_COMMON_STAND_XML:Class;		
		[Embed(source = "../media/monsters/goblin/common/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_GOBLIN_COMMON_HARM_XML:Class;				
		[Embed(source = "../media/monsters/goblin/rare/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_GOBLIN_RARE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/goblin/rare/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_GOBLIN_RARE_HARM_XML:Class;
		[Embed(source = "../media/monsters/goblin/elite/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_GOBLIN_ELITE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/goblin/elite/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_GOBLIN_ELITE_HARM_XML:Class;			
		
		[Embed(source = "../media/monsters/sentry/common/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SENTRY_COMMON_STAND_XML:Class;		
		[Embed(source = "../media/monsters/sentry/common/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SENTRY_COMMON_HARM_XML:Class;				
		[Embed(source = "../media/monsters/sentry/rare/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SENTRY_RARE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/sentry/rare/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SENTRY_RARE_HARM_XML:Class;
		[Embed(source = "../media/monsters/sentry/elite/stand.xml", mimeType = "application/octet-stream")] private static const MONSTER_SENTRY_ELITE_STAND_XML:Class;		
		[Embed(source = "../media/monsters/sentry/elite/harm.xml", mimeType = "application/octet-stream")] private static const MONSTER_SENTRY_ELITE_HARM_XML:Class;				
		
		//preloader
		//[Embed(source = '../media/misc/rename_button_preloader.png')] private static const RENAME_PRELOADER_TEXTURE: Class;
		//[Embed(source = "../media/misc/rename_button_preloader.xml", mimeType = "application/octet-stream")] private static const RENAME_PRELOADER_XML:Class;		
		
		//misc atlas: fonts, stun, preloader etc.
		[Embed(source = '../media/misc/miscAtlas.png')] private static const MISC_TEXTURE: Class;
		[Embed(source = "../media/misc/miscAtlas.xml", mimeType = "application/octet-stream")] private static const MISC_XML:Class;
		
		//shady bg
		[Embed(source = '../media/miscOld/shady_bg.png')] public static const SHADY_BG_TEXTURE: Class;
		
		//INTERFACE
		
		[Embed(source = '../media/interface.png')] private static const INTERFACE_ATLAS_TEXTURE: Class;
		[Embed(source = "../media/interface.xml", mimeType = "application/octet-stream")] private static const INTERFACE_ATLAS_XML:Class;
		
		[Embed(source = '../media/doors.atf', mimeType="application/octet-stream")] private static const DOORS_ATLAS_TEXTURE: Class;
		[Embed(source = "../media/doors.xml", mimeType = "application/octet-stream")] private static const DOORS_ATLAS_XML:Class;	
		
		//[Embed(source = '../media/loading_screen_atlas.atf', mimeType="application/octet-stream")] private static const LOADING_SCREEN_ATLAS_TEXTURE: Class;
		[Embed(source = '../media/loading_screen_atlas.png')] private static const LOADING_SCREEN_ATLAS_TEXTURE: Class;
		[Embed(source = "../media/loading_screen_atlas.xml", mimeType = "application/octet-stream")] private static const LOADING_SCREEN_ATLAS_XML:Class;			
		//[Embed(source = '../media/hand.png')] public static const Cursor:Class;
						
		//CURSOR
		//[Embed(source = '../media/hand.png')] public static const CursorHand: Class;
		//[Embed(source = '../media/weapon_0.png')] public static const CursorWeapon0: Class;
		//[Embed(source = '../media/weapon_1.png')] public static const CursorWeapon1: Class;
		//[Embed(source = '../media/weapon_2.png')] public static const CursorWeapon2: Class;
		
		//sounds
		[Embed(source = '../media/sfx/dungeons/orangery_theme2.ogg',mimeType="application/octet-stream")] public static const DungeonTheme0: Class;
		//[Embed(source = '../media/sfx/dungeons/graveyard_theme.ogg', mimeType = "application/octet-stream")] public static const DungeonTheme1: Class;		
		
		[Embed(source = '../media/sfx/punches/punch1.ogg',mimeType="application/octet-stream")] public static const Hit0: Class;
		[Embed(source = '../media/sfx/punches/punch2.ogg',mimeType="application/octet-stream")] public static const Hit1: Class;
		[Embed(source = '../media/sfx/punches/punch3.ogg',mimeType="application/octet-stream")] public static const Hit2: Class;
		[Embed(source = '../media/sfx/death/death5.ogg',mimeType="application/octet-stream")] public static const Death0: Class;		
		[Embed(source = '../media/sfx/coins/drop.ogg',mimeType="application/octet-stream")] public static const Drop0: Class;
		[Embed(source = '../media/sfx/coins/collect.ogg',mimeType="application/octet-stream")] public static const Collect0: Class;			
		[Embed(source = '../media/sfx/purchase/purchase.ogg',mimeType="application/octet-stream")] public static const Purchase: Class;		
		[Embed(source = '../media/sfx/poison/poison.ogg',mimeType="application/octet-stream")] public static const Poison: Class;		
		[Embed(source = '../media/sfx/menu/Locked_Down_Gears_01.ogg', mimeType = "application/octet-stream")] public static const WindowMove1: Class;
		[Embed(source = '../media/sfx/menu/menu2.ogg', mimeType = "application/octet-stream")] public static const WindowMove2: Class;
		[Embed(source = '../media/sfx/doors/doors_close.ogg', mimeType = "application/octet-stream")] public static const DoorsClose: Class;
		[Embed(source = '../media/sfx/doors/doors_open.ogg', mimeType = "application/octet-stream")] public static const DoorsOpen: Class;		
		[Embed(source = '../media/sfx/ui/click.ogg', mimeType = "application/octet-stream")] public static const ClickSound: Class;
		[Embed(source = '../media/sfx/ui/hover.ogg', mimeType = "application/octet-stream")] public static const HoverSound: Class;			
		[Embed(source = '../media/sfx/level/fanfares.ogg', mimeType = "application/octet-stream")] public static const LevelComplete0: Class;		
		[Embed(source = '../media/sfx/hero_atk/atk3.ogg', mimeType = "application/octet-stream")] public static const HeroAttack: Class;		
		[Embed(source = '../media/sfx/fail/fail3.ogg', mimeType = "application/octet-stream")] public static const Fail: Class;		
		[Embed(source = '../media/sfx/quests/quest_received2.ogg', mimeType = "application/octet-stream")] public static const QuestReceived: Class;		
		[Embed(source = '../media/sfx/quests/quest_menu_move.ogg', mimeType = "application/octet-stream")] public static const QuestMenuMove: Class;
		[Embed(source = '../media/sfx/quests/scroll_drop.ogg', mimeType = "application/octet-stream")] public static const ScrollDrop: Class;		
		[Embed(source = '../media/sfx/quests/quest_done.ogg', mimeType = "application/octet-stream")] public static const QuestDone: Class;			
		[Embed(source = '../media/sfx/stun/stun.ogg', mimeType = "application/octet-stream")] public static const Stun: Class;			
		[Embed(source = '../media/sfx/timer/timer.ogg', mimeType = "application/octet-stream")] public static const RunningOutOfTime: Class;	
		[Embed(source = '../media/sfx/ui/fade.ogg', mimeType = "application/octet-stream")] public static const Fade: Class;
		[Embed(source = '../media/sfx/skill/Skill4.ogg', mimeType = "application/octet-stream")] public static const SkillPurchased: Class;
		[Embed(source = '../media/sfx/hypnosis/hypnosis.ogg', mimeType = "application/octet-stream")] public static const Hypnosis: Class;		
		[Embed(source = '../media/sfx/heal/heal.ogg', mimeType = "application/octet-stream")] public static const Heal1: Class;
		
		public static function get modelNames(): Vector.<String> 
		{
			return _modelNames;
		}
		
		public static function InitLocalAssets(): void 
		{
			MConsole.Write("_ii", "Initing local assets");
			bgLayoutData[Dungeon.DUNGEON_ORANGERY] = { fgPos: "br" };
			bgLayoutData[Dungeon.DUNGEON_GRAVEYARD] = { fgPos: "br" };
			bgLayoutData[Dungeon.DUNGEON_TWILIGHT_CRYPT] = { fgPos: "br" };
			_sharedObject = SharedObject.getLocal("MonsterHunters", "/");
			miscAtlas = new TextureAtlas(GetPermanentTexture("MISC_TEXTURE"), XML(new MISC_XML));
			interfaceAtlas = new TextureAtlas(GetPermanentTexture("INTERFACE_ATLAS_TEXTURE"), XML(new INTERFACE_ATLAS_XML));
			doorsAtlas = new TextureAtlas(GetEmbeddedAtfTexture("DOORS_ATLAS_TEXTURE"), XML(new DOORS_ATLAS_XML));
			loadingScreenAtlas = new TextureAtlas(GetPermanentTexture("LOADING_SCREEN_ATLAS_TEXTURE"), XML(new LOADING_SCREEN_ATLAS_XML));
			LoadFonts();			
		}
		
		public static function ClearCache():void 
		{
			_sharedObject.clear();
		}
		
		public static function GetDungeonResources(dng: Object,rareMode: int): Array 
		{
			var resources: Array = new Array;
			if (rareMode !== RARE_MODE_ONLY_RARE)
			{
				if (dng.type < 3)
					resources.push("BG_LOCATION_" + dng.type);
				else
					resources.push("BG_LOCATION_0");
			}
			var endR: int = MonsterData.NUMBER_OF_RARE_TYPES;
			if (rareMode === RARE_MODE_WITHOUT_RARE)
				endR = 1;
			var startR: int = 0;
			if (rareMode === RARE_MODE_ONLY_RARE)
				startR = 1;
			for (var i: int = 0; i < dng.possibleMonsters.length; i++)
			{
				var modelName: String = MonsterSprite.GetMonsterModelNameByType(dng.possibleMonsters[i]);
				for (var r: int = startR; r < endR; r++)
				{
					for (var s: int = 0; s < _monsterStateNames.length; s++)
					{
						resources.push(GetMonsterModelResourceName(modelName, r, s));
					}
				}
			}
			return resources;
		}
				
		public static function LoadDungeonResources(dng: Object,rareMode: int = RARE_MODE_ALL, async: Boolean = false):void 
		{
			MConsole.Write("_ii", "Loading dungeon resources");
			HSounds.StopTheme();
			if (rareMode === RARE_MODE_ALL)
				FreeDungeonResources();								
			_loadIntoMemoryCounter = 0;	
			_loadIntoMemoryQueue = GetDungeonResources(dng,rareMode);
			_asyncLoad = async;
			
			if (!_asyncLoad)
			{
				for (_loadIntoMemoryCounter = 0; _loadIntoMemoryCounter < _loadIntoMemoryQueue.length; _loadIntoMemoryCounter++)
				{
					LoadBitmapDataFromSharedObjectOnFly(_loadIntoMemoryQueue[_loadIntoMemoryCounter],true);
				}
				DungeonLoadComplete();
			}
			else 
			{
				_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, OnOnFlyBitmapDataDecodingComplete);				
				DungeonLoadNextAsync();					
			}
		}		
			
		private static function DungeonLoadNextAsync():void 
		{			
			World.gWorld.dispatchEventWith("dungeonLoadProgress", false, { loaded: _loadIntoMemoryCounter, total: _dungeonFilesToLoad } );	
			if (_loadIntoMemoryCounter == _loadIntoMemoryQueue.length)
			{
				DungeonLoadComplete();
			}
			else
			{
				setTimeout(LoadBitmapDataFromSharedObjectOnFly,200,_loadIntoMemoryQueue[_loadIntoMemoryCounter], true);	
			}
		}
			
		private static function DungeonLoadComplete():void 
		{
			MConsole.Write("_ii", "Dungeon resources loaded");
			_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, OnOnFlyBitmapDataDecodingComplete);
			//_sharedObject.close();
			//_sharedObject = null;
			
			//for (var i: int = 0; i < dng.possibleMonsters.length; i++)
			//{
				//var modelName: String = MonsterSprite.GetMonsterModelNameByType(dng.possibleMonsters[i]);
				//_monsterAtlases[modelName] = new Vector.<Vector.<Object>>;
				//for (var k: int = 0; k < _rareModelNames.length; k++)
				//{
					//_monsterAtlases[modelName][k] = new Vector.<Object>;
					//for (var j: int = 0; j < MonsterSprite.NUMBER_OF_STATES; j++)
					//{
						//var str: String = "MONSTER_" + modelName.toUpperCase() +"_"+ _rareModelNames[k].toUpperCase()+"_"+_monsterStateNames[j].toUpperCase();
						//var xml: XML = XML(new Assets[str + "_XML"]);
						//_monsterAtlases[modelName][k].push({ str: str, xml: xml });
					//}
				//}
			//}			
			
			setTimeout(World.gWorld.dispatchEventWith,100,"dungeonLoadDone");			
		}
		
		public static function FreeDungeonResources():void 
		{
			//for (var modelKey in _monsterAtlases)
			//{
				//if (_monsterAtlases[modelKey] != undefined)
				//{
					//for (var rarity in _monsterAtlases[modelKey])
					//{
						//for (var state in _monsterAtlases[modelKey][rarity])
							//TextureAtlas(_monsterAtlases[modelKey][rarity][state]).dispose();
					//}
				//}
			//}
			//_monsterAtlases = null;
			//_monsterAtlases = new Dictionary;
			
			for (var key in _textures)
			{
				Texture(_textures[key]).dispose();				
			}
			_textures = new Dictionary;
			
			for (key in _locationTextures)
			{
				Texture(_locationTextures[key]).dispose();				
			}
			_locationTextures = new Dictionary;	
			
			for (key in _bitmapDataHolder)
			{
				BitmapData(_bitmapDataHolder[key]).dispose();
			}
			_bitmapDataHolder = new Dictionary;
			
			for (key in _atfDataHolder)
			{
				_atfDataHolder[key] = null;
			}
			_atfDataHolder = new Dictionary;				
		}
		
		public static function LoadFonts():void 
		{
			MConsole.Write("_ii", "Loading fonts");
			var fontTexture: Texture;
			
			fontTexture = miscAtlas.getTexture("tutorialFont");
			var bitmapFont: BitmapFont = new BitmapFont(fontTexture, XML(new TutorialFontXML));
			TextField.registerBitmapFont(bitmapFont, "tutorialFont");
			
			fontTexture = miscAtlas.getTexture("gloryFont");
			TextField.registerBitmapFont(new BitmapFont(fontTexture, XML(new GloryFontXml)), "gloryFont");
			var char: BitmapChar = new BitmapChar(64, Assets.interfaceAtlas.getTexture("renown"), 0, 0, Assets.interfaceAtlas.getTexture("renown").width);
			TextField.getBitmapFont("gloryFont").addChar(64, char);
			
			fontTexture = miscAtlas.getTexture("damageFont");
			damageFont = new BitmapFont(fontTexture, XML(new DamageFontXml));
			TextField.registerBitmapFont(damageFont, "damageFont");
			
			fontTexture = miscAtlas.getTexture("goldFont");
			TextField.registerBitmapFont(new BitmapFont(fontTexture, XML(new GoldFontXml)), "goldFont");			
			
			fontTexture = miscAtlas.getTexture("bold24_0");
			bbold24 = new BitmapFont(fontTexture, XML(new BBold24Xml));
			TextField.registerBitmapFont(bbold24, "bbold24");
			TextField.registerBitmapFont(new BitmapFont(fontTexture, XML(new BBold24ButtonsXml)), "bbold24buttons");
				
			fontTexture = miscAtlas.getTexture("bold18_0");
			bbold18 = new BitmapFont(fontTexture, XML(new BBold18Xml));
			TextField.registerBitmapFont(bbold18,"bbold18");
			
			fontTexture = interfaceAtlas.getTexture("bold16_0");
			bbold16 = new BitmapFont(fontTexture, XML(new BBold16Xml));
			TextField.registerBitmapFont(bbold16, "bbold16");
			TextField.registerBitmapFont(new BitmapFont(fontTexture, XML(new BBold16ButtonsXml)), "bbold16buttons");
			
			fontTexture = interfaceAtlas.getTexture("bold15_0");
			bbold15 = new BitmapFont(fontTexture, XML(new BBold15Xml));
			TextField.registerBitmapFont(bbold15,"bbold15");
			
			TextField.registerBitmapFont(new BitmapFont(fontTexture, XML(new BBold15BgProgressXml)), "bbold15bgProgress");
			
			fontTexture = interfaceAtlas.getTexture("bold14_0");
			bbold14 = new BitmapFont(fontTexture, XML(new BBold14Xml));
			TextField.registerBitmapFont(bbold14,"bbold14");		
			
			fontTexture = interfaceAtlas.getTexture("bold13_0");
			bbold13 = new BitmapFont(fontTexture, XML(new BBold13Xml));
			TextField.registerBitmapFont(bbold13,"bbold13");
			
			fontTexture = interfaceAtlas.getTexture("bold12_0");
			bbold12 = new BitmapFont(fontTexture, XML(new BBold12Xml));		
			TextField.registerBitmapFont(bbold12, "bbold12");
			char = new BitmapChar(64, Assets.interfaceAtlas.getTexture("into_the_dungeon_button_arrow"), 0, 0, Assets.interfaceAtlas.getTexture("into_the_dungeon_button_arrow").width);
			bbold12.addChar(64, char);				
			TextField.registerBitmapFont(new BitmapFont(fontTexture, XML(new BBold12ButtonsXml)), "bbold12buttons");
			
			fontTexture = interfaceAtlas.getTexture("bold11_0");
			bbold11 = new BitmapFont(fontTexture, XML(new BBold11Xml));
			TextField.registerBitmapFont(bbold11, "bbold11");
			
			TextField.registerBitmapFont(new BitmapFont(fontTexture, XML(new BBold11ButtonsXml)), "bbold11buttons");
						
			fontTexture = interfaceAtlas.getTexture("regular12_0");
			bregular12 = new BitmapFont(fontTexture, XML(new BRegular12Xml));
			TextField.registerBitmapFont(bregular12,"bregular12");	
			
			fontTexture = interfaceAtlas.getTexture("regular13_0");
			bregular13 = new BitmapFont(fontTexture, XML(new BRegular13Xml));
			TextField.registerBitmapFont(bregular13, "bregular13");
			TextField.registerBitmapFont(new BitmapFont(fontTexture, XML(new BRegular13QuestsXml)), "bregular13quests");
			
			fontTexture = interfaceAtlas.getTexture("regular14_0");
			bregular14 = new BitmapFont(fontTexture, XML(new BRegular14Xml));
			TextField.registerBitmapFont(bregular14, "bregular14");
			
			fontTexture = interfaceAtlas.getTexture("bold12o_0");
			TextField.registerBitmapFont(new BitmapFont(fontTexture, XML(new BBold12oXml)), "bbold12o");
			
			fontTexture = interfaceAtlas.getTexture("notoserif_bi_0");
			TextField.registerBitmapFont(new BitmapFont(fontTexture, XML(new NotoSerifBi13XML)), "notoserif_bi");	
			
			fontTexture = miscAtlas.getTexture("oopsFont");
			TextField.registerBitmapFont(new BitmapFont(fontTexture, XML(new OopsFontXML)), "oopsFont");			
		}
		
		public static function GetLocationTexture(dungeonType: String, textureName: String): Texture 
		{
			if (textureName == "mid")
				return GetTexture("MID_LOCATION_0");
			else
				return GetTexture(textureName.toUpperCase() + "_LOCATION_" + dungeonType);
		}
		
		public static function InitAssests(): void
		{
			_heroNames[HeroData.HERO_BARBARIAN] = "barbarian";
			_heroNames[HeroData.HERO_RANGER] = "ranger";
			_heroNames[HeroData.HERO_MAGE] = "mage";
			_heroNames[HeroData.HERO_ROGUE] = "rogue";
			_heroNames[HeroData.HERO_ENGINEER] = "engineer";
			_heroNames[HeroData.HERO_PALADIN] = "paladin";			
			
			_modelNames[0] = "dummy";
			_modelNames[1] = "slime";
			_modelNames[2] = "sapling";
			_modelNames[3] = "plant";
			
			_modelNames[4] = "bat";
			_modelNames[5] = "shadow";
			_modelNames[6] = "lurker";
			
			_modelNames[7] = "skeleton";
			_modelNames[8] = "goblin";
			_modelNames[9] = "sentry";					
			
			_rareModelNames[0] = "common";
			_rareModelNames[1] = "rare";
			_rareModelNames[2] = "elite";
			
			_monsterStateNames[MonsterSprite.STATE_STANDING] = "stand";
			_monsterStateNames[MonsterSprite.STATE_HARMED] = "harm";
			
			_lastLocationTextureNumber = 2;	
			
			for (var i: int = 0; i <= _lastLocationTextureNumber; i++)
			{
				_downloadResources["BG_LOCATION_" + i] = "d" + i + "/bg_atlas.atf";
			}
					
			for (i = 0; i < _modelNames.length; i++)
			{
				for (var k: int = 0; k < _rareModelNames.length; k++)
				{
					for (var j: int = 0; j < MonsterSprite.NUMBER_OF_STATES; j++)
					{
						var resourceName: String = GetMonsterModelResourceName(_modelNames[i], k, j);
						var resourcePath: String = "monsters/" + _modelNames[i] + "/" + _rareModelNames[k] + "/" + _monsterStateNames[j] + ".atf";
						_downloadResources[resourceName] = resourcePath;
					}
				}
			}
			
			_idSharedObject = SharedObject.getLocal("MonsterHuntersId");
			if (clearSharedObject)
				_sharedObject.clear();		
			if (_sharedObject.data.version != ProgramData.version)
			{
				if (ProgramData.clearId)
					_idSharedObject.clear();				
				if (ProgramData.clearAllCacheNeeded)
				{
					_sharedObject.clear();
				}
				else
				{
					for (i = 0; i < ProgramData.clearFollowingImages.length; i++)
					{
						if (_sharedObject.data.images)
						{
							var obj = _sharedObject.data.images[ProgramData.clearFollowingImages[i]];
							if (obj != undefined)
							{
								delete _sharedObject.data.images[ProgramData.clearFollowingImages[i]];
							}
						}
					}						
				}	
			}
			if (_sharedObject.data.images == undefined)
				_sharedObject.data.images = { };
			_sharedObject.data.version = ProgramData.version;
			if (_idSharedObject.data.profile != undefined)
			{
				World.gWorld.socialProfile.InitProfileFromSharedData(_idSharedObject.data.profile);
			}
			_idSharedObject.flush();	
			
			_loader.contentLoaderInfo.addEventListener(Event.COMPLETE, OnBitmapDataDecodingComplete);
            _loader.contentLoaderInfo.addEventListener(SecurityErrorEvent.SECURITY_ERROR, sSecurityErrorHandler);
            _loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, sIoErrorHandler);	
			 
			_urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
			_urlLoader.addEventListener(Event.COMPLETE, OnFileDownloadComplete);
            _urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, sSecurityErrorHandler);
            _urlLoader.addEventListener(IOErrorEvent.IO_ERROR, sIoErrorHandler);
		}
		
		public static function GetMonsterModelResourceName(modelName: String, rarity: int, state: int): String 
		{
			var str: String = "MONSTER_" + modelName.toUpperCase() +"_" +_rareModelNames[rarity].toUpperCase() + "_" + _monsterStateNames[state].toUpperCase();
			return str;
		}
		
		public static function DownloadAssets(dataObject: Object): void
		{
			_downloadNumber++;
			_downloadQueue = new Array();
			_downloadCounter = 0;
			
			if (_downloadNumber === 1)
			{
				AddDungeonToDownloadQueue(dataObject.dungeons[dataObject.game.currentDungeon],RARE_MODE_ALL);
			}
			else if (_downloadNumber === 2)
			{
				for (var i: int = 0; i < dataObject.dungeons.length; i++)
				{
					if (i != dataObject.game.currentDungeon)
					{
						AddDungeonToDownloadQueue(dataObject.dungeons[i],RARE_MODE_ALL);
					}
				}
			}	
					
			if (_downloadQueue.length > 0)
				DownloadNext();
			else
				ContinueInit();
		}		
		
		//public static function DownloadAssets(dataObject: Object): void
		//{
			//_downloadNumber++;
			//_downloadQueue = new Array();
			//_downloadCounter = 0;
			//
			//if (_downloadNumber === 1)
			//{
				//AddDungeonToDownloadQueue(dataObject.dungeons[dataObject.game.currentDungeon],RARE_MODE_WITHOUT_RARE);
			//}
			//else if (_downloadNumber === 2)
			//{
				//AddDungeonToDownloadQueue(dataObject.dungeons[dataObject.game.currentDungeon],RARE_MODE_ONLY_RARE);
			//}
			//else if (_downloadNumber === 3)
			//{
				//for (var i: int = 0; i < dataObject.dungeons.length; i++)
				//{
					//if (i != dataObject.game.currentDungeon)
					//{
						//AddDungeonToDownloadQueue(dataObject.dungeons[i],RARE_MODE_ALL);
					//}
				//}
			//}	
					//
			//if (_downloadQueue.length > 0)
				//DownloadNext();
			//else
				//ContinueInit();
		//}
		
		private static function AddDungeonToDownloadQueue(dng: Object,rareMode: int):void 
		{
			var resources: Array = GetDungeonResources(dng,rareMode);
			for (var i: int = 0; i < resources.length; i++)
			{
				_downloadQueue.push([ resources[i], _downloadResources[resources[i]] ]);
			}
		}
		
		private static function OnDownloadProgress(event: ProgressEvent):void 
		{
			//MConsole.Write("_ii", "Downloaded", event.bytesLoaded / 1024, "kb of", event.bytesTotal/1024,"kb");
		}
		
		private static function OnDecodeProgress(event: ProgressEvent):void 
		{
			//MConsole.Write("_ii", "Decoded", event.bytesLoaded / 1024, "kb of", event.bytesTotal/1024,"kb");
		}		
		
		private static function sSecurityErrorHandler(event: SecurityErrorEvent):void 
		{
			MConsole.Write("e", event.text);
			if (_disconnectTime == 0)
				_disconnectTime = Misc.GetTimeSecs();
			if (Misc.GetTimeSecs() - _disconnectTime > 60)
				World.gWorld.dispatchEventWith("N_StartDungeonTransition", false, { type: -1, message: Language.GetString("CantLoadResources") } );
		}
		
		private static function sIoErrorHandler(event: IOErrorEvent):void 
		{
			MConsole.Write("e", event.text);
			if (_disconnectTime == 0)
				_disconnectTime = Misc.GetTimeSecs();
			if (Misc.GetTimeSecs() - _disconnectTime > 60)			
				World.gWorld.dispatchEventWith("N_StartDungeonTransition", false, { type: -1, message: Language.GetString("CantLoadResources") } );			
		}
		
		public static function DownloadNext():void 
		{
			var obj: Object = _sharedObject.data.images[_downloadQueue[_downloadCounter][0]];
			MConsole.Write("_ii", "Loading asset", _downloadQueue[_downloadCounter][1]);
			MConsole.Write("_ii", "System memory: ", System.totalMemoryNumber / 1024, "k");
			MConsole.Write("_ii", "Free memory: ", System.freeMemory / 1024, "k");
			if (obj != null)
			{
				MConsole.Write("_ii", "Asset already loaded");
				//LoadBitmapDataFromSharedObject();
				IncreaseDownloadCounter();
			}
			else
			{
				var url: URLRequest;
				if (_localLoad)
					url = new URLRequest(_localResourcesUrlBeginning + _downloadQueue[_downloadCounter][1] + "d");
				else
					url = new URLRequest(_resourcesUrlBeginning + _downloadQueue[_downloadCounter][1] + "d");
				_urlLoader.load(url);
			}
		}
		
		private static function OnFileDownloadComplete(event: Event): void 
		{
			var l: uint = _downloadQueue.length;
			MConsole.Write("_ii", "Loaded", _downloadCounter + 1, "assets of", l);
			World.gWorld.dispatchEventWith("downloadProgress", false, { loaded: _downloadCounter + 1, total: l } );
			_sharedObject.data.images[_downloadQueue[_downloadCounter][0]] = _urlLoader.data;
			IncreaseDownloadCounter();
			//LoadBitmapDataFromSharedObject();
		}		
		
		private static function LoadIntoMemory(resource: String): Texture 
		{
			if (_permanentTextures[resource] !== undefined)
				return _permanentTextures[resource];
			if (_locationTextures[resource] != undefined)
				return  _locationTextures[resource];
			//if (_textures[resource] == undefined)
			//{
				//_bytes = new ByteArray;
				//_bytes.writeBytes(_atfDataHolder[resource]);
				//_bytes.uncompress();
				//_textures[resource] = Texture.fromAtfData(_bytes);
				////_bitmapData = _bitmapDataHolder[resource];
				//PushToLru(resource);
				//_bitmap = _bitmapDataHolder[resource];
				//if (_bitmap != undefined)
				//{
					////var size: Number = GetTextureSizeInBytes(_bitmapData.width, _bitmapData.height);
					//var size: Number = GetTextureSizeInBytes(_bitmap.width, _bitmap.height);
					//FreeEnoughMemory(size);
					//_textures[resource] = Texture.fromBitmap(_bitmapDataHolder[resource], false);
					////_textures[resource] = Texture.fromBitmapData(_bitmapDataHolder[resource], false);
					//_videoMemoryUsed += size;
				//}
				//else
					//return null
			//}
			return _textures[resource];
		}
		
		//private static function PushToLru(resource: String):void 
		//{
			//for (var i: int = 0; i < _lruList.length; i++)
			//{
				//if (_lruList[i] == resource)
				//{
					//_lruList.splice(i, 1);
					//break;
				//}
			//}
			//_lruList.push(resource);			
		//}
		
		private static function LoadBitmapDataFromSharedObjectOnFly(resource: String, isAtf: Boolean): void 
		{
			_bytes = new ByteArray;
			_bytes.writeBytes(_sharedObject.data.images[resource]);
			if (_bytes != null)
			{
				_bytes.uncompress();
				if (!isAtf)
				{					
					_loader.loadBytes(_bytes);
				}
				else
				{
					_atfDataHolder[resource] = _bytes;
					_textures[resource] = Texture.fromAtfData(_bytes);
					
					if (resource.slice(0, 11) === "BG_LOCATION")
					{
						var s: String = resource.slice(12);
						if (currentBgAtlas)
						{
							currentBgAtlas.dispose();
							currentBgAtlas = null;
						}
						currentBgAtlas = new TextureAtlas(_textures[resource], XML(new Assets["BG_ATLAS_XML_" + s]));
					}
					if (_asyncLoad)
					{
						_loadIntoMemoryCounter++;
						DungeonLoadNextAsync();
					}
				}
			}
		}		
		
		private static function LoadBitmapDataFromSharedObject(): void 
		{
			_bytes = new ByteArray;
			_bytes.writeBytes(_sharedObject.data.images[_loadIntoMemoryQueue[_loadIntoMemoryCounter][0]]);
			if (_bytes != null)
			{
				_bytes.uncompress();
				if (_loadIntoMemoryCounter <= _lastLocationTextureNumber)
				{					
					_loader.loadBytes(_bytes);
				}
				else
				{
					//_atfDataHolder[_loadQueue[_loadCounter][0]] = _bytes;
					_textures[_loadIntoMemoryQueue[_loadIntoMemoryCounter][0]] = Texture.fromAtfData(_bytes);
					IncreaseDownloadCounter();
				}
			}		
		}
		
		private static function OnBitmapDataDecodingComplete(event: Event):void 
		{
			//_bitmapData = Bitmap(_loader.content).bitmapData;
			//_bitmapData = Bitmap(_loader.content).bitmapData;
			//_bitmapDataHolder[_loadQueue[_loadCounter][0]] = _bitmapData;
			IncreaseDownloadCounter();
		}
		
		private static function OnOnFlyBitmapDataDecodingComplete(event: Event):void 
		{
			if (_bitmapDataHolder[_loadIntoMemoryQueue[_loadIntoMemoryCounter]] == undefined)
			{
				_bitmapData = Bitmap(_loader.content).bitmapData;
				_bitmapDataHolder[_loadIntoMemoryQueue[_loadIntoMemoryCounter]] = _bitmapData;
			}
			else
			{
				_bitmapData = _bitmapDataHolder[_loadIntoMemoryQueue[_loadIntoMemoryCounter]];
			}

			_locationTextures[_loadIntoMemoryQueue[_loadIntoMemoryCounter]] = Texture.fromBitmapData(_bitmapData);			
			_loadIntoMemoryCounter++;
			World.gWorld.dispatchEventWith("dungeonLoadProgress", false, { loaded: _loadIntoMemoryCounter, total: _loadIntoMemoryQueue.length } );			
			
			DungeonLoadNextAsync();
		}		
		
		//private static function LoadTextureFromSharedObject(resource: String): void 
		//{
			//_bytes = new ByteArray;
			//_bytes.writeBytes(_sharedObject.data.images[resource]);
			//if (_bytes != null)
			//{
				//_bytes.uncompress();
				//_bitmapData = PNGDecoder2.decodeImage(_bytes);
				//_textures[resource] = Texture.fromBitmapData(_bitmapData);
			//}		
		//}
				
		//public static function LoadNext():void 
		//{
			//var obj: Object = _sharedObject.data.images[_loadQueue[_loadCounter][0]];
			//MConsole.Write("_ii", "Loading asset", _loadQueue[_loadCounter][1]);
			//MConsole.Write("_ii", "System memory: ", System.totalMemoryNumber / 1024, "k");
			//MConsole.Write("_ii", "Free memory: ", System.freeMemory / 1024, "k");
			//if (obj != null)
			//{
				//MConsole.Write("_ii", "Asset already loaded");
				//IncreaseCounter();
			//}
			//else
			//{
				//var url: URLRequest = new URLRequest(_resourcesUrlBeginning + _loadQueue[_loadCounter][1]);	
				//_loader.load(url,new LoaderContext(true));
			//}
		//}
		//
		//private static function OnComplete(event: Event): void 
		//{
			//MConsole.Write("_ii", "Loaded", _loadCounter+1, "assets of", _loadQueue.length);
			//_bitmapData = Bitmap(_loader.content).bitmapData;
			//_bytes = _bitmapData.getPixels(_bitmapData.rect);
			//_bytes.compress();
			//_sharedObject.data.images[_loadQueue[_loadCounter][0]] = { bytes: _bytes, width: _bitmapData.width, height: _bitmapData.height };
			////_sharedObject.flush();
			//IncreaseCounter();
		//}		
		//
		//private static function LoadIntoMemory(resource: String): Texture 
		//{
			//if (_textures[resource] != undefined)
				//return _textures[resource]
			//else
			//{
				//var obj: Object = _sharedObject.data.images[resource];
				//if (obj != null)
				//{
					////var bitmapData: BitmapData = new BitmapData(obj.width, obj.height, true);
					////var bytes: ByteArray = ByteArray(obj.bytes);
					//_bitmapData = new BitmapData(obj.width, obj.height, true);
					//_bytes = new ByteArray();
					//_bytes.writeBytes(ByteArray(obj.bytes));
					//_bytes.uncompress();
					//if (System.totalMemoryNumber + _bytes.length > MEMORY_MAX)
						//FreeEnoughMemory(_bytes.length);
					//_bitmapData.setPixels(new Rectangle(0, 0, obj.width, obj.height), _bytes);
					//_textures[resource] = Texture.fromBitmapData(_bitmapData);
					//for (var i: int = 0; i < _lruList.length; i++)
					//{
						//if (_lruList[i] == resource)
						//{
							//_lruList.splice(i, 1);
							//break;
						//}
					//}
					//_lruList.push(resource);
					//return _textures[resource];
				//}
				//else
					//return null;
			//}
		//}
		
		private static function GetTextureSizeInBytes(w: Number, h: Number): Number 
		{
			return w*h*4;
		}
		
		//private static function FreeEnoughMemory(needed: Number):void 
		//{
			//while (VIDEO_MEMORY_MAX - _videoMemoryUsed < needed)
			//{
				//_videoMemoryUsed -= GetTextureSizeInBytes(_textures[_lruList[0]].width, _textures[_lruList[0]].height);
				//_textures[_lruList[0]].dispose();
				//_textures[_lruList[0]] = null;
				//delete _textures[_lruList[0]];
				//_lruList.splice(0, 1);
			//}
		//}		
		
		private static function IncreaseDownloadCounter():void 
		{
			_downloadCounter++;
			if (_downloadCounter < _downloadQueue.length)
			{
				DownloadNext();
			}
			else
			{
				//_loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, OnBitmapDataDecodingComplete);
				//_urlLoader.removeEventListener(Event.COMPLETE, OnFileDownloadComplete);
				//_urlLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, sSecurityErrorHandler);
				//_urlLoader.removeEventListener(IOErrorEvent.IO_ERROR, sIoErrorHandler);				
				//_urlLoader = null;
				
				ContinueInit();
			}			
		}
					
		public static function ContinueInit(): void 
		{
			CreateAtlases();
			var l: uint = _downloadQueue.length;			
			_soNeedsFlush = true;
			World.gWorld.dispatchEventWith("downloadProgress", false, { loaded: l, total: l } );
			World.gWorld.dispatchEventWith("downloadDone", false, _downloadNumber);
			
			//World.gWorld.Init();
		}
		
		public static function MaybeFlushSo():void 
		{
			if (_soNeedsFlush)
			{
				_sharedObject.flush();
			}
		}
		
		public static function CreateAtlases():void 
		{
			for (var i: int = 0; i < _modelNames.length; i++)
			{
				_monsterAtlases[_modelNames[i]] = new Vector.<Vector.<Object>>;
				for (var k: int = 0; k < _rareModelNames.length; k++)
				{
					_monsterAtlases[_modelNames[i]][k] = new Vector.<Object>;
					for (var j: int = 0; j < MonsterSprite.NUMBER_OF_STATES; j++)
					{
						var str: String = "MONSTER_" + _modelNames[i].toUpperCase() +"_"+ _rareModelNames[k].toUpperCase()+"_"+_monsterStateNames[j].toUpperCase();
						var xml: XML = XML(new Assets[str + "_XML"]);
						_monsterAtlases[_modelNames[i]][k].push({ str: str, xml: xml });
					}
				}
			}			
		}
		
		//public static function InitCursor():void 
		//{
			//var cursorBitmaps:Vector.<BitmapData> = new Vector.<BitmapData>();
			//cursorBitmaps.push((new Cursor() as Bitmap).bitmapData);
//
			//var mouseCursorData:MouseCursorData = new MouseCursorData();
			//mouseCursorData.data        = cursorBitmaps;
			//mouseCursorData.frameRate   = 30;
			//mouseCursorData.hotSpot     = new Point(0, 0);
			//var name: String = "my_cursor";
			//Mouse.registerCursor(name, mouseCursorData);
			//Mouse.cursor = name;			
		//}
		
		public static function GetBG(n: int = 0): Texture 
		{
			return GetTexture("BG_LOCATION_" + String(n));
		}
		
		public static function GetFG(n: int = 0): Texture
		{
			return GetTexture("FG_LOCATION_" + String(n));
		}
				
		public static function GetMid(n: int = 0): Texture
		{
			return GetTexture("MID_LOCATION_0");
		}
		
		public static function GetArmorTexture(armorType: int) : Texture
		{
			switch(armorType)
			{
				case MonsterData.ARMOR_DARK:
					return interfaceAtlas.getTexture("dark_armor");
					break;
				case MonsterData.ARMOR_FORTIFIED:
					return interfaceAtlas.getTexture("fortified_armor");
					break;
				case MonsterData.ARMOR_HEAVY:
					return interfaceAtlas.getTexture("heavy_armor");
					break;
				case MonsterData.ARMOR_LIGHT:
					return interfaceAtlas.getTexture("light_armor");
					break;
				case MonsterData.ARMOR_MAGIC:
					return interfaceAtlas.getTexture("magic_armor");
					break;
				case MonsterData.ARMOR_NO_ARMOR:
					return interfaceAtlas.getTexture("no_armor");
					break;
				default:
					return interfaceAtlas.getTexture("no_armor");
					break;
			}
		}
		
		public static function GetMonsterAtlas(modelName: String, rarity: int, monsterState: int): TextureAtlas
		{
			var obj: Object = null;
			if (_monsterAtlases[modelName] != undefined)
				obj = _monsterAtlases[modelName][rarity][monsterState];
			if (obj != null)
			{
				var texture: Texture = GetTexture(obj.str);
				var atlas: TextureAtlas = new TextureAtlas(texture, obj.xml);
				return atlas;
			}
			return null;
		}		
		
		//public static function GetMonsterAtlas(modelName: String, rarity: int, monsterState: int): TextureAtlas
		//{
			//if (_monsterAtlases[modelName][rarity][monsterState] !== undefined)
				//return _monsterAtlases[modelName][rarity][monsterState];
			//else if (_monsterAtlases[modelName][0][monsterState] !== undefined)
				//return _monsterAtlases[modelName][0][monsterState];
			//else
				//return _monsterAtlases[modelName][rarity][0];
		//}
		
		public static function GetEmbeddedAtfTexture(textureName: String): Texture 
		{		
			if (_permanentTextures[textureName] == undefined)
			{
				if (Assets[textureName] == undefined)
					return undefined;
				else
					_permanentTextures[textureName] = Texture.fromEmbeddedAsset(Assets[textureName]);
			}
			return _permanentTextures[textureName];				
		}
		
		public static function GetPermanentTexture(textureName: String): Texture 
		{
			if (_permanentTextures[textureName] == undefined)
			{
				if (Assets[textureName] == undefined)
					return undefined;
				else
					_permanentTextures[textureName] = Texture.fromBitmapData(Bitmap(new Assets[textureName]()).bitmapData,false);
			}
			return _permanentTextures[textureName];			
		}
		
		public static function GetTexture(textureName: String, dontPushToLru: Boolean = false): Texture
		{
			if (dontPushToLru)
			{
				if (_textures[textureName] == undefined)
				{
					if (Assets[textureName] == undefined)
						return undefined;
					else
						_textures[textureName] = Texture.fromBitmap(new Assets[textureName](),false);
				}
				return _textures[textureName];
			}
			else
			{
				return LoadIntoMemory(textureName);
			}
		}		
		
		//public static function GetTexture(textureName: String): Texture
		//{
			//if (_textures[textureName] == undefined)
			//{
				//if (Assets[textureName] == undefined)
					//return undefined;
				//_textures[textureName] = Texture.fromBitmap(new Assets[textureName]());
			//}
			//return _textures[textureName];
		//}
		
		public static function GetBitmapData(name: String): BitmapData
		{
			if (_bitmapDataDictionary[name] == undefined)
			{
				if (Assets[name] == undefined)
					return undefined;
				_bitmapDataDictionary[name] = Bitmap(new Assets[name]()).bitmapData;
			}
			return _bitmapDataDictionary[name];			
		}

		
	}

}