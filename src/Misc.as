package  
{
	/**
	 * ...
	 * @author Aikei
	 */
	import flash.display.BitmapData;
	import flash.display3D.Context3D;
	import flash.utils.ByteArray;
	import logic.ClientLogic;
	import Math;
	import starling.core.RenderSupport;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.textures.Texture;
	import flash.geom.Rectangle;
	import starling.utils.Color;
	import system.Base64Decoder;
	import system.MurmurHash2;
	
	public class Misc 
	{
		public static var BIG_INT_HERO_ATTACK_COOLDOWN: BigInt = BigInt.shStr2BigInt(ClientLogic.HEROES_ATTACK_COOLDOWN.toString());
		
		//public static const NUMBER_OF_LOCATIONS: int = 1;
		public static const DAMAGE_TIMES_PER_SECOND: int = 1;
		public static const PI: Number = 3.1415926535;
		public static const BIG_INT_ZERO: BigInt = BigInt.shStr2BigInt("0");
		public static const BIG_INT_ONE: BigInt = BigInt.shStr2BigInt("1");
		public static const BIG_INT_TWO: BigInt = BigInt.shStr2BigInt("2");
		public static const BIG_INT_ONE_HUNDRED: BigInt = BigInt.shStr2BigInt("100");
		public static const BIG_INT_DPS_DIVIDER: BigInt = BigInt.shStr2BigInt(DAMAGE_TIMES_PER_SECOND.toString());

		public static const DAY_IN_SECONDS: Number = 86400;
		public static const HOUR_IN_SECONDS: Number = 3600;
		
		public static const DICE_WIDTH: Number = 10000;
		
		public static const SCREEN_WIDTH: Number = 900;
		public static const SCREEN_HEIGHT: Number = 690;
		
		public static var base64decoder: Base64Decoder = new Base64Decoder;
		public static var timeCompensation: Number = 0;
		
		static public function Init(): void
		{
			
		}
		
		static public function ParseTime(timeInSeconds: Number): Object 
		{
			var obj: Object = new Object;
			obj.days = Math.floor(timeInSeconds / 86400);
			var remainder: uint = timeInSeconds % 86400;
			obj.hours = Math.floor(remainder / 3600);
			remainder = remainder % 3600;
			obj.minutes = Math.floor(remainder / 60);
			obj.seconds = remainder % 60;
			return obj;
		}
		
		static public function ThrowDice(): Number 
		{
			return Random(0, DICE_WIDTH);
		}
		
		static public function GetServerTime(): Number 
		{
			return GetTimeSecs() + timeCompensation;
		}
		
		static public function Random(min:Number, max:Number): Number
		{
			return Math.floor(Math.random()*(max-min))+min;			
		}
		
		static public function RandomNoFloor(min:Number, max:Number): Number
		{
			return Math.random()*(max-min)+min;			
		}		
		
		static public function GetTimeSecs(): Number 
		{
			return new Date().getTime()/1000.0;
		}
		
		static public function GetTimeMSecs(): Number 
		{
			return new Date().getTime();
		}
		
		static public function IsCharCodePrintable(charCode: int): Boolean
		{
			if (charCode < 32 || charCode > 126) 
				return false;
			return true;
		}
		
		static public function CheckIfStringAllCaps(s: String): Boolean
		{
			for (var i: int = 0; i < s.length; i++)
			{
				if (!CheckIfCharCodeIsCapital(s.charCodeAt(i)))
					return false;
			}
			return true;
		}
		
		static public function CheckIfCharCodeIsCapital(charCode: Number): Boolean 
		{
			if ((charCode >= 65 && charCode <= 95) || (charCode >= 48 && charCode <= 57))
				return true;
			return false;
		}
		
		static public function CheckIfCharCodeIsDigit(charCode: Number): Boolean 
		{
			if ((charCode >= 48 && charCode <= 57))
				return true;
			return false;
		}
		
		static public function CheckIfStringIsAllNumbers(s: String): Boolean
		{
			if (s.charAt(0) === "0" && s.charAt(1) === "x")
				return true;
			for (var i: int = 0; i < s.length; i++)
			{
				if (i === 10)
					return false;
				if (!CheckIfCharCodeIsDigit(s.charCodeAt(i)))
					return false;
			}
			return true;
		}			
		
		static public function CheckIfStringAllLowerCase(s: String): Boolean
		{
			for (var i: int = 0; i < s.length; i++)
			{
				if (!CheckIfCharCodeIsLowerCase(s.charCodeAt(i)))
					return false;
			}
			return true;
		}		
		
		static public function CheckIfCharCodeIsLowerCase(charCode: Number): Boolean 
		{
			if ((charCode >= 97 && charCode <= 122) || (charCode >= 48 && charCode <= 57) || (charCode >= 91 && charCode <= 95))
				return true;
			return false;
		}
		
		static public function TextureToBitmapData(texture: Texture): BitmapData 
		{
			var image: Image = new Image(texture);
			return DisplayObjectToBitmapData(image);
		}		
		
		static public function DisplayObjectToBitmapData(sprite: DisplayObject): BitmapData 
		{
			if (sprite == null) return null;
			var resultRect:Rectangle = new Rectangle();
			sprite.getBounds(sprite, resultRect);
			var context:Context3D = Starling.context;
			var support:RenderSupport = new RenderSupport();
			support.clear();
			support.setProjectionMatrix(0, 0, Starling.current.stage.stageWidth, Starling.current.stage.stageHeight);
			//support.setOrthographicProjection(0, 0, Starling.current.stage.stageWidth, Starling.current.stage.stageHeight);
			if (sprite.root)
				support.transformMatrix(sprite.root);
			support.translateMatrix( -resultRect.x, -resultRect.y);
			var result:BitmapData = new BitmapData(resultRect.width, resultRect.height, true, 0x00000000);
			support.pushMatrix();
			support.transformMatrix(sprite);
			sprite.render(support, 1.0);
			support.popMatrix();
			support.finishQuadBatch();
			context.drawToBitmapData(result);
			return result;
		}
		
		static public function GetColorFrom8DigitHexColor(hexColor: String): uint
		{
			if (hexColor.length !== 10)
				throw "Wrong nubmer of digits!";
			var alpha: Number = Number("0x" + hexColor.substr(hexColor.length - 2));
			var color: uint = Number(hexColor.substr(0, hexColor.length - 2));
			return Color.argb(alpha, Color.getRed(color), Color.getGreen(color), Color.getBlue(color));
		}
		
		static public function RoundPosition(obj: Object):void 
		{
			obj.x = Math.round(obj.x);
			obj.y = Math.round(obj.y);
		}
		
		static public function RoundToDecimalPlaces(n: Number,places: Number): Number 
		{
			return (n * (places * 10)) / (places * 10);
		}
		
		static public function CenterObject(obj: Object, centerx: Number, centery: Number):void 
		{
			obj.x = centerx - obj.width / 2;
			obj.y = centery - obj.height / 2;
			RoundPosition(obj);
		}
		
		static public function UnpackDeflated(deflatedInBase64: String): Object 
		{						
			base64decoder.decode(deflatedInBase64);
			var bytes: ByteArray = base64decoder.toByteArray();
			bytes.uncompress();
			var obj: Object = JSON.parse(bytes.toString());
			return obj;
		}
		
		static public function GetCheckHash(str: String): String 
		{
			var a: Number = 3;
			if (str.length < 4)
				a = 0;
			var b: Number = 7;
			if (str.length < 8)
				b = 2;
			var hash: String = MurmurHash2.Hash(str, str.charCodeAt(a) + str.charCodeAt(b));
			return hash;
		}
		
		//static public function GetCheckHash(str: String): String 
		//{
			//var charCodes: Vector.<Number> = new Vector.<Number>;
			//if (str.length > 1)
				//charCodes.push(str.charCodeAt(str.length - 2));
			//for (var i: int = 0; i*2 < str.length; i++)
			//{
				//charCodes.push(str.charCodeAt(i*2));
			//}
			//var n: Number = 0;
			//for (i = 0; i < charCodes.length; i++)
			//{
				//n += charCodes[i];
			//}
			//n *= charCodes[0];
			//trace("hash:", n);
			//return n;
		//}
	}

}