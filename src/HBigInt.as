package  
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class HBigInt 
	{
		private var _array: Vector.<int>;
		static public var _maxOrderValue: int = 999999999;
		static public var _minOrderValue: int = -999999999;
		
		public function HBigInt(array: Vector.<int> = null) 
		{
			if (array === null)
			{
				_array = new Vector.<int>;
				_array.push(0);
			}
			else
			{
				_array = array;
			}
		}
		
		public function DeepCopy(): HBigInt
		{
			var nArray = new Vector.<int>;
			for (var i: int = 0; i < _array.length; i++)
			{
				var a: int = _array[i];
				nArray.push(a);
			}
			return new HBigInt(nArray);			
		}
		
		private function _addOrder(order: int, valueToAdd: int)
		{
			var maxLeft: int = _maxOrderValue - _array[order];
			if (valueToAdd <= maxLeft)
			{
				_array[order] += valueToAdd;
			}
			else
			{
				var remainder: int = (maxLeft + 1) - valueToAdd;
				_array[order] = remainder;
				order++;
				if (_array.length <= order)
				{
					_array.push(0);
				}
				_addOrder(order, 1);
			}
		}
		
		public function AddBigInt(other: HBigInt): HBigInt
		{
			var result: HBigInt = DeepCopy();
			for (var i: int = 0; i < result._array.length; i++)
			{
				result._addOrder(i, other._array[i]);
			}
			return result;
		}
		
		private function _subtractOrder(order: int, valueToSubtract: int): void
		{
			if (valueToSubtract <= _array[order])
			{
				_array[order] -= valueToSubtract;
			}
			else
			{
				var remainder: int = valueToSubtract - _array[order];
				_array[order] = (_maxOrderValue+1) - remainder;
				var newOrder: int = order+1;
				if (_array.length <= newOrder)
				{
					_array = new Vector.<int>;
					_array.push(0);
					return;
				}
				_subtractOrder(newOrder, 1);
			}
		}		
		
		private function _popEmptyOrders(): void 
		{
			var isZero: Boolean = true;
			for (var i: int = _array.length - 1; isZero === true; i--)
			{
				if (_array[i] === 0)
				{
					isZero = true;
					_array.pop();
				}
				else
				{
					isZero = false;
				}
			}
		}
		
		public function SubtractBigInt(other: HBigInt): HBigInt
		{
			var result: HBigInt = this.DeepCopy();
			for (var i: int = 0; i < result._array.length; i++)
			{
				result._subtractOrder(i, other._array[i]);
			}
			result._popEmptyOrders();
			return result;			
		}
		
	}

}