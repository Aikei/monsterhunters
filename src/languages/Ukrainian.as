package languages 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import logic.HeroData;
	import logic.MonsterData;
	import flash.utils.Dictionary;
	import logic.Weapon;
	import logic.Damage;
	import logic.Trait;
	import logic.Skill;
	import logic.DungeonEffect;
	import logic.Affliction;
	
	public class Ukrainian extends LanguageData
	{
				
		public function Ukrainian(): void 
		{
			strings["Hero" + String(HeroData.HERO_BARBARIAN)] = "Варвар";
			strings["Hero" + String(HeroData.HERO_RANGER)] = "Рейнджер";
			strings["Hero" + String(HeroData.HERO_MAGE)] = "Чародій";
			strings["Hero" + String(HeroData.HERO_ROGUE)] = "Розбійник";
			strings["Hero" + String(HeroData.HERO_ENGINEER)] = "Інженер";
			strings["Hero" + String(HeroData.HERO_PALADIN)] = "Паладин";
			
			strings["Weapon" + String(Weapon.WEAPON_WOODEN_SWORD)] = "Дерев'яний меч";
			strings["Weapon" + String(Weapon.WEAPON_APPRENTICE_BLADE)] = "Залізний меч";
			strings["Weapon" + String(Weapon.WEAPON_KNIGHTS_SWORD)] = "Стальний меч";			
			
			strings["DamageType" + Damage.DAMAGE_TYPE_RANGED] = "Дальня";
			strings["DamageType" + Damage.DAMAGE_TYPE_HOLY] = "Свята";
			strings["DamageType" + Damage.DAMAGE_TYPE_PIERCE] = "Колюча";
			strings["DamageType" + Damage.DAMAGE_TYPE_SIEGE] = "Облогова";
			strings["DamageType" + Damage.DAMAGE_TYPE_SLASH] = "Рубаюча";
			strings["DamageType" + Damage.DAMAGE_TYPE_SPELL] = "Магічна";
			
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_RANGED] = "Дальня атака завдає збільшену шкоду монстрам Без захисту";
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_HOLY] = "Свята атака завдає збільшену шкоду монстрам з Темним захистом";
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_PIERCE] = "Колюча атака завдає збільшену шкоду монстрам з Важким захистом";
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_SIEGE] = "Облогова атака завдає збільшену шкоду монстрам з Укріпленим захистом";
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_SLASH] = "Рубаюча атака завдає збільшену шкоду монстрам з Легким захистом";
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_SPELL] = "Магічна атака завдає збільшену шкоду монстрам з Магічним захистом";
			
			strings["ArmorType" + MonsterData.ARMOR_DARK] = "Темний";
			strings["ArmorType" + MonsterData.ARMOR_FORTIFIED] = "Укріплений";
			strings["ArmorType" + MonsterData.ARMOR_HEAVY] = "Важкий";
			strings["ArmorType" + MonsterData.ARMOR_LIGHT] = "Легкий";
			strings["ArmorType" + MonsterData.ARMOR_MAGIC] = "Магічний";
			strings["ArmorType" + MonsterData.ARMOR_NO_ARMOR] = "Без захисту";
			
			strings["ExplainArmorType" + MonsterData.ARMOR_DARK] = "Проти Темного захисту ефективні Святі атаки - вони завдають цьому монстру збільшену шкоду";
			strings["ExplainArmorType" + MonsterData.ARMOR_FORTIFIED] = "Проти Укріпленого захисту ефективні Облогові атаки - вони завдають цьому монстру збільшену шкоду";
			strings["ExplainArmorType" + MonsterData.ARMOR_HEAVY] = "Проти Важкого захисту ефективні Колючі атаки - вони завдають цьому монстру збільшену шкоду";
			strings["ExplainArmorType" + MonsterData.ARMOR_LIGHT] = "Проти Легкого захисту ефективні Рубаючі атаки - вони завдають цьому монстру збільшену шкоду";
			strings["ExplainArmorType" + MonsterData.ARMOR_MAGIC] = "Проти Магічного захисту ефективні Магічні атаки - вони завдають цьому монстру збільшену шкоду";
			strings["ExplainArmorType" + MonsterData.ARMOR_NO_ARMOR] = "Проти монстра Без захисту ефективні Дальні атаки - вони завдають йому збільшену шкоду";		
						
			strings["SkillDescription" + Skill.SKILL_INCREASE_GLOBAL_DAMAGE] = "$n до шкоди всіх героїв";
			strings["SkillDescription" + Skill.SKILL_DECREASE_MONSTER_ARMOR] = "$n від броні всіх монстрів";
			strings["SkillDescription" + Skill.SKILL_INCREASE_MONSTER_COINS] = "Монстри дають $n золота";
			strings["SkillDescription" + Skill.SKILL_INCREASE_STUN_CHANCE] = "$n сек. до тривалості оглушення";
			strings["SkillDescription" + Skill.SKILL_EMPOWER_HERO] = "Атаки $hero наносять на $n більше шкоди";
			strings["SkillDescription" + Skill.SKILL_HEAL_CHANCE] = "$n ймовірність вилікувати негативний ефект героя в секунду";
			
			strings["LostConnection"] = "Загублено з'єднання з сервером. Але не хвилюйтеся, весь ваш прогрес збережений! Спробуйте перезапустити гру.";
			strings["ServerUnavailable"] = "Не вдається встановити з'єднання з сервером. Спробуйте зайти пізніше.";
			strings["CantLoadResources"] = "Неможливо завантажити ресурси. Можливо, сервер недоступний або у вас проблеми з підключенням.";
			
			strings["Buy"] = "Купити";
			strings["Upgrade"] = "Поліпшити";
			strings["Lv"] = "рів.";
			strings["Level"] = "Рівень";
			strings["Dps"] = "шкода/сек.";			
			strings["Damage"] = "шкода";
			
			//strings["NewSquadName"] = "New squad name";
			
			strings["sec"] = "сек";
			strings["SecondsAbbreviation"] = "с";
			
			strings["Group"] = "Загін";
			strings["Leader"] = "Лідер";
			strings["Glory"] = "Слава";
			
			strings["Heroes"] = "ГероЇ";
			strings["Skills"] = "Навики";			
			strings["Weapons"] = "Зброя";
			
			strings["Dungeon"] = "Підземелля";
			strings["YouAreHere"] = "Ваш загін тут";
			
			//strings["NameTaken"] = "Упс! Похоже такое название уже занято";
			
			strings["Dungeon1"] = "Зачарована оранжерея";
			strings["Dungeon2"] = "Двір Похмурого склепу";
			strings["Dungeon3"] = "Зали Похмурого склепу";
			strings["Dungeon4"] = "Цитадель гоблінів";
			strings["Dungeon5"] = "Підземелля 5";
			
			//strings["DecodingSounds"] = "Декодировка звуков";
			
			strings["ExplainEffectRarity" + 2000] = "Іноді";
			strings["ExplainEffectRarity" + 4000] = "Часто";
			strings["ExplainDungeonEffect"+DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS] = "У монстрів цього підземелля $r зустрічається риса $t";		
			
			strings["MonstersPossibleInDungeon"] = "В цьому підземеллі зустрічаються наступні монстри: ";
			strings["PlayersInThisDungeon"] = "Зараз гравців у цьому підземеллі: ";
			
			strings["Conan"] = "Конан";
			strings["Leoric"] = "Леорік";
			
			strings["ExplainArmor"] = "1 одиниця броні знижує втрати від кожної атаки на 1 одиницю, але не нижче, ніж до 30%";
			strings["ExplainTimer"] = "Коли цей таймер дійде до нуля, монстр втече";
			
			strings["EffectOfTheDay"] = "Ефект дня";
			
			strings["Rarity" + MonsterData.RARITY_COMMON] = "Звичайний";
			strings["Rarity" + MonsterData.RARITY_RARE] = "Рідкісний";
			strings["Rarity" + MonsterData.RARITY_ELITE] = "Елітний";
			
			strings["ExplainRareMonster"] = "Це $rarity монстр. За його вбивство ви отримаєте славу.";
			strings["ExplainCommonMonster"] = "Це звичайний монстр. За його вбивство ви не отримаєте слави.";
			
			strings["Trait" + Trait.TRAIT_FAT] = "Жирний";
			strings["Trait" + Trait.TRAIT_FAST] = "Швидкий";
			strings["Trait" + Trait.TRAIT_TOUGH] = "Міцний";
			strings["Trait" + Trait.TRAIT_SHADOW] = "Тінь";
			strings["Trait" + Trait.TRAIT_POISONOUS] = "Отруйний";
			
			strings["ExplainTrait" + Trait.TRAIT_FAT] = "У жирних монстрів більше здоров'я";
			strings["ExplainTrait" + Trait.TRAIT_FAST] = "Швидкі монстри тікають швидше";
			strings["ExplainTrait" + Trait.TRAIT_TOUGH] = "У міцних монстрів більше броні";
			strings["ExplainTrait" + Trait.TRAIT_SHADOW] = "Атаки неефективних типів завдають значно менше шкоди монстрам з рисою тінь";
			strings["ExplainTrait" + Trait.TRAIT_POISONOUS] = "Отруйні монстри мають шанс отруїти одного з героїв, знижуючи шкоду от його атаки";				
			
			strings["Affliction" + Affliction.AFFLICTION_POISON] = "Отруєння";
			strings["ExplainAffliction" + Affliction.AFFLICTION_POISON] = "Цього героя отруїли, його атака ослаблена";
			
			strings["TOP"] = "ТОП";
			
			strings["None"] = "Немає";
			
			strings["DamageAtLevel"] = "Шкода на наступному рівні: ";
			
			strings["DefaultGroupName"] = "Мій загін";
			
			strings["Exhausted"] = "Виснажене";
			strings["ExplainExhaustion"] = "Коли цей лічильник дійде до кінця, підземелля виснажиться. Монстри виснаженого підземелля приносять менше золота. Чим більше гравців у підземеллі, тим швидше воно виснажується.";
			strings["ExplainExhausted"] = "Підземелля виснажене. Монстри приносять лише 20% золота.";
			
			strings["Loading"] = "Завантажування";
			
			//tips
			
			strings["TipHeroes"] = "Наймаючи героїв ви позбавляєте себе від необхідності багато клікати, вони завдають монстрам шкоди без вашої участі";
			strings["TipWeapons"] = "Зброя визначає силу вашого удару";
						
			monsterNames["Monster" + MonsterData.MONSTER_DUMMY + "Rarity" + MonsterData.RARITY_COMMON] = "Городне опудало";
			monsterNames["Monster" + MonsterData.MONSTER_DUMMY + "Rarity" + MonsterData.RARITY_RARE] = "Садовий різник";
			monsterNames["Monster" + MonsterData.MONSTER_DUMMY + "Rarity" + MonsterData.RARITY_ELITE] = "Повелитель гарбузів";
			
			monsterNames["Monster" + MonsterData.MONSTER_SLIME + "Rarity" + MonsterData.RARITY_COMMON] = "Садова мутація";
			monsterNames["Monster" + MonsterData.MONSTER_SLIME + "Rarity" + MonsterData.RARITY_RARE] = "Жуткий кисель";
			monsterNames["Monster" + MonsterData.MONSTER_SLIME + "Rarity" + MonsterData.RARITY_ELITE] = "Величезний холодець";
			
			monsterNames["Monster" + MonsterData.MONSTER_SAPLING + "Rarity" + MonsterData.RARITY_COMMON] = "Агресивний коренеплід";
			monsterNames["Monster" + MonsterData.MONSTER_SAPLING + "Rarity" + MonsterData.RARITY_RARE] = "Доглядач грядок";
			monsterNames["Monster" + MonsterData.MONSTER_SAPLING + "Rarity" + MonsterData.RARITY_COMMON] = "Страж рослин";
			
			monsterNames["Monster" + MonsterData.MONSTER_PLANT + "Rarity" + MonsterData.RARITY_COMMON] = "М'ясоїдна поросль";
			monsterNames["Monster" + MonsterData.MONSTER_PLANT + "Rarity" + MonsterData.RARITY_RARE] = "Хижий бур'ян";
			monsterNames["Monster" + MonsterData.MONSTER_PLANT+"Rarity" + MonsterData.RARITY_ELITE] = "Смертоквіт";			

			strings["DecimalDivider"] = ",";	
		}
		
	}

}