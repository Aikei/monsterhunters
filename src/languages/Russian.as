package languages 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import logic.Dungeon;
	import logic.HeroData;
	import logic.MonsterData;
	import flash.utils.Dictionary;
	import logic.Quest;
	import logic.Weapon;
	import logic.Damage;
	import logic.Trait;
	import logic.Skill;
	import logic.DungeonEffect;
	import logic.Affliction;
	
	public class Russian extends LanguageData
	{
				
		public function Russian(): void 
		{
			language = "ru";
			
			strings["Hero" + String(HeroData.HERO_BARBARIAN)] = "Олаф Суровый";
			strings["Hero" + String(HeroData.HERO_BARBARIAN)+Language.CASE_GENETIVE] = "Воина";
			strings["Hero" + String(HeroData.HERO_RANGER)] = "Крадущаяся Тень";
			strings["Hero" + String(HeroData.HERO_RANGER)+Language.CASE_GENETIVE] = "Рейнджера";
			strings["Hero" + String(HeroData.HERO_MAGE)] = "Август Меригольд";
			strings["Hero" + String(HeroData.HERO_MAGE)+Language.CASE_GENETIVE] = "Чародея";
			strings["Hero" + String(HeroData.HERO_ROGUE)] = "Лоренцо Чероне";
			strings["Hero" + String(HeroData.HERO_ROGUE)+Language.CASE_GENETIVE] = "Разбойника";
			strings["Hero" + String(HeroData.HERO_ENGINEER)] = "Рори Огнебород";
			strings["Hero" + String(HeroData.HERO_ENGINEER)+Language.CASE_GENETIVE] = "Инженера";
			strings["Hero" + String(HeroData.HERO_PALADIN)] = "Сэр Ричард";
			strings["Hero" + String(HeroData.HERO_PALADIN)+Language.CASE_GENETIVE] = "Паладина";
			
			strings["Weapon" + String(Weapon.WEAPON_WOODEN_SWORD)] = "Деревянный меч";
			strings["TitleExplainWeapon" + String(Weapon.WEAPON_WOODEN_SWORD)] = "Деревянный меч";
			strings["ExplainWeapon" + String(Weapon.WEAPON_WOODEN_SWORD)] = "Каждый великий герой начинал свой путь с чего-то подобного. Повезло еще, что это меч, а не ножка от табуретки.";
			
			strings["Weapon" + String(Weapon.WEAPON_APPRENTICE_BLADE)] = "Клинок новичка";
			strings["TitleExplainWeapon" + String(Weapon.WEAPON_APPRENTICE_BLADE)] = "Клинок новичка";
			strings["ExplainWeapon" + String(Weapon.WEAPON_APPRENTICE_BLADE)] = "Что может быть страшнее, чем тупой клинок в руках начинающего? Переломы и синяки обеспечены всем вашим врагам.";
			
			strings["Weapon" + String(Weapon.WEAPON_KNIGHTS_SWORD)] = "Рыцарский клинок";
			strings["TitleExplainWeapon" + String(Weapon.WEAPON_KNIGHTS_SWORD)] = "Рыцарский клинок";
			strings["ExplainWeapon" + String(Weapon.WEAPON_KNIGHTS_SWORD)] = "Красивое турнирное оружие. Осталось только докупить лошадь.";	
			
			strings["Weapon" + String(Weapon.AMETHYST_BLADE)] = "Аметистовый меч";
			strings["TitleExplainWeapon" + String(Weapon.AMETHYST_BLADE)] = "Аметистовый меч";
			strings["ExplainWeapon" + String(Weapon.AMETHYST_BLADE)] = "Стальные мечи нередко ломаются о крепкие шкуры и панцири. Клинки c аметистовым напылением лишены подобного недостатка.";
			
			strings["Weapon" + String(Weapon.ASSAULT_SWORD)] = "Штурмовой меч";
			strings["TitleExplainWeapon" + String(Weapon.ASSAULT_SWORD)] = "Штурмовой меч";
			strings["ExplainWeapon" + String(Weapon.ASSAULT_SWORD)] = "Механизированный клинок с серебряным лезвием. Такое оружие используют элитные отряды королевской армии.";	
			
			strings["Weapon" + String(Weapon.BONEMOURN)] = "Костяная скорбь";
			strings["TitleExplainWeapon" + String(Weapon.BONEMOURN)] = "Костяная скорбь";
			strings["ExplainWeapon" + String(Weapon.BONEMOURN)] = "От клинка исходит зеленоватое свечение. Говорят, разрушительная сила этого оружия лишает его обладателей рассудка, они начают слышать голоса, зовующие их в какую-то заснеженную цитадель.";
			
			strings["Weapon" + String(Weapon.INCINERATOR)] = "Испепелитель";
			strings["TitleExplainWeapon" + String(Weapon.INCINERATOR)] = "Испепелитель";
			strings["ExplainWeapon" + String(Weapon.INCINERATOR)] = "Выкованный из драконита меч переполнен мощью огненной стихии. Лезвие, раскаленное до 9000 градусов, одним касанием разрезает сталь. Опустив его в воду, можно вскипятить небольшое озеро.";
			
			strings["DamageType" + Damage.DAMAGE_TYPE_RANGED] = "Дальняя";
			strings["DamageType" + Damage.DAMAGE_TYPE_HOLY] = "Святая";
			strings["DamageType" + Damage.DAMAGE_TYPE_PIERCE] = "Колющая";
			strings["DamageType" + Damage.DAMAGE_TYPE_SIEGE] = "Осадная";
			strings["DamageType" + Damage.DAMAGE_TYPE_SLASH] = "Рубящая";
			strings["DamageType" + Damage.DAMAGE_TYPE_SPELL] = "Магическая";
			
			
			strings["ArmorType" + MonsterData.ARMOR_DARK] = "Темная";
			strings["ArmorType" + MonsterData.ARMOR_FORTIFIED] = "Укрепленная";
			strings["ArmorType" + MonsterData.ARMOR_HEAVY] = "Тяжелая";
			strings["ArmorType" + MonsterData.ARMOR_LIGHT] = "Лёгкая";
			strings["ArmorType" + MonsterData.ARMOR_MAGIC] = "Магическая";
			strings["ArmorType" + MonsterData.ARMOR_NO_ARMOR] = "Без защиты";			
			
			strings["LostConnection"] = "Потеряно соединение с сервером. Но не волнуйтесь, весь ваш прогресс сохранен! Попробуйте перезапустить игру.";
			strings["ServerUnavailable"] = "Не удается установить соединение с сервером. Попробуйте зайти попозже.";
			strings["CantLoadResources"] = "Не удается загрузить ресурсы. Возможно, сервер недоступен или у вас проблемы с подключением.";
			
			//time parse
			strings["HoursAbbreviarion"] = "ч";
			strings["MinutesAbbreviarion"] = "м";
			strings["SecondsAbbreviarion"] = "с";
			
			//wall post
			strings["ReportShareMessage"] = "Мой отряд «$sq» разнес целую армию монстров! Теперь карман греет $gold золота. Присоединяйтесь! #охотникинамонстров";
			strings["ErrorOccured"] = "Произошла ошибка";
			
			//bg report texts
			strings["Share"] = "Рассказать";
			strings["Sir"] = "Сэр";
			strings["Dame"] = "Миледи";
			strings["BgReportComment"] = "Пока вас не было отряд $sq охотился на монстров и заработал золото";
			//strings["BgReportComment"] = "Пока вас не было отряд $sq победил $num монстров и заработал золото";
			
			//strings["BgReportComment"] = "Пока вас не было отряд «$sq» победил $num монстров, заработал золото и славу";
			
			//misc
			strings["sec"] = "сек";
			strings["SecondsAbbreviation"] = "с";
			strings["NotAvailableInBeta"] = "Недоступно в бета-версии";
			strings["MonsterRanAway"] = "Монстр сбежал!";
			strings["LevelComplete"] = "Уровень $n пройден!";
			strings["Oops"] = "Упс!";
			strings["ClickingTooMuch"] = "Вы кликаете слишком часто. Отдохните немного.";		
			strings["DungeonsNotLoaded"] = "Подождите, ресурсы других подземелий еще загружаются...";
			
			//quest
			strings["Quest"] = "Задание";
			//strings["QuestKillAny"] = "Убейте $n монстров, находясь в игре";
			//strings["ShortQuestKillAny"] = "Победите $n монстров"
			//strings["QuestKillOfType"] = "Убейте $n монстров типа $t в подземелье $d, находясь в игре";
			//strings["ShortQuestKillOfType"] = "Победите $n $m"
			strings["QuestProgress"] = "Прогресс";
			strings["QuestComplete"] = "Задание выполнено";
			strings["AcceptQuest"] = "Выполнить";
			strings["RefuseQuest"] = "Отказаться";
			strings["CancelQuest"] = "Отказаться от задания";
			strings["Reward"] = "Награда";

			strings["QuestName" + Quest.QUEST_TYPE_KILL + Quest.MONSTER_TYPE_SPECIFIC] = "Скрытая угроза";
			strings["QuestDescription" + Quest.QUEST_TYPE_KILL + Quest.MONSTER_TYPE_SPECIFIC] = "В $dungeon копит силы зло. Нанесите контрудар, сразив не менее $number $monsters, и получите награду от Королевской армии.";
			strings["ShortQuestDescription" + Quest.QUEST_TYPE_KILL + Quest.MONSTER_TYPE_SPECIFIC] = "Победите $m"
			
			strings["QuestName" + Quest.QUEST_TYPE_KILL + Quest.MONSTER_TYPE_ANY] = "На службе короны";
			strings["QuestDescription" + Quest.QUEST_TYPE_KILL + Quest.MONSTER_TYPE_ANY] = "Монстров в этих землях все больше. Король объявляет награду для героев, что готовы бороться с чудовищами. Победите $number любых монстров, и награда ваша.";
			strings["ShortQuestDescription" + Quest.QUEST_TYPE_KILL + Quest.MONSTER_TYPE_ANY] = "Победите монстров";
			
			strings["QuestName" + Quest.QUEST_TYPE_REACH_LEVEL] = "Испытание героя";
			strings["QuestDescription" + Quest.QUEST_TYPE_REACH_LEVEL] = "Лига героев предлагает всем желающим испытать стойкость своего духа. Доберитесь до $level уровня $dungeon, и вас ждет награда чемпиона.";
			strings["ShortQuestDescription" + Quest.QUEST_TYPE_REACH_LEVEL ] = "Доберитесь до уровня $n";
			
			strings["QuestName" + Quest.QUEST_TYPE_KILL_BOSS] = "Вооружён и опасен";
			strings["QuestDescription" + Quest.QUEST_TYPE_KILL_BOSS] = "В $dungeon скрывается чудовище по прозвищу $name. Избавьте наш мир от него, и вас ждут золото и слава.";
			strings["ShortQuestDescription" + Quest.QUEST_TYPE_KILL_BOSS] = "Победите $name";
			
			//squad window
			strings["Buy"] = "Купить";
			strings["Hire"] = "Нанять";
			strings["Upgrade"] = "Улучшить";
			strings["Lv"] = "ур.";
			strings["Level"] = "Уровень";
			strings["Dps"] = "урон в сек.";			
			strings["Damage"] = "урон";
			strings["DamagePerClick"] = "урон за клик";
			strings["NewSquadName"] = "Новое название";			
			
			strings["Armor"] = "Броня";
			
			strings["Group"] = "Отряд";
			strings["Leader"] = "Лидер";
			strings["Glory"] = "Слава";
			
			strings["Heroes"] = "Герои";
			strings["Skills"] = "Навыки";			
			strings["Weapons"] = "Оружие";
			
			strings["Dungeon"] = "Подземелье";
			strings["YouAreHere"] = "Ваш отряд здесь";
			strings["NameTaken"] = "Упс! Похоже такое название уже занято";
			
			//loading texts
			strings["DecodingSounds"] = "Декодирование звуков";
			strings["Loading"] = "Загрузка";
			strings["LoadingDungeon"] = "Загрузка ресурсов подземелья";
			
					
			//dungeons
			strings["Dungeon" + Dungeon.DUNGEON_ORANGERY] = "Заколдованная оранжерея";
			strings["Dungeon" + Dungeon.DUNGEON_GRAVEYARD] = "Могильный звон";
			strings["Dungeon" + Dungeon.DUNGEON_TWILIGHT_CRYPT] = "Изумрудный склеп";
			strings["Dungeon" + Dungeon.DUNGEON_GOBLIN_CITADEL] = "Цитадель гоблинов";
			
			strings["Dungeon" + Dungeon.DUNGEON_ORANGERY + Language.CASE_GENETIVE] = "Заколдованной оранжереи";
			strings["Dungeon" + Dungeon.DUNGEON_GRAVEYARD + Language.CASE_GENETIVE] = "Могильного звона";
			strings["Dungeon" + Dungeon.DUNGEON_TWILIGHT_CRYPT + Language.CASE_GENETIVE] = "Изумрудного склепа";
			strings["Dungeon" + Dungeon.DUNGEON_GOBLIN_CITADEL + Language.CASE_GENETIVE] = "Цитадели гоблинов";
			
			strings["Dungeon" + Dungeon.DUNGEON_ORANGERY + Language.CASE_PREPOSITIONAL_LOCATIVE] = "Заколдованной оранжерее";
			strings["Dungeon" + Dungeon.DUNGEON_GRAVEYARD + Language.CASE_PREPOSITIONAL_LOCATIVE] = "Могильном звоне";
			strings["Dungeon" + Dungeon.DUNGEON_TWILIGHT_CRYPT + Language.CASE_PREPOSITIONAL_LOCATIVE] = "Изумрудном склепе";
			strings["Dungeon" + Dungeon.DUNGEON_GOBLIN_CITADEL + Language.CASE_PREPOSITIONAL_LOCATIVE] = "Цитадели гоблинов";				

			
			strings["IntoTheDungeonButtonLabel"] = "В подземелье";
			
			strings["ExplainEffectRarity" + 2000] = "Некоторые";
			strings["ExplainEffectRarity" + 4000] = "Многие";
			
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS] = "У монстров в этом подземелье $r встречается черта $t";
			
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_ORANGERY + Trait.TRAIT_FAT] = "В Оранжерее отличный урожай. $r монстры получают черту Толстый.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_ORANGERY + Trait.TRAIT_FAST] = "Чары ускорили метаболизм растений. $r монстры получают черту Быстрый.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_ORANGERY + Trait.TRAIT_TOUGH] = "Местная флора активно мутирует. $r монстры получают черту Крепкий.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_ORANGERY + Trait.TRAIT_SHADOW] = "Реликтовые виды пробудились. $r монстры получают черту Древний.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_ORANGERY + Trait.TRAIT_POISONOUS] = "Токсические споры в воздухе. $r монстры получают черту Ядовитый.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_ORANGERY + Trait.TRAIT_CHARMER] = "Местные обитатели пропитались магией. $r монстры получают черту Заклинатель.";

			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_TWILIGHT_CRYPT + Trait.TRAIT_FAT] = "Склеп дает силы своим обитателям. $r монстры получают черту Толстый.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_TWILIGHT_CRYPT + Trait.TRAIT_FAST] = "Проклятые сокровища замедляют грабителей. $r монстры получают черту Быстрый.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_TWILIGHT_CRYPT + Trait.TRAIT_TOUGH] = "Защитные чары склепа активированы. $r монстры получают черту Крепкий.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_TWILIGHT_CRYPT + Trait.TRAIT_SHADOW] = "Запечатанные залы открыты. $r монстры получают черту Древний.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_TWILIGHT_CRYPT + Trait.TRAIT_POISONOUS] = "Нижние этажи заполнил едкий газ. $r монстры получают черту Ядовитый.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_TWILIGHT_CRYPT + Trait.TRAIT_CHARMER] = "Изумрудные залы лишают рассудка. $r монстры получают черту Заклинатель.";
			
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_GRAVEYARD + Trait.TRAIT_FAT] = "Нежить хорошо питается героями. $r монстры получают черту Толстый.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_GRAVEYARD + Trait.TRAIT_FAST] = "Чудовища ловко прячутся в тенях. $r монстры получают черту Быстрый.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_GRAVEYARD + Trait.TRAIT_TOUGH] = "В полнолунии нежить черпает силы. $r монстры получают черту Крепкий.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_GRAVEYARD + Trait.TRAIT_SHADOW] = "Страшные чудища подняты из тьмы веков. $r монстры получают черту Древний.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_GRAVEYARD + Trait.TRAIT_POISONOUS] = "Укусы нежити заражают кровь. $r монстры получают черту Ядовитый.";
			strings["ExplainDungeonEffect" + DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS + Dungeon.DUNGEON_GRAVEYARD + Trait.TRAIT_CHARMER] = "Темная магия поднимает мертвых. $r монстры получают черту Заклинатель.";			
			
			strings["MonstersPossibleInDungeon"] = "В этом подземелье встречаются следующие монстры: ";
			strings["PlayersInThisDungeon"] = "Сейчас игроков в этом подземелье: ";
			
			strings["Conan"] = "Конан";
			strings["Leoric"] = "Леорик";			
			
			strings["EffectOfTheDay"] = "Эффект дня";
			
			strings["Rarity" + MonsterData.RARITY_COMMON] = "Обычный";
			strings["Rarity" + MonsterData.RARITY_RARE] = "Редкий";
			strings["Rarity" + MonsterData.RARITY_ELITE] = "Элитный";		
			
			strings["Trait" + Trait.TRAIT_FAT] = "Толстый";
			strings["Trait" + Trait.TRAIT_FAST] = "Быстрый";
			strings["Trait" + Trait.TRAIT_TOUGH] = "Крепкий";
			strings["Trait" + Trait.TRAIT_SHADOW] = "Древний";
			strings["Trait" + Trait.TRAIT_POISONOUS] = "Ядовитый";
			strings["Trait" + Trait.TRAIT_CHARMER] = "Заклинатель";			
			
			strings["Affliction" + Affliction.AFFLICTION_POISON] = "Герой отравлен";
			strings["TitleExplainAffliction" + Affliction.AFFLICTION_POISON] = "Герой отравлен";
			strings["ExplainAffliction" + Affliction.AFFLICTION_POISON] = "Его урон снижен на 50%";
			strings["Affliction" + Affliction.AFFLICTION_CHARM] = "Герой зачарован";
			strings["TitleExplainAffliction" + Affliction.AFFLICTION_CHARM] = "Герой зачарован";
			strings["ExplainAffliction" + Affliction.AFFLICTION_CHARM] = "Вместо атаки он исцеляет монстров";
			
			strings["Test"] = "Тест";
			strings["Web"] = "Веб";
			strings["WebPlayer"] = "Веб-игрок";				
			strings["TOP"] = "ТОП";
			
			strings["GloryPerDay"] = "За сутки";
			strings["GloryTotal"] = "За все время";
			
			strings["None"] = "Нет";
			
			strings["DamageAtLevel"] = "Урон на следующем уровне: ";
			
			strings["DefaultGroupName"] = "Безымянный отряд";
			
			strings["Exhausted"] = "Истощено";
			
			//explain
			strings["TitleExplainNotEnoughGLory"] = "Недостаточно славы";
			strings["ExplainNotEnoughGLory"] = "Эта ступень навыков откроется, когда ваш отряд заработает достаточно славы";
			
			strings["TitleExplainDungeonsButton"] = "Атлас подземелий";
			
			strings["ExplainMonstersKilled"] = "Вам осталось победить $n монстров, чтобы перейти на следующий уровень подземелья";
			
			strings["TitleExplainTimer"] = "Бегство";
			strings["ExplainTimer"] = "Если этот таймер дойдет до конца, монстр сбежит, не оставив вам награды";
			
			strings["TitleExplainArmor"] = "Броня";			
			strings["ExplainArmor"] = "Поглощает урон в размере равном показателю брони, но не более 70%";
			
			strings["TitleBigNumbers"] = "Золото";
			
			strings["TitleExplainGlory"] = "Слава"; 
			strings["ExplainGlory"] = "Определяет вашу позицию в рейтинге охотников, а также открывает новые навыки отряда";
			
			strings["TitleExplainExhaustion"] = "Истощение";
			strings["ExplainExhaustion"] = "Когда этот счетчик дойдет до конца, подземелье истощится. Монстры в истощённом подземелье приносят меньше золота. Чем больше игроков в подземелье, тем быстрее оно истощается.";
			
			strings["TitleExplainExhausted"] = "Истощено";
			strings["ExplainExhausted"] = "Подземелье истощено. Монстры приносят только 20% золота.";
			
			//strings["TitleTraits"] = "Черты монстра";
			strings["TitleExplainTrait" + Trait.TRAIT_FAT] = "Толстый";
			strings["ExplainTrait" + Trait.TRAIT_FAT] = "Запас здоровья увеличен на 35%";
			
			strings["TitleExplainTrait" + Trait.TRAIT_FAST] = "Быстрый";
			strings["ExplainTrait" + Trait.TRAIT_FAST] = "Монстр убегает на 35% быстрее";
			
			strings["TitleExplainTrait" + Trait.TRAIT_TOUGH] = "Крепкий";
			strings["ExplainTrait" + Trait.TRAIT_TOUGH] = "Броня увеличена на 50%";
			
			strings["TitleExplainTrait" + Trait.TRAIT_SHADOW] = "Древний";
			strings["ExplainTrait" + Trait.TRAIT_SHADOW] = "Защита монстра поглощает 50% урона, если герой не эффективен против этого типа защиты";
			
			strings["TitleExplainTrait" + Trait.TRAIT_POISONOUS] = "Ядовитый";
			strings["ExplainTrait" + Trait.TRAIT_POISONOUS] = "С некоторой вероятностью может отравить героя, снижая его урон";	
			
			strings["TitleExplainTrait" + Trait.TRAIT_CHARMER] = "Заклинатель";
			strings["ExplainTrait" + Trait.TRAIT_CHARMER] = "С некоторой вероятностью может зачаровать героя, заставляя его исцелять монстров вместо атаки";
			
			strings["TitleExplainDamageType" + Damage.DAMAGE_TYPE_RANGED] = "Рейнджер";
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_RANGED] = "Эффективен против монстров Без защиты и наносит им увеличенный урон";
			
			strings["TitleExplainDamageType" + Damage.DAMAGE_TYPE_HOLY] = "Паладин";
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_HOLY] = "Эффективен против монстров с Темной защитой и наносит им увеличенный урон";
			
			strings["TitleExplainDamageType" + Damage.DAMAGE_TYPE_PIERCE] = "Разбойник";
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_PIERCE] = "Эффективен против монстров с Тяжелой защитой и наносит им увеличенный урон";
			
			strings["TitleExplainDamageType" + Damage.DAMAGE_TYPE_SIEGE] = "Инженер";
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_SIEGE] = "Эффективен против монстров с Укрепленной защитой и наносит им увеличенный урон";
			
			strings["TitleExplainDamageType" + Damage.DAMAGE_TYPE_SLASH] = "Воин";
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_SLASH] = "Эффективен против монстров с Лёгкой защитой и наносит им увеличенный урон";
			
			strings["TitleExplainDamageType" + Damage.DAMAGE_TYPE_SPELL] = "Чародей";
			strings["ExplainDamageType" + Damage.DAMAGE_TYPE_SPELL] = "Эффективен против монстров с Магической защитой и наносит им увеличенный урон";
			
			strings["TitleExplainArmorType" + MonsterData.ARMOR_DARK] = "Темная защита";
			strings["ExplainArmorType" + MonsterData.ARMOR_DARK] = "Атаки Паладина наносят на 100% больше урона";
			
			strings["TitleExplainArmorType" + MonsterData.ARMOR_FORTIFIED] = "Укреплённая защита";
			strings["ExplainArmorType" + MonsterData.ARMOR_FORTIFIED] = "Атаки Инженера наносят на 100% больше урона";
			
			strings["TitleExplainArmorType" + MonsterData.ARMOR_HEAVY] = "Тяжелая защита";
			strings["ExplainArmorType" + MonsterData.ARMOR_HEAVY] = "Атаки Разбойника наносят на 100% больше урона";
			
			strings["TitleExplainArmorType" + MonsterData.ARMOR_LIGHT] = "Лёгкая защита";
			strings["ExplainArmorType" + MonsterData.ARMOR_LIGHT] = "Атаки Воина наносят на 100% больше урона";
			
			strings["TitleExplainArmorType" + MonsterData.ARMOR_MAGIC] = "Магическая защита";
			strings["ExplainArmorType" + MonsterData.ARMOR_MAGIC] = "Атаки Чародея наносят на 100% больше урона";
			
			strings["TitleExplainArmorType" + MonsterData.ARMOR_NO_ARMOR] = "Без защиты";		
			strings["ExplainArmorType" + MonsterData.ARMOR_NO_ARMOR] = "Атаки Рейнджера наносят на 100% больше урона";					
			
			strings["TitleExplainEliteMonster"] = "Элитный";
			strings["ExplainEliteMonster"] = "За победу вы получите не только золото, но и славу";
			
			strings["TitleExplainRareMonster"] = "Редкий";
			strings["ExplainRareMonster"] = "За победу вы получите не только золото, но и славу";
			
			strings["TitleExplainCommonMonster"] = "Обычный";
			strings["ExplainCommonMonster"] = "За победу вы получите только золото";			
			
			strings["Skill" + Skill.SKILL_INCREASE_GLOBAL_DAMAGE + "Subtype0"] = "Боевая ярость";
			strings["SkillDescription" + Skill.SKILL_INCREASE_GLOBAL_DAMAGE] = "На $n увеличивает урон всех героев";
			
			strings["Skill" + Skill.SKILL_DECREASE_MONSTER_ARMOR + "Subtype0"] = "Раскол брони";
			strings["SkillDescription" + Skill.SKILL_DECREASE_MONSTER_ARMOR] = "Снижает броню монстров на $n";
			
			strings["Skill" + Skill.SKILL_INCREASE_MONSTER_COINS + "Subtype0"] = "Счастливая монетка";
			strings["SkillDescription" + Skill.SKILL_INCREASE_MONSTER_COINS] = "С вероятностью 10% добавляет дополнительную монетку с каждого монстра";
			
			strings["Skill" + Skill.SKILL_INCREASE_STUN_CHANCE + "Subtype0"] = "Оглушающие атаки";
			strings["SkillDescription" + Skill.SKILL_INCREASE_STUN_CHANCE] = "На $n увеличивает вероятность того, что герой оглушит монстра при атаке";
			
			strings["Skill" + Skill.SKILL_EMPOWER_HERO + "Subtype" + HeroData.HERO_BARBARIAN] = "Мощные удары";
			strings["Skill" + Skill.SKILL_EMPOWER_HERO + "Subtype" + HeroData.HERO_RANGER] = "Меткие выстрелы";
			strings["Skill" + Skill.SKILL_EMPOWER_HERO + "Subtype" + HeroData.HERO_MAGE] = "Тайные знания";
			strings["Skill" + Skill.SKILL_EMPOWER_HERO + "Subtype" + HeroData.HERO_ROGUE] = "Особые приемы";
			strings["Skill" + Skill.SKILL_EMPOWER_HERO + "Subtype" + HeroData.HERO_ENGINEER] = "Улучшенные бомбы";
			strings["Skill" + Skill.SKILL_EMPOWER_HERO + "Subtype" + HeroData.HERO_PALADIN] = "Медитативные практики";			
			strings["SkillDescription" + Skill.SKILL_EMPOWER_HERO] = "Атаки $hero наносят на $n больше урона";
			
			strings["Skill" + Skill.SKILL_HEAL_CHANCE + "Subtype0"] = "Знание трав";
			strings["SkillDescription" + Skill.SKILL_HEAL_CHANCE] = "На $n увеличивает вероятность снять негативный эффект с героя каждую секунду";
			
			strings["Skill" + Skill.SKILL_MERCENARY + "Subtype0"] = "Работа по найму";
			strings["SkillDescription" + Skill.SKILL_MERCENARY] = "Увеличивает награду за задания на $n";
			
			strings["Skill" + Skill.SKILL_PATHFINDER + "Subtype0"] = "Чтение следов";			
			strings["SkillDescription" + Skill.SKILL_PATHFINDER] = "На $n1 увеличивает вероятность встретить редкого монстра и на $n2 - элитного";	
			
			strings["TitleExplainHero" + HeroData.HERO_BARBARIAN] = "Олаф Суровый";
			strings["ExplainHero" + HeroData.HERO_BARBARIAN] = "Выросший в землях, продуваемых ледяными ветрами, Олаф не всегда был известен под своим прозвищем. Он получил его в тот день, когда, изрядно набравшись, после пира завалился спать в медвежью пещеру. Одолев медведя голыми руками, Олаф получил не только громкое прозвище, но и теплую шкуру, которую носит на своих широких плечах по сей день.";
			
			strings["TitleExplainHero" + HeroData.HERO_RANGER] = "Крадущаяся Тень";
			strings["ExplainHero" + HeroData.HERO_RANGER] = "Всякий, кого дорога приключений завела в заповедные леса эльфов, рискует получить стрелу в колено от этой ловкой лучницы. Из-за непроизносимости эльфийских имен люди зовут ее Крадущейся Тенью, примерно так переводится её настоящее имя. Она хочет посмотреть весь мир и с удовольствием отправится в путешествие с теми, кому нужна ее помощь.";
			
			strings["TitleExplainHero" + HeroData.HERO_MAGE] = "Август Меригольд";
			strings["ExplainHero" + HeroData.HERO_MAGE] = "Этот выдающийся чародей путешествует по миру, собирая по крупицам знания об Академическом волшебстве. Тех, кто встает у него на пути, он щедро осыпает молниями, огненными шарами и чародейскими стрелами. Из-за странного недуга заклинание после произнесения почему-то тут же вылетает из его головы, и чтобы вспомнить магические слова, волшебнику приходится заглядывать в свой гримуар.";
			
			strings["TitleExplainHero" + HeroData.HERO_ROGUE] = "Лоренцо Чероне";
			strings["ExplainHero" + HeroData.HERO_ROGUE] = "Доподлинно неизвестно, откуда прибыл этот скрытный иностранец, но, похоже, криминальные круги, в которых он вращался, больше ему не рады. Лоренцо не особо разговорчив, и даже если вам удастся завязать с ним беседу, половину сказанного вы все равно не разберете из-за жуткого акцента. Впрочем, длинные разговоры ни к чему — вы просто называете имя человека, которого вам нужно убрать, и отдаете деньги. В этом мрачном деле Лоренцо настоящий профессионал."
			
			strings["TitleExplainHero" + HeroData.HERO_ENGINEER] = "Рори Огнебород";
			strings["ExplainHero" + HeroData.HERO_ENGINEER] = "До недавнего времени алхимик и изобретатель Рори жил в Горной крепости и занимался тем, что готовил лечебные настойки. Все изменилось в тот день, когда напутав с ингридиентами, он вызвал сильнейший взрыв, разнесший в щепки его лабораторию, а заодно и сокровищницу Горного короля. Чудом выжившего алхимика изгнали из крепости, но это его не беспокоило. Теперь Рори хотел лишь знать, как сделать взрыв еще мощнее.";
			
			strings["TitleExplainHero" + HeroData.HERO_PALADIN] = "Сэр Ричард";
			strings["ExplainHero" + HeroData.HERO_PALADIN] = "Кодекс рыцаря-паладина требует трех вещей: отказа от собственности, самодисциплины и служения обществу. Следуя принципу сострадания, паладин должен быть защитником для слабых и обязан оказывать помощь нуждающимся в ней. Завершив десятилетнее обучение в стенах Белого Ордена, сэр Ричард отправился в путешествие по миру, чтобы принести Свет даже в самые темные его уголки.";
			
			//tutorial
			strings["Tutorial0"] = "Кликай по монстру, чтобы победить его";
			
			//tips
			
			strings["TipHeroes"] = "Герои продолжают сражаться с монстрами даже когда вас нет в игре. Вернувшись, вы получите все заработанные ими награды";
			strings["TipWeapons"] = "Оружие определяет силу вашего удара";			
			
			//orangery
			
			monsterNames["Monster" + MonsterData.MONSTER_DUMMY + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Огородный страшила";
			monsterNames["Monster" + MonsterData.MONSTER_DUMMY + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Огородных страшил";			
			monsterNames["Monster" + MonsterData.MONSTER_DUMMY + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Садовый потрошитель";
			monsterNames["Monster" + MonsterData.MONSTER_DUMMY + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Садовых потрошителей";
			monsterNames["Monster" + MonsterData.MONSTER_DUMMY + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Повелитель тыкв";
			monsterNames["Monster" + MonsterData.MONSTER_DUMMY + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Повелителей тыкв";
			
			monsterNames["Monster" + MonsterData.MONSTER_SLIME + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Липкая жижа";
			monsterNames["Monster" + MonsterData.MONSTER_SLIME + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Липких жиж";
			monsterNames["Monster" + MonsterData.MONSTER_SLIME + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Садовая мутация";
			monsterNames["Monster" + MonsterData.MONSTER_SLIME + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Садовых мутаций";
			monsterNames["Monster" + MonsterData.MONSTER_SLIME + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Голодный студень";
			monsterNames["Monster" + MonsterData.MONSTER_SLIME + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Голодных студней";
			
			monsterNames["Monster" + MonsterData.MONSTER_SAPLING + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Агрессивный корнеплод";
			monsterNames["Monster" + MonsterData.MONSTER_SAPLING + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Смотритель грядок";
			monsterNames["Monster" + MonsterData.MONSTER_SAPLING + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Страж растений";
			monsterNames["Monster" + MonsterData.MONSTER_SAPLING + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Агрессивных корнеплодов";
			monsterNames["Monster" + MonsterData.MONSTER_SAPLING + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Смотрителей грядок";
			monsterNames["Monster" + MonsterData.MONSTER_SAPLING + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Стражей растений";
			
			monsterNames["Monster" + MonsterData.MONSTER_PLANT + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Плотоядная поросль";
			monsterNames["Monster" + MonsterData.MONSTER_PLANT + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Хищный сорняк";
			monsterNames["Monster" + MonsterData.MONSTER_PLANT + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Ползучий живоглот";
			monsterNames["Monster" + MonsterData.MONSTER_PLANT + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Плотоядных порослей";
			monsterNames["Monster" + MonsterData.MONSTER_PLANT + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Хищных сорняков";
			monsterNames["Monster" + MonsterData.MONSTER_PLANT + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Ползучих живоглотов";
			
			//graveyard
			
			monsterNames["Monster" + MonsterData.MONSTER_DARK_DUMMY + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Оживленное чучело";
			monsterNames["Monster" + MonsterData.MONSTER_DARK_DUMMY + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Пожиратель ворон";
			monsterNames["Monster" + MonsterData.MONSTER_DARK_DUMMY + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Ужас кладбища";
			monsterNames["Monster" + MonsterData.MONSTER_DARK_DUMMY + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Оживленных чучел";
			monsterNames["Monster" + MonsterData.MONSTER_DARK_DUMMY + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Пожирателей ворон";
			monsterNames["Monster" + MonsterData.MONSTER_DARK_DUMMY + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Ужасов кладбища";
			
			monsterNames["Monster" + MonsterData.MONSTER_SHADOW + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Чудовищный слуга";
			monsterNames["Monster" + MonsterData.MONSTER_SHADOW + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Сшитая нежить";
			monsterNames["Monster" + MonsterData.MONSTER_SHADOW + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Кошмарный людоед";
			monsterNames["Monster" + MonsterData.MONSTER_SHADOW + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Чудовищных слуг";
			monsterNames["Monster" + MonsterData.MONSTER_SHADOW + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Сшитой нежити";
			monsterNames["Monster" + MonsterData.MONSTER_SHADOW + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Кошмарных людоедов";
			
			monsterNames["Monster" + MonsterData.MONSTER_BAT + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Подсмотритель";
			monsterNames["Monster" + MonsterData.MONSTER_BAT + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Летучий спиногрыз";
			monsterNames["Monster" + MonsterData.MONSTER_BAT + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Крылатый костеглод";
			monsterNames["Monster" + MonsterData.MONSTER_BAT + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Подсмотрителей";
			monsterNames["Monster" + MonsterData.MONSTER_BAT + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Летучих спиногрызов";
			monsterNames["Monster" + MonsterData.MONSTER_BAT + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Крылатых костеглодов";
			
			monsterNames["Monster" + MonsterData.MONSTER_LURKER + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Скрытень";
			monsterNames["Monster" + MonsterData.MONSTER_LURKER + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Мрачный копатель";
			monsterNames["Monster" + MonsterData.MONSTER_LURKER + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Взрыхлитель кладбищ";
			monsterNames["Monster" + MonsterData.MONSTER_LURKER + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Скрытней";
			monsterNames["Monster" + MonsterData.MONSTER_LURKER + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Мрачных копателей";
			monsterNames["Monster" + MonsterData.MONSTER_LURKER + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Взрыхлителей кладбищ";
			
			//crypt
			
			monsterNames["Monster" + MonsterData.MONSTER_SKELETON + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Пробужденный";
			monsterNames["Monster" + MonsterData.MONSTER_SKELETON + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Истлевший воин";
			monsterNames["Monster" + MonsterData.MONSTER_SKELETON + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Владыка нежити";
			monsterNames["Monster" + MonsterData.MONSTER_SKELETON + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Пробужденных";
			monsterNames["Monster" + MonsterData.MONSTER_SKELETON + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Истлевших воинов";
			monsterNames["Monster" + MonsterData.MONSTER_SKELETON + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Владык нежити";
			
			monsterNames["Monster" + MonsterData.MONSTER_SENTRY + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Живой доспех";
			monsterNames["Monster" + MonsterData.MONSTER_SENTRY + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Страж смерти";
			monsterNames["Monster" + MonsterData.MONSTER_SENTRY + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Хранитель склепа";
			monsterNames["Monster" + MonsterData.MONSTER_SENTRY + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Живых доспехов";
			monsterNames["Monster" + MonsterData.MONSTER_SENTRY + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Стражей смерти";
			monsterNames["Monster" + MonsterData.MONSTER_SENTRY + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Хранителей склепа";
			
			monsterNames["Monster" + MonsterData.MONSTER_GOBLIN + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Гоблин-грабитель";
			monsterNames["Monster" + MonsterData.MONSTER_GOBLIN + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Зеленокожий мародер";
			monsterNames["Monster" + MonsterData.MONSTER_GOBLIN + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Расхититель гробниц";
			monsterNames["Monster" + MonsterData.MONSTER_GOBLIN + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Гоблинов-грабителей";
			monsterNames["Monster" + MonsterData.MONSTER_GOBLIN + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Зеленокожих мародеров";
			monsterNames["Monster" + MonsterData.MONSTER_GOBLIN + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Расхитителей гробниц";
			
			monsterNames["Monster" + MonsterData.MONSTER_CRYPT_SLIME + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_NOMINATIVE] = "Жидкий падальщик";
			monsterNames["Monster" + MonsterData.MONSTER_CRYPT_SLIME + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_NOMINATIVE] = "Мертвецкий кисель";
			monsterNames["Monster" + MonsterData.MONSTER_CRYPT_SLIME + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_NOMINATIVE] = "Пожиратель нежити";
			monsterNames["Monster" + MonsterData.MONSTER_CRYPT_SLIME + "Rarity" + MonsterData.RARITY_COMMON + Language.CASE_OBJECTIVE_PLURAL] = "Жидких падальщиков";
			monsterNames["Monster" + MonsterData.MONSTER_CRYPT_SLIME + "Rarity" + MonsterData.RARITY_RARE + Language.CASE_OBJECTIVE_PLURAL] = "Мертвецких киселей";
			monsterNames["Monster" + MonsterData.MONSTER_CRYPT_SLIME + "Rarity" + MonsterData.RARITY_ELITE + Language.CASE_OBJECTIVE_PLURAL] = "Пожирателей нежити";			
			
			
			
			//otheers
						
			//monsterNames["Monster" + MonsterData.MONSTER_BEHOLDER + "Rarity" + MonsterData.RARITY_COMMON] = "Плотоядная поросль";
			//monsterNames["Monster" + MonsterData.MONSTER_BEHOLDER + "Rarity" + MonsterData.RARITY_RARE] = "Хищный сорняк";
			//monsterNames["Monster" + MonsterData.MONSTER_BEHOLDER + "Rarity" + MonsterData.RARITY_ELITE] = "Смертоцвет";			
			
			strings["DecimalDivider"] = ",";
			
			//monsterPhrases.push("Я тебя уничтожу!");
			
			loadingPhrases.push
			(
					"Кормим монстров", "Разыскиваем ваш отряд", "Поджигаем факелы", "Моделируем игровую вселенную", "Наполняем моря", "Пишем историю", "Украшаем пугала", "Тренируем героев",
					"Загружаем экран загрузки", "Прибираемся в подземельях", "Прячем сокровища", "Усмиряем чудовищ", "Плетем паутину", "Расставляем ловушки", "Прячем скрытней", "Ловим гоблинов",
					"Сгущаем слизней", "Осматриваем подсмотрителей", "Косим хищные сорняки", "Подкручиваем кнопки", "Точим клинки", "Пишем задания", "Прячем мирных жителей",
					"Вращаем шестерни", "Выращиваем деревья", "Будим древнее зло", "Заполняем индикаторы", "Поднимаем нежить"
			);
			
			CreateOrdersData();
			CreareBossNamePhraseData();
			CreateMonsterPhrases();
		}
		
		private function CreateMonsterPhrases(): void
		{
			monsterPhrases[MonsterData.MONSTER_BAT] = new Vector.<String>;
			monsterPhrases[MonsterData.MONSTER_BAT].push("Р-р-р!", "*Хлоп-хлоп*", "Гр-р!", "С-с-с...", "*Хрум-хрум*");
			
			monsterPhrases[MonsterData.MONSTER_DARK_DUMMY] = new Vector.<String>;
			monsterPhrases[MonsterData.MONSTER_DARK_DUMMY].push("Как снять этот костюм?!", "Куда укатилась моя голова?", "Взгляни в глаза тыквы!", "Я - воплощение твоих кошмаров!", "Слышишь звон? Это по тебе!");
			
			monsterPhrases[MonsterData.MONSTER_LURKER] = new Vector.<String>;
			monsterPhrases[MonsterData.MONSTER_LURKER].push("Из-под земли достану!", "Эту яму я вырыл для тебя…", "Я стану последним, кого ты видел!", "Сыграем в ящик?", "Я не стану тебя убивать. Просто закопаю!");
			
			monsterPhrases[MonsterData.MONSTER_SHADOW] = new Vector.<String>;
			monsterPhrases[MonsterData.MONSTER_SHADOW].push("Еда! Еда идти ко мне!", "Хрустящие косточки!", "Мой кулак в твой голова!", "Мясо! Мясо!", "Ы-ы-ы");
								
			monsterPhrases[MonsterData.MONSTER_DUMMY] = new Vector.<String>;
			monsterPhrases[MonsterData.MONSTER_DUMMY].push("Подходи, дам тебе в тыкву!", "Я сегодня съел птицу. Хочешь покажу?", "Это лицо я себе сам вырезал!", "Давай-ка мы тебе нарисуем улыбку?", "Сделаю из твоей башки фонарик!");
			
			//monsterPhrases[MonsterData.MONSTER_PLANT] = new Vector.<String>;
			//monsterPhrases[MonsterData.MONSTER_PLANT].push("Я тебя съем!");
			
			monsterPhrases[MonsterData.MONSTER_SAPLING] = new Vector.<String>;
			monsterPhrases[MonsterData.MONSTER_SAPLING].push("Убери от меня свои грабли!", "Братва! Убивают!", "Проваливай с моей грядки!", "Я сейчас баклажанов позову!", "Кинуть в тебя удобрением?");
			
			monsterPhrases[MonsterData.MONSTER_SLIME] = new Vector.<String>;
			monsterPhrases[MonsterData.MONSTER_SLIME].push("*буль-буль*", "*хлюп-хлюп*", "*буль-буль-буль*", "Гр-р-р", "Гр-ы-ы", "*чявк-чявк-чявк*");
			
			monsterPhrases[MonsterData.MONSTER_SKELETON] = new Vector.<String>;
			monsterPhrases[MonsterData.MONSTER_SKELETON].push("Кто нарушил мой покой?..", "Тысячелетний...голод...", "Смерть мне не грозит", "Ты будешь...одним из нас", "Тебя ждет...вечный покой");
			
			monsterPhrases[MonsterData.MONSTER_GOBLIN] = new Vector.<String>;
			monsterPhrases[MonsterData.MONSTER_GOBLIN].push("Спрячь это, а то порежешься!", "Я уже позеленел от злости!", "Босс хорошо заплатит за твою голову", "Какая встреча!", "Мертвяки в другом зале, отвали!");
			
			monsterPhrases[MonsterData.MONSTER_SENTRY] = new Vector.<String>;
			monsterPhrases[MonsterData.MONSTER_SENTRY].push("Пробуждение...", "Замечены нарушители", "Вам не уйти", "Я тебя вижу", "Ты не спрячешься");
			
			monsterPhrases[MonsterData.MONSTER_CRYPT_SLIME] = new Vector.<String>;
			monsterPhrases[MonsterData.MONSTER_CRYPT_SLIME].push("*буль-буль*", "*хлюп-хлюп*", "*буль-буль-буль*", "Гр-р-р", "Гр-ы-ы", "*чявк-чявк-чявк*");			
		}
		
		private function CreareBossNamePhraseData():void 
		{
			bossNamePhraseHeads.push("Клык", "Коготь", "Жнец", "Плеть", "Вихрь", "Шёпот", "Страж", "Дух", "Сердце", "Воплощение", "Душа", "Лезвие", "Демон", "Слуга", "Герцог", 
										"Барон", "Сущность", "Длань", "Крик", "Владыка");
			bossNamePhraseHeadsObjective.push("Клыка", "Когтя", "Жнеца", "Плеть", "Вихрь", "Шёпот", "Стража", "Духа", "Сердце", "Воплощение", "Душу", "Лезвие", "Демона", "Слугу", "Герцога", 
										"Барона", "Сущность", "Длань", "Крик", "Владыку");
			bossNamePhraseDependents.push("Тьмы", "Зла", "Хаоса", "Пустоты", "Погибели", "Душ", "Проклятых", "Холода", "Пламени", "Звезд", "Отчаяния", "Боли", "Праха", "Гнева",
											"Кошмаров", "Ужаса", "Сумрака", "Забвения", "Коварства", "Алчности");
		}
				
		private function CreateOrdersData():void 
		{
			//orderAbbreviations.push("т", "м", "М", "Т", "к", "К", "с", "С", "О", "Н", "д", "У", "Д", "*");
			orderAbbreviations.push("K", "M", "B", "T", "q", "Q", "s", "S", "O", "N", "d", "U", "D", "*");
			orderNames.push("тысяча", "миллион", "миллиард", "триллион", "квадриллион", "квинтиллион", "секстилион", "септиллион", "октиллион", "нониллион", "дециллион", "ундециллион", "додециллион", "еще больше");
		}
		
	}

}