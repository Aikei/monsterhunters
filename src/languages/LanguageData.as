package languages 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import flash.utils.Dictionary;
	
	public class LanguageData 
	{
		public static const HEAD_FIRST: int = 0;
		public static const DEPENDENT_FIRST: int = 1;
		
		public var language: String;	
		public var strings : Dictionary = new Dictionary;
		public var monsterNames: Dictionary = new Dictionary;
		public var names: Vector.<String> = new Vector.<String>;
		public var monsterPhrases: Dictionary = new Dictionary;			
		public var loadingPhrases: Vector.<String> = new Vector.<String>;
		public var orderAbbreviations: Vector.<String> = new Vector.<String>;
		public var orderNames: Vector.<String> = new Vector.<String>;
		
		public var bossNamePhraseDependents: Vector.<String> = new Vector.<String>;
		
		public var bossNamePhraseHeads: Vector.<String> = new Vector.<String>;
		public var bossNamePhraseHeadsObjective: Vector.<String> = new Vector.<String>;
		
		public var bossNamePhraseOrder: int = HEAD_FIRST;
	}

}