package languages 
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import logic.HeroData;
	import logic.MonsterData;
	import flash.utils.Dictionary;
	import logic.Weapon;
	import logic.Damage;
	import logic.Skill;
	import logic.Trait;
	import logic.DungeonEffect;
	import logic.Affliction;	
	
	public class English extends LanguageData
	{
				
		public function English(): void 
		{
			strings["Hero" + String(HeroData.HERO_BARBARIAN)] = "Barbarian";

			strings["Weapon" + String(Weapon.WEAPON_WOODEN_SWORD)] = "Wooden Sword";
			strings["Weapon" + String(Weapon.WEAPON_APPRENTICE_BLADE)] = "Iron Sword";
			strings["Weapon" + String(Weapon.WEAPON_KNIGHTS_SWORD)] = "Steel Sword";
			
			strings["Damage" + Damage.DAMAGE_TYPE_RANGED] = "Ranged";
			
			//talents description
			strings["SkillDescription" + Skill.SKILL_INCREASE_GLOBAL_DAMAGE] = "$n to all heroes' DPS";
			strings["SkillDescription" + Skill.SKILL_DECREASE_MONSTER_ARMOR] = "$n to all monsters' armor";
			strings["SkillDescription" + Skill.SKILL_INCREASE_MONSTER_COINS] = "Monsters drop $n gold";
			strings["SkillDescription" + Skill.SKILL_INCREASE_STUN_CHANCE] = "$n seconds to stun duration";
			strings["SkillDescription" + Skill.SKILL_EMPOWER_HERO] = "$hero deals $n more damage";
			strings["SkillDescription" + Skill.SKILL_HEAL_CHANCE] = "$n chance to heal negative hero effect";			
			
			//time parse
			strings["HoursAbbreviarion"] = "h";
			strings["MinutesAbbreviarion"] = "m";
			strings["SecondsAbbreviarion"] = "s";			
			
			//wall post
			strings["ReportShareMessage"] = "I got $gold gold and $glory glory in Monster Hunters. Join in!";			
			
			//bg report texts
			strings["Share"] = "Share";
			strings["Sir"] = "Sir";
			strings["Dame"] = "Dame";
			strings["BgReportComment"] = "While you were away $sq vanquished $num monsters, earned gold and glory";
			
			//misc
			strings["sec"] = "sec";
			strings["SecondsAbbreviation"] = "s";
			strings["NotAvailableInBeta"] = "Not available in beta";
			strings["MonsterRanAway"] = "Monster ran away!";
			
			//quest
			strings["Quest"] = "Quest";
			strings["QuestKillAny"] = "Defeat $n monsters while in game";
			strings["QuestKillOfType"] = "Defeat $n $t in $d while in game";
			strings["QuestProgress"] = "Progress";
			strings["QuestComplete"] = "Quest complete";
			strings["AcceptQuest"] = "Accept";
			strings["RefuseQuest"] = "Refuse";
			strings["Reward"] = "Reward";
			strings["CancelQuest"] = "Cancel quest";
			
			//squad window
			strings["Buy"] = "Buy";
			strings["Upgrade"] = "Upgrade";
			strings["Lv"] = "lv";
			strings["Level"] = "Level";
			strings["Dps"] = "Dps";			
			strings["Damage"] = "Damage";
			strings["NewSquadName"] = "New name";			
			
			strings["Heroes"] = "Heroes";
			strings["Skills"] = "Skills";
			strings["Weapons"] = "Weapons";
			
			strings["Dungeon"] = "Dungeon";
			strings["YouAreHere"] = "You are here";
			strings["NameTaken"] = "Oops! This name is already taken";
			
			strings["Dungeon1"] = "Magic Green-House";
			strings["Dungeon2"] = "Twilight Crypt Yard";
			strings["Dungeon3"] = "Twilight Crypt Halls";
			strings["Dungeon4"] = "Goblin Stronghold";
			strings["Dungeon5"] = "Dungeon 5";
			
			strings["DecodingSounds"] = "Decoding soundsв";
			strings["ExplainEffectRarity" + 2000] = "Sometimes";
			strings["ExplainEffectRarity" + 4000] = "Often";
			strings["ExplainDungeonEffect"+DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS] = "Monsters in this dungeon $r have the trait $t";		
			
			strings["MonstersPossibleInDungeon"] = "Following monsters are possible in this dungeon: ";
			strings["PlayersInThisDungeon"] = "Players currently in this dungeon: ";			
			
			strings["Conan"] = "Conan";
			
			strings["ExplainArmor"] = "Each 1 point of armor decreases damage of each attack by 1, but effective damage can't be lower than 30% of original damage";
			strings["ExplainTimer"] = "When this time runs out, the monster will flee";
			
			strings["Rarity"+MonsterData.RARITY_COMMON] = "Common";
			strings["Rarity" + MonsterData.RARITY_RARE] = "Rare";
			strings["Rarity" + MonsterData.RARITY_ELITE] = "Elite";
			
			strings["ExplainRareMonster"] = "This is a %rarity monster. You will receive Glory for killing it.";
			strings["ExplainCommonMonster"] = "This is a common mosnter. You will not received glory for killing it.";
			
			strings["Trait" + Trait.TRAIT_FAT] = "Fat";
			strings["Trait" + Trait.TRAIT_FAST] = "Fast";
			strings["Trait" + Trait.TRAIT_TOUGH] = "Tough";
			strings["Trait" + Trait.TRAIT_SHADOW] = "Shadow";
			strings["Trait" + Trait.TRAIT_POISONOUS] = "Poisonous";
			
			strings["ExplainTrait" + Trait.TRAIT_FAT] = "Fat monster have more health";
			strings["ExplainTrait" + Trait.TRAIT_FAST] = "Fast monsters run away faster";
			strings["ExplainTrait" + Trait.TRAIT_TOUGH] = "Touch monsters have more armor";
			strings["ExplainTrait" + Trait.TRAIT_SHADOW] = "Non-effective attacks deal considerably less damage to Shadow monsters";
			strings["ExplainTrait" + Trait.TRAIT_POISONOUS] = "Poisonous monsters have a chance to poison a random hero decreasing their DPS";			
			
			strings["TOP"] = "TOP";
			
			strings["None"] = "None";
			
			strings["DefaultGroupName"] = "My group";
			
			strings["Exhausted"] = "Exhausted";
			
			strings["Loading"] = "Loading";
			
			//tips
			
			strings["TipHeroes"] = "By hiring heroes you free yourself of necessity to click, they damage monsters without your participation";
			strings["TipWeapons"] = "The weapon defines your click damage";					
			
			strings["DecimalDivider"] = ".";
			
			Orders();
		}
		
		private function Orders():void 
		{
			orderAbbreviations.push("K", "M", "B", "T", "q", "Q", "s", "S", "O", "N", "d", "U", "D", "*");
			orderNames.push("Thousand", "Million", "Billion", "Trillion", "Quadrillion", "Quintillion", "Sextillion", "Septillion", "Octillion", "Nonillion", "Decillion", "Undecillion", "Duodecillion", "Even more");
		}		
	}

}