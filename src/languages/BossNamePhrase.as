package languages 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class BossNamePhrase 
	{
		//public var order = Language.languageData.bossNamePhraseOrder;
		private var _headNumber: int = 0;
		private var _dependentNumber: int = 0;
		
		public function get headNumber(): int 
		{
			return _headNumber;
		}
		
		public function get dependentNumber(): int 
		{
			return _dependentNumber;
		}
		
		public function BossNamePhrase(headNumber: int, dependentNumber: int) 
		{
			this._headNumber = headNumber;
			this._dependentNumber = dependentNumber;
		}
		
		public function GetString(nCase: int = Language.CASE_NOMINATIVE): String 
		{
			var head: String;
			if (nCase === Language.CASE_OBJECTIVE && Language.languageData.bossNamePhraseHeadsObjective.length > 0 && Language.languageData.bossNamePhraseHeadsObjective[_headNumber])
			{
				head = Language.languageData.bossNamePhraseHeadsObjective[_headNumber];
			}
			else
			{
				head = Language.languageData.bossNamePhraseHeads[_headNumber];
			}
			var dependent: String = Language.languageData.bossNamePhraseDependents[_dependentNumber];
			if (Language.languageData.bossNamePhraseOrder == LanguageData.HEAD_FIRST)
			{
				return head + " " + dependent;
			}
			else
			{
				return dependent + " " + head;
			}
		}
		
	}

}