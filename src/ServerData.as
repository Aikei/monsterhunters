package  
{
	import logic.Affliction;
	import logic.Dungeon;
	import logic.HeroData;
	import logic.Weapon;
	import logic.MonsterData;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class ServerData 
	{		
		public var heroes: Vector.<HeroData> = new Vector.<HeroData>;
		public var weapons: Vector.<Weapon> = new Vector.<Weapon>;
		public var monsters: Vector.<MonsterData> = new Vector.<MonsterData>;
		public var afflictions: Vector.<Affliction> = new Vector.<Affliction>;
		public function InitFromServerData(data: Object): void 
		{
			for (var i: int = 0; i < data.heroes.length; i++)
			{
				heroes.push(HeroData.CreateFromDataObject(data.heroes[i]));
			}
			for (i = 0; i < data.weapons.length; i++)
			{
				weapons.push(Weapon.CreateFromDataObject(data.weapons[i]));
			}
			for (i = 0; i < data.afflictions.length; i++)
			{
				afflictions.push(Affliction.CreateNewFromDataObject(data.afflictions[i]));
			}
			//for (i = 0; i < data.monsters.length; i++)
			//{
				//monsters.push(MonsterData.CreateFromDataObject(data.monsters[i]));
			//}		
		}
				
	}

}