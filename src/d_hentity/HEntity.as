package d_hentity 
{	
	//import flash.events.Event
	import com.greensock.TweenLite;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	import starling.events.Event;
	import flash.geom.Point;
	import flash.utils.Timer;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import flash.events.TimerEvent;
	import starling.events.TouchEvent;
	import starling.events.Touch;
	import starling.events.TouchPhase;
	import view.GameView;
	import view.gameviewelements.custom.HoverChecker;
	import view.gameviewelements.MessageWindow.MessageWindow;
	import system.HCursor;
	import view.gameviewelements.MessageWindow.TooltipWindow;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HEntity extends Sprite 
	{
		protected var _moving: Boolean = false;
		protected var _velocity: Vec2 = new Vec2;
		protected var _acceleration: Vec2 = new Vec2;
		protected var _stopOnPoint: Boolean = false;
		protected var _stopY: Number = -1;
		protected var _timer: Timer = null;
		protected var _timerPeriod: Number = 1.0 / 60.0;
		protected var _previousTime: Number;
		protected var _timePassed: Number = 0;
		protected var _hovered: Boolean = false;
		protected var _messageWindow: MessageWindow = null;
		protected var _touchInited: Boolean = false;
		protected var _rotationSpeed: Number = 0;
		protected var _rotating: Boolean = false;
		protected var _hoverTime: Number = 0;
		protected var _hoverChecker: HoverChecker;
		protected var _hoverTipTimeout: uint;
		//protected var _memberElements: Vector.<HEntity> = new Vector.<HEntity>;
		
		public var showTooltipOnHover: Boolean = false;
		public var tooltipText: String = "";
		public var tooltipTitleText: String = "";
		public var specialId: int = 0;
		public var saveX: Number;
		public var saveY: Number;
		private var _previousRunTime: Number = 0;
		private var _deltaTime: Number;
		protected var _useWorldsPassedTime: Boolean = false;
		
		static public var sPoint: Point = new Point;
		
		//public function set showTooltipOnHover(show: Boolean): void
		//{
			//_hoverChecker.showTooltipOnHover = show;
		//}
		//
		//public function get showTooltipOnHover(): Boolean
		//{
			//return _hoverChecker.showTooltipOnHover;
		//}
		//
		//public function set tooltipText(text: String): void
		//{
			//_hoverChecker.tooltipText = text;
		//}
		//
		//public function get tooltipText(): String
		//{
			//return _hoverChecker.tooltipText;
		//}	
		
		public function HEntity(initTimer: Boolean = false, initTouch: Boolean = false) 
		{
			super();
			if (initTouch)
			{
				InitTouch();
			}
			if (initTimer)
			{
				if (stage)
					stage.addEventListener(EnterFrameEvent.ENTER_FRAME, OnEnterFrame);
				else
					addEventListener(Event.ADDED_TO_STAGE, BaseAddedToStage);
			}			
		}
		
		private function BaseAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, BaseAddedToStage);
			stage.addEventListener(EnterFrameEvent.ENTER_FRAME, OnEnterFrame);
		}
		
		public function MaybeShowTip():void 
		{
			if (Misc.GetServerTime() - _hoverTime >= HoverChecker.TIP_DELAY)
			{
				clearTimeout(_hoverTipTimeout);
				_hoverTipTimeout = 0;					
				if (_hovered && showTooltipOnHover)
				{			
					if (_messageWindow === null)
					{
						if (tooltipTitleText.length > 0)
							_messageWindow = new TooltipWindow(tooltipText,tooltipTitleText);
						else if (Language.HasTitle(tooltipText))
							_messageWindow = new TooltipWindow(tooltipText);
						else if (Language.HasEntry(tooltipText))
							_messageWindow = new MessageWindow(Language.GetString(tooltipText));
						else
							_messageWindow = new MessageWindow(tooltipText);
						_messageWindow.alpha = HoverChecker.TIP_BASE_ALPHA;
						GameView.gView.tipHolder.addChild(_messageWindow);
						if (HoverChecker.TIP_APPEAR_TIME > 0)
						{
							_messageWindow.alpha = 0;
							TweenLite.to(_messageWindow, HoverChecker.TIP_APPEAR_TIME, { alpha: HoverChecker.TIP_BASE_ALPHA } );
						}						
					}
					
					_messageWindow.x = HCursor.GetCursor().x - _messageWindow.width / 2;
					_messageWindow.y = HCursor.GetCursor().y + 60;
					
					if (_messageWindow.y + _messageWindow.height >= Preferences.SCREEN_HEIGHT)
					{
						_messageWindow.y = HCursor.GetCursor().y - _messageWindow.height - 5;
					}
				}
			}
		}		
		//private function OnTimer(e:TimerEvent):void 
		//{
			//var timeNow: Number = Misc.GetTimeSecs();
			//if (_previousRunTime == 0)
				//_deltaTime = _timer.delay;
			//else
				//_deltaTime = timeNow - _previousRunTime;
			//_previousRunTime = timeNow;
			//var multiplier: Number = -1;
			//if (_moving)
			//{				
				//if (_stopOnPoint && y >= _stopY)
				//{
					//StopMyMovementAndAcceleration();
					//OnFall();
					//if (y > _stopY + 10)
					//{
						//y = _stopY;
					//}
				//}
				//if (World.gWorld.physics === World.gWorld.PHYSICS_DISCRETE || World.gWorld.firstFrameAfterDiscrete)
				//{
					//multiplier = _timerPeriod;
					//World.gWorld.firstFrameAfterDiscrete = false;
				//}
				//else if (World.gWorld.physics === World.gWorld.PHYSICS_CONTINUOUS)
				//{
					//multiplier = _deltaTime * World.gWorld.physSpeedMultiplier;
				//}
				//_velocity = _velocity.AddVector(_acceleration.MultScalar(multiplier));
				//x += _velocity.x * multiplier;
				//y += _velocity.y * multiplier;					
			//}
			//if (_rotating)
			//{
				//if (multiplier == -1)
				//{
					//if (World.gWorld.physics === World.gWorld.PHYSICS_DISCRETE || World.gWorld.firstFrameAfterDiscrete)
					//{
						//multiplier = _timerPeriod;
						//World.gWorld.firstFrameAfterDiscrete = false;
					//}
					//else if (World.gWorld.physics === World.gWorld.PHYSICS_CONTINUOUS)
					//{
						//multiplier = _deltaTime * World.gWorld.physSpeedMultiplier;
					//}
				//}
				//rotation += _rotationSpeed*multiplier;
			//}			
		//}
		
		public function InitTouch():void 
		{
			_touchInited = true;
			addEventListener(TouchEvent.TOUCH, OnTouch);
		}
		
		//public function InitTouch():void 
		//{
			//_touchInited = true;
			//_hoverChecker = new HoverChecker(this);
			////_hoverChecker.HoverCallback = OnHover;
			////_hoverChecker.StopHoverCallback = OnStopHover;
			////_hoverChecker.TouchBeganCallback = OnTouchPhaseBegan;
			////_hoverChecker.TouchEndedCallback = OnTouchPhaseEnded;
			//
			////addEventListener(TouchEvent.TOUCH, OnTouch);
			//_hoverChecker.addEventListener("H_Hover", OnHover);
			//_hoverChecker.addEventListener("H_StopHover", OnStopHover);
			//_hoverChecker.addEventListener("H_TouchBegan", OnTouchPhaseBegan);
			//_hoverChecker.addEventListener("H_TouchEnded", OnTouchPhaseEnded);
		//}	
		
		//public function InitTouch():void 
		//{
			//_touchInited = true;
			//addEventListener(TouchEvent.TOUCH, OnTouch);
		//}
		
		//protected function OnTouch(e:TouchEvent):void 
		//{
			//
		//}
		
		protected function OnHover():void 
		{
			if (!_hovered)
			{
				_hoverTime = Misc.GetServerTime();
				_hovered = true;
				_hoverTipTimeout = setTimeout(MaybeShowTip, HoverChecker.TIP_DELAY * 1000);
			}
			MaybeShowTip();				
			
			//if (showTooltipOnHover)
			//{
				//if (_messageWindow === null)
				//{
					//_messageWindow = new MessageWindow(tooltipText);
					//_messageWindow.alpha = 0.98;
					//World.gWorld.addChild(_messageWindow);
				//}
				//_messageWindow.x = HCursor.GetCursor().x - _messageWindow.width / 2;
				//_messageWindow.y = HCursor.GetCursor().y - _messageWindow.height - 4;				
			//}
		}
		
		protected function OnStopHover():void 
		{
			if (_hovered)
			{
				_hovered = false;
				if (_messageWindow)
				{
					_messageWindow.Destroy();
					_messageWindow = null;			
				}
			}
		}
		
		protected function OnTouchPhaseBegan():void 
		{
			
		}
		
		protected function OnTouchPhaseEnded():void 
		{
			
		}		
		
		//protected function OnTouch(event: TouchEvent): void
		//{
			//var touch: Touch = event.getTouch(this);
			//if (touch != null)
			//{
				//if (touch.phase === TouchPhase.HOVER || touch.phase === TouchPhase.STATIONARY)
				//{
					//_hovered = true;
					//OnHover();
				//}
				//if (touch.phase === TouchPhase.BEGAN)
				//{
					//OnTouchPhaseBegan();
					////OnStopHover();
				//}
				//else if (touch.phase === TouchPhase.ENDED)
				//{
					//OnTouchPhaseEnded();
					////OnStopHover();
				//}
				//else if (touch.phase === TouchPhase.MOVED)
				//{
					//globalToLocal(new Point(touch.globalX, touch.globalY), sPoint);
					//if (sPoint.x < 0 || sPoint.x >= width || sPoint.y < 0 || sPoint.y >= height)
					//{
						//OnStopHover();
					//}
				//}				
			//}
			//else
			//{
				//OnStopHover();
			//}
		//}
		
		public function OnTouch(e:TouchEvent):void 
		{
			var touch: Touch = e.getTouch(this);
			if (touch)
			{
				if (touch.phase === TouchPhase.HOVER)
				{
					OnHover();						
				}
				else
				{
					globalToLocal(new Point(touch.globalX, touch.globalY), sPoint);
					if (sPoint.x < 0 || sPoint.x >= width || sPoint.y < 0 || sPoint.y >= height)
					{
						OnStopHover();
					}
					
					if (touch.phase === TouchPhase.BEGAN)
					{
						OnTouchPhaseBegan();
					}
					else if (touch.phase === TouchPhase.ENDED)
					{
						OnTouchPhaseEnded();
					}
					
					ContinueHover();
				}
			}
			else
			{
				OnStopHover();					
			}
		}
		
		protected function ContinueHover():void 
		{
			if (_hovered)
				MaybeShowTip();
		}		
		
		public function OnEnterFrame(event: EnterFrameEvent): void 
		{
			var multiplier: Number = -1;
			var passedTime: Number;
			if (!_useWorldsPassedTime)
				passedTime = event.passedTime;
			else
				passedTime = World.framePassedTime;
			if (_moving)
			{				
				if (_stopOnPoint && y >= _stopY)
				{
					StopMyMovementAndAcceleration();
					OnFall();
					if (y > _stopY + 10)
					{
						y = _stopY;
					}
				}
				if (World.gWorld.physics === World.gWorld.PHYSICS_DISCRETE || World.gWorld.firstFrameAfterDiscrete)
				{
					multiplier = _timerPeriod;
					World.gWorld.firstFrameAfterDiscrete = false;
				}
				else if (World.gWorld.physics === World.gWorld.PHYSICS_CONTINUOUS)
				{
					multiplier = passedTime * World.gWorld.physSpeedMultiplier;
				}
				//_velocity = _velocity.AddVector(_acceleration.MultScalar(multiplier));
				_velocity.x += _acceleration.x * multiplier;
				_velocity.y += _acceleration.y * multiplier;
				x += _velocity.x * multiplier;
				y += _velocity.y * multiplier;					
			}
			if (_rotating)
			{
				if (multiplier == -1)
				{
					if (World.gWorld.physics === World.gWorld.PHYSICS_DISCRETE || World.gWorld.firstFrameAfterDiscrete)
					{
						multiplier = _timerPeriod;
						World.gWorld.firstFrameAfterDiscrete = false;
					}
					else if (World.gWorld.physics === World.gWorld.PHYSICS_CONTINUOUS)
					{
						multiplier = passedTime * World.gWorld.physSpeedMultiplier;
					}
				}
				rotation += _rotationSpeed*multiplier;
			}
		}
		
		protected function OnFall():void 
		{
			
		}
		
		public function StopMyMovement(): void 
		{
			_moving = false;
			_stopOnPoint = false;
			_rotationSpeed = 0;
			_velocity.x = 0;
			_velocity.y = 0;
		}
		
		public function StopMyMovementAndAcceleration():void 
		{
			_moving = false;
			_stopOnPoint = false;
			_velocity.x = 0;
			_velocity.y = 0;
			_acceleration.x = 0;
			_acceleration.y = 0;
		}
		
		public function StopMyRotation():void 
		{
			_rotating = false;
			_rotationSpeed = 0;
		}
		
		public function ApplyAbsAcceleration(direction: Vec2, value: Number): void 
		{
			_moving = true;
			direction.x = direction.x - x;
			direction.y = direction.y - y;
			_acceleration = _acceleration.AddVector(direction.MultScalar(value));
		}
		
		public function ApplyRelativeAcceleration(relativeDirection: Vec2, value: Number): void 
		{
			_moving = true;
			_acceleration = _acceleration.AddVector(relativeDirection.MultScalar(value));
		}
		
		public function ApplyAbsVelocity(direction: Vec2, value: Number): void 
		{
			_moving = true;
			direction.x = direction.x - x;
			direction.y = direction.y - y;
			direction.Normalize();
			_velocity = _velocity.AddVector(direction.MultScalar(value));
		}
		
		public function ApplyRelativeVelocity(relativeDirection: Vec2, value: Number): void 
		{
			_moving = true;
			_velocity = _velocity.AddVector(relativeDirection.MultScalar(value));
		}
		
		public function ApplyGravity(value: Number, stopY: Number): void 
		{
			ApplyRelativeAcceleration(new Vec2(0, 1), value);
			_stopOnPoint = true;
			_stopY = stopY;
		}
		
		public function ApplyRotation(speedInRadians: Number):void 
		{
			_rotating = true;
			_rotationSpeed = speedInRadians;
		}
		
		public function Destroy():void 
		{			
			if (_timer)
				stage.removeEventListener(EnterFrameEvent.ENTER_FRAME, OnEnterFrame);
			if (_touchInited)
			{
				removeEventListener(TouchEvent.TOUCH, OnTouch);
				
				//_hoverChecker.removeEventListener("H_Hover", OnHover);
				//_hoverChecker.removeEventListener("H_StopHover", OnStopHover);
				//_hoverChecker.removeEventListener("H_TouchBegan", OnTouchPhaseBegan);
				//_hoverChecker.removeEventListener("H_TouchEnded", OnTouchPhaseEnded);	
				
				//_hoverChecker.HoverCallback = null;
				//_hoverChecker.StopHoverCallback = null;
				//_hoverChecker.TouchBeganCallback = null;
				//_hoverChecker.TouchEndedCallback = null;				
			}
			if (_hoverChecker)
				_hoverChecker.Destroy();
			removeFromParent(true);	
		}
		
	}

}