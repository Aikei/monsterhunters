package d_hentity {
	import flash.utils.Timer;
	import starling.display.Sprite3D;
	import starling.events.EnterFrameEvent;
	import flash.events.TimerEvent;
	/**
	 * ...
	 * @author Aikei
	 */
	public class HEntity3D extends Sprite3D 
	{		
		protected var _moving: Boolean = false;
		protected var _velocity: Vec2 = new Vec2;
		protected var _rotationVelocity: Vec2 = new Vec2;
		protected var _acceleration: Vec2 = new Vec2;
		protected var _stopOnPoint: Boolean = false;
		protected var _stopY: Number = -1;
		protected var _timer: Timer;
		protected var _timerPeriod: Number = 1.0 / 60.0;
		//protected var _previousTime: Number = 0;
		//protected var _timePassed: Number = 0;
		
		public var specialId: int = 0;
		
		public function HEntity3D(initTimer: Boolean = false) 
		{
			super();
			if (initTimer)
			{
				_timer = new Timer(_timerPeriod*1000, 0);
				_timer.addEventListener(TimerEvent.TIMER, OnTimerTick);
				_timer.start();
			}
		}
		
		public function OnTimerTick(event: TimerEvent): void 
		{
			if (_moving)
			{
				//_timePassed = Misc.GetTimeSecs() - _previousTime;
				if (_stopOnPoint && y >= _stopY)
				{
					StopMyMovement();
				}
				_velocity = _velocity.AddVector(_acceleration);
				x += _velocity.x * _timerPeriod;
				y += _velocity.y * _timerPeriod;
				rotationX += _rotationVelocity.x * _timerPeriod;
				rotationY += _rotationVelocity.y * _timerPeriod;
				//_previousTime = Misc.GetTimeSecs();
			}
		}
		
		public function StopMyMovement(): void 
		{
			rotationX = 0;
			rotationY = 0;
			_moving = false;
			_stopOnPoint = false;
		}
		
		public function ApplyAbsAcceleration(direction: Vec2, value: Number): void 
		{
			_moving = true;
			direction.x = direction.x - x;
			direction.y = direction.y - y;
			_acceleration = _acceleration.AddVector(direction.MultScalar(value));
		}
		
		public function ApplyRelativeAcceleration(relativeDirection: Vec2, value: Number): void 
		{
			_moving = true;
			_acceleration = _acceleration.AddVector(relativeDirection.MultScalar(value));
		}
		
		public function ApplyAbsVelocity(direction: Vec2, value: Number): void 
		{
			_moving = true;
			direction.x = direction.x - x;
			direction.y = direction.y - y;
			_velocity = _velocity.AddVector(direction.MultScalar(value));
		}
		
		public function ApplyRelativeVelocity(relativeDirection: Vec2, value: Number): void 
		{
			_moving = true;
			_velocity = _velocity.AddVector(relativeDirection.MultScalar(value));
		}
		
		public function ApplyGravity(value: Number, stopY: Number): void 
		{
			ApplyRelativeAcceleration(new Vec2(0, 1), value);
			_stopOnPoint = true;
			_stopY = stopY;
		}
		
		public function ApplyRotationVelocity(value: Vec2): void 
		{
			_rotationVelocity = value;
		}
		
		public function Destroy():void 
		{
			if (_timer)
			{
				_timer.stop();
				_timer.removeEventListener(TimerEvent.TIMER, OnTimerTick);
			}
			removeFromParent(true);	
		}
		
	}

}