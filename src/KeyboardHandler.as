package  
{
	import starling.display.Sprite;
	import starling.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import starling.core.Starling;
	/**
	 * ...
	 * @author Aikei
	 */
	public class KeyboardHandler extends Sprite
	{
		
		public function KeyboardHandler() 
		{
			addEventListener(KeyboardEvent.KEY_DOWN, OnKeyDown);
		}
		
		protected function OnKeyDown(event: KeyboardEvent):void 
		{
			if (event.keyCode === Keyboard.D)
			{
				Starling.current.showStats = !Starling.current.showStats;
			}
		}
		
	}

}