package  
{
	import com.greensock.*;
	import com.greensock.easing.*;
	import com.greensock.plugins.*;
	import d_hentity.HEntity;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	
	import starling.utils.Color;	
	/**
	 * ...
	 * @author Aikei
	 */
	public class Tweens 
	{
		
		public static function Init(): void
		{
			TweenPlugin.activate([HexColorsPlugin]);
			TweenPlugin.activate([Ease]);
			TweenPlugin.activate([RoughEase]);
			TweenPlugin.activate([BounceInOut]);
			TweenPlugin.activate([Elastic]);
			TweenPlugin.activate([Linear]);
			TweenPlugin.activate([Expo]);
			TweenPlugin.activate([Back]);
			TweenPlugin.activate([HexColorsPlugin]);
			//TweenPlugin.activate([Physics2DPlugin]);
		}
		
		static public function TweenTo(entity: Object, time: Number, toX: Number, toY: Number, onCompleteFunction: Function = null): TweenLite
		{
			return TweenLite.to(entity, time, { x: toX, y: toY, onComplete:  onCompleteFunction } );
		}
		
		static public function TweenToExpoIn(entity: Object, time: Number, toX: Number, toY: Number, onCompleteFunction: Function = null): TweenLite
		{
			return TweenLite.to(entity, time, { x: toX, y: toY, ease: Expo.easeIn, onComplete:  onCompleteFunction } );
		}
		
		static public function TweenToExpoOut(entity: Object, time: Number, toX: Number, toY: Number): TweenLite
		{
			return TweenLite.to(entity, time, { x: toX, y: toY, ease: Expo.easeOut } );
		}
		
		static public function TweenToExpoInOut(entity: Object, time: Number, toX: Number, toY: Number, onCompleteFunction: Function = null): TweenLite
		{
			return TweenLite.to(entity, time, { x: toX, y: toY, ease: Expo.easeInOut, onComplete:  onCompleteFunction } );
		}
		
		
		
		static public function TweenToLinearIn(entity: Object, time: Number, toX: Number, toY: Number, onCompleteFunction: Function = null): TweenLite
		{
			return TweenLite.to(entity, time, { x: toX, y: toY, ease: Linear.easeIn, onComplete:  onCompleteFunction } );
		}
		
		static public function TweenToLinearOut(entity: Object, time: Number, toX: Number, toY: Number): TweenLite
		{
			return TweenLite.to(entity, time, { x: toX, y: toY, ease: Linear.easeOut } );
		}
		
		static public function TweenToLinearInOut(entity: Object, time: Number, toX: Number, toY: Number, onCompleteFunction: Function = null): TweenLite
		{
			return TweenLite.to(entity, time, { x: toX, y: toY, ease: Linear.easeInOut, onComplete:  onCompleteFunction } );
		}			
		
		
		
		static public function TweenToBounceInOut(entity: Object, time: Number, toX: Number, toY: Number): TweenLite
		{
			return TweenLite.to(entity, time, { x: toX, y: toY, ease: Bounce.easeInOut } );
		}
		
		static private function ReturnTween(entity: DisplayObject, saveX: Number, saveY: Number): void
		{
			entity.x = saveX;
			entity.y = saveY;
		}		
		
		static public function TweenShake(entity: DisplayObject, time: Number = 0.4, repeatTimes: Number = -1, dist: Number = 1, howStrong: Number = 2, length: Number = 15): TweenMax
		{
			var saveX = entity.x;
			var saveY = entity.y;
			var ret: TweenMax = TweenMax.fromTo(entity, time, 
					{ repeat: repeatTimes, x: entity.x - dist, onComplete: Tweens.ReturnTween, onCompleteParams: [entity, saveX, saveY] }, 
					{ repeat: repeatTimes, x: entity.x + dist, ease:RoughEase.ease.config( { strength: howStrong, points: length, template: Linear.easeNone, randomize: false } ), 
					onComplete: Tweens.ReturnTween, onCompleteParams: [entity, saveX, saveY] } ); 
			return ret;
		}
		
		static public function StunShake(entity: DisplayObject): TweenMax 
		{
			return Tweens.TweenShake(entity,0.1,3,6,6);
		}
		
	}

}