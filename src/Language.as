package
{
	import flash.events.Event;
	import flash.utils.Dictionary;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import logic.Affliction;
	import logic.DungeonEffect;
	import logic.MonsterData;
	import logic.Quest;
	import logic.Trait;
	import logic.Weapon;
	import logic.HeroData;
	import logic.Damage;
	import logic.Skill;
	import languages.*;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class Language 
	{
		public static const CASE_NOMINATIVE: int = 0;
		public static const CASE_GENETIVE: int = 1;
		public static const CASE_OBJECTIVE_PLURAL: int = 2;
		public static const CASE_PREPOSITIONAL_LOCATIVE: int = 3;
		public static const CASE_OBJECTIVE: int = 4;
		
		private static var _languageData: LanguageData;
		private static var _language: String = "ru";
		private static var _gameLink: String = "";
		
		public static function get languageData(): LanguageData 
		{
			return _languageData;
		}
		
		public static function get decimalDivider(): String
		{
			return GetString("DecimalDivider");
		}
		
		public static function Init(): void
		{
			MConsole.Write("_ii", "Initing language");
			if (World.platform === "vk")
			{
				_gameLink = "http://vk.com/app4982684";
				//if (World.gWorld.flashVars.language == "0") 
				//{
					//_language = "ru";
				//}
				//else if (World.gWorld.flashVars.language == "1")
				//{
					//_language = "ua";
				//}
			}
			if (_language === "ru")
			{
				_languageData = new Russian();
			}
			else if (_language === "en")
			{
				_languageData = new English();
			}
			else if (_language === "ua")
			{
				_languageData = new Ukrainian();
			}				
		}
		
		public static function ParsedTimeObjectToString(obj: Object): String 
		{
			var str: String = Number(obj.hours).toFixed(0) + " " + GetString("HoursAbbreviarion") + " " + Number(obj.minutes).toFixed(0) + " " + GetString("MinutesAbbreviarion") + " " + Number(obj.seconds).toFixed(0) + " " + GetString("SecondsAbbreviarion");
			return str;
		}
		
		//public function OnLoaded(ev: Event): void
		//{
			//var arrayOfLines: Array = ev.target.data.split(/\n/);
			//for (var i: int = 0, l: int = arrayOfLines.length; i < l; i++)
			//{
				//var strings: Array = arrayOfLines[i].split(" === ");
				//_strings[strings[0]] = strings[1];
			//}
		//}
		
		public static function GetString(str: String, nCase: int = CASE_NOMINATIVE): String
		{
			var result: String = _languageData.strings[str+nCase];
			if (result != undefined)
			{
				return result;
			}
			else
			{
				result = _languageData.strings[str];
				if (result != undefined)
					return result
				return str;
			}
		}
		
		public static function HasEntry(str: String):Boolean 
		{
			if (_languageData.strings[str])
				return true;
			return false;			
		}
		
		public static function HasTitle(str: String): Boolean 
		{
			if (_languageData.strings["Title"+str])
				return true;
			return false;
		}
		
		public static function GetGameLink(): String 
		{
			return _gameLink;
		}
		
		public static function ProcessSquadName(name: String): String 
		{
			if (name.slice(0,7) == "Default")
				return Language.GetString("DefaultGroupName")+" "+name.slice(7);
			return name;
		}
		
		public static function ProcessPlayerName(name: String): String //, id: String): String 
		{
			if (name.slice(0,10) == "Web Player")
				return Language.GetString("WebPlayer")+" "+name.slice(10);
			return name
		}
		
		public static function ReverseProcessPlayerName(name: String): String 
		{
			var defName: String = Language.GetString("DefaultGroupName");
			if (name.length > defName.length && name.slice(0, defName.length) == defName)
			{
				name = name.slice(defName.length);
				if (name.charAt(0) == ' ')
				{ 
					if (name.length > 1)
						name = name.slice(1);
					else
						return "Default";
				}
				name = "Default" + name;
			}
			return name;
		}
		
		public static function SetDecimalDivider(numberString: String): String 
		{
			numberString = numberString.replace(".", decimalDivider);
			return numberString;
		}
						
		static public function GetMonsterName(type: int, rarity: int, nCase: int = CASE_NOMINATIVE): String 
		{
			var m: String = _languageData.monsterNames["Monster" + type + "Rarity" + rarity+nCase];
			if (m == null)
			{
				if (nCase != CASE_NOMINATIVE)
				{
					m = _languageData.monsterNames["Monster" + type + "Rarity" + rarity + CASE_NOMINATIVE];
					if (m)
						return m;
				}
				return "No_Name";
			}
			else
				return m;
		}
		
		public static function GetMonsterPhrase(monsterType: int): String 
		{
			if (!_languageData.monsterPhrases[monsterType])
				return null;
			var n: int = Misc.Random(0, _languageData.monsterPhrases[monsterType].length);
			return _languageData.monsterPhrases[monsterType][n];
		}
		
		public static function get loadingPrases(): Vector.<String> 
		{
			return _languageData.loadingPhrases;
		}
		
		public static function GetQuestName(q: Quest): String 
		{
			var name: String = Language.GetString("QuestName" + q.type);
			switch (q.type) 
			{
				case Quest.QUEST_TYPE_KILL:
					if (q.monsterType == -1)
					{
						name = Language.GetString("QuestName" + q.type + Quest.MONSTER_TYPE_ANY);
					}
					else
					{
						name = Language.GetString("QuestName" + q.type + Quest.MONSTER_TYPE_SPECIFIC);
					}												
					break;
			}
			return name;
		}
		
		public static function GetQuestDescription(q: Quest): String 
		{
			var str: String = Language.GetString("QuestDescription" + q.type);;
			switch (q.type) 
			{
				case Quest.QUEST_TYPE_KILL:
					if (q.monsterType == -1)
					{
						str = Language.GetString("QuestDescription" + q.type + Quest.MONSTER_TYPE_ANY);
					}
					else
					{
						str = Language.GetString("QuestDescription" + q.type + Quest.MONSTER_TYPE_SPECIFIC);
						str = str.replace("$monsters", GetMonsterName(q.monsterType, q.monsterRarity, CASE_OBJECTIVE_PLURAL));
						str = str.replace("$dungeon", GetString("Dungeon"+q.dungeonType,CASE_PREPOSITIONAL_LOCATIVE));
					}
					str = str.replace("$number", q.numberOfMonstersToKill);
					break;
				case Quest.QUEST_TYPE_KILL_BOSS:
					str = str.replace("$dungeon", GetString("Dungeon" + q.dungeonType, CASE_PREPOSITIONAL_LOCATIVE));
					str = str.replace("$name", q.uniqueTargetName.GetString(CASE_NOMINATIVE));
					break;
			}
			return str;
		}
		
		public static function GetBossNamePhrase(): BossNamePhrase 
		{
			var headNumber: int = Misc.Random(0, _languageData.bossNamePhraseHeads.length);
			var dependentNumber: int = Misc.Random(0, _languageData.bossNamePhraseDependents.length);
			return new BossNamePhrase(headNumber, dependentNumber);
		}
		
		//public static function GetQuestDescription(quest: Quest): String 
		//{
			//var str: String = "";
			//switch (quest.type) 
			//{
				//case Quest.QUEST_TYPE_KILL:
					//if (quest.monsterType == -1)
					//{
						//str = GetString("QuestKillAny");
					//}
					//else
					//{
						//str = GetString("QuestKillOfType");
						//str = str.replace("$t", GetMonsterName(quest.monsterType, quest.monsterRarity));
						//str = str.replace("$d", GetString("Dungeon"+quest.dungeonType));
					//}
					//str = str.replace("$n", quest.numberOfMonstersToKill);
					//break;
			//}
			//return str;
		//}
		
		public static function GetShortQuestDescription(quest: Quest): String
		{
			var str: String = GetString("ShortQuestDescription" + quest.type);
			switch (quest.type) 
			{
				case Quest.QUEST_TYPE_KILL:
					if (quest.monsterType == -1)
					{
						str = GetString("ShortQuestDescription" + quest.type + Quest.MONSTER_TYPE_ANY);
					}
					else
					{
						str = GetString("ShortQuestDescription" + quest.type + Quest.MONSTER_TYPE_SPECIFIC);
						str = str.replace("$m", GetMonsterName(quest.monsterType, quest.monsterRarity, CASE_OBJECTIVE_PLURAL));
					}
					//str = str.replace("$n", quest.numberOfMonstersToKill);
					break;
				case Quest.QUEST_TYPE_KILL_BOSS:
					str = str.replace("$name", quest.uniqueTargetName.GetString(CASE_OBJECTIVE));
					break;					
			}
			return str;			
		}
		
		//public static function GetBossName(nCase: int = CASE_NOMINATIVE): String 
		//{
			//var name: String = "";
			//var headNumber: int = Misc.Random(0, _languageData.bossNamePhraseHeads.length);			
			//var head: String;			
			//if (nCase == CASE_OBJECTIVE && _languageData.bossNamePhraseHeadsObjective.length > 0 && _languageData.bossNamePhraseHeadsObjective[headNumber])
			//{
				//head = _languageData.bossNamePhraseHeadsObjective[headNumber];
			//}
			//else
			//{
				//head = _languageData.bossNamePhraseHeads[headNumber];	
			//}
			//var dependentNumber: int = Misc.Random(0, _languageData.bossNamePhraseDependents.length);
			//var dependent: String = _languageData.bossNamePhraseDependents[dependentNumber];
			//if (_languageData.bossNamePhraseOrder === LanguageData.HEAD_FIRST)
			//{
				//name = head + " " + dependent;
			//}
			//else
			//{
				//name = dependent + " " + head;			
			//}
			//return name;
		//}
		
		public static function GetOrderAbbreviation(num: int): String 
		{
			return _languageData.orderAbbreviations[num-1];
		}
		
		public static function GetOrderName(num: int): String 
		{
			return _languageData.orderNames[num];
		}
		
		public static function GetOrdersHelp(): String 
		{
			var s: String = "";
			for (var i: int = 0; i < _languageData.orderAbbreviations.length; i++)
			{
				s += _languageData.orderAbbreviations[i] + " - " + _languageData.orderNames[i] + "\n";
			}
			return s;
		}
		
		//public static function GetRandomName(): String 
		//{
			//return _names[Misc.Random(0, _names.length)];
		//}
		
		public static function TransliterateLatin(word: String): String
		{
			if (_language === "en")
			{
				return word;
			}
			else if (_language === "ru")
			{
				var result: String = "";
				for (var i: int = 0; i < word.length; i++)
				{
					var ch: String = word.charAt(i);
					switch (ch)
					{
						case "a":
							ch = "а";
							break;
							
						case "b":
							ch = "б";
							break;
							
						case "c":
							ch = "к";
							break;
						case "d":
							ch = "д";
							break;
							
						case "e":
							ch = "е";
							break;
							
						case "f":
							ch = "ф";
							break;
							
						case "g":
							ch = "г";
							break;
							
						case "h":
							ch = "х";
							break;
							
						case "i":
							ch = "и";
							break;
							
						case "j":
							ch = "дж";
							break;
							
						case "k":
							ch = "к";
							break;
							
						case "l":
							ch = "л";
							break;
							
						case "m":
							ch = "м";
							break;
							
						case "n":
							ch = "н";
							break;
							
						case "o":
							ch = "о";
							break;
							
						case "p":
							ch = "п";
							break;
							
						case "q":
							ch = "к";
							break;
							
						case "r":
							ch = "р";
							break;
							
						case "s":
							ch = "с";
							break;
							
						case "t":
							ch = "т";
							break;
							
						case "u":
							ch = "у";
							break;
							
						case "v":
							ch = "в";
							break;
							
						case "w":
							ch = "у";
							break;
							
						case "x":
							ch = "кс";
							break;
							
						case "y":
							ch = "й";
							break;
							
						case "z":
							ch = "з";
							break;						
					}
					result += ch;
				}
				return result;
			}
			else
			{
				return word;
			}
		}			
	}


}