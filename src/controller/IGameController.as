package controller 
{
	import hevents.*;
	import logic.Affliction;
	import logic.Quest;
	import logic.Skill;
	import starling.events.Event;
	/**
	 * ...
	 * @author Aikei
	 */
	public class IGameController 
	{
		protected var _newDungeonType: int;
		protected var _addCoins: BigInt;
		protected var _addGlory: BigInt;
		
		public function IGameController() 
		{
			World.gWorld.addEventListener("doorsClosed", OnDoorsClosed);
		}
		
		protected function OnDoorsClosed(event: Event): void 
		{
			
		}
		
		public function Init(): void 
		{
			
		}
		
		public function Start(): void 
		{
			
		}		
		
		public function KillMonster(): void 
		{
			
		}
		
		public function RunMonster():void 
		{
			World.gWorld.dispatchEventWith("N_TimeUp");
		}
		
		public function BuyHero(type: int): void 
		{
			
		}
		
		public function UpgradeHero(id: int): void 
		{
			
		}
		
		public function RequestLeaderboard(forced: Boolean):void 
		{
			
		}
		
		public function ChangeSquadName(newName: String):void 
		{
			//World.gWorld.dispatchEventWith("squadNameChanged",false,newName);
		}
		
		public function AddAffliction(a: Affliction, heroType: int): void 
		{
			World.gWorld.dispatchEventWith("addAffliction", false, { affliction: a, heroType: heroType } );
		}
		
		public function RemoveAffliction(afflictionType: int, heroType: int): void 
		{
			World.gWorld.dispatchEventWith("removeAffliction", false, { afflictionType: afflictionType, heroType: heroType } );
		}		
		
		public function AddCoins(n: Object): void 
		{
			if (n is BigInt)
				_addCoins = BigInt(n);
			else if (n is String || n is Number || n is int || n is uint)
				_addCoins = BigInt.shStr2BigInt(String(n));
			World.gWorld.dispatchEventWith("N_CoinsReceived",false,_addCoins);
		}
		
		public function AddGlory(n: Object): void 
		{
			if (n is BigInt)
				_addGlory = BigInt(n);
			else if (n is String || n is Number || n is int || n is uint)
				_addGlory = BigInt.shStr2BigInt(String(n));
			World.gWorld.dispatchEventWith("N_GloryAdded",false,_addGlory);
		}		
		
		public function BuyWeapon(weaponType: int): void 
		{
			
		}
		
		public function UpgradeWeapon(weaponType: int): void 
		{
			
		}
		
		public function ChangeDungeon(newDungeonType: int): void 
		{
			World.gWorld.SetPhase(World.PHASE_IN_TRANSITION);
			World.gWorld.dispatchEventWith("N_StartDungeonTransition",false, { type: 0, dungeonType: newDungeonType } );
			_newDungeonType = newDungeonType;
		}
		
		public function PickSkill(req: BigInt, skill: Skill): void 
		{
			World.gWorld.dispatchEventWith("skillPicked", false, skill);
		}
		
		public function ShareBackgroundProgressReport(gold: String, glory: String, squadName: String):void 
		{
			
		}
		
		public function TutorialDone():void 
		{
			World.SHOW_TUTORIAL = false;
		}
		
		public function AcceptQuest(quest: Quest):void 
		{
			
		}
		
		public function RefuseQuest():void 
		{
			
		}
		
		public function RareDownloaded():void 
		{
			
		}
		
		public function StartGame(): void
		{
			
		}
		
		public function ChangeLevel(newLevel: int):void 
		{
			
		}
		
		public function SendMessage(n: String, s: String):void 
		{
			
		}
		
	}

}