package controller 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import controller.lsevents.*;
	import logic.local.LocalLogic;
	import hevents.*;
	import starling.events.Event;
	
	public class LocalGameController extends IGameController 
	{
		private var _localLogic: LocalLogic;		
		
		public function LocalGameController(): void 
		{
			super();
			_localLogic = new LocalLogic;			
		}
		
		override public function Init():void 
		{
			_localLogic.Init();
		}
		
		override public function Start():void 
		{
			_localLogic.Start();
		}			
		
		override public function KillMonster(): void 
		{
			_localLogic.KillMonster();
		}
		
		override public function BuyHero(type: int): void 
		{
			_localLogic.BuyHero(type);
		}
		
		override public function UpgradeHero(id: int):void 
		{
			_localLogic.UpgradeHero(id);
		}
		
		override public function AddCoins(n: Object): void 
		{
			super.AddCoins(n);
			_localLogic.AddCoins(_addCoins);			
		}
		
		override public function BuyWeapon(weaponType: int): void 
		{
			super.BuyWeapon(weaponType);
			_localLogic.BuyWeapon(weaponType);
		}
		
		override public function UpgradeWeapon(weaponType: int): void 
		{
			super.UpgradeWeapon(weaponType);
			_localLogic.UpgradeWeapon(weaponType);
		}
		
		override public function ChangeDungeon(newDungeonType: int): void 
		{
			super.ChangeDungeon(newDungeonType);
		}
		
		override protected function OnDoorsClosed(event: Event):void 
		{
			super.OnDoorsClosed(event);
			_localLogic.ChangeDungeon(_newDungeonType);
		}		

	}

}