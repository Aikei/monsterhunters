package controller.lsevents {
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class LS_HEventMonsterClickHit extends Event 
	{
		public static const LS_H_EVENT_MONSTER_CLICK_HIT: String = "MonsterClickHit";
		
		public function LS_HEventMonsterClickHit(bubbles:Boolean=false) 
		{
			super(LS_H_EVENT_MONSTER_CLICK_HIT, bubbles);			
		}
		
	}

}