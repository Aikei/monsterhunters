package controller.remote.messages 
{
	/**
	 * ...
	 * @author Aikei
	 */

	public class MessageNames
	{
		static public const kTime: String = "a";
		static public const kCreatePlayer: String = "b";
		static public const kReady: String = "c";
		static public const kCheater: String = "cheat";
		static public const kServerInfo: String = "si";
		static public const kListPlayers: String = "lp";
		static public const kServerMessage: String = "k";
		static public const kNewDay: String = "n";
		static public const kPreferences: String = "prefs";
		static public const kFirstConnect: String = "x";
		static public const kConfirmConnect: String = "v";		
		
		static public const kGameMessage: String = "g";
		
			static public const ksTutorialDone: String = "aa";
			static public const ksChangeLevel: String = "a";
			static public const ksQuestAccepted: String = "b";
			static public const ksQuestRefused: String = "c";
			static public const ksMonsterKilled: String = "d";
			static public const ksNewMonster: String = "e";
			static public const ksCoinsReceived: String = "f";
			static public const ksWeaponUpgraded: String = "g";
			static public const ksWeaponBought: String = "h";
			static public const ksNextLevel: String = "i";
			static public const ksChangeDungeon: String = "j";
			static public const ksStartGame: String = "k";
			static public const ksBuyHero: String = "l";
			static public const ksUpgradeHero: String = "m";
			static public const ksSkillPicked: String = "n";
			static public const ksMonsterRan: String = "o";
			static public const ksAfflictionAdded: String = "p";
			static public const ksAfflictionRemoved: String = "q";
			static public const ksNewSkillPack: String = "r";
			static public const ksNextSkillPackAvailable: String = "s";
			static public const ksRequestLeaderboard: String = "t";
			static public const ksSquadNameChanged: String = "u";
			
			static public const ksRareLoaded: String = "vv";
			static public const ksGloryReceived: String = "w";
			static public const ksQuestProgress: String = "x";
			static public const ksRenameResponse: String = "y";
			static public const ksBgReportShared: String = "z";
	}
		
}