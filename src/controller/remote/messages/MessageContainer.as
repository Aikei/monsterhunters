package controller.remote.messages 
{
	public class MessageContainer
	{		
		/** Message name */
		public var n: String;
 
		/** Message string */
		public var s: String;
		
		public var h: String;
		public var z: Boolean = false;
		/** Message data - to be filled in by client */
		public var m_data:*;		
	}

}