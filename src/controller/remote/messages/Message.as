package controller.remote.messages 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import flash.net.*;
	import flash.utils.*;
	 
	import controller.remote.Helpers;
	import controller.remote.Connector;

	
	public class Message
	{ 
		/**
		 * Deserialise a message from a string
		 * 
		 * @param messageString
		 * @return MessageContainer
		 *
		 */			
		static public function Get(messageString:String): MessageContainer
		{
			var containerA: Object = Connector.DeSerialize(messageString);
			
			//if (containerA.s != undefined)
			//{
				//containerA.m_data = Multiplayer.DeSerialize(containerA.s);
			//}
 
			// clone into concrete type
			var container: MessageContainer = Helpers.CloneIntoR(containerA, MessageContainer);
			if (container.z)
				container.m_data = Misc.UnpackDeflated(container.m_data);
			return container;
		}
		
		static public function CreateDataMessage(n: String, s: String, data:*): MessageContainer
		{
			var messageContainer: MessageContainer = new MessageContainer;
			messageContainer.n = n;
			messageContainer.s = s;
			messageContainer.m_data = data;
			
			messageContainer.h = Misc.GetCheckHash(messageContainer.n+messageContainer.s+JSON.stringify(messageContainer.m_data));			
			
			return messageContainer;
		}
		 
	}

}