package controller.remote {
	/**
	 * ...
	 * @author Aikei
	 */
	import controller.IGameController;
	import flash.net.XMLSocket;
	import controller.remote.messages.*;
	import logic.Quest;
	import starling.events.Event;
	import logic.Skill;
	import logic.Affliction;
	
	public class RemoteGameController extends IGameController 
	{	
		protected var _connector: Connector;
		protected var _lastLeaderboardRequestTime: Number = 0;
		
		public function RemoteGameController() 
		{
			super();
			_connector = new Connector();
		}
		
		override public function Init(): void 
		{
			_connector.MaybeStart();
		}		
		
		override public function AddAffliction(a: Affliction, heroType: int): void 
		{
			super.AddAffliction(a, heroType);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksAfflictionAdded,  { afflictionType: a.type, heroType: heroType } );
			_connector.SerializeAndSend(m);			
		}
		
		override public function RemoveAffliction(afflictionType: int, heroType: int): void 
		{
			super.RemoveAffliction(afflictionType, heroType);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksAfflictionAdded,  { afflictionType: afflictionType, heroType: heroType } );
			_connector.SerializeAndSend(m);			
		}		
		
		override public function KillMonster(): void 
		{
			super.KillMonster();
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksMonsterKilled, null);
			_connector.SerializeAndSend(m);
		}
		
		override public function RunMonster():void 
		{
			super.RunMonster();
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksMonsterRan, null);
			_connector.SerializeAndSend(m);			
		}
		
		override public function BuyHero(type: int): void 
		{
			super.BuyHero(type);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksBuyHero, type);
			_connector.SerializeAndSend(m);
		}
		
		override public function UpgradeHero(id: int):void 
		{
			super.UpgradeHero(id);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksUpgradeHero, id);
			_connector.SerializeAndSend(m);			
		}
		
		override public function AddCoins(n: Object): void 
		{
			super.AddCoins(n);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksCoinsReceived, BigInt.bigInt2str(_addCoins,10));
			_connector.SerializeAndSend(m);						
		}
		
		override public function AddGlory(n: Object): void 
		{
			super.AddGlory(n);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksGloryReceived, BigInt.bigInt2str(_addGlory,10));
			_connector.SerializeAndSend(m);						
		}		
		
		override public function BuyWeapon(weaponType: int): void 
		{
			super.BuyWeapon(weaponType);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksWeaponBought, weaponType);
			_connector.SerializeAndSend(m);
		}
		
		override public function UpgradeWeapon(weaponType: int): void 
		{
			super.UpgradeWeapon(weaponType);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksWeaponUpgraded, weaponType);
			_connector.SerializeAndSend(m);		
		}
		
		override public function RequestLeaderboard(forced: Boolean):void 
		{
			var timeNow: Number = Misc.GetTimeSecs();
			var timeDiff: Number = timeNow - _lastLeaderboardRequestTime;
			if (forced || _lastLeaderboardRequestTime == 0 || timeDiff > 60)
			{
				_lastLeaderboardRequestTime = timeNow;
				var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksRequestLeaderboard, null);
				_connector.SerializeAndSend(m);
			}
		}
		
		override public function ChangeSquadName(newName: String):void 
		{
			newName = Language.ReverseProcessPlayerName(newName);
			super.ChangeSquadName(newName);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksSquadNameChanged, newName);
			_connector.SerializeAndSend(m);
		}		
		
		//
		//override public function ChangeDungeon(newDungeonType: int): void 
		//{
			//super.ChangeDungeon(newDungeonType);
		//}
		//
		
		override public function PickSkill(req: BigInt, skill: Skill): void 
		{
			super.PickSkill(req,skill);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksSkillPicked, { req: BigInt.bigInt2str(req,10), skillType: skill.type, subType: skill.subtype, packNum: skill.packNum } );
			_connector.SerializeAndSend(m);				
		}		
		
		override protected function OnDoorsClosed(event: Event):void 
		{
			super.OnDoorsClosed(event);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksChangeDungeon, _newDungeonType);
			_connector.SerializeAndSend(m);				
		}
		
		override public function ShareBackgroundProgressReport(gold: String, glory: String, squadName: String):void 
		{
			_connector.ShareBackgroundProgressReport(gold, glory, squadName);
		}
		
		override public function TutorialDone():void 
		{
			super.TutorialDone()
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksTutorialDone, null);
			_connector.SerializeAndSend(m);	
		}
		
		override public function AcceptQuest(quest: Quest):void 
		{
			var data: Object = null;
			if (quest.type === Quest.QUEST_TYPE_KILL_BOSS)
			{
				data = new Object;
				data.h = quest.uniqueTargetName.headNumber;
				data.d = quest.uniqueTargetName.dependentNumber;
			}
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksQuestAccepted, data);
			_connector.SerializeAndSend(m);				
		}
		
		override public function RefuseQuest():void 
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksQuestRefused, null);
			_connector.SerializeAndSend(m);					
		}
		
		override public function RareDownloaded():void 
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksRareLoaded, null);
			_connector.SerializeAndSend(m);					
		}
		
		override public function StartGame(): void
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksStartGame, null);
			_connector.SerializeAndSend(m);				
		}
		
		override public function ChangeLevel(newLevel: int):void 
		{
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksChangeLevel, { l: newLevel });
			_connector.SerializeAndSend(m);					
		}
		
		override public function SendMessage(n: String, s: String):void 
		{
			_connector.SendMessage(n, s);
		}		
		
	}

}