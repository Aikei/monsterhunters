package controller.remote 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import ads.Ads;
	import flash.net.*;
	import flash.utils.*;
	import flash.events.DataEvent;
	import flash.system.Security;
	import controller.remote.messages.*;
	import flash.errors.*;
	import flash.events.*;
	import hevents.*;
	import logic.HeroData;
	import logic.MonsterData;
	import logic.Weapon;
	import system.Analytics;
	import vk.APIConnection;
	
	public class Connector 
	{
		public static const kMessageTerminator: String = "\n";
		
		private var xmlSocket: XMLSocket;
		private var m_latency: Number = Number.MAX_VALUE;
		//private var m_timeCompensation: Number = 0;
		private var _servers: Vector.<String> = new Vector.<String>;
		//private var _serverPort: int = 8500;

		private var _serverPort: int;
		private var _httpPort: int = 11500;
		private var _dataReceived: String = "";
		private var _timeLostConnection: Number = 0;
		private var _serverNumber: int = 0;
		private var _connected: Boolean = false;
		private var vkApiVersion: String = "5.28";
		private var _loader: URLLoader;
		private var _disconnectionTime: Number = -1;
		private var _everConnected: Boolean = false;
		private var _preparationReady: Boolean = false;
		private var _errorStart: Number = -1;
		private var _timeInSync: Boolean = false;
		private var _lastTimeSync: Number = 0;
		
		static public var VK: APIConnection = null;
		
		static public function Serialize(obj: Object): String
		{
			var s: String = JSON.stringify(obj)+kMessageTerminator;
			return s;
		}
		
		static public function DeSerialize(msg: String): Object
		{
			return JSON.parse(msg);
		}
		
		public function SetServer(n: int): void
		{
			if (n < _servers.length)
			{
				if (xmlSocket.connected)
					xmlSocket.close();		
			}			
		}
		
		public function Connector() 
		{		
			//if (World.gWorld.flashVars.viewer_id !== undefined)
			if (Preferences.USE_TEST_PORT)
				_serverPort = Preferences.TEST_PORT;
			else
				_serverPort = Preferences.PRODUCTION_PORT;
			if (World.platform == "vk")
			{
				GetVKInfo();
			}
			else
			{
				MConsole.Write("_ii", "viewer id undefined");
				MaybeStart();
				//Start();
			}
		}
		
		public function SendMessage(n: String, s: String):void 
		{
			var m: MessageContainer = Message.CreateDataMessage(n, s, null);
			SerializeAndSend(m);
		}
		
		public function MaybeStart():void 
		{
			if (_preparationReady)
				Start();
			else
				_preparationReady = true;
		}
		
		private function Start(): void
		{
			_timeLostConnection = m_LocalTimeSeconds;
			if (Preferences.TRY_LOCAL_HOST)
				_servers.push("localhost");
			if (Preferences.TRY_REMOTE_HOST)
				_servers.push(Preferences.REMOTE_HOST);
			
			Security.loadPolicyFile("http://" + _servers[_serverNumber] + ":"+String(_httpPort)+"/crossdomain.xml");
			MConsole.Write("creating xml socket...");
			xmlSocket = new XMLSocket();
			MConsole.Write("adding listeners...");
			xmlSocket.addEventListener(DataEvent.DATA, OnIncomingData);			
			xmlSocket.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			xmlSocket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			xmlSocket.addEventListener(Event.CONNECT, OnConnect);
			SyncClocks();			
		}
		
		public function OnConnect(event:Event): void
		{
			MConsole.Write("_ii", "connected to server!");
			//if (MyWorld.gWorld.GetPhase() == MyWorld.PHASE_CONNECTING_TO_SERVER)
			//{							
				//MyWorld.gWorld.SetPhase(MyWorld.PHASE_IN_LOBBY);
			//}
		}
		
		public function ioErrorHandler(ev:IOErrorEvent): void
		{
			MConsole.Write("e","async ioerror!", ev.text);
		}
		
		public function securityErrorHandler(ev:SecurityErrorEvent): void
		{
			MConsole.Write("e","async security error!", ev.text);
		}		
		
		public function Connect(): void
		{
			if (!xmlSocket.connected)
			{
				if (m_LocalTimeSeconds - _timeLostConnection >= 5)
				{
					_serverNumber++;
					if (_serverNumber >= _servers.length)
						_serverNumber = 0;
					_timeLostConnection = m_LocalTimeSeconds;
				}
				MConsole.Write("_ii","xmlSocket not connected");
				MConsole.Write("_ii","connecting to", _servers[_serverNumber],":", _serverPort);
				try 
				{				
					xmlSocket.connect(_servers[_serverNumber], _serverPort);
				}
				catch (ioError:IOError) 
				{
					MConsole.Write("e","ioerror!", ioError.message);
				} 
				catch (secError:SecurityError) 
				{
					MConsole.Write("e","securityError!", secError.message);
				}
			}
		}
		
		public function SerializeAndSend(obj: Object): void
		{
			//MConsole.Write("_ii", "sending message { s:",obj.s," n: ",obj.n," }");
			if (xmlSocket.connected)
				xmlSocket.send(Serialize(obj));
		}
		
		/**
		 * Send clock synchronisation message
		 */
		private function SyncClocks(): void
		{
			Connect();
			if (xmlSocket.connected)
			{
				_everConnected = true;
				_disconnectionTime = -1;
				if (!_timeInSync && (!_connected || m_LocalTimeSeconds - _lastTimeSync > 3))
				{
					_timeInSync = true;
					var m: MessageContainer = Message.CreateDataMessage(MessageNames.kTime, null, { m_clientTime: m_LocalTimeSeconds } );
					var s: String = Serialize(m);
					xmlSocket.send(s);
				}
				//if (!_connected)
				//{
					//_connected = true;
					//var mm: MessageContainer = Message.CreateDataMessage(MessageNames.kFirstConnect, null, World.gWorld.socialProfile);
					//SerializeAndSend(mm);
				//}
				//else
				//{
					//var m: MessageContainer = Message.CreateDataMessage(MessageNames.kTime, null, { m_clientTime: m_LocalTimeSeconds } );
					//var s: String = Serialize(m);
					//xmlSocket.send(s);
				//}
				
			}
			else
			{
				if (_disconnectionTime === -1)
					_disconnectionTime = Misc.GetServerTime();
				_connected = false;
				if (Misc.GetServerTime() - _disconnectionTime > 30)
				{
					if (_everConnected)
						World.gWorld.dispatchEventWith("N_StartDungeonTransition", false, { type: -1, message: Language.GetString("LostConnection") } );
					else
						World.gWorld.dispatchEventWith("N_StartDungeonTransition", false, { type: -1, message: Language.GetString("ServerUnavailable") } );
				}
			}
			// check again in 5 seconds
			setTimeout(SyncClocks, 5*1000);
		}
		 
		/**
		 * Get the time on this local machine, in seconds
		 */
		static public function get m_LocalTimeSeconds(): Number
		{
			return new Date().getTime()/1000.0;
		}		
		
		/**
		 * Syncronise clocks
		 * 
		 * @param message
		 *
		 */
		public function ProcessTimeMessage(message:MessageContainer): void
		{
			_timeInSync = false;
			var now: Number = m_LocalTimeSeconds;
			_lastTimeSync = now;
			var clientTime: Number = message.m_data.m_clientTime;
			var serverTime: Number = message.m_data.m_serverTime;
		 
			// round trip time in seconds
			var roundTripSeconds: Number = now-clientTime;
		 
			// latency is how long message took to get to server
			var latency: Number = roundTripSeconds/2;
		 
			// difference between server time and client time
			var serverDeltaSeconds: Number = serverTime-now;
		 
			// store averages
			if (m_latency != Number.MAX_VALUE)
			{
				m_latency = (m_latency+latency)*0.5;
			}
			else 
			{
				m_latency = latency;
			}
		 
			// this is the current compenstation
			var totalDeltaSeconds: Number = serverDeltaSeconds+m_latency;
			Misc.timeCompensation = totalDeltaSeconds;
			//MConsole.Write("latency: ", Misc.ConvertToFixedPoint(m_latency,1000));
			//MConsole.Write("time compensation: ", Misc.ConvertToFixedPoint(m_timeCompensation,1000));
			if (!_connected)
			{
				_connected = true;
				var m: MessageContainer = Message.CreateDataMessage(MessageNames.kFirstConnect, null, World.gWorld.socialProfile);
				SerializeAndSend(m);				
			}
		}				
		 
		private function OnIncomingData(event:DataEvent):void
		{
			//trace("[" + event.type + "] " + event.data);
			var i: int;
			_dataReceived += event.data;
			var regExp: RegExp = /\0/g;
			_dataReceived = _dataReceived.replace(regExp, "");
			var strings: Array = _dataReceived.split("\n");
			var lastMsg: int = strings.length - 1;
			if (String(strings[lastMsg]).length != 0)
			{
				_dataReceived += String(strings[lastMsg]);
			}
			else
			{
				_dataReceived = "";
			}
			
			for (i = 0; i < lastMsg; i++)
			{
				var m: MessageContainer = Message.Get(String(strings[i]));
				//MConsole.Write("_ii", "received message: { s: ", m.s, ", n: ", m.n, " } ");
				if (m.n === MessageNames.kTime)
				{
					ProcessTimeMessage(m);
				}
				else if (m.n === MessageNames.kConfirmConnect)
				{
					World.gWorld.dispatchEventWith("N_DataReceived",false,m.m_data);
				}
				else if (m.n === MessageNames.kNewDay)
				{
					World.gWorld.dispatchEventWith("newDay", false, m.m_data);
				}
				else if (m.n === MessageNames.kGameMessage)
				{
					if (m.s === MessageNames.ksNewMonster)
					{
						var monsterData: MonsterData = MonsterData.CreateFromDataObject(m.m_data.m);
						World.gWorld.dispatchEventWith("N_NewMonster", false, { monsterData: monsterData, monstersKilled: m.m_data.kl } );
						World.gWorld.dispatchEventWith("N_DungeonsData", false, m.m_data.dd);
					}
					else if (m.s === MessageNames.ksWeaponUpgraded)
					{
						var weapon: Weapon = Weapon.CreateFromDataObject(m.m_data);
						World.gWorld.dispatchEvent(new HEventWeaponUpgraded(weapon));
					}
					else if (m.s === MessageNames.ksNextLevel)
					{
						monsterData = MonsterData.CreateFromDataObject(m.m_data.m);
						World.gWorld.dispatchEventWith("N_DungeonsData", false, m.m_data.dd);
						var goldLimit: BigInt = BigInt.shStr2BigInt(m.m_data.gl);
						World.gWorld.dispatchEvent(new HEventNextLevel(monsterData, m.m_data.l, goldLimit));
						
					}
					else if (m.s === MessageNames.ksChangeDungeon)
					{
						Analytics.TrackEvent("Dungeon Change", "Dungeon Change");
						monsterData = MonsterData.CreateFromDataObject(m.m_data.m);
						World.gWorld.dispatchEventWith("N_DungeonsData", false, m.m_data.dd);
						goldLimit = BigInt.shStr2BigInt(m.m_data.gl);
						World.gWorld.dispatchEvent(new HEventDungeonChanged(m.m_data.cd, monsterData, goldLimit));
					}
					else if (m.s === MessageNames.ksBuyHero)
					{
						World.gWorld.dispatchEvent(new HEventHeroBought(m.m_data.type, m.m_data.name, m.m_data.id));
					}
					else if (m.s === MessageNames.ksUpgradeHero)
					{
						var heroData: HeroData = HeroData.CreateFromDataObject(m.m_data.hd);
						World.gWorld.dispatchEvent(new HEventHeroUpgraded(m.m_data.ah, heroData));
					}
					else if (m.s === MessageNames.ksWeaponBought)
					{
						World.gWorld.dispatchEvent(new HEventNewWeaponBought(m.m_data));
					}
					//else if (m.s === MessageNames.ksNextSkillPackAvailable)
					//{
						//World.gWorld.dispatchEventWith("nextSkillPackAvailable", false, m.m_data);
					//}
					else if (m.s === MessageNames.ksRequestLeaderboard)
					{
						m.m_data.l = Misc.UnpackDeflated(m.m_data.l);
						m.m_data.dl = Misc.UnpackDeflated(m.m_data.dl);
						World.gWorld.dispatchEventWith("leaderboardArrived", false, m.m_data);
					}
					else if (m.s === MessageNames.ksRenameResponse)
					{
						World.gWorld.dispatchEventWith("renameResponse", false, m.m_data);
					}
					else if (m.s === MessageNames.ksQuestProgress)
					{
						World.gWorld.dispatchEventWith("questProgress", false, m.m_data);
					}
					else if (m.s === MessageNames.ksNewSkillPack)
					{
						World.gWorld.dispatchEventWith("newSkillPackReceived", false, m.m_data);
					}
					else if (m.s === MessageNames.ksChangeLevel)
					{
						monsterData = MonsterData.CreateFromDataObject(m.m_data.m);
						World.gWorld.dispatchEventWith("N_DungeonsData", false, m.m_data.dd);
						goldLimit = BigInt.shStr2BigInt(m.m_data.gl);
						var monstersKilled: int = m.m_data.mk;
						World.gWorld.dispatchEventWith("changeLevel", false, { monsterData: monsterData, monstersKilled: monstersKilled, level: m.m_data.l, goldLimit: goldLimit, lst: m.m_data.lst } );
					}
				}
				else if (m.n === MessageNames.kCheater)
				{
					var str: String = "Got Cheater message";
					if (m.m_data)
						str += ": " + m.m_data;
					Analytics.TrackEvent("Cheater", str);
					MConsole.Write("_ii", str);
				}
			}
		}
		
		static public function SetVkWindowSize(w: Number, h: Number): void 
		{
			if (VK != null)
				VK.callMethod("resizeWindow", w, h);
		}				
		
		private function GetVKInfo(): void
		{			
			//var url:String = "https://api.vk.com/method/users.get?user_id=3373076&v="+vkApiVersion;
			VK = new APIConnection(World.gWorld.flashVars);
			SetVkWindowSize(Preferences.SCREEN_WIDTH+Ads.RIGHT_BLOCK_WIDTH, Preferences.SCREEN_HEIGHT+Ads.BOTTOM_BLOCK_HEIGHT);
			var url:String = "https://api.vk.com/method/users.get?user_id=" + String(World.gWorld.flashVars.viewer_id)+"&v="+vkApiVersion+"&fields=sex";
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.GET;

			var variables:URLVariables = new URLVariables();

			_loader = new URLLoader();
			_loader.addEventListener(Event.COMPLETE, OnGetVkInfoComplete);
			_loader.dataFormat = URLLoaderDataFormat.TEXT;
			_loader.load(request);			
		}
		
		private function OnGetVkInfoComplete(event: Event): void
		{
			_loader.removeEventListener(Event.COMPLETE, OnGetVkInfoComplete);
			World.vkWindowIncreased = true;
			var obj: Object = JSON.parse(event.target.data);
			var name: String = obj.response[0].first_name;
			var lastName: String = obj.response[0].last_name;
			MConsole.Write("_ii", "Your name: ", name + " " + lastName);
			World.gWorld.socialProfile.InitProfile("vk", name, lastName, World.gWorld.flashVars.viewer_id);
			//MConsole.Write("_ii", "vk sex: ", obj.sex);
			if (obj.response[0].sex == 1)
				World.gWorld.socialProfile.sex = "f";
			//MConsole.Write("_ii", "social sex: ", World.gWorld.socialProfile.sex );
			if (_preparationReady)
				Start();
			else
				_preparationReady = true;
		}
		
		public function ShareBackgroundProgressReport(gold: String, glory: String, squadName: String):void 
		{
			if (World.platform === "web")
			{
				OnBackgroundShareFail();
				return;
			}
			Analytics.TrackEvent("Background Report", "Share");
			var str: String = Language.GetString("ReportShareMessage");
			str = str.replace("$gold", gold);
			//str = str.replace("$glory", glory);
			str = str.replace("$sq", squadName);
			if (World.platform === "vk")
			{
				VK.api("wall.post",
				{
					message: str,
					attachments: "photo-94460462_377519155," + Language.GetGameLink()
					//test_mode: 1
				},
					OnVkBackgroundShareComplete,
					OnVkBackgroundShareFail
				);
			}
		}
		
		protected function OnVkBackgroundShareComplete(result: Object):void 
		{
			OnBackgroundShareComplete();
		}
		
		protected function OnVkBackgroundShareFail(result: Object):void 
		{
			Analytics.TrackEvent("Background Report", "Share Failed");
			//switch (result) 
			//{
				//case 214:
					//MConsole.CmdWrite("214: Превышен лимит на число публикаций в сутки.");
					//break;
				//case 219:
					//MConsole.CmdWrite("219: Рекламный пост уже недавно публиковался.");
					//break;
				//case 220:
					//MConsole.CmdWrite("220: Слишком много получателей.");
					//break;
				//default:
					//MConsole.CmdWrite("Ошибка номер " + result);
					//break;
			//}
			OnBackgroundShareFail();
		}			
		
		protected function OnBackgroundShareComplete():void 
		{
			var shareTime: Number = Misc.GetServerTime();
			World.gWorld.dispatchEventWith("backgroundShareComplete",false,shareTime);
			var m: MessageContainer = Message.CreateDataMessage(MessageNames.kGameMessage, MessageNames.ksBgReportShared, shareTime);
			SerializeAndSend(m);				
		}
		
		protected function OnBackgroundShareFail():void 
		{
			World.gWorld.dispatchEventWith("backgroundShareFail");
		}		
	}
}