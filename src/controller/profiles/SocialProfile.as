package controller.profiles 
{
	import flash.net.SharedObject;
	import system.MurmurHash2;
	/**
	 * ...
	 * @author Aikei
	 */
	public class SocialProfile 
	{
		public var type: String = "web";
		public var name: String = "w";
		public var lastName: String = "w";
		public var squadId: uint = 0;
		public var id: uint = 0;
		public var sex: String = "m";
		public var deviceId: String;
		public var h: String;
		//private var _id: uint = 0;
		//
		//public function get id(): uint
		//{
			//return _id;
		//}
		
		//public function set id(newId: uint): void 
		//{
			//_id = newId;
			//if (Assets.idSharedObject == null)
			//{
				//var so: SharedObject = SharedObject.getLocal("MonsterHuntersId");
				//so.data.id = newId;
				//so.flush();
				//so.close();
				//so = null;
			//}
			//else
			//{
				//Assets.idSharedObject.data.id = newId;
				//Assets.idSharedObject.flush();
			//}
		//}
		
		public function SocialProfile() 
		{
			var l: uint = 4 + Misc.Random(0, 6);
			deviceId = "";
			for (var i: uint = 0; i < l; i++)
			{
				var n: Number = Misc.Random(0x30, 0x7e);
				deviceId += String.fromCharCode(n);
			}
			h = MurmurHash2.Hash(deviceId, deviceId.charCodeAt(2));
		}
		
		public function InitProfile(type: String, name: String, lastName: String, id: uint): void
		{
			this.type = type;
			this.name = name;
			this.lastName = lastName;
			this.id = id;
		}
		
		public function InitProfileFromFullProfileData(data:*): void
		{
			this.type = data.socialProfile.type;
			this.name = data.socialProfile.name;
			this.lastName = data.socialProfile.lastName;
			this.id = data.socialProfile.numId;
			this.squadId = data.sqid;
			
			Assets.idSharedObject.data.profile = this;
			Assets.idSharedObject.flush();
		}
		
		public function InitProfileFromSharedData(data: Object): void
		{
			this.type = data.type;
			this.name = data.name;
			this.lastName = data.lastName;
			this.squadId = data.squadId;
			this.id = data.id;
			this.deviceId = data.deviceId;
			this.h = data.h;
		}		
		
		public function GetName(): String 
		{
			return Language.ProcessPlayerName(this.name+' '+this.lastName);
		}
				
		//public function GetGroupName(): String 
		//{
			//return Language.ProcessPlayerName(this.name+' '+this.lastName);
		//}
		
		public function Reset():void 
		{
			this.id = 0;
			
			Assets.idSharedObject.data.profile = this;
			Assets.idSharedObject.flush();			
		}
		
	}

}