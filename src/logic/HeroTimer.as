package logic 
{
	import d_hentity.HEntity;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import view.GameView;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HeroTimer extends HEntity 
	{
		//public var heroId: int;
		//public var firstRun: Boolean = false;
		//public var startTimeoutId: uint = 0;
		private var _timer: Timer;
		private var _timerDivider: Number = 10;
		
		private var _attackStep: uint = ClientLogic.HEROES_ATTACK_COOLDOWN * _timerDivider;
		private var _allAttackSteps: Vector.<uint> = new Vector.<uint>;
		private var _allAttackStepsRem: Vector.<uint> = new Vector.<uint>;
		
		private var _animStepDif: uint = GameView.HERO_PROJECTILE_TIME * _timerDivider;
		private var _allAnimSteps: Vector.<uint> = new Vector.<uint>;
		private var _allAnimStepsRem: Vector.<uint> = new Vector.<uint>;
		
		private var _startIntervalStep: uint = ClientLogic.HEROES_ATTACK_START_INTERVAL * _timerDivider;
		private var _stepsStartIntervalMax: uint = _startIntervalStep * HeroData.NUMBER_OF_HEROES;
		private var _heroIdStart: uint = 0;		
		
		private var _steps: uint = 0;		
		private var _stepsMax: uint;
		
		public function HeroTimer() 
		{
			_stepsMax = _stepsStartIntervalMax;
			CountSteps();
			_timer = new Timer(1000 / _timerDivider);
			_timer.addEventListener(TimerEvent.TIMER, OnTimerTick);
		}
		
		public function RecalculateData():void 
		{
			_attackStep = ClientLogic.HEROES_ATTACK_COOLDOWN * _timerDivider;
			_animStepDif = GameView.HERO_PROJECTILE_TIME * _timerDivider;
			_startIntervalStep = ClientLogic.HEROES_ATTACK_START_INTERVAL * _timerDivider;
			_stepsStartIntervalMax = _startIntervalStep * HeroData.NUMBER_OF_HEROES;
			_stepsMax = _stepsStartIntervalMax;
			CountSteps();
		}
		
		private function CountSteps():void 
		{
			_allAttackSteps = new Vector.<uint>;
			_allAttackStepsRem = new Vector.<uint>;
			_allAnimSteps = new Vector.<uint>;
			_allAnimStepsRem = new Vector.<uint>;
			for (var i: int = 0; i < HeroData.NUMBER_OF_HEROES; i++)
			{
				_allAttackSteps.push(_startIntervalStep * i + _attackStep);
				_allAttackStepsRem.push(_allAttackSteps[i]);
				_allAnimSteps.push(_allAttackSteps[i] - _animStepDif);
				_allAnimStepsRem.push(_allAnimSteps[i]);
			}
		}
		
		public function StopTimer():void 
		{
			_timer.reset();
			_steps = 0;
			for (var i: int = 0; i < _allAttackStepsRem.length; i++)
			{
				_allAttackStepsRem[i] = _allAttackSteps[i];
			}
			
			for (i = 0; i < _allAnimStepsRem.length; i++)
			{						
				_allAnimStepsRem[i] = _allAnimSteps[i];					
			}				
		}
		
		public function StartTimer():void 
		{
			_timer.start();
		}
		
		private function OnTimerTick(e:TimerEvent):void 
		{
			Step();
		}
		
		private function Step():void 
		{
			_steps++;
			
			for (var i: int = 0; i < HeroData.NUMBER_OF_HEROES; i++)
			{
				if (_steps == _allAttackStepsRem[i])
				{
					_allAttackStepsRem[i] += _attackStep;
					dispatchEventWith("damageTime", false, i);
					//_nextAttackIndex++;
					//if (_nextAttackIndex >= HeroData.NUMBER_OF_HEROES)
						//_nextAttackIndex = 0;
				}	
				
				if (_steps == _allAnimStepsRem[i])
				{
					_allAnimStepsRem[i] += _attackStep;
					dispatchEventWith("animTime", false, i);
					//_nextAnimIndex++;
					//if (_nextAnimIndex >= HeroData.NUMBER_OF_HEROES)
						//_nextAnimIndex = 0;				
				}
			}
			
			if (_steps >= _stepsMax)
			{
				_steps = 0;
								
				for (i = 0; i < _allAttackStepsRem.length; i++)
				{
					_allAttackStepsRem[i] -= _stepsMax;
					if (_allAttackStepsRem[i] <= 0)
					{
						_allAttackStepsRem[i] = _allAttackSteps[i];
					}
				}
				
				for (i = 0; i < _allAnimStepsRem.length; i++)
				{						
					_allAnimStepsRem[i] -= _stepsMax;
					if (_allAnimStepsRem[i] <= 0)
					{
						_allAnimStepsRem[i] = _allAnimSteps[i];
					}
				}				
			}
		}
		
		//override public function start(): void
		//{
			//startTimeoutId = 0;
			//super.start();
		//}
		
	}

}