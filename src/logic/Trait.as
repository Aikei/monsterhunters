package logic 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class Trait 
	{
		public static const NO_TRAIT: int = 0;
		public static const TRAIT_FAT: int = 1;
		public static const TRAIT_FAST: int = 2;
		public static const TRAIT_TOUGH: int = 3;
		public static const TRAIT_SHADOW: int = 4;
		public static const TRAIT_POISONOUS: int = 5;
		public static const TRAIT_CHARMER: int = 6;
		
		public var type: int;
		public var args: Array;
		
		public function Trait(type: int) 
		{
			this.type = type;
		}
		
		public function Copy(): Trait 
		{
			var t: Trait = new Trait(this.type);
			t.args = new Array;
			for (var i: int = 0; i < this.args.length; i++)
				t.args[i] = this.args[i];
			return t;
		}
		
		static public function CreateFromDataObject(dataObject: Object): Trait 
		{
			var t: Trait = new Trait(dataObject.id);
			t.args = dataObject.args;
			return t;
		}
		
	}

}