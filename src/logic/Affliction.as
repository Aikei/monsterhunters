package logic 
{
	/**
	 * ...
	 * @author Aikei
	 */

	import starling.utils.Color;
	import system.HSounds;
	
	public class Affliction 
	{
		public static const AFFLICTION_POISON: int = 0;
		public static const AFFLICTION_CHARM: int = 1;
		public static const NUMBER_OF_AFFLICTIONS: int = 2;
		
		public var type: int;
		public var timeAfflicted: Number;
		public var duration: Number;
		public var iconName: String = "";
		public var counterColor: uint = Color.GREEN;
		public var args: Array = new Array;
		public var soundName: String = "";
		
		public function Affliction(type: int) 
		{
			this.type = type;
		}
		
		public function CopyNew(): Affliction 
		{
			var a: Affliction = new Affliction(this.type);
			a.duration = this.duration;
			a.timeAfflicted = Misc.GetServerTime();
			a.iconName = this.iconName;
			a.soundName = this.soundName;
			for (var i: int = 0; i < this.args.length; i++)
				a.args[i] = this.args[i];
			return a;
		}
		
		public function OnAdded():void 
		{
			if (soundName.length > 0)
				HSounds.PlaySound(soundName);
		}
		
		static public function CreateNewFromDataObject(dataObject: Object): Affliction 
		{
			var a: Affliction = new Affliction(dataObject.type);			
			a.duration = dataObject.duration;
			a.timeAfflicted = Misc.GetServerTime();
			a.args = dataObject.args;
			switch (a.type) 
			{
				case AFFLICTION_POISON:
					a.iconName = "frame_poisoned";
					a.soundName = "Poison";
					a.counterColor = Color.GREEN;
					break;
					
				case AFFLICTION_CHARM:
					a.iconName = "frame_hypnotized";
					a.soundName = "Hypnosis";
					a.counterColor = Color.PURPLE;
					break;
			}
			
			return a;
		}
		
		static public function LoadFromDataObject(dataObject: Object): Affliction 
		{
			var a: Affliction = CreateNewFromDataObject(dataObject);
			a.timeAfflicted = dataObject.timeAfflicted;
			return a;
		}		
		
	}

}