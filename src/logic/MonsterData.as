package logic 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class MonsterData 
	{
		
		public static const MONSTER_DUMMY: int = 0;
		public static const MONSTER_SLIME: int = 1;	
		public static const MONSTER_SAPLING: int = 2;	
		public static const MONSTER_PLANT: int = 3;
		
		public static const MONSTER_BAT: int = 4;
		public static const MONSTER_SHADOW: int = 5;	
		public static const MONSTER_DARK_DUMMY: int = 6;		
		public static const MONSTER_LURKER: int = 7;

		public static const MONSTER_SKELETON: int = 8;
		public static const MONSTER_SENTRY: int = 9;
		public static const MONSTER_GOBLIN: int = 10;
		public static const MONSTER_CRYPT_SLIME: int = 11;			
		
		public static const NUMBER_OF_MONSTERS: int = 12;
		
		public static const ARMOR_NONE: int = 0;
		public static const ARMOR_LIGHT: int = 1;
		public static const ARMOR_NO_ARMOR: int = 2;
		public static const ARMOR_MAGIC: int = 3;
		public static const ARMOR_HEAVY: int = 4;
		public static const ARMOR_FORTIFIED: int = 5;
		public static const ARMOR_DARK: int = 6;
		
		public static const RARITY_COMMON: int = 0;
		public static const RARITY_RARE: int = 1;
		public static const RARITY_ELITE: int = 2;
		public static const NUMBER_OF_RARE_TYPES: int = 3;
		
		public var rarity: int;
		public var type: int = -1;
		public var originalHp: BigInt;
		public var originalArmor: BigInt;		
		public var hp: BigInt = Misc.BIG_INT_ZERO;
		public var armor: BigInt;			
		public var coinsData: Object;
		public var armorType: int = ARMOR_NONE;	
		public var boss: Boolean = false;
		public var timerDuration: Number;
		public var creationTime: Number;
		public var traits: Vector.<Trait> = new Vector.<Trait>;
		public var glory: BigInt;
		public var level: uint;
		public var isQuestBoss: Boolean = false;
		
		public var quest: Quest = null;
		
		public function Copy(): MonsterData
		{
			var newData: MonsterData = new MonsterData;
			newData.hp = BigInt.dup(this.hp);
			newData.type = this.type;
			newData.armor = this.armor;
			newData.coinsData = this.coinsData;
			newData.armorType = this.armorType;
			newData.rarity = this.rarity;
			newData.glory = BigInt.dup(this.glory);
			newData.level = this.level;
			newData.quest = this.quest;
			newData.isQuestBoss = this.isQuestBoss;
			for (var i: int = 0; i < this.traits.length; i++)
			{
				newData.traits.push(this.traits[i].Copy());
			}
			return newData;
		}
		
		public static function CreateFromDataObject(dataObject: Object): MonsterData 
		{
			var m: MonsterData = new MonsterData;
			m.glory = BigInt.shStr2BigInt(dataObject.glory);
			m.rarity = dataObject.rarity;
			m.creationTime = dataObject.creationTime;
			m.timerDuration = dataObject.timerDuration;
			m.type = dataObject.type;
			m.hp = BigInt.shStr2BigInt(dataObject.monsterData.hp);
			m.armor = BigInt.shStr2BigInt(dataObject.monsterData.armor);
			m.level = dataObject.level;
			m.originalHp = BigInt.shStr2BigInt(dataObject.originalData.hp);
			m.originalArmor = BigInt.shStr2BigInt(dataObject.originalData.armor);
			if (dataObject.sp === "t")
				m.isQuestBoss = true;
			if (dataObject.q)
			{
				m.quest = Quest.CreateFromDataObject(dataObject.q);
			}
			
			if (TalentEffects.armorDecrease > 0)
			{
				m.armor = BigInt.DecreaseByPercent(m.armor, TalentEffects.armorDecrease);
			}
				
			m.coinsData = dataObject.coinsData;
			m.armorType = dataObject.armorType;
			
			for (var i: int = 0; i < dataObject.traits.length; i++)
			{
				m.traits.push(Trait.CreateFromDataObject(dataObject.traits[i]));
			}
			
			return m;
		}
	}

}