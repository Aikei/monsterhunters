package logic.local 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import logic.Affliction;
	import logic.HeroData;
	
	public class ActualHero 
	{
		private var _heroData: HeroData;
		//private var _name: String;
		private var _id: int;
		private var _damageSum: BigInt = Misc.BIG_INT_ZERO;
		private var _afflictions: Vector.<Affliction> = new Vector.<Affliction>;
		
		public var stunNextTime: Boolean = false;
		
		public function get id(): int 
		{
			return _id
		}
		
		public function get heroData(): HeroData
		{
			return _heroData;
		}
		
		public function get afflictions(): Vector.<Affliction> 
		{
			return _afflictions;
		}
		
		//public function get name(): String 
		//{
			//return _name;
		//}
		
		public function AddAffliction(a: Affliction): void 
		{
			_afflictions.push(a);
			a.OnAdded();
		}
		
		public function GetRandomAfflictionType(): int
		{
			return _afflictions[Misc.Random(0, _afflictions.length)].type;
		}
				
		public function RemoveAffliction(type: int): Boolean 
		{
			if (type === -2)
			{
				if (_afflictions.length > 0)
					type = _afflictions[Misc.Random(0, _afflictions.length)].type;
			}
			else if (type === -1)
			{
				_afflictions = new Vector.<Affliction>;
				return true;
			}
			for (var i: int = 0; i < _afflictions.length; i++)
			{
				if (_afflictions[i].type === type)
				{
					_afflictions.splice(i, 1);
					return true;
				}
			}
			return false;
		}
		
		//public function RemoveAllAfflictions():void 
		//{
			//
		//}
		
		public function GetAffliction(type: int): Affliction 
		{
			for (var i: int = 0; i < _afflictions.length; i++)
			{
				if (_afflictions[i].type === type)
				{
					return _afflictions[i];
				}
			}
			return null;			
		}		
		
		public function HasAffliction(type: int = -1): Boolean 
		{
			if (type === -1 && _afflictions.length > 0)
				return true;
			for (var i: int = 0; i < _afflictions.length; i++)
			{
				if (_afflictions[i].type === type)
				{
					return true;
				}
			}
			return false;			
		}
		
		public function ActualHero(heroData: HeroData, id: int) 
		{
			_heroData = heroData;
			_id = id;
			//_name = Language.GetString(name);
		}
		
		public function SendHeroDamageInfo(): void 
		{
			if (!BigInt.equals(_damageSum, Misc.BIG_INT_ZERO))
			{
				World.gWorld.dispatchEventWith("heroDamageInfo", false, { heroId: _id, heroType: _heroData.type, damage: _damageSum, effectsData: heroData.damage.effectsData } );
				_damageSum = Misc.BIG_INT_ZERO;
			}
		}
		
		public function AddToDamageSum(damageValueToAdd: BigInt): void 
		{
			_damageSum = BigInt.add(_damageSum, damageValueToAdd);
		}
		
		public function Upgrade(newHeroData: HeroData): void 
		{
			_heroData = newHeroData;
		}
		
		static public function CreateFromDataObject(dataObject: Object): ActualHero 
		{
			var heroData: HeroData = HeroData.CreateFromDataObject(dataObject.heroData);
			var hero: ActualHero = new ActualHero(heroData, dataObject.id);
			//for (var i: int = 0; i < dataObject.afflictions.length; i++)
			//{
				//var a: Affliction = Affliction.LoadFromDataObject(dataObject.afflictions[i]);
				//hero._afflictions.push(a);
			//}
			return hero;
		}
	}

}