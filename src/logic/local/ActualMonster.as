package logic.local 
{
	import logic.Damage;
	import logic.MonsterData;
	import logic.TalentEffects;
	import logic.Trait;
	/**
	 * ...
	 * @author Aikei
	 */
	public class ActualMonster 
	{			
		public var monsterData: MonsterData;
		public var currentHp: BigInt;
		private var _alive: Boolean = true;
		
		public function get alive(): Boolean 
		{
			return _alive;
		}
		
		public function set alive(a: Boolean):void 
		{
			_alive = a;
		}
		
		public function get traits(): Vector.<Trait> 
		{
			return monsterData.traits;
		}
		
		public function ActualMonster(monsterData: MonsterData)
		{
			this.monsterData = monsterData.Copy();
			currentHp = BigInt.dup(monsterData.hp);
		}
		
		public function GetTrait(type: int): Trait 
		{
			for (var i: int = 0; i < monsterData.traits.length; i++)
			{
				if (monsterData.traits[i].type === type)
					return monsterData.traits[i];
			}
			return null;
		}
		
		public function Harm(damage: Damage): BigInt
		{
			var damageValue: BigInt;
			if (damage.type !== monsterData.armorType)
			{
				if (damage.type === Damage.DAMAGE_TYPE_NONE)
					damageValue = damage.adjustedValue;
				else
				{
					var trait: Trait = GetTrait(Trait.TRAIT_SHADOW);
					if (trait === null)
						damageValue = damage.adjustedValue;
					else
						damageValue = BigInt.DecreaseByPercent(damage.adjustedValue, trait.args[0]);
				}
			}
			else
			{
				damage.effectsData.wasEffectiveLastTime = true;
				damageValue = BigInt.mult(damage.adjustedValue, Misc.BIG_INT_TWO);
				//damageValue = BigInt.IncreaseByPercent(damageValue, TalentEffects.effectiveEmpower);
			}
			if (BigInt.greater(damageValue, Misc.BIG_INT_ZERO))
			{
				var minimum: BigInt = BigInt.DecreaseByPercent(damage.adjustedValue, 70);
				if (BigInt.greater(minimum, damageValue))
					minimum = damageValue;
				if (BigInt.greater(Misc.BIG_INT_ONE, minimum))
					minimum = Misc.BIG_INT_ONE;
				if (BigInt.greater(damageValue, monsterData.armor))
				{					
					damageValue = BigInt.sub(damageValue, monsterData.armor);
					if (BigInt.greater(minimum, damageValue))
						damageValue = minimum;					
				}
				else
				{
					damageValue = minimum;	
				}
				//if (BigInt.greater(damageValue, monsterData.armor))
					//damageValue = BigInt.sub(damageValue, monsterData.armor);
				//else
					//damageValue = Misc.BIG_INT_ZERO;
				if (damage.effectsData.wasHealingLastTime)
				{
					currentHp = BigInt.add(currentHp, damageValue);
					if (BigInt.greater(currentHp, monsterData.hp))
						currentHp = monsterData.hp;
				}
				else
				{
					if (BigInt.greater(damageValue, currentHp))
					{
						//damageValue = currentHp;
						currentHp = Misc.BIG_INT_ZERO;
					}
					else
						currentHp = BigInt.sub(currentHp, damageValue);
				}
				if (BigInt.equals(currentHp, Misc.BIG_INT_ZERO))
				{
					_alive = false;
				}
			}
			return damageValue;
		}
	}

}