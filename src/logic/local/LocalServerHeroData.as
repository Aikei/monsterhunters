package logic.local 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import logic.Damage;
	
	public class LocalServerHeroData 
	{
		public var type: int;
		public var dps: Object;		
		public var levelUpDamageIncrease: String;
		public var name: String;
		//public var portraitName: String;
		
		public var level: String = "1";
		public var buyCost: String = "1000";
		public var levelUpCost: String = "2000";
		public var levelUpCostIncrease: String = "1000";		
		
	}

}