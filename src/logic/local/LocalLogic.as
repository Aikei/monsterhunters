package logic.local 
{
	import logic.*;
	import hevents.*;
	import controller.lsevents.*;
	import flash.utils.setTimeout;
	/**
	 * ...
	 * @author Aikei
	 */
	public class LocalLogic 
	{
		private var _currentMonster: ActualMonster = null;
		private var _heroes: Vector.<ActualHero> = new Vector.<ActualHero>;
		private var _data: Object = new Object;
		private var _weapon: Weapon;
		private var _coins: BigInt = BigInt.shStr2BigInt("0");
		private var _dungeon: Dungeon;
		private var _dungeonsList: Vector.<Dungeon> = new Vector.<Dungeon>;
		
		public function get dungeon(): Dungeon 
		{
			return _dungeon;
		}
		
		public function Init():void 
		{
			InitHeroes();
			InitMonsters();
			InitWeapons();
			InitGame();
			World.gWorld.dispatchEventWith("N_DataReceived",false,_data);
		}
		
		public function Start():void 
		{					
			//CreateRandomMonster();
		}		
		
		private function InitHeroes():void 
		{
			_data.heroes = new Array;
			
			var serverHeroData: LocalServerHeroData = new LocalServerHeroData;
			serverHeroData.type = HeroData.HERO_BARBARIAN;
			serverHeroData.dps = new Object;
			serverHeroData.dps.value = "10";
			serverHeroData.dps.type = Damage.DAMAGE_TYPE_RANGED;
			serverHeroData.levelUpDamageIncrease = "10";
			//serverHeroData.portraitName = "portrait_barbarian";
			
			_data.heroes.push(serverHeroData);			
		}
		
		private function InitMonsters():void 
		{
			_data.monsters = new Array;
			var monsterData: Object = new Object;
			
			monsterData.type = MonsterData.MONSTER_DUMMY;
			monsterData.hp = "1000";
			
			_data.monsters.push(monsterData);						
		}
		
		private function InitWeapons(): void 
		{
			_data.weapons = new Array;
			
			var weapon: Object = new Object;
			weapon.type = Weapon.WEAPON_WOODEN_SWORD;
			weapon.damage = new Object;
			weapon.damage.value = "100";
			weapon.damage.type = Damage.DAMAGE_TYPE_NONE;
			weapon.levelUpDamageIncrease = "100";
			weapon.levelUpCost = "1000";
			weapon.levelUpCostIncrease = "1000";
			weapon.buyCost = "0";
			weapon.level = "1";
			
			_data.weapons.push(weapon);					
		}
		
		private function InitGame(): void 
		{
			_data.game = new Object;
			_data.game.currentWeapon = _data.weapons[Weapon.WEAPON_WOODEN_SWORD];
			_data.game.currentHeroes = new Array; //.heroData, .id, .name
			_data.game.coins = "0";
			
			_data.game.dungeonsList = new Array;
			
			var d: Object = new Object;
			d.type = 0;
			d.level = 1;
			d.name = Language.GetString("Dungeon") + " 1";
			d.current = true;						
			_data.game.dungeonsList.push(d);			
			
			d = new Object;
			d.type = 1;
			d.level = 1;
			d.name = Language.GetString("Dungeon") + " 2";			
			_data.game.dungeonsList.push(d);
			
			d = new Object;
			d.type = 2;
			d.level = 1;
			d.name = Language.GetString("Dungeon") + " 3";
			_data.game.dungeonsList.push(d);
			
			d = new Object;
			d.type = 3;
			d.level = 1;
			d.name = Language.GetString("Dungeon") + " 4";
			_data.game.dungeonsList.push(d);	
			
			d = new Object;
			d.type = 4;
			d.level = 1;
			d.name = Language.GetString("Dungeon") + " 5";						
			_data.game.dungeonsList.push(d);			
			
			var monsterType: int = Misc.Random(0, MonsterData.NUMBER_OF_MONSTERS);
			_data.game.currentMonster = _data.monsters[monsterType];
			
			for (var i: int = 0; i < _data.game.dungeonsList.length; i++)
			{
				_dungeon = Dungeon.CreateFromDataObject(_data.game.dungeonsList[i]);
				_dungeonsList.push(_dungeon);
			}
			
			_dungeon = _dungeonsList[0];
			_weapon = Weapon.CreateFromDataObject(_data.game.currentWeapon);
		}

		public function CreateMonster(monsterType: int): void 
		{
			var monsterData: MonsterData = World.data.monsters[monsterType];
			_currentMonster = new ActualMonster(monsterData);
			World.gWorld.dispatchEventWith("N_MonsterData",false, {monsterData: monsterData, monstersKilled: _dungeon.monstersKilled } ));
		}
		
		public function CreateRandomMonster(): void
		{
			var n: int = Misc.Random(0, MonsterData.NUMBER_OF_MONSTERS);
			CreateMonster(n);
		}
		
		private function GetRandomMonsterData(): MonsterData 
		{
			var n: int = Misc.Random(0, MonsterData.NUMBER_OF_MONSTERS);
			return World.data.monsters[n];
		}
		
		private function NextLevel(): void 
		{
			_dungeon.monstersKilled = 0;
			_dungeon.level++;			
			var m: MonsterData = GetRandomMonsterData();
			_currentMonster = new ActualMonster(m);
			World.gWorld.dispatchEvent(new HEventNextLevel(m,_dungeon.level));			
		}
		
		public function KillMonster(): void 
		{
			_currentMonster = null;
			_dungeon.monstersKilled++;
			if (_dungeon.monstersKilled === Dungeon.MONSTERS_MAX)
			{
				NextLevel();
			}
			else
			{
				setTimeout(CreateRandomMonster, 500);
			}
		}
		
		public function BuyHero(type: int): void 
		{
			var cost: BigInt = BigInt.shStr2BigInt(_data.heroes[type].buyCost);
			if (!BigInt.greater(cost, _coins))
			{
				_coins = BigInt.sub(_coins, cost);
				var heroData: HeroData = HeroData.CreateFromDataObject(_data.heroes[type]);
				var actualHero: ActualHero = new ActualHero(heroData,_heroes.length);
				_heroes.push(actualHero);
				World.gWorld.dispatchEvent(new HEventHeroBought(type, actualHero.name, actualHero.id));
			}
			else
			{
				World.gWorld.dispatchEvent(new L_HEventCheater);
			}
		}
		
		public function UpgradeHero(id: int): void 
		{
			var cost: BigInt = _heroes[id].heroData.levelUpCost;
			if (!BigInt.greater(cost, _coins))
			{
				_coins = BigInt.sub(_coins, cost);
				_heroes[id].Upgrade();
				World.gWorld.dispatchEvent(new HEventHeroUpgraded(id));
			}
			else
			{
				World.gWorld.dispatchEvent(new L_HEventCheater);
			}			
		}
		
		public function BuyWeapon(weaponType: int): void 
		{
			var cost: BigInt = BigInt.shStr2BigInt(_data.weapons[weaponType].buyCost);
			if (!BigInt.greater(cost, _coins))
			{
				_coins = BigInt.sub(_coins, cost);
				_weapon = Weapon.CreateFromDataObject(_data.weapons[weaponType]);
				World.gWorld.dispatchEvent(new HEventNewWeaponBought(weaponType));
			}
			else
			{
				World.gWorld.dispatchEvent(new L_HEventCheater);
			}
		}
		
		public function UpgradeWeapon(weaponType: int): void 
		{
			var cost: BigInt = BigInt.shStr2BigInt(_data.weapons[weaponType].levelUpCost);
			if (!BigInt.greater(cost, _coins))
			{
				_coins = BigInt.sub(_coins, cost);
				_weapon.Upgrade();
				World.gWorld.dispatchEvent(new HEventWeaponUpgraded(weaponType));
			}
			else
			{
				World.gWorld.dispatchEvent(new L_HEventCheater);
			}			
		}		
		
		public function AddCoins(n: BigInt): void 
		{
			_coins = BigInt.add(_coins, n);
		}
		
		public function ChangeDungeon(newDungeonType: int): void 
		{
			_dungeon = _dungeonsList[newDungeonType];
			var monsterType: int = Misc.Random(0, MonsterData.NUMBER_OF_MONSTERS);
			var monsterData: MonsterData = MonsterData.CreateFromDataObject(_data.monsters[monsterType]);
			World.gWorld.dispatchEvent(new HEventDungeonChanged(_dungeon.type, monsterData));
		}
		
	}

}