package logic 
{
	import flash.utils.clearTimeout;
	import logic.local.ActualHero;
	import logic.local.ActualMonster;
	import hevents.*;
	import flash.utils.Timer;
	import flash.utils.setTimeout;
	import flash.events.TimerEvent;
	import starling.events.Event;
	import system.HCursor;
	import starling.utils.Color;
	import system.HSounds;
	import view.GameView;
	import view.gameviewelements.heroshowpanel.visualizers.HealVisualizer;
	import view.gameviewelements.misc.FallingThing;
	import view.gameviewelements.misc.ScrollThing;
	/**
	 * ...
	 * @author Aikei
	 */
	public class ClientLogic 
	{				
		static public var HEROES_ATTACK_COOLDOWN: Number = 3.0;			//! may be set in script
		static public var HEROES_ATTACK_START_INTERVAL: Number = 0.5;	//! may be set in script
		
		private var _monster: ActualMonster = null;
		private var _heroes: Vector.<ActualHero> = new Vector.<ActualHero>;
		private var _weapon: Weapon = null;
		//private var _heroAnimTimers: Vector.<HeroTimer> = new Vector.<HeroTimer>;	
		//private var _heroAttackTimers: Vector.<HeroTimer> = new Vector.<HeroTimer>;
		private var _timer: HeroTimer;
		private var _heroesUpdateTimer: Timer;
		private var _monsterTimer: Timer;
		private var _coins: BigInt = BigInt.shStr2BigInt("0");
		private var _dungeon: Dungeon;
		private var _dungeonsList: Vector.<Dungeon> = new Vector.<Dungeon>;
		private var _heroDamageTickCounter: int = 0;
		private var _glory: BigInt = Misc.BIG_INT_ZERO;
		private var _dungeonLoaded: Boolean = true;
		private var _newMonsterData: MonsterData = null;
		private var _quest: Quest = null;
		
		static public var gLogic: ClientLogic;
		
		public function get quest(): Quest 
		{
			return _quest;
		}
		
		public function set attack_cooldown(cd: Number): void
		{
			HEROES_ATTACK_COOLDOWN = cd;
			StopHeroTimers();
			_timer.RecalculateData();
			StartHeroTimers();
		}
		
		public function set attack_start_interval(secs: Number): void
		{
			HEROES_ATTACK_START_INTERVAL = secs;
			StopHeroTimers();
			_timer.RecalculateData();
			StartHeroTimers();
		}
		
		public function get attack_cooldown(): Number
		{
			return HEROES_ATTACK_COOLDOWN;
		}
		
		public function get attack_start_interval(): Number
		{
			return HEROES_ATTACK_START_INTERVAL;
		}			
		
		public function ClientLogic() 
		{
			gLogic = this;
			World.gWorld.addEventListener("N_LocalClickHit", OnClickHit);
			World.gWorld.addEventListener("N_NewMonster", OnNewMonster);
			
			World.gWorld.addEventListener(HEventHeroBought.H_EVENT_HERO_BOUGHT, OnHeroBought);
			World.gWorld.addEventListener(HEventHeroUpgraded.H_EVENT_HERO_UPGRADED, OnHeroUpgraded);
			
			World.gWorld.addEventListener(HEventNewWeaponBought.H_EVENT_NEW_WEAPON_BOUGHT, OnNewWeaponBought);			
			World.gWorld.addEventListener(HEventWeaponUpgraded.EVENT_TYPE, OnWeaponUpgraded);			
			World.gWorld.addEventListener(HEventNextLevel.HEVENT_NEXT_LEVEL, OnNextLevel);
			World.gWorld.addEventListener("changeLevel", OnChangeLevel);
			World.gWorld.addEventListener("N_CoinsReceived", OnCoinsReceived);
			World.gWorld.addEventListener("heroBuyButtonClicked", OnHeroBuyButtonClicked);
			World.gWorld.addEventListener("heroUpgradeButtonClicked", OnHeroUpgradeButtonClicked);
			World.gWorld.addEventListener("weaponBuyButtonClicked", OnWeaponBuyButtonClicked);
			World.gWorld.addEventListener("weaponUpgradeButtonClicked", OnWeaponUpgradeButtonClicked);
			World.gWorld.addEventListener("N_TimeUp", OnTimeUp);
			World.gWorld.addEventListener("removeAffliction", OnRemoveAffliction);
			World.gWorld.addEventListener(L_HEventDungeonLevelSet.L_HEVENT_DUNGEON_LEVEL_SET, OnDungeonLevelSet);
			World.gWorld.addEventListener(HEventDungeonChanged.H_EVENT_DUNGEON_CHANGED, OnDungeonChanged);
			World.gWorld.addEventListener(L_HEventMonsterKilled.H_EVENT_MONSTER_KILLED, OnMonsterKilled);
			World.gWorld.addEventListener("N_DungeonsData", OnDungeonsData);
			World.gWorld.addEventListener("N_GloryAdded", OnGloryAdded);
			World.gWorld.addEventListener("newQuest", OnNewQuest);
			//World.gWorld.addEventListener("newDay", OnNewDay);
			//for (var i: int = 0; i < HeroData.NUMBER_OF_HEROES; i++)
			//{
				//_heroAttackTimers.push(new HeroTimer(HEROES_ATTACK_COOLDOWN*1000,i));
				//_heroAttackTimers[i].addEventListener(TimerEvent.TIMER, OnHeroAttackTimerTick);
				//
				//_heroAnimTimers.push(new HeroTimer(HEROES_ATTACK_COOLDOWN*1000,i));
				//_heroAnimTimers[i].addEventListener(TimerEvent.TIMER, OnHeroAnimTimerTick);				
			//}
			_heroesUpdateTimer = new Timer(1000, 0);
			_heroesUpdateTimer.addEventListener(TimerEvent.TIMER, OnHeroesUpdate);
			_heroesUpdateTimer.start();
			_timer = new HeroTimer;
			_timer.addEventListener("damageTime", OnDamageTime);
			_timer.addEventListener("animTime", OnAnimTime);
			_monsterTimer = new Timer(1000,0);
			_monsterTimer.addEventListener(TimerEvent.TIMER, OnMonsterTimerTick);
			_monsterTimer.start();
		}
		
		//private function OnNewDay(e:Event):void 
		//{
			//for (var i: int = 0; i < _dungeonsList.length; i++)
			//{
				//for (var j: int = 0; j < e.data.dungeons[i].effects.length; j++)
				//{
					//_dungeonsList[i].effects = new Vector.<DungeonEffect>;
					//_dungeonsList[i].effects.push(DungeonEffect.CreateFromDataObject(e.data.dungeons[i].effects[j]));
				//}
			//}
			//World.gWorld.dispatchEventWith("dungeonsUpdated", false, _dungeonsList);
		//}
		
		private function OnHeroesUpdate(e:TimerEvent):void 
		{
			if (TalentEffects.healChance > 0)
			{
				var afflicted: Vector.<int> = new Vector.<int>;
				if (Misc.Random(0, 100) < TalentEffects.healChance)
				{
					for (var i: int = 0; i < _heroes.length; i++)
					{
						if (_heroes[i].HasAffliction( -1))
							afflicted.push(i);
					}
					if (afflicted.length > 0)
					{
						var n: Number = Misc.Random(0, afflicted.length);
						//World.gWorld.dispatchEventWith("heroPortraitVisualize", false, { heroType: _heroes[afflicted[n]].heroData.type, texture: Assets.interfaceAtlas.getTexture("talent_heal") });
						World.gWorld.dispatchEventWith("heroPortraitVisualize", false, { heroType: _heroes[afflicted[n]].heroData.type, object: new HealVisualizer });
						World.gameController.RemoveAffliction(_heroes[afflicted[n]].GetRandomAfflictionType(), _heroes[afflicted[n]].heroData.type);
						HSounds.PlaySound("Heal");
					}
				}
			}				
		}
		
		private function OnNewQuest(e:Event):void 
		{
			var q: Quest = e.data as Quest;
			if (q.dungeonType > -1 && q.dungeonType != _dungeon.type)
			{
				World.gWorld.dispatchEventWith("questNotInThisDungeon");
			}
		}
		
		private function OnGloryAdded(e:Event):void 
		{
			_glory = BigInt.add(_glory, BigInt(e.data));
			World.gWorld.dispatchEventWith("N_GloryChanged", false, _glory);
		}
		
		private function OnAnimTime(e:Event):void 
		{
			if (World.gWorld.phase === World.PHASE_IN_GAME)
			{
				if (_heroes.length > uint(e.data))
				{			
					if (_monster.alive)
					{
						World.gWorld.dispatchEventWith("heroAnimStart", false, _heroes[e.data as int]);
					}					
				}
			}			
		}
		
		private function OnDamageTime(e:Event):void 
		{
			if (World.gWorld.phase === World.PHASE_IN_GAME)
			{
				if (_heroes.length > uint(e.data))
				{					
					if (_monster.alive)
					{
						var ah: ActualHero = _heroes[uint(e.data)];
						HarmMonsterWithHero(ah);
						ah.SendHeroDamageInfo();		
					}					
				}
			}			
		}
				
		private function OnDungeonLoadComplete(e: Event): void 
		{
			World.gWorld.removeEventListener("dungeonLoadDone", OnDungeonLoadComplete);
			if (!_dungeonLoaded)
			{
				World.gWorld.dispatchEvent(new L_HEventDungeonLevelSet(_dungeon));
				World.gWorld.dispatchEventWith("N_NewMonster", false, { monsterData: _newMonsterData, monstersKilled: _dungeon.monstersKilled } );
			}
			_dungeonLoaded = true;
		}
				
		protected function GetHeroByType(type: int): ActualHero 
		{
			for (var i: int = 0; i < _heroes.length; i++)
			{
				if (_heroes[i].heroData.type === type)
					return _heroes[i];
			}
			return null;
		}
		
		protected function OnRemoveAffliction(event: Event): void 
		{
			var h: ActualHero = GetHeroByType(event.data.heroType);
			if (h != null)
			{
				h.RemoveAffliction(event.data.afflictionType);
			}
			else
			{
				throw "Tried to remove affliction from unexistant hero!";
			}
		}
		
		protected function OnMonsterKilled(event: L_HEventMonsterKilled): void 
		{
			if (_dungeon.monstersKilled === Dungeon.MONSTERS_MAX)
			{
				World.gWorld.dispatchEventWith("levelComplete",false,_dungeon.level);
			}					
			_monsterTimer.reset();
			if (_monster.monsterData.quest != null)
			{
				this._quest = _monster.monsterData.quest;
				var scroll: ScrollThing = new ScrollThing(_quest, 0, 0);
				World.gWorld.dispatchEventWith("addFallingThing", false, scroll);
			}
			//if (_heroDamageTickCounter < Misc.DAMAGE_TIMES_PER_SECOND)
			//{
				for (var i: int = 0; i < _heroes.length; i++)
				{
					_heroes[i].SendHeroDamageInfo();
				}
				//_heroDamageTickCounter = 0;
			//}
			
			//if (BigInt.greater(event.glory, Misc.BIG_INT_ZERO))
			//{
				//this._glory = BigInt.add(this._glory,event.glory);
				//World.gWorld.dispatchEventWith("N_GloryChanged", false, this._glory);
			//}
		}
		
		protected function OnTimeUp(event: Event): void 
		{
			//if (_heroDamageTickCounter < Misc.DAMAGE_TIMES_PER_SECOND)
			//{
				for (var i: int = 0; i < _heroes.length; i++)
				{
					_heroes[i].SendHeroDamageInfo();
				}
				//_heroDamageTickCounter = 0;
				_monster.alive = false;
			//}			
		}
		
		protected function OnDungeonsData(event: Event): void 
		{
			for (var i: int = 0; i < _dungeonsList.length; i++)
			{
				_dungeonsList[i].CopyFromDataObject(event.data.sh[i], event.data.d[i]);
			}
			World.gWorld.dispatchEventWith("dungeonsUpdated", false, _dungeonsList);
			//_dungeon = _dungeonsList[current];			
		}
		
		protected function OnDungeonChanged(event: HEventDungeonChanged): void 
		{
			if (_dungeon.type != event.dungeonType)
			{
				if (_quest && event.dungeonType === _quest.dungeonType)
					World.gWorld.dispatchEventWith("enteredQuestDungeon");
				_dungeon = _dungeonsList[event.dungeonType];
				_newMonsterData = MonsterData(event.monsterData);
				World.gWorld.addEventListener("dungeonLoadDone", OnDungeonLoadComplete);
				_dungeonLoaded = false;
				Assets.LoadDungeonResources(_dungeon);
			}
		}
		
		protected function OnNextLevel(event: HEventNextLevel): void 
		{
			_dungeon.level++;
			if (_dungeon.level > _dungeon.maxLevel)
				_dungeon.maxLevel = _dungeon.level;
			_dungeon.monstersKilled = 0;
			setTimeout(World.gWorld.dispatchEventWith, 1000, "N_NewMonster", false, { monsterData: event.monsterData, monstersKilled: 0 } );
		}
		
		protected function OnChangeLevel(event: Event): void 
		{
			_dungeon.level = event.data.level;
			_dungeon.lastLevel = event.data.lst;
			_dungeon.monstersKilled = event.data.monstersKilled;
			setTimeout(World.gWorld.dispatchEventWith, 1000, "N_NewMonster", false, { monsterData: event.data.monsterData, monstersKilled: event.data.monstersKilled } );
		}		
		
		public function Init(dataObject: Object): void 
		{
			World.gWorld.SetPhase(World.PHASE_IN_GAME);
			
			//weapon
			
			_weapon = Weapon.CreateFromDataObject(dataObject.game.currentWeapon);			
			World.gWorld.dispatchEvent(new L_HEventWeaponSet(_weapon, false));
			
			//heroes
			
			for (var i: int = 0; i < dataObject.game.currentHeroes.length; i++)
			{
				var hero: ActualHero = ActualHero.CreateFromDataObject(dataObject.game.currentHeroes[i]);
				_heroes.push(hero);
				World.gWorld.dispatchEvent(new L_HEventHeroBought(hero, false));
			}
			
			//coins
			
			_coins = BigInt.shStr2BigInt(dataObject.game.coins);
			World.gWorld.dispatchEventWith("N_CoinsNow",false,_coins);			
			
			//dungeons and current monster
			
			var current: int = dataObject.game.currentDungeon;
			for (i = 0; i < dataObject.game.dungeonShellsList.length; i++)
			{
				_dungeon = Dungeon.CreateFromDataObject(dataObject.game.dungeonShellsList[i],dataObject.dungeons[i]);
				_dungeonsList.push(_dungeon);
			}			
			_dungeon = _dungeonsList[current];
			World.gWorld.dispatchEvent(new L_HEventDungeonsListInitialized(_dungeonsList));
			_dungeonLoaded = false;
			
			if (dataObject.game.q)
			{
				_quest = Quest.CreateFromDataObject(dataObject.game.q);
				World.gWorld.dispatchEventWith("newQuest",false,_quest);
			}			
			
			World.gWorld.dispatchEvent(new L_HEventDungeonLevelSet(_dungeon));
			World.gWorld.dispatchEventWith("N_NewMonster", false, { monsterData: MonsterData.CreateFromDataObject(dataObject.game.currentMonster), monstersKilled: _dungeon.monstersKilled } );			
			
			//Assets.LoadDungeonResources(_dungeon);
			//_newMonsterData = ;
																	
			//talent effects
			
			TalentEffects.CreateFromDataObject(dataObject.game.talentEffects);
			
			//glory
			
			this._glory = BigInt.add(this._glory, BigInt.shStr2BigInt(dataObject.game.glory));
			World.gWorld.dispatchEventWith("N_GloryChanged", false, this._glory);

			HSounds.SetDungeonTheme(_dungeon.type);						
		}
		
		protected function OnClickHit(event: Event): void
		{
			var damage: BigInt = HarmMonster(_weapon.damage);
			if (damage != null)
			{
				World.gWorld.dispatchEventWith("createFlyingText", false, 
					{ x: HCursor.GetCursor().x, y: HCursor.GetCursor().y, font: "damageFont", timeToDieInMs: 500, text: BigInt.bigint2HString(damage), jumping: true } );
			}
			//World.gWorld.dispatchEventWith("monsterHarmedWithWepon", false, damage);
		}
		
		protected function HarmMonster(damage: Damage): BigInt 
		{
			var ret: BigInt = null;
			if (_monster.alive)
			{
				ret = _monster.Harm(damage);
				if (!_monster.alive)
				{
					if (_dungeon.maxLevel == _dungeon.level)
						_dungeon.monstersKilled++;
					var mKilled: int = _dungeon.monstersKilled;
					if (_dungeon.maxLevel != _dungeon.level)
						mKilled = 10;
					World.gWorld.dispatchEvent(new L_HEventMonsterKilled(mKilled, _monster.monsterData.glory));
					World.gameController.KillMonster();
				}
				else
				{
					World.gWorld.dispatchEvent(new L_HEventMonsterHpChanged(_monster.currentHp));
				}
			}
			return ret;
		}
		
		protected function HarmMonsterWithHero(hero: ActualHero): void 
		{
			if (_monster.alive)
			{
				if (BigInt.greater(hero.heroData.dps.value, Misc.BIG_INT_ZERO))
				{
					var d: Damage = hero.heroData.damage;
					d.ResetEffects();
					d.adjustedValue = d.value;
					if (TalentEffects.heroDpsIncrease > 0)
						d.adjustedValue = BigInt.IncreaseByPercent(d.adjustedValue, TalentEffects.heroDpsIncrease);
					if (TalentEffects.empowerHero[hero.heroData.type] > 0)
						d.adjustedValue = BigInt.IncreaseByPercent(d.adjustedValue, TalentEffects.empowerHero[hero.heroData.type]);
					for (var i: int = 0; i < hero.afflictions.length; i++)
					{
						if (hero.afflictions[i].type === Affliction.AFFLICTION_POISON)
						{
							d.adjustedValue = BigInt.DecreaseByPercent(d.adjustedValue, hero.afflictions[i].args[0]);
						}
						else if (hero.afflictions[i].type === Affliction.AFFLICTION_CHARM)
						{
							d.effectsData.wasHealingLastTime = true;
						}
					}					
										
					var damage: BigInt = _monster.Harm(d);
					hero.AddToDamageSum(damage);
					if (!_monster.alive)
					{
						if (_dungeon.maxLevel == _dungeon.level)
							_dungeon.monstersKilled++;
						var mKilled: int = _dungeon.monstersKilled;
						if (_dungeon.maxLevel != _dungeon.level)
							mKilled = 10;						
						World.gWorld.dispatchEvent(new L_HEventMonsterKilled(mKilled,_monster.monsterData.glory));
						World.gameController.KillMonster();
					}
					else
					{
						if (hero.stunNextTime)
						{
							if (!d.effectsData.wasHealingLastTime)
							{
								d.effectsData.wasStunningLastTime = true;
								World.gWorld.dispatchEventWith("stunMonster", false, TalentEffects.stunDuration);
							}
							hero.stunNextTime = false;
						}						
						World.gWorld.dispatchEvent(new L_HEventMonsterHpChanged(_monster.currentHp));
					}
				}
				var dice: Number = Misc.Random(0, 100);
				if (dice < TalentEffects.stunChance)
				{
					hero.stunNextTime = true;
				}
			}
		}
		
		protected function OnNewMonster(event: Event): void 
		{
			_monster = new ActualMonster(event.data.monsterData as MonsterData);
			//HeroAnimStart();
			StartHeroTimers();
			_monsterTimer.start();
		}
		
		protected function OnHeroBought(event: HEventHeroBought): void 
		{
			var hero: ActualHero = new ActualHero(World.data.heroes[event.heroType].Copy(), _heroes.length);
			_heroes.push(hero);
			World.gWorld.dispatchEvent(new L_HEventHeroBought(hero));
		}
		
		protected function OnHeroUpgraded(event: HEventHeroUpgraded): void 
		{
			_heroes[event.ah.id].Upgrade(event.heroData);
		}
		
		//protected function HeroAnimStart():void 
		//{
			//World.gWorld.dispatchEventWith("heroAnimStart");
		//}
		
		protected function StartHeroTimers():void 
		{
			_timer.StartTimer();
		}
		
		protected function StopHeroTimers():void 
		{
			_timer.StopTimer();
		}
		//protected function StartHeroTimers():void 
		//{
			//for (var i: int = 0; i < HeroData.NUMBER_OF_HEROES; i++)
			//{
				//_heroAttackTimers[i].firstRun = true;
				//if (_heroAttackTimers[i].startTimeoutId > 0)
					//clearTimeout(_heroAttackTimers[i].startTimeoutId);
				//_heroAttackTimers[i].startTimeoutId = setTimeout(_heroAttackTimers[i].start, HEROES_ATTACK_START_INTERVAL * i * 1000);
				//_heroAnimTimers[i].firstRun = true;
				//if (_heroAnimTimers[i].startTimeoutId > 0)
					//clearTimeout(_heroAnimTimers[i].startTimeoutId);				
				//_heroAnimTimers[i].startTimeoutId = setTimeout(_heroAnimTimers[i].start, (HEROES_ATTACK_START_INTERVAL * i+(HEROES_ATTACK_COOLDOWN-GameView.gView.HERO_PROJECTILE_TIME))*1000);				
			//}			
		//}
		//
		//protected function StopHeroTimers():void 
		//{
			//for (var i: int = 0; i < HeroData.NUMBER_OF_HEROES; i++)
			//{
				//_heroAttackTimers[i].reset();
				//_heroAnimTimers[i].reset();
			//}			
		//}
		
		private function OnMonsterTimerTick(e:TimerEvent):void 
		{
			if (World.gWorld.phase === World.PHASE_IN_GAME)
			{
				if (_monster && _monster.alive)
				{
					var trait: Trait = _monster.GetTrait(Trait.TRAIT_POISONOUS);
					if (trait)
						MaybeAddAffliction(Affliction.AFFLICTION_POISON,trait.args[0]);					
					
					trait = _monster.GetTrait(Trait.TRAIT_CHARMER);
					if (trait)
						MaybeAddAffliction(Affliction.AFFLICTION_CHARM,trait.args[0]);				
				}
			}
		}
		
		private function FindMostHarmfulHero(withoutAffliction: int = -1): ActualHero 
		{
			var biggestDamage: BigInt = Misc.BIG_INT_ZERO;
			var mostHarmfulHero: ActualHero = null;
			
			for (var i: int = 0; i < _heroes.length; i++)
			{
				var damageValue: BigInt = _heroes[i].heroData.damage.value;
				if (_heroes[i].heroData.damage.type === _monster.monsterData.armorType)
					damageValue = BigInt.add(damageValue, damageValue);
				if (BigInt.greater(damageValue, biggestDamage) && (withoutAffliction === -1 || !_heroes[i].HasAffliction(withoutAffliction)))
				{
					mostHarmfulHero = _heroes[i];
					biggestDamage = damageValue;
				}
			}
			return mostHarmfulHero;
		}
		
		private function MaybeAddAffliction(afflictionType: int, probability: Number): void
		{
			if (_heroes.length === 0)
				return;
			var dice: Number = Misc.Random(0, 100);
			if (dice < probability)
			{
				var target: ActualHero = null;
				dice = Misc.Random(0, 100);				
				if (dice < 60)
				{
					target = FindMostHarmfulHero();
				}
				if (!target)
				{
					dice = Misc.Random(0, _heroes.length);
					target = _heroes[dice];
				}
				if (target.HasAffliction())
				{
					World.gameController.RemoveAffliction(-1, target.heroData.type);
				}
				var a: Affliction = World.data.afflictions[afflictionType].CopyNew();
				if (TalentEffects.poisonDurationDecrease > 0)
					a.duration -= TalentEffects.poisonDurationDecrease;
				if (a.duration < 1)
					a.duration = 1;
				target.AddAffliction(a);
				World.gameController.AddAffliction(a, target.heroData.type);
			}			
		}
		
		//protected function OnHeroAnimTimerTick(event: TimerEvent): void 
		//{
			//if (World.gWorld.phase === World.PHASE_IN_GAME)
			//{
				//var t: HeroTimer = HeroTimer(event.target);
				//if (_heroes.length > t.heroType)
				//{
					//if (t.firstRun)
						//t.firstRun = false;					
					//if (_monster.alive)
					//{
						//World.gWorld.dispatchEventWith("heroAnimStart", false, t.heroType);
					//}					
				//}
			//}
		//}		
		
		//protected function OnHeroAttackTimerTick(event: TimerEvent): void 
		//{
			//if (World.gWorld.phase === World.PHASE_IN_GAME)
			//{
				//var t: HeroTimer = HeroTimer(event.target);
				//if (_heroes.length > t.heroId)
				//{
					//if (t.firstRun)
					//{
						//t.firstRun = false;
						//if (_heroAnimTimers[t.heroId].firstRun == false)
							//return;
					//}					
					//if (_monster.alive)
					//{
						//HarmMonsterWithHero(_heroes[t.heroId]);
						//_heroes[t.heroId].SendHeroDamageInfo();		
					//}					
				//}
			//}
		//}
		//protected function OnTimerTick(event: TimerEvent): void 
		//{			
			//if (World.gWorld.phase === World.PHASE_IN_GAME)
			//{
				//if (_monster.alive)
				//{					
					//var trait: Trait = _monster.GetTrait(Trait.TRAIT_POISONOUS);
					//if (trait != null && _heroes.length > 0)
					//{
						//var dice: Number = Misc.Random(0, 100);
						//if (dice < trait.args[0])
						//{
							//dice = Misc.Random(0, _heroes.length);
							//if (_heroes[dice].HasAffliction(Affliction.AFFLICTION_POISON))
								//_heroes[dice].RemoveAffliction(Affliction.AFFLICTION_POISON);
							//var a: Affliction = World.data.afflictions[trait.args[1]].CopyNew();
							//if (TalentEffects.poisonDurationDecrease > 0)
								//a.duration -= TalentEffects.poisonDurationDecrease;
							//if (a.duration < 1)
								//a.duration = 1;
							//_heroes[dice].AddAffliction(a);
							//World.gameController.AddAffliction(a, _heroes[dice].heroData.type);
						//}
					//}
					//_heroDamageTickCounter++;
					//for (var i: int = 0; i < _heroes.length; i++)
					//{
						//if (!_monster.alive)
						//{
							//if (_heroDamageTickCounter === Misc.DAMAGE_TIMES_PER_SECOND)
								//_heroes[i].SendHeroDamageInfo();
						//}
						//else
						//{
							//HarmMonsterWithHero(_heroes[i]);
							//if (_heroDamageTickCounter === Misc.DAMAGE_TIMES_PER_SECOND)
								//_heroes[i].SendHeroDamageInfo();
						//}
					//}				
					//if (_heroDamageTickCounter === Misc.DAMAGE_TIMES_PER_SECOND)
						//_heroDamageTickCounter = 0;
					//if (_monster.alive)
						//HeroAnimStart();
				//}
			//}
		//}
		
		protected function OnNewWeaponBought(event: HEventNewWeaponBought): void 
		{
			//_weapon = Weapon.CreateFromDataObject(World.data.weapons[event.weaponType]);
			_weapon = Weapon.CreateFromDataObject(event.dataObject);
			World.gWorld.dispatchEvent(new L_HEventWeaponSet(_weapon));
		}
		
		protected function OnWeaponUpgraded(event: HEventWeaponUpgraded): void 
		{
			_weapon = event.weapon;
			World.gWorld.dispatchEvent(new L_HEventWeaponUpgraded(_weapon));
		}		
		
		protected function OnCoinsReceived(event: Event): void
		{
			_coins = BigInt.add(_coins, event.data as BigInt);
			World.gWorld.dispatchEventWith("N_CoinsNow",false,_coins);
		}
		
		protected function OnHeroBuyButtonClicked(event: Event): void 
		{
			var cost: BigInt = World.data.heroes[event.data as int].buyCost;
			if (!BigInt.greater(cost, _coins))
			{
				_coins = BigInt.sub(_coins, cost);
				World.gWorld.dispatchEventWith("N_CoinsNow",false,_coins);
				World.gameController.BuyHero(event.data as int);
			}
			else
			{
				World.gWorld.dispatchEvent(new Event("untriggerButton"));
			}			
		}
		
		protected function OnHeroUpgradeButtonClicked(event: Event): void
		{
			var cost: BigInt = _heroes[event.data as int].heroData.levelUpCost;
			if (!BigInt.greater(cost, _coins))
			{
				_coins = BigInt.sub(_coins, cost);
				World.gWorld.dispatchEventWith("N_CoinsNow",false,_coins);
				World.gameController.UpgradeHero(event.data as int);
			}
			else
			{
				World.gWorld.dispatchEvent(new Event("untriggerButton"));
			}
		}
		
		protected function OnWeaponBuyButtonClicked(event: Event): void 
		{
			var cost: BigInt = World.data.weapons[event.data as int].buyCost;
			if (!BigInt.greater(cost, _coins))
			{
				_coins = BigInt.sub(_coins, cost);
				World.gWorld.dispatchEventWith("N_CoinsNow",false,_coins);
				World.gameController.BuyWeapon(event.data as int);
			}
			else
			{
				World.gWorld.dispatchEvent(new Event("untriggerButton"));
			}			
		}
		
		protected function OnWeaponUpgradeButtonClicked(event: Event): void
		{
			var cost: BigInt = _weapon.levelUpCost;
			if (!BigInt.greater(cost, _coins))
			{
				_coins = BigInt.sub(_coins, cost);
				World.gWorld.dispatchEventWith("N_CoinsNow",false,_coins);
				World.gameController.UpgradeWeapon(event.data as int);
			}
			else
			{
				World.gWorld.dispatchEvent(new Event("untriggerButton"));
			}
		}
		
		protected function OnDungeonLevelSet(event: L_HEventDungeonLevelSet): void 
		{
			if (_dungeon.type != event.dungeon.type)
			{
				_dungeon = event.dungeon;
				Assets.LoadDungeonResources(_dungeon);
			}
		}
		
	}

}