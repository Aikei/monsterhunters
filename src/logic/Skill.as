package logic 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class Skill 
	{		
		public static const SKILL_INCREASE_GLOBAL_DAMAGE: int = 0;

		public static const SKILL_INCREASE_MONSTER_COINS: int = 1;
		public static const SKILL_INCREASE_STUN_CHANCE: int = 2;
		public static const SKILL_EMPOWER_HERO: int = 3;
		public static const SKILL_HEAL_CHANCE: int = 4;
		public static const SKILL_PATHFINDER: int = 5;
		public static const SKILL_MERCENARY: int = 6;
		
		public static const SKILL_DECREASE_MONSTER_ARMOR: int = 7;
		
		public static const NUMBER_OF_SKILLS: int = 7;
		
		public var type: int;
		public var subtype: int;
		public var args: Array = new Array;
		public var price: BigInt;
		public var iconName: String;
		public var info: String;
		public var infoTitle: String;
		public var packNum: int;
		
		public function Skill() 
		{
			
		}
		
		//static private function PercentageArgumentToString(arg: Number): String 
		//{
			//var s: String = "";
			//if (arg > 1.0)
			//{
				//s = "+" + String(int((arg - 1.0) * 100.0)) + "% ";
			//}
			//else
			//{
				//s = "-" + String(int(Math.ceil((1.0 - arg)*100))) + "% ";
			//}
			//return s;
		//}
		
	
		
	
		static public function CreateFromDataObejct(dataObject: Object,packObject: Object): Skill 
		{
			var skill: Skill = new Skill;
			skill.type = dataObject.type;
			skill.subtype = dataObject.subtype;
			skill.args = dataObject.args;
			//skill.price = BigInt.shStr2BigInt(packObject.cost);
			skill.info = Language.GetString("SkillDescription" + skill.type);
			skill.infoTitle = "Skill" + skill.type + "Subtype" + skill.subtype;
			skill.packNum = packObject.num;
			switch (skill.type) 
			{
				case SKILL_INCREASE_GLOBAL_DAMAGE:
					skill.iconName = "talent_damage";					
					skill.info = skill.info.replace("$n", String(skill.args[0]) + "%");
					break;
				case SKILL_DECREASE_MONSTER_ARMOR:					
					skill.iconName = "talent_armor";
					skill.info = skill.info.replace("$n",String(skill.args[0])+"%");
					break;
				case SKILL_INCREASE_MONSTER_COINS:
					skill.iconName = "talent_money";
					skill.info = skill.info.replace("$n",String(skill.args[0])+"%");
					break;
				case SKILL_INCREASE_STUN_CHANCE:
					skill.iconName = "talent_stun";
					skill.info = skill.info.replace("$n",String(skill.args[0])+"%");
					break;
				case SKILL_EMPOWER_HERO:
					skill.iconName = "talent_empower_"+Assets.heroNames[skill.subtype];
					skill.info = skill.info.replace("$hero",Language.GetString("Hero"+skill.subtype,Language.CASE_GENETIVE));
					skill.info = skill.info.replace("$n",String(skill.args[0])+"%");
					break;
				case SKILL_HEAL_CHANCE:
					skill.iconName = "talent_heal";
					skill.info = skill.info.replace("$n",String(skill.args[0])+"%");
					break;
				case SKILL_PATHFINDER:
					skill.iconName = "talent_pathfinder";
					skill.info = skill.info.replace("$n1",String(skill.args[0])+"%");
					skill.info = skill.info.replace("$n2",String(skill.args[1])+"%");					
					break;	
				case SKILL_MERCENARY:
					skill.iconName = "talent_mercenary";
					skill.info = skill.info.replace("$n",String(skill.args[0])+"%");
					break;	
			}
			
			//for (var i: int = 0; i < dataObject.args.length; i++)
			//{
				//skill.args.push(dataObject.args[i]);
			//}
			
			
			return skill;
		}
		
	}

}