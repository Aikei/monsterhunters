package logic 
{
	import starling.events.Event;
	/**
	 * ...
	 * @author Aikei
	 */
	public class TalentEffects 
	{
		static public var heroDpsIncrease: Number = 0;
		static public var armorDecrease: Number = 0;
		static public var overallGoldIncrease: Number = 0;
		static public var coinsIncrease: Number = 0;		
		static public var empowerHero: Array = new Array;
		static public var poisonDurationDecrease: Number = 0;
		static public var healChance: Number = 0;		
		static public var addCoinChance: Number = 0;
		static public var stunDuration: Number = 2;
		static public var stunChance: Number = 1;
		static public var rareChanceIncrease: Number = 0;
		static public var eliteChanceIncrease: Number = 0;
		static public var questRewardsIncrease: Number = 0;
		
		static public function Init():void 
		{
			World.gWorld.addEventListener("skillPicked", OnSkillPicked);
		}
		
		static public function OnSkillPicked(event: Event): void 
		{
			var skill: Skill = Skill(event.data);
			switch (skill.type) 
			{
				case Skill.SKILL_INCREASE_GLOBAL_DAMAGE:
					heroDpsIncrease += skill.args[0];
					break;
					
				case Skill.SKILL_DECREASE_MONSTER_ARMOR:
					var addAmount: Number = Number(skill.args[0]);
					armorDecrease += addAmount-((addAmount * armorDecrease) / 100);
					break;
					
				case Skill.SKILL_INCREASE_MONSTER_COINS:
					coinsIncrease += skill.args[0];
					break;
					
				case Skill.SKILL_HEAL_CHANCE:
					healChance += skill.args[0];
					break;
					
				case Skill.SKILL_INCREASE_STUN_CHANCE:
					stunChance += skill.args[0];
					break;
					
				case Skill.SKILL_EMPOWER_HERO:
					empowerHero[skill.subtype] += skill.args[0];
					break;
					
				case Skill.SKILL_PATHFINDER:
					rareChanceIncrease += skill.args[0];
					eliteChanceIncrease += skill.args[1];
					break;
					
				case Skill.SKILL_MERCENARY:
					questRewardsIncrease += skill.args[0];
					break;
			}
		}
		
		static public function CreateFromDataObject(dataObject: Object): void 
		{
			heroDpsIncrease = dataObject.heroDpsIncrease;
			armorDecrease = dataObject.armorDecrease;
			coinsIncrease = dataObject.coinsIncrease;
			addCoinChance = dataObject.addCoinChance;
			overallGoldIncrease = dataObject.overallGoldIncrease;
			stunDuration = dataObject.stunDuration;
			poisonDurationDecrease = dataObject.poisonDurationDecrease;
			stunChance = dataObject.stunChance;
			healChance = dataObject.healChance;
			empowerHero = dataObject.empowerHero;
		}
		
	}

}