package logic 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class Weapon 
	{
		public static const WEAPON_WOODEN_SWORD: int = 0;
		public static const WEAPON_APPRENTICE_BLADE: int = 1;
		public static const WEAPON_KNIGHTS_SWORD: int = 2;
		public static const AMETHYST_BLADE: int = 3;
		public static const ASSAULT_SWORD: int = 4;
		public static const BONEMOURN: int = 5;
		public static const INCINERATOR: int = 6;
		
		public static const NUMBER_OF_WEAPONS: int = 7;
		
		public var damage: Damage = null;
		//public var levelUpDamageIncrese: BigInt = null;
		public var buyCost: BigInt = BigInt.shStr2BigInt("0");
		public var levelUpCost: BigInt = null;
		//public var levelUpCostIncrese: BigInt = null;
		public var level: BigInt = BigInt.shStr2BigInt("1");
		public var type: int = -1;
		public var nextLevelWeapon: Weapon = null;
		public var animated: Boolean = false;
		
		public function Copy(): Weapon 
		{
			var n: Weapon = new Weapon;
			n.type = type;			
			n.damage = damage.Copy();
			n.buyCost = BigInt.dup(buyCost);
			n.levelUpCost = BigInt.dup(levelUpCost);			
			n.level = BigInt.dup(level);
			n.nextLevelWeapon = this.nextLevelWeapon;
			return n;
		}
		
		public function GetName(): String 
		{
			return Language.GetString("Weapon" + String(type));
		}
		
		static public function CreateFromDataObject(dataObject: Object): Weapon 
		{
			var w: Weapon = new Weapon;
			w.type = dataObject.type;
			if (w.type === INCINERATOR)
				w.animated = true;
			w.damage = new Damage(BigInt.shStr2BigInt(dataObject.damage.value), dataObject.damage.type);
			w.buyCost = BigInt.shStr2BigInt(dataObject.buyCost);
			w.levelUpCost = BigInt.shStr2BigInt(dataObject.levelUpCost);
			w.level = BigInt.shStr2BigInt(dataObject.level);
			if (dataObject.nextLevelWeapon)
				w.nextLevelWeapon = Weapon.CreateFromDataObject(dataObject.nextLevelWeapon);
			return w;
		}
	}

}