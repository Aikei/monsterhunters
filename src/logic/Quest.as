package logic 
{
	import languages.BossNamePhrase;
	/**
	 * ...
	 * @author Aikei
	 */
	public class Quest 
	{
		public static const QUEST_TYPE_KILL: int = 0;
		public static const QUEST_TYPE_KILL_BOSS: int = 1;		
		public static const QUEST_TYPE_REACH_LEVEL: int = 2;
		
		public static const MONSTER_TYPE_ANY: int = 0;
		public static const MONSTER_TYPE_SPECIFIC: int = 1;
		
		public var showProgress: Boolean = true;
		public var type: int;
		public var progress: Number;
		public var progressGoal: Number;
		public var monsterType: int;
		public var dungeonType: int = -1;
		public var numberOfMonstersToKill: int;
		public var monsterRarity: int;
		public var gloryReward: uint = 0;
		public var goldReward: BigInt = Misc.BIG_INT_ZERO;
		public var name: String;
		public var description: String;
		public var shortDescription: String;
		public var accepted: Boolean;
		public var uniqueTargetName: BossNamePhrase;
		
		public function Quest() 
		{
			
		}
		
		public static function CreateFromDataObject(dataObject: Object): Quest 
		{
			var q: Quest = new Quest;
			
			q.type = dataObject.t;
			q.progress = dataObject.p;
			q.progressGoal = dataObject.pg;
			q.gloryReward = dataObject.rw.gl;
			q.goldReward = BigInt.shStr2BigInt(dataObject.rw.go);
			
			switch (q.type) 
			{
				case QUEST_TYPE_KILL:
					q.monsterType = dataObject.ar.mt;
					q.numberOfMonstersToKill = dataObject.ar.num;
					q.dungeonType = dataObject.ar.dun;
					q.monsterRarity = dataObject.ar.r;
					break;
					
				case QUEST_TYPE_KILL_BOSS:
					if (dataObject.dt.bph)
						q.uniqueTargetName = new BossNamePhrase(dataObject.dt.bph.h, dataObject.dt.bph.d);
					else
						q.uniqueTargetName = Language.GetBossNamePhrase();
					q.dungeonType = dataObject.ar.dun;
					q.showProgress = false;
					break;
			}
			q.name = Language.GetQuestName(q);
			q.description = Language.GetQuestDescription(q);
			q.shortDescription = Language.GetShortQuestDescription(q);
			q.accepted = dataObject.ac;
			return q;
		}
	}

}