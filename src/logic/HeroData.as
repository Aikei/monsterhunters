package logic {
	/**
	 * ...
	 * @author Aikei
	 */
	
	public class HeroData 
	{
		public static const HERO_BARBARIAN: int = 0;
		public static const HERO_RANGER: int = 1;
		public static const HERO_MAGE: int = 2;
		public static const HERO_ROGUE: int = 3;
		public static const HERO_ENGINEER: int = 4;
		public static const HERO_PALADIN: int = 5;
		public static const NUMBER_OF_HEROES: int = 6;
		
		public static const portraitNames: Vector.<String>;
		
		private var _dps: Damage;	
		private var _damage: Damage;
		
		public var type: int;			
		public var level: BigInt = BigInt.shStr2BigInt("1");
		public var buyCost: BigInt = BigInt.shStr2BigInt("1000");
		public var levelUpCost: BigInt = BigInt.shStr2BigInt("2000");
		public var nextLevelHero: HeroData = null;
		
		private var _portraitName: String;
		
		public function get portraitName(): String 
		{
			return _portraitName;
		}
		
		public function get dps(): Damage 
		{
			return _dps;
		}
		
		public function set dps(d: Damage):void 
		{
			_dps = d;
			_damage = new Damage(BigInt.mult(d.value,Misc.BIG_INT_HERO_ATTACK_COOLDOWN),_dps.type);
			//_damage = new Damage(BigInt.divide(d.value, Misc.BIG_INT_DPS_DIVIDER),_dps.type);
		}
		
		private function set dpsvalue(value: BigInt): void 
		{
			_dps.value = value;
			_damage.value = BigInt.mult(_damage.value,Misc.BIG_INT_HERO_ATTACK_COOLDOWN);
			//_damage.value = BigInt.divide(value, Misc.BIG_INT_DPS_DIVIDER);
		}
		
		public function get damage(): Damage
		{
			return _damage;
		}
		
		public function Copy(): HeroData 
		{
			var hdata: HeroData = new HeroData;
			hdata.type = type;
			hdata.dps = dps.Copy();
			hdata.buyCost = BigInt.dup(this.buyCost);
			hdata.level = BigInt.dup(level);
			hdata.levelUpCost = BigInt.dup(levelUpCost);
			hdata._portraitName = _portraitName;
			hdata.nextLevelHero = nextLevelHero;
			return hdata;
		}		
		
		static public function CreateFromDataObject(dataObject: Object): HeroData 
		{
			var heroData: HeroData = new HeroData;
			heroData.type = dataObject.type;
			heroData.dps = new Damage(BigInt.shStr2BigInt(dataObject.dps.value), dataObject.dps.type);
			heroData.level = BigInt.shStr2BigInt(dataObject.level);
			heroData.buyCost = BigInt.shStr2BigInt(dataObject.buyCost);
			heroData.levelUpCost = BigInt.shStr2BigInt(dataObject.levelUpCost);
			heroData._portraitName = Assets.heroNames[heroData.type];
			//switch (heroData.type) 
			//{
				//case HERO_BARBARIAN:
					//heroData._portraitName = "barbarian";
					//break;
				//case HERO_RANGER:
					//heroData._portraitName = "ranger";
					//break;				
				//case HERO_MAGE:
					//heroData._portraitName = "mage";
					//break;				
				//case HERO_ROGUE:
					//heroData._portraitName = "rogue";
					//break;				
				//case HERO_ENGINEER:
					//heroData._portraitName = "engineer";
					//break;
				//case HERO_PALADIN:
					//heroData._portraitName = "paladin";
					//break;				
				//default:
					//heroData._portraitName = "barbarian";
					//break;				
			//}
			if (dataObject.nextLevelHero)
				heroData.nextLevelHero = HeroData.CreateFromDataObject(dataObject.nextLevelHero);
			return heroData;
		}
		
	}

}