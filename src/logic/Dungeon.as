package logic 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class Dungeon 
	{
		public static const MONSTERS_MAX: uint = 10;
		
		public static const DUNGEON_ORANGERY: int = 0;
		public static const DUNGEON_GRAVEYARD: int = 1;
		public static const DUNGEON_TWILIGHT_CRYPT: int = 2;
		public static const DUNGEON_GOBLIN_CITADEL: int = 3;
		
		public static const NUMBER_OF_DUNGEONS: int = 3;		
				
		public var level: uint;
		public var type: int;
		public var monstersKilled: uint = 0;
		public var name: String;
		public var current: Boolean = false;
		public var effects: Vector.<DungeonEffect>;
		public var possibleMonsters: Array;
		public var exhaustion: uint = 0;
		public var numberOfPlayers: uint = 0;
		public var lastLevel: Boolean = true;
		public var maxLevel: int;
		
		public function CopyFromDataObject(shellObject: Object, dungeonObject: Object):void 
		{
			this.level = shellObject.level;
			this.maxLevel = shellObject.maxLevel;
			this.type = shellObject.type;
			this.name = Language.GetString("Dungeon"+this.type);
			this.possibleMonsters = dungeonObject.possibleMonsters;
			this.monstersKilled = shellObject.monstersKilled;
			this.exhaustion = dungeonObject.exhaustion;
			this.numberOfPlayers = dungeonObject.playersInDungeon;
			this.effects = new Vector.<DungeonEffect>;
			for (var i: int = 0; i < dungeonObject.effects.length; i++)			
				this.effects.push(DungeonEffect.CreateFromDataObject(dungeonObject.effects[i],shellObject.type));
			if (shellObject.current === true)
				this.current = true;
		}
		
		public static function CreateFromDataObject(shellObject: Object, dungeonObject: Object): Dungeon 
		{
			var d: Dungeon = new Dungeon;
			d.CopyFromDataObject(shellObject, dungeonObject);
			return d;
		}
		
	}

}