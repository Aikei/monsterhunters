package logic 
{
	import starling.textures.Texture;
	/**
	 * ...
	 * @author Aikei
	 */
	public class Damage 
	{
		public static const DAMAGE_TYPE_NONE: int = 0;
		public static const DAMAGE_TYPE_SLASH: int = 1;
		public static const DAMAGE_TYPE_RANGED: int = 2;
		public static const DAMAGE_TYPE_SPELL: int = 3;
		public static const DAMAGE_TYPE_PIERCE: int = 4;
		public static const DAMAGE_TYPE_SIEGE: int = 5;
		public static const DAMAGE_TYPE_HOLY: int = 6;
		
		public var value: BigInt;
		public var adjustedValue: BigInt;
		public var type: int;
		public var portraitTextureName: String;
		public var effectsData: Object = new Object;
		
		public function Damage(value: BigInt, type: int) 
		{
			effectsData.wasEffectiveLastTime = false;
			effectsData.wasStunningLastTime = false;
			effectsData.wasHealingLastTime = false;
			
			this.value = BigInt.dup(value);
			this.adjustedValue = this.value;
			this.type = type;
			
			switch (type)
			{
				case DAMAGE_TYPE_NONE:
				case DAMAGE_TYPE_SLASH:
					portraitTextureName = "atk_slash";
					break;
				case DAMAGE_TYPE_RANGED:
					portraitTextureName = "atk_range";
					break;					
				case DAMAGE_TYPE_SPELL:
					portraitTextureName = "atk_spell";
					break;
				case DAMAGE_TYPE_PIERCE:
					portraitTextureName = "atk_pierce";
					break;
				case DAMAGE_TYPE_SIEGE:
					portraitTextureName = "atk_siege";
					break;
				case DAMAGE_TYPE_HOLY:
					portraitTextureName = "atk_holy";
					break;					
				default:
					portraitTextureName = "";
					break;
			}
		}
		
		public function ResetEffects():void 
		{
			effectsData.wasEffectiveLastTime = false;
			effectsData.wasStunningLastTime = false;
			effectsData.wasHealingLastTime = false;			
		}
		
		public function Copy(): Damage 
		{
			return new Damage(value, type);
		}
		
	}

}