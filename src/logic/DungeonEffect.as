package logic 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class DungeonEffect 
	{
		public static const EFFECT_MONSTERS_WITH_TRAITS: int = 0;
		public static const NUMBER_OF_EFFECTS: int = 1;			
		
		public var type: int;
		public var args: Array;
		public var explanation: String;
		
		public function DungeonEffect()
		{
			
		}
		
		static public function CreateFromDataObject(dataObject: Object,dungeonType: int): DungeonEffect
		{
			var ef: DungeonEffect = new DungeonEffect;
			ef.type = dataObject.type;
			ef.args = dataObject.args;
			
			if (ef.type === EFFECT_MONSTERS_WITH_TRAITS)
			{
				ef.explanation = Language.GetString("ExplainDungeonEffect" + ef.type + dungeonType + ef.args[0]);
				ef.explanation = ef.explanation.replace("$r",Language.GetString("ExplainEffectRarity" + ef.args[1]));
				//ef.explanation = ef.explanation.replace("$t",Language.GetString("Trait" + ef.args[0]));
			}
			return ef;
		}
		
	}

}