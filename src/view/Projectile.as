package view 
{
	import d_hentity.HEntity;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	import flash.utils.setTimeout;
	import view.gameviewelements.misc.DisappearingParticle;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class Projectile extends HEntity 
	{
		private var _image: Image;
		private var _onDieTexture: Texture = null;
		
		public function Projectile(texture: Texture, x: Number, y: Number, onDieTexture: Texture = null) 
		{
			super(true);
			this.touchable = false;
			_image = new Image(texture);
			//pivotX = -texture.width/2;
			//pivotY = -texture.height/2;
			this.x = x;
			this.y = y;
			alignPivot();
			//this.x = x - _image.width/2;
			//this.y = y - _image.height / 2;
			_onDieTexture = onDieTexture;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_image);
		}
		
		public function DirectTo(toX: Number, toY: Number):void 
		{
			var angle = Math.atan2(toY - y, toX - x);
			rotation = Misc.PI/2+angle;			
		}
		
		public function OnTimeout():void 
		{
			if (!_onDieTexture)
			{
				Destroy();
			}
			else
			{
				StopMyMovementAndAcceleration();
				StopMyRotation();				
				_image.visible = false;
				var particle: DisappearingParticle = new DisappearingParticle(1.0, _onDieTexture, _onDieTexture.width, _onDieTexture.height, 0, 0);
				rotation = 0;
				addChild(particle);
				setTimeout(Destroy, 1000);
			}
		}
		
		public function DieWithin(timeInMsecs: Number):void 
		{
			setTimeout(OnTimeout, timeInMsecs);
		}
		
		public function SendTo(toX: Number, toY: Number, speed: Number, directed: Boolean = false):void 
		{
			ApplyAbsVelocity(new Vec2(toX, toY), speed);
			if (directed)
				DirectTo(toX, toY);
		}
		
	}

}