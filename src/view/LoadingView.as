package view 
{
	import com.greensock.TweenLite;
	import flash.geom.Rectangle;
	import flash.utils.setTimeout;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import d_hentity.HEntity;
	import starling.utils.Color;
	import system.HSounds;
	import view.gameviewelements.HText;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class LoadingView extends HEntity 
	{
		public static var LOADING_TEXT_PLUS_X: Number = 0;
		public static var LOADING_TEXT_PLUS_Y: Number = 0;
		
		private static var LOADING_TEXT_CENTER_X: Number;
		private static var LOADING_TEXT_CENTER_Y: Number;
		
		private static const DOWNLOAD_QUAD_WIDTH: Number = 400;
		private static const DOWNLOAD_QUAD_HEIGHT: Number = 40;
		
		
		private var _leftDoor: Image = new Image(Assets.loadingScreenAtlas.getTexture("loading_door_left"));
		private var _rightDoor: Image = new Image(Assets.loadingScreenAtlas.getTexture("loading_door_right"));
		private var _loadingBar: Image = new Image(Assets.loadingScreenAtlas.getTexture("loading_bar"));
		
		//private var _image: Image = new Image(Assets.GetPermanentTexture("LOADING_SCREEN_TEXTURE"));
		//private var _downloadQuad: Quad = new Quad(DOWNLOAD_QUAD_WIDTH,DOWNLOAD_QUAD_HEIGHT);
		//private var _backgroundQuad: Quad = new Quad(DOWNLOAD_QUAD_WIDTH, DOWNLOAD_QUAD_HEIGHT, Color.BLACK);
		//private var _backgroundQuad2: Quad = new Quad(DOWNLOAD_QUAD_WIDTH+4, DOWNLOAD_QUAD_HEIGHT+4, Color.GRAY);
		//private var _overlayQuad: Quad = new Quad(DOWNLOAD_QUAD_WIDTH, DOWNLOAD_QUAD_HEIGHT, Color.BLACK);
		
		private var _loadingText: HText = new HText(Language.GetString("Loading"), "bbold18", -1);
		private var _newText: HText = new HText(Language.GetString("Loading"), "bbold18", -1);
		private var _oldText: HText = new HText(Language.GetString("Loading"), "bbold18", -1);
		private var _swapText: HText;
		
		private var _whiteQuad: starling.display.Quad = new starling.display.Quad(Preferences.SCREEN_WIDTH, Preferences.SCREEN_HEIGHT, 0);
		private var _loadingDungeon: Boolean = false;
		private var _decodingSounds: Boolean = false;
		private var _downloadingData: Boolean = false;
		private var _textHolder: HEntity = new HEntity;
		private var _loadingPhrases: Vector.<String>;
		private var _currentPhrase: int = -1;
		private var _newPhraseTimeout: uint = 0;
		private var _tweenToY: Number = 0;
		//private var _dbgQuad: Quad;
		private static const DOWNLOAD_QUAD_CENTER_X: Number = Misc.SCREEN_WIDTH / 2;
		private static const DOWNLOAD_QUAD_CENTER_Y: Number = Misc.SCREEN_HEIGHT / 2 + 200;		
		
		public function LoadingView() 
		{
			super();
			
			_loadingPhrases = Language.loadingPrases;
			//_image.width = Misc.SCREEN_WIDTH;
			//_image.height = Misc.SCREEN_HEIGHT;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			
			_rightDoor.x = _leftDoor.width;
			_loadingBar.x = 259;
			_loadingBar.y = 485;
			
			_textHolder.x = _loadingBar.x;
			_textHolder.y = _loadingBar.y;
			_textHolder.width = _loadingBar.texture.width;
			_textHolder.height = _loadingBar.texture.height;
			_textHolder.clipRect = new Rectangle(0, 0, _loadingBar.texture.width, _loadingBar.texture.height);
			//_dbgQuad = new Quad(_loadingBar.texture.width, _loadingBar.texture.height);
			//_dbgQuad.x = _textHolder.x;
			//_dbgQuad.y = _textHolder.y;
			//_dbgQuad.alpha = 0.5;
			
			//LOADING_TEXT_CENTER_X = _loadingBar.x + _loadingBar.width / 2;
			//LOADING_TEXT_CENTER_Y = _loadingBar.y + _loadingBar.height / 2;
			//Misc.CenterObject(_loadingText, LOADING_TEXT_CENTER_X, LOADING_TEXT_CENTER_Y);
			
			LOADING_TEXT_CENTER_X = _loadingBar.texture.width / 2 + LOADING_TEXT_PLUS_X;
			LOADING_TEXT_CENTER_Y = _loadingBar.texture.height / 2 + LOADING_TEXT_PLUS_Y;			
			_tweenToY = Math.round(LOADING_TEXT_CENTER_Y - _newText.height / 2-4)
			
			
			touchable = false;
			visible = true;
			
			//Misc.CenterObject(_backgroundQuad, DOWNLOAD_QUAD_CENTER_X, DOWNLOAD_QUAD_CENTER_Y);
			//Misc.CenterObject(_backgroundQuad2, DOWNLOAD_QUAD_CENTER_X, DOWNLOAD_QUAD_CENTER_Y);
			//Misc.CenterObject(_downloadQuad, DOWNLOAD_QUAD_CENTER_X, DOWNLOAD_QUAD_CENTER_Y);
			//Misc.CenterObject(_overlayQuad, DOWNLOAD_QUAD_CENTER_X, DOWNLOAD_QUAD_CENTER_Y);
			//Misc.CenterObject(_loadingText.textField, DOWNLOAD_QUAD_CENTER_X, DOWNLOAD_QUAD_CENTER_Y);

			//_downloadQuad.setVertexColor(0, Color.RED);
			//_downloadQuad.setVertexColor(2, Color.RED);
			//_downloadQuad.setVertexColor(1, Color.GREEN);
			//_downloadQuad.setVertexColor(3, Color.GREEN);
			
			World.gWorld.addEventListener("downloadProgress", OnDownloadProgress);
			World.gWorld.addEventListener("dungeonLoadProgress", OnDungeonLoadProgress);
			World.gWorld.addEventListener("soundsDecodingProgress", OnSoundsDecodingProgress);
			NewPhrase();
		}
		
		public function NewPhrase():void 
		{
			var n: int;
			do
			{
				n = Misc.Random(0, _loadingPhrases.length);
			}while (n == _currentPhrase);			
			_currentPhrase = n;
			
			_swapText = _oldText;
			_oldText = _newText;
			_newText = _swapText; 
			
			_newText.text = _loadingPhrases[_currentPhrase];
			_newText.x = Math.round(LOADING_TEXT_CENTER_X - _newText.width / 2);
			_newText.y = Math.round(_tweenToY - _newText.height / 2) - 50;
			
			TweenLite.to(_newText, 1, { y : _tweenToY } );
			TweenLite.to(_oldText, 1, { y : _tweenToY+50 } );
			_newPhraseTimeout = setTimeout(NewPhrase, 5000);
		}
		
		public function OpenDoors():void 
		{
			_loadingBar.visible = false;
			_loadingText.visible = false;
			_textHolder.visible = false;
			HSounds.PlaySound("DoorsOpen");
			TweenLite.to(_leftDoor, 1.0, { x: -_leftDoor.width, y: 0, onComplete: function ():void 
			{
				_leftDoor.visible = false;
				_rightDoor.visible = false;
			} } );			
			TweenLite.to(_rightDoor, 1.0, { x: Misc.SCREEN_WIDTH, y: 0 } );	
			TweenLite.to(_whiteQuad, 3.0, { alpha: 0, onComplete: function ():void 
			{
				visible = false;
			} } );
		}
		
		private function OnSoundsDecodingProgress(e:Event):void 
		{
			if (!_decodingSounds)
			{
				_decodingSounds = true;
				_downloadingData = false;
				_loadingDungeon = false;
				SetLoadingText(Language.GetString("DecodingSounds"));
			}
			var portion: Number = Number(e.data.loaded) / Number(e.data.total);
			SetProgressBar(portion);
		}
		
		private function SetLoadingText(text: String):void 
		{
			_loadingText.textField.text = text;
			Misc.CenterObject(_loadingText, LOADING_TEXT_CENTER_X, LOADING_TEXT_CENTER_Y);
		}
		
		private function OnDungeonLoadProgress(e:Event):void 
		{
			if (!_loadingDungeon)
			{
				_decodingSounds = false;
				_downloadingData = false;
				_loadingDungeon = true;
				SetLoadingText(Language.GetString("LoadingDungeon"));
			}
			var portion: Number = Number(e.data.loaded) / Number(e.data.total);
			SetProgressBar(portion);		
		}
		
		protected function OnDownloadProgress(event: Event): void 
		{
			if (!_downloadingData)
			{
				_decodingSounds = false;
				_downloadingData = true;
				_loadingDungeon = false;
				SetLoadingText(Language.GetString("Loading"));
			}
			var portion: Number = Number(event.data.loaded) / Number(event.data.total);
			SetProgressBar(portion);
		}
		
		protected function SetProgressBar(portion: Number): void
		{
			_loadingBar.width = _loadingBar.texture.width * portion;
			_loadingBar.setTexCoordsTo(1, portion, 0);
			_loadingBar.setTexCoordsTo(3, portion, 1);				
		}
		
		protected function OnAddedToStage(event: Event): void 
		{
			addChild(_whiteQuad);
			addChild(_leftDoor);
			addChild(_rightDoor);
			addChild(_loadingBar);
			
			addChild(_textHolder);
			_textHolder.addChild(_oldText);
			_textHolder.addChild(_newText);			
		}
		
	}

}