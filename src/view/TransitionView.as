package view 
{
	import d_hentity.HEntity;
	import flash.utils.clearTimeout;
	import hevents.HEventDungeonChanged;
	import logic.Dungeon;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.Color;
	import flash.utils.setTimeout;
	import starling.text.TextFieldAutoSize;
	import hevents.*;
	import system.Analytics;
	import system.HSounds;
	import view.gameviewelements.MessageWindow.MessageWindow;
	
	import com.greensock.*;
	import com.greensock.easing.*;	
	/**
	 * ...
	 * @author Aikei
	 */
	public class TransitionView extends HEntity 
	{
		public static var DOORS_CLOSE_TIME: Number = 1.0;	//! set in script
		
		public static const TEXT_X: Number = Misc.SCREEN_WIDTH / 2;
		public static const TEXT_Y: Number = Misc.SCREEN_HEIGHT / 2;
		
		private var _leftDoor: Image = new Image(Assets.doorsAtlas.getTexture("door_left"));
		private var _rightDoor: Image = new Image(Assets.doorsAtlas.getTexture("door_right"));
		private var _dungeonNameText: TextField = new TextField(0, 0, "", "bbold18", 18, Color.WHITE);
		private var _centerx: int = Misc.SCREEN_WIDTH / 2;
		private var _dungeonsList: Vector.<Dungeon>;
		private var _dungeonTypeToTransit: int;
		private var _whiteQuad: starling.display.Quad = new starling.display.Quad(Preferences.SCREEN_WIDTH, Preferences.SCREEN_HEIGHT, 0);
		private var _firstLoad: Boolean = true;
		private var _whiteQuadTimeout: uint = 0;
		
		public function TransitionView() 
		{
			super();
			touchable = false;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			_leftDoor.x = -_leftDoor.width;
			_leftDoor.y = 0;
			_rightDoor.x = Misc.SCREEN_WIDTH;
			_whiteQuad.x = 0;
			_whiteQuad.y = 0;
			_whiteQuad.visible = false;
			_dungeonNameText.x = TEXT_X - _dungeonNameText.width / 2;
			_dungeonNameText.y = TEXT_Y - _dungeonNameText.height / 2;
			Misc.RoundPosition(_dungeonNameText);
			_dungeonNameText.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
			_dungeonNameText.alpha = 0;
			
			visible = false;
			
			World.gWorld.addEventListener(HEventDungeonChanged.H_EVENT_DUNGEON_CHANGED, OnDungeonChanged);
			World.gWorld.addEventListener("N_StartDungeonTransition", OnDungeonTransitionStarted);
			World.gWorld.addEventListener(L_HEventDungeonsListInitialized.L_HEVENT_DUNGEONS_LIST_INITIALIZED, OnDungeonsListInialized);
			World.gWorld.addEventListener("dungeonLoadDone", DungeonLoadDone);
		}
		
		private function DungeonLoadDone(e:Event):void 
		{
			if (!_firstLoad)
				HSounds.SetDungeonTheme(_dungeonTypeToTransit);
			else
				_firstLoad = false;
			Analytics.TrackPage("/game_screen/main_window");
			_dungeonNameText.visible = false;
			HSounds.PlaySound("DoorsOpen");
			TweenLite.to(_leftDoor, 1.0, { x: -_leftDoor.width, y: 0, onComplete: function ():void 
			{
				World.gWorld.dispatchEventWith("doorsOpened");
			} } );			
			TweenLite.to(_rightDoor, 1.0, { x: Misc.SCREEN_WIDTH, y: 0 });
						
 			//_whiteQuad.visible = true;
			//_whiteQuad.alpha = 1;
			TweenLite.to(_whiteQuad, 3, { alpha: 0 } );
			_whiteQuadTimeout = setTimeout(function (quad: starling.display.Quad, v: TransitionView):void 
			{
				_whiteQuadTimeout = 0;
				quad.visible = false;				
				v.visible = false;
			},3000, _whiteQuad, this);
		}
		
		public function Init():void 
		{
			
		}
		
		protected function OnDungeonsListInialized(event: L_HEventDungeonsListInitialized): void 
		{
			_dungeonsList = event.dungeonsList;
		}
		
		protected function OnDungeonTransitionStarted(event: Event):void 
		{
			Analytics.TrackPage("/dungeon_transition_screen");
			if (_whiteQuadTimeout > 0)
				clearTimeout(_whiteQuadTimeout);
			_dungeonTypeToTransit = event.data.dungeonType;
			if (event.data.type == 0)
			{				
				_dungeonNameText.text = _dungeonsList[_dungeonTypeToTransit].name; //Language.GetString("Dungeon"+_dungeonsList[_dungeonTypeToTransit].type);
				_dungeonNameText.x = TEXT_X - _dungeonNameText.width / 2;
				_dungeonNameText.y = TEXT_Y - _dungeonNameText.height / 2;
				Misc.RoundPosition(_dungeonNameText);
			}				
			visible = true;
			HSounds.PlaySound("DoorsClose");
			_whiteQuad.visible = true;
			_whiteQuad.alpha = 0;
			TweenLite.to(_whiteQuad, DOORS_CLOSE_TIME, { alpha: 1 } );
			TweenLite.to(_leftDoor, DOORS_CLOSE_TIME, { x: 0, y: 0, ease: Bounce.easeOut } );			
			TweenLite.to(_rightDoor, DOORS_CLOSE_TIME, { x: Misc.SCREEN_WIDTH - _rightDoor.width, y: 0, ease: Bounce.easeOut } );	
			setTimeout(OnTweenHalfEnd, 750);
			setTimeout(OnTweenEnd, DOORS_CLOSE_TIME*1000, event.data);
		}
		
		private function OnTweenHalfEnd():void 
		{
			
		}
		
		protected function OnTweenEnd(data: Object):void 
		{			
			if (data.type == -1)
			{
				var message: MessageWindow = new MessageWindow(data.message, "bregular14", -1, Color.WHITE);
				message.x = TEXT_X - message.width / 2;
				message.y = TEXT_Y - message.height / 2;
				message.alpha = 0;
				addChild(message);
				TweenLite.to(message, 0.5, { alpha: 1 } );
			}
			else
			{
				//OnDoorsClosed();
				_dungeonNameText.visible = true;
				_dungeonNameText.alpha = 0;
				TweenLite.to(_dungeonNameText, 0.5, { alpha: 1 } );
				setTimeout(World.gWorld.dispatchEvent, 750, new Event("doorsClosed"));
			}
		}
		
		//protected function OnDoorsClosed():void 
		//{
			//Assets.LoadDungeonResources(_dungeonsList[_dungeonTypeToTransit]);
		//}
		
		protected function OnDungeonChanged(event: HEventDungeonChanged): void 
		{
			
			//TweenLite.to(_leftDoor, 0.75, { x: -_leftDoor.width, y: 0 } );
			//TweenLite.to(_rightDoor, 0.75, { x: Misc.SCREEN_WIDTH, y: 0 } );
			//setTimeout(World.gWorld.dispatchEvent, 750, new Event("doorsOpened"));
			//setTimeout(SetInvisible, 750);
		}
		
		protected function SetInvisible():void 
		{
			visible = false;
		}
		
		protected function OnAddedToStage(event: Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_whiteQuad);
			addChild(_leftDoor);
			addChild(_rightDoor);
			addChild(_dungeonNameText);			
		}
		
	}

}