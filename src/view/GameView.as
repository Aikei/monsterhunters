package view 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import com.greensock.easing.ElasticOut;
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.utils.*;
	import logic.HeroData;
	import logic.local.ActualHero;
	import logic.Quest;
	import starling.core.Starling;
	import starling.display.MovieClip;
	import starling.events.EnterFrameEvent;
	import starling.events.TouchEvent;
	import starling.textures.Texture;
	import starling.textures.TextureSmoothing;
	import starling.utils.Color;
	import logic.local.LocalLogic;
	import starling.display.BlendMode;
	import system.Analytics;
	import system.HCursor;
	import system.HSounds;
	import view.gameviewelements.BackgroundProgressWindow;
	import view.gameviewelements.dungeonpanel.DungeonsWindow;
	import view.gameviewelements.HButton;
	import view.gameviewelements.heroshowpanel.DamageAnimText;
	import view.gameviewelements.HText;
	import view.gameviewelements.HToggleButton;
	import view.gameviewelements.leaderboard.LeaderboardWindow;
	import view.gameviewelements.MessageWindow.MessageWindow;
	import view.gameviewelements.misc.Coin;
	import view.gameviewelements.misc.FallingThing;
	import view.gameviewelements.misc.GloryThing;
	import view.gameviewelements.misc.ScrollThing;
	import view.gameviewelements.misc.Settings;
	import view.gameviewelements.misc.TutorialHolder;
	import view.gameviewelements.q.QuestWindow;
	import view.gameviewelements.misc.RibbonImage;
	import view.gameviewelements.MonsterSpeechBubble;
	import view.gameviewelements.rename.RenamePanel;
	import view.gameviewelements.smoke.SmokeCreator;
	import view.gameviewelements.TimerWindow;
	import view.MonsterSprite;
	import logic.MonsterData;
	import starling.display.Image;
	import starling.events.Event;
	import hevents.*;
	import view.gameviewelements.heroshowpanel.HeroShowPanel;
	import view.gameviewelements.MonsterHpPanel;
	import view.gameviewelements.Window;
	import d_hentity.HEntity;
	
	
	public class GameView extends HEntity
	{
		public static var HERO_PROJECTILE_TIME: Number = 0.5;		//! may be set in script
		public static var SHOW_DAMAGE_TEXT: Boolean = true;			//! may be set in script
		public static var SHOW_PROJECTILES: Boolean = false; 		//! may be set in script
		public static var PROJECTILE_ON_STUN: Boolean = false; 		//! may be set in script		
		public static var STUN_FPS: Number = 15;					//! may be set in script
		public static var LEFT_HOLDER_X: Number = -4;				//! may be set in script
		public static var LEFT_HOLDER_Y: Number = 0;				//! may be set in script
		public static var CLICK_RECORD_TIME: Number = 6;			//! may be set in script
		public static var MAX_CLICKS_PER_SECOND: Number = 7;		//! may be set in script
		
		public static var TIME_UP_IMAGE_PLUS_X: Number = 0;
		public static var TIME_UP_IMAGE_Y: Number = 133;		
		public static var TIME_UP_IMAGE_SLIDE_FROM_Y: Number = 300;
		public static var TIME_UP_IMAGE_APPEAR_TIME: Number = 0.5;
		public static var TIME_UP_IMAGE_DISAPPEAR_TIME: Number = 0.5;
		public static var TIME_UP_IMAGE_REMAIN_TIME: Number = 0.5;
		
		private var _bg: Image = null;
		private var _fg: Image = null;
		//private var _mid: Image = null;
		
		public var tipHolder: HEntity = new HEntity;
		
		private var _window: Window;
		private var _dungeonsWindow: DungeonsWindow;
		private var _leaderboardWindow: LeaderboardWindow;
		private var _backgroundProgressWindow: BackgroundProgressWindow;
		
		private var _toppestHolder: HEntity;
		private var _smokeCreator: SmokeCreator;
		private var _monsterHpPanel: MonsterHpPanel;
		private var _heroShowPanel: HeroShowPanel;
		private var _renamePanel: RenamePanel = new RenamePanel;
		private var _cursor: HCursor = new HCursor;
		//private var _timerWindow: TimerWindow;
		private var _attackAnim: Image;
		private var _bottomHolder: HEntity;
		private var _topHolder: HEntity;
		private var _bottomestHolder: HEntity;
		private var _timeUpImage: RibbonImage;
		private var _levelCompleteImage: RibbonImage;
		private var _timer: Timer = null;
		private var _coinsHolder: HEntity = new HEntity;
		private var _clicksCounter: Number = 0;
		private var _cps: Number = 0;
		private var _timeCountingClicks: Number = 0;
		private var _settings: Settings = new Settings;
		//private var _muteMusicButton: HToggleButton = new HToggleButton;
		//private var _muteSoundsButton: HToggleButton = new HToggleButton;
		
		private var _leftHolder: HEntity = new HEntity;
		private var _monsterHolder: HEntity = new HEntity;
		private var _monster: MonsterSprite;		
		private var _coinsData: Object;
		private var _timeOutId: uint = 0;
		private var _baseAttackAnimWidth: Number;
		private var _baseAttackAnimHeight: Number;
		private var _dieAnimationEndTimeout: uint = 0;
		private var _inited: Boolean = false;
		private var _windowMoveTime: Number = -1;
		private var _created: Boolean = false;
		private var _glory: uint;
		private var _questWindow: QuestWindow = new QuestWindow;
		private var _memberElements: Vector.<HEntity> = new Vector.<HEntity>;
		private var _tutorial: TutorialHolder;
		
		static public var gView: GameView = null;
		
		public function GetCoinCounterCenter(): Point 
		{
			return _window.GetCoinCounterCenter();
		}
		
		public function set commonMonsterScale(scale: Number):void 
		{
			if (MonsterSprite.sCommonMonsterScale != scale && _monster && _monster.rarity == MonsterData.RARITY_COMMON)
			{
				_monster.ChangeRarityScale(scale);
			}
			MonsterSprite.sCommonMonsterScale = scale;			
		}
		
		public function set rareMonsterScale(scale: Number):void 
		{
			if (MonsterSprite.sRareMonsterScale != scale && _monster && _monster.rarity == MonsterData.RARITY_RARE)
			{
				_monster.ChangeRarityScale(scale);
			}
			MonsterSprite.sRareMonsterScale = scale;			
		}	
		
		public function set eliteMonsterScale(scale: Number):void 
		{
			if (MonsterSprite.sEliteMonsterScale != scale && _monster && _monster.rarity == MonsterData.RARITY_ELITE)
			{
				_monster.ChangeRarityScale(scale);
			}
			MonsterSprite.sEliteMonsterScale = scale;			
		}
		
		public function SetMonsterModelScale(model: String, scale: Number):void 
		{
			if (MonsterSprite.sMonsterModelScales[model] != scale && _monster.modelName == model)
			{
				_monster.ChangeModelScale(scale);
			}
			MonsterSprite.sMonsterModelScales[model] = scale;
		}		
		
		public function GameView() 
		{			
			MonsterSprite.sMonsterModelScales["dummy"] = 1;
			MonsterSprite.sMonsterModelScales["slime"] = 1;
			MonsterSprite.sMonsterModelScales["plant"] = 1;
			MonsterSprite.sMonsterModelScales["sapling"] = 1;
			MonsterSprite.sMonsterModelScales["bat"] = 1;
			MonsterSprite.sMonsterModelScales["shadow"] = 1;
			MonsterSprite.sMonsterModelScales["sentry"] = 1;
			
			gView = this;
			
			//addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			World.gWorld.addEventListener("N_NewMonster", OnEventNewMonster);
			World.gWorld.addEventListener(L_HEventMonsterKilled.H_EVENT_MONSTER_KILLED, OnEventMonsterKilled);
			World.gWorld.addEventListener("N_TimeUp", OnTimeUp);
			World.gWorld.addEventListener("createFlyingText", OnCreateFlyingText);
			World.gWorld.addEventListener("groupWindowDungeonsButtonPressed", OnGroupWindowDungeonsButtonPressed);
			World.gWorld.addEventListener("dungeonsWindowDungeonsButtonPressed", OnDungeonsWindowDungeonsButtonPressed);
			World.gWorld.addEventListener("setMonsterFps", OnSetMonsterFps);
			World.gWorld.addEventListener("tintMon", OnTintMonsterState);
			World.gWorld.addEventListener("N_LocalClickHit", OnClickHit);
			World.gWorld.addEventListener(L_HEventDungeonLevelSet.L_HEVENT_DUNGEON_LEVEL_SET, OnDungeonLevelSet);
			World.gWorld.addEventListener("N_NewMonster", OnNewMonster);
			World.gWorld.addEventListener("createAttackAnim", OnCreateAttackAnim);
			World.gWorld.addEventListener("createDamageText", OnCreateDamageText);
			World.gWorld.addEventListener("changeGroupNameButtonTriggered", ChangeGroupNameButtonTriggered);
			World.gWorld.addEventListener("squadNameChanged", OnSquadNameChanged);
			//World.gWorld.addEventListener("newQuest", OnNewQuest);
			World.gWorld.addEventListener("showBigCenterText", OnShowBigCenterText);
			World.gWorld.addEventListener("levelComplete", OnLevelComplete);
			World.gWorld.addEventListener("addFallingThing", OnAddFallingThing);
			World.gWorld.addEventListener("stunMonster", OnMonsterStunned);
			//addEventListener(EnterFrameEvent.ENTER_FRAME, OnEnterFrame);
		}
		
		private function OnMonsterStunned(e:Event):void 
		{
			Tweens.StunShake(_bg);
		}
			
		private function OnLevelComplete(e:Event):void 
		{
			HSounds.PlaySound("LevelComplete");
			_levelCompleteImage.visible = true;
			var str: String = Language.GetString("LevelComplete");
			str = str.replace("$n", e.data as int);
			_levelCompleteImage.visible = true;
			_levelCompleteImage.alpha = 1;
			_levelCompleteImage.text = str;
			var tl: TimelineLite = new TimelineLite;
			tl.from(_levelCompleteImage, 0.5, { y: TIME_UP_IMAGE_SLIDE_FROM_Y, alpha: 0, ease: ElasticOut } );	
			tl.to(_levelCompleteImage, 0.5, 
			{ alpha: 0, onComplete: function ():void 
				{
					_levelCompleteImage.visible = false;					
				}  
			}
			, "+=1.5");
		}
				
		private function OnShowBigCenterText(e:Event):void 
		{
			var text: HText = new HText(e.data.text as String, "Lora", 40, 0xffffff);
			text.x = Math.round(Preferences.SCREEN_WIDTH / 2 - text.width / 2);
			text.y = Math.round(Preferences.SCREEN_HEIGHT / 2 - text.height / 2);
			_toppestHolder.addChild(text);
			var tl: TimelineLite = new TimelineLite;
			tl.to(text, e.data.disappearDuration, { alpha: 0, onComplete: function ():void { text.Destroy(); } }, "+="+e.data.showDuration); 
		}
		
		//private function OnNewQuest(e:Event):void 
		//{
			//_questWindow.InitQuest(Quest(e.data));
		//}
		
		//private function InitQuest(quest: Quest):void 
		//{
					//
		//}
		
		private function OnSquadNameChanged(e:Event):void 
		{
			_renamePanel.Show(false);
		}
		
		private function ChangeGroupNameButtonTriggered(e:Event):void 
		{
			_renamePanel.Show(true,e.data as String);
		}
		
		private function OnCreateDamageText(e:Event):void 
		{
			if (SHOW_DAMAGE_TEXT)
			{
				var dt: DamageAnimText = DamageAnimText.Get(e.data);
				if (dt.newlyCreated)
				{
					_memberElements.push(dt);
					_bottomHolder.addChild(dt);
				}
			}
		}
		
		public function OnGlobalEnterFrame(event: EnterFrameEvent): void 
		{
			_timeCountingClicks += World.framePassedTime;
			if (_timeCountingClicks >= CLICK_RECORD_TIME)
			{				
				_cps = _clicksCounter / _timeCountingClicks;
				_timeCountingClicks = 0;
				_clicksCounter = 0;
				CheckCps();
			}
			if (_smokeCreator)
				_smokeCreator.OnGlobalEnterFrame(event);
			if (_window && _window.visible)
				_window.OnGlobalEnterFrame(event);
			if (_dungeonsWindow && _dungeonsWindow.visible)
				_dungeonsWindow.OnGlobalEnterFrame(event);
			for (var i: int = 0; i < _memberElements.length; i++)
			{
				if (_memberElements[i].visible)
					_memberElements[i].OnEnterFrame(event);
			}
			for (i = 0; i < _coinsHolder.numChildren; i++)
			{
				var child: FallingThing = FallingThing(_coinsHolder.getChildAt(i));
				if (child.visible)
				{
					child.OnEnterFrame(event);
				}
			}
		}
		
		public function OnGlobalTouch(event: TouchEvent): void 
		{
			//if (_smokeCreator)
				//_smokeCreator.OnGlobalTouch(event);
			if (_window && _window.visible)
				_window.OnGlobalTouch(event);
			if (_dungeonsWindow && _dungeonsWindow.visible)
				_dungeonsWindow.OnGlobalTouch(event);
			for (var i: int = 0; i < _coinsHolder.numChildren; i++)
			{
				var child: FallingThing = FallingThing(_coinsHolder.getChildAt(i));
				if (child.visible && child is ScrollThing)
					child.OnTouch(event);
			}
		}		
		
		protected function CheckCps(): void
		{
			if (_cps > MAX_CLICKS_PER_SECOND)
			{
				ShowMessageWindow(Language.GetString("ClickingTooMuch"),5);
				Preferences.clickingAllowed = false;
				setTimeout(function ():void 
				{
					Preferences.clickingAllowed = true;
				}, 30000);
			}
		}
		
		public function ShowMessageWindow(text: String, dieIn: Number = 0):void 
		{
			var win: MessageWindow = new MessageWindow(text);
			win.dieIn = dieIn;
			win.dieGently = true;
			Misc.CenterObject(win, Preferences.SCREEN_WIDTH / 2, Preferences.SCREEN_HEIGHT / 2);
			addChild(win);
		}
		
		private function OnCreateAttackAnim(e:Event):void 
		{
			var hero: ActualHero = e.data.hero as ActualHero;
			if (SHOW_PROJECTILES || (PROJECTILE_ON_STUN && hero.stunNextTime))
			{
				if (_monster)
				{
					var textureName: String = Assets.heroNames[hero.heroData.type] + "_atk_anim";
					var toX: Number = _monster.x + _monster.width / 2;
					var toY: Number = _monster.y + _monster.height / 2;
					toX += Misc.Random(0, 128) - 64;
					var p: Projectile = new Projectile(Assets.interfaceAtlas.getTexture(textureName), e.data.x, e.data.y, Assets.interfaceAtlas.getTexture('projectile_particle'));
					if (hero.heroData.type != HeroData.HERO_RANGER)
						p.ApplyRotation(Misc.PI);
					var dist: Number = new Vec2(p.x - toX, p.y - toY).GetLength();
					var speed: Number = dist / HERO_PROJECTILE_TIME;
					_toppestHolder.addChild(p);
					p.SendTo(toX, toY, speed, true);
					p.DieWithin(HERO_PROJECTILE_TIME*1000);
				}
			}
		}		
		
		//private function OnCreateAttackAnim(e:Event):void 
		//{			
			//if (_monster)
			//{
				//var textureName: String = Assets.heroNames[e.data.heroType] + "_atk_anim";
				////var globalMonster: Point = localToGlobal(new Point(_monster.x, _monster.y));
				//var toX: Number = _monster.x + _monster.width / 2;
				//var toY: Number = _monster.y + _monster.height / 2;
				//toX += Misc.Random(0, 64) - 32;
				//toY += Misc.Random(0, 64) - 32;
				//CreateFlyingImage(Assets.interfaceAtlas.getTexture(textureName), e.data.x, e.data.y, toX, toY, true);
			//}
		//}
		
		private function OnNewMonster(e:Event):void 
		{
			//_timeUpImage.visible = false;
			//_timeUpImage.alpha = 1;
		}
		
		private function OnDungeonLevelSet(e:L_HEventDungeonLevelSet):void 
		{
			MoveDungeonsWindowUp();
			_bg.texture = Assets.currentBgAtlas.getTexture("bg");
			_fg.texture = Assets.currentBgAtlas.getTexture("fg");
			_fg.width = _fg.texture.width;
			_fg.height = _fg.texture.height;
			if (Assets.bgLayoutData[e.dungeon.type].fgPos === "br")
			{
				_fg.x = Preferences.SCREEN_WIDTH - _fg.width;
				_fg.y = Preferences.SCREEN_HEIGHT - _fg.height;
			}
		}
		
		private function OnClickHit(event: Event): void 
		{
			_clicksCounter++;
			if (_timeOutId != 0)
				clearTimeout(_timeOutId);
			var s: Number = Misc.Random(0.25, 0.75);
			var r: Number = Misc.Random(0, Misc.PI);
			_attackAnim.visible = true;
			_attackAnim.width = _baseAttackAnimWidth*s;
			_attackAnim.height = _baseAttackAnimHeight*s;
			_attackAnim.rotation = r;
			_attackAnim.x = HCursor.GetCursor().x;
			_attackAnim.y = HCursor.GetCursor().y;
			_attackAnim.alignPivot();
			_timeOutId = setTimeout(OnAttackStopped, 100);
		}
		
		private function OnAttackStopped(): void
		{
			_attackAnim.visible = false;
		}
		
		private function OnTintMonsterState(event: Event): void
		{
			if (event.data.tint === undefined)
				return;
			var tintColor: uint = uint(event.data.tint);
			if (event.data.tint is String)
			{					
				if (event.data.tint.length === 10)
					tintColor = Misc.GetColorFrom8DigitHexColor(event.data.tint);
			}				
			if (event.data.state != undefined)
			{				
				_monster.Tint(event.data.state, tintColor);
			}
			else
			{
				for (var i: int = 0; i < MonsterSprite.NUMBER_OF_STATES; i++)
					_monster.Tint(i, tintColor);
			}
		}
		
		private function OnSetMonsterFps(event: Event):void 
		{
			MonsterSprite.BASE_STAND_FPS = event.data as Number;
			MonsterSprite.HARM_FPS = MonsterSprite.BASE_STAND_FPS * MonsterSprite.HARM_FPS_MULTIPLIER;
			MonsterSprite.X6_STAND_FPS = MonsterSprite.BASE_STAND_FPS * MonsterSprite.X6_MULTIPLIER;
			_monster.SetFps(event.data as Number);
		}
		
		private function CreateEveryThing():void 
		{
			MonsterSprite.StunMovie = new MovieClip(Assets.miscAtlas.getTextures("stun_000"),STUN_FPS);
			Starling.juggler.add(MonsterSprite.StunMovie);
			MonsterSprite.StunMovie.visible = false;
			
			_leftHolder.x = LEFT_HOLDER_X;
			_window  = new Window;
			_dungeonsWindow = new DungeonsWindow;
			_leaderboardWindow = new LeaderboardWindow;
			_leaderboardWindow.addEventListener("moveLeaderboardWindowOut", OnMoveLeaderboardWindowOutCommandReceived);
			_leaderboardWindow.addEventListener("moveLeaderboardWindowIn", OnMoveLeaderboardWindowInCommandReceived);
			
			_toppestHolder = new HEntity;
			_smokeCreator = new SmokeCreator;
			_monsterHpPanel = new MonsterHpPanel;
			_heroShowPanel = new HeroShowPanel;
			_attackAnim = new Image(Assets.interfaceAtlas.getTexture("atk"));
			_bottomHolder = new HEntity;
			_topHolder = new HEntity;
			_bottomestHolder = new HEntity;	
			_timeUpImage = new RibbonImage(Assets.interfaceAtlas.getTexture("timesup"), Language.GetString("MonsterRanAway"));
			_timeUpImage.topText = Language.GetString("Oops");
			_timeUpImage.visible = false;
			_timeUpImage.touchable = false;
			_timeUpImage.x = Math.round(MonsterSprite.START_X - _timeUpImage.image.texture.width / 2) + TIME_UP_IMAGE_PLUS_X;
			_timeUpImage.y = TIME_UP_IMAGE_Y;
			
			_levelCompleteImage = new RibbonImage(Assets.interfaceAtlas.getTexture("level_complete"),"",RibbonImage.TEXT_CENTER_X,24);
			_levelCompleteImage.visible = false;
			_levelCompleteImage.touchable = false;
			_levelCompleteImage.x = Math.round(MonsterSprite.START_X - _levelCompleteImage.image.texture.width / 2) + TIME_UP_IMAGE_PLUS_X;
			_levelCompleteImage.y = TIME_UP_IMAGE_Y;		
			
			_smokeCreator.x = MonsterSprite.START_X;
			_smokeCreator.y = MonsterSprite.START_Y;
			_attackAnim.visible = false;
			_attackAnim.smoothing = TextureSmoothing.NONE;
			_baseAttackAnimWidth = _attackAnim.texture.width;
			_baseAttackAnimHeight = _attackAnim.texture.height;
			
			//_muteMusicButton.x = 12;
			//_muteMusicButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("music_on"));
			//_muteMusicButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("music_on"));
			//_muteMusicButton.hoverSkin.filter = Filters.highlightFilter;			
			//_muteMusicButton.defaultSelectedSkin = new Image(Assets.interfaceAtlas.getTexture("music_off"));
			//_muteMusicButton.selectedHoverSkin = new Image(Assets.interfaceAtlas.getTexture("music_off"));
			//_muteMusicButton.selectedHoverSkin.filter = Filters.highlightFilter;
			//_muteMusicButton.validate();
			//_muteMusicButton.addEventListener(Event.CHANGE, OnMuteMusicButtonChange);
			//
			//_muteSoundsButton.x = 48;
			//_muteSoundsButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("sounds_on"));
			//_muteSoundsButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("sounds_on"));
			//_muteSoundsButton.hoverSkin.filter = Filters.highlightFilter;			
			//_muteSoundsButton.defaultSelectedSkin = new Image(Assets.interfaceAtlas.getTexture("sounds_off"));
			//_muteSoundsButton.selectedHoverSkin = new Image(Assets.interfaceAtlas.getTexture("sounds_off"));
			//_muteSoundsButton.selectedHoverSkin.filter = Filters.highlightFilter;
			//_muteSoundsButton.validate();
			//_muteSoundsButton.addEventListener(Event.CHANGE, OnMuteSoundsButtonChange);
			
			_backgroundProgressWindow = new BackgroundProgressWindow;
		}
		
		//private function OnMuteSoundsButtonChange(e:Event):void 
		//{
			//HSounds.muteSounds = _muteSoundsButton.isSelected;
		//}
		//
		//private function OnMuteMusicButtonChange(e:Event):void 
		//{
			//HSounds.muteMusic = _muteMusicButton.isSelected;
		//}
		
		//private function OnTimerTick(e:TimerEvent):void 
		//{
			//if (!_monster)
				//return;
			//var d: Number = Misc.Random(0, 100);
			//if (d < 50)
			//{
				//_monster.Speak();
			//}
		//}
		
		private function OnMoveLeaderboardWindowInCommandReceived(e:Event):void 
		{
			Analytics.TrackPage("/game_screen/leaderboard_window");
			MoveWindowIn(_leaderboardWindow);
			MoveWindowOut(_window);
			MoveWindowOut(_dungeonsWindow);
		}
		
		private function OnMoveLeaderboardWindowOutCommandReceived(e:Event):void 
		{
			Analytics.TrackPage("/game_screen/main_window");
			MoveWindowOut(_leaderboardWindow, true, false, LeaderboardWindow.EXTRUDE_Y);
			MoveWindowIn(_window);
		}
		
		public function Init(dataObject: Object):void 
		{
			_inited = true;
			
			var dungeonType: int = dataObject.game.dungeonShellsList[dataObject.game.currentDungeon].type;
			_bg = new Image(Assets.currentBgAtlas.getTexture("bg"));
			_bg.x = -18;
			_bg.blendMode = BlendMode.NONE;
			_bg.touchable = false;			
			
			_fg = new Image(Assets.currentBgAtlas.getTexture("fg"));
			_fg.touchable = false;
			
			if (Assets.bgLayoutData[dungeonType].fgPos === "br")
			{
				_fg.x = Preferences.SCREEN_WIDTH - _fg.width;
				_fg.y = Preferences.SCREEN_HEIGHT - _fg.height;
			}			
			if (!_created)
			{
				CreateEveryThing();
				if (stage)
					AddEverythingToStage();
				else
					addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
				_created = true;
			}
			_window.Init(dataObject.game);
			_leaderboardWindow.Init(dataObject.leaderboard);
			_backgroundProgressWindow.Init(dataObject.game);
		}		
				
		private function OnAddedToStage(event: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			if (_inited)
				AddEverythingToStage();
		}
		
		private function AddEverythingToStage():void 
		{
			addChild(_bg);	
			
			addChild(_bottomestHolder);
			//_bottomestHolder.addChild(_dungeonsWindow);	
			
			addChild(_leftHolder);
			_leftHolder.addChild(_monsterHolder);
			_leftHolder.addChild(_smokeCreator);			
			_leftHolder.addChild(_monsterHpPanel);			
			_leftHolder.addChild(_bottomHolder);
			_leftHolder.addChild(_heroShowPanel);
			addChild(MonsterSprite.StunMovie);
			//addChild(_muteMusicButton);
			//addChild(_muteSoundsButton);
			addChild(_dungeonsWindow);
			addChild(_window);
			
			if (World.SHOW_TUTORIAL)
			{
				_tutorial = new TutorialHolder;
				addChild(_tutorial);
			}				
			
			addChild(_timeUpImage);
			addChild(_levelCompleteImage);
			addChild(_topHolder);
			_topHolder.addChild(_coinsHolder);
			_topHolder.addChild(_attackAnim);
			addChild(_leaderboardWindow);
			addChild(_toppestHolder);
			addChild(_settings);
			addChild(_questWindow);
			addChild(_fg);
			
		
			
			addChild(_renamePanel);
			addChild(_backgroundProgressWindow);			
			addChild(_cursor);
			addChild(tipHolder);
		}
				
		private function CreateCoins(): void 
		{
			var xx: Number, yy: Number, endy: Number;
			var xRange: Number = 70;
			var startX: Number = _monster.x + _monster.width / 2 - xRange / 2;
			if (_coinsData)
			{
				for (var i: int = 0; i < _coinsData.number; i++)
				{
					xx = startX+Misc.Random(0, xRange);
					endy = _monsterHpPanel.y + 30;
					yy = endy - 50 - Misc.Random(0, 100);
					var coin: Coin = Coin.GetCoin(BigInt.shStr2BigInt(_coinsData.worth), xx, yy, _coinsHolder);
					coin.specialId = 1;
				}
			}
			if (_glory > 0)
			{
				CreateGloryThings(_glory);
			}
		}
		
		private function OnAddFallingThing(e:Event):void 
		{
			var thing: FallingThing = FallingThing(e.data);
			var xRange: Number = 70;
			var startX: Number = _monster.x + _monster.width / 2 - xRange / 2;
			var xx = startX + Misc.Random(0, xRange) - xRange / 2;
			var endy = _monsterHpPanel.y + 30;
			var yy = endy - 50 - Misc.Random(0, 100);
			_coinsHolder.addChild(thing);
			thing.InitFallingThing(xx, yy);
		}		
		
		public function CreateGloryThings(num: uint, xRange: Number = 70, startX: Number = -1, startY: Number = -1, jump: Boolean = true):void 
		{			
			if (startX === -1)
				startX = _monster.x + _monster.width / 2;
			startX -= xRange / 2;
			var endy = _monsterHpPanel.y + 30;
			if (startY === -1)
				startY = endy - 50 - Misc.Random(0, 100);
			for (var i: int = 0; i < num; i++)
			{
				startY = startY - 10;
				startY += Misc.Random(0, 20);
				var thing: GloryThing = GloryThing.GetGloryThing(1, startX + Misc.Random(0, xRange), startY, _coinsHolder, jump); 
			}			
		}
		
		public function CreateBonusGold(value: BigInt, xRange: Number = 70, startX: Number = -1, startY: Number = -1, jump: Boolean = true):void 
		{
			var num: int = Misc.Random(5, 10);
			var oneCoinValue: BigInt;
			if (BigInt.greater(BigInt.shStr2BigInt(num.toString()), value))
			{
				num = int(BigInt.bigInt2str(value,10));
				oneCoinValue = Misc.BIG_INT_ONE;
			}
			else
			{
				oneCoinValue = BigInt.divide(value,BigInt.shStr2BigInt(num.toString()));
			}
			var valueSum: BigInt = Misc.BIG_INT_ZERO;
			var xx: Number = startX+Misc.Random(0, xRange);
			var endy: Number = _monsterHpPanel.y + 30;
			var yy: Number = endy - 50 - Misc.Random(0, 100);			
			for (var i: int = 0; i < num; i++)
			{
				Coin.GetCoin(oneCoinValue, xx, yy, _coinsHolder, jump);
				valueSum = BigInt.add(valueSum, oneCoinValue);
			}
			valueSum = BigInt.sub(value, valueSum);
			if (BigInt.greater(valueSum, Misc.BIG_INT_ZERO))
			{
				Coin.GetCoin(valueSum, xx, yy, _coinsHolder, jump);
			}
		}
		
		public function CreateBonusGlory(value: uint, xRange: Number = 70, startX: Number = -1, startY: Number = -1, jump: Boolean = true):void 
		{
			var num: int = Misc.Random(5, 10);
			var oneCoinValue: uint;
			if (num > value)
			{
				num = value;
				oneCoinValue = 1;
			}
			else
			{
				oneCoinValue = Math.floor(value / num);
			}
			var valueSum: uint = 0;
			var xx: Number = startX+Misc.Random(0, xRange);
			var endy: Number = _monsterHpPanel.y + 30;
			var yy: Number = endy - 50 - Misc.Random(0, 100);			
			for (var i: int = 0; i < num; i++)
			{
				GloryThing.GetGloryThing(oneCoinValue, xx, yy, _coinsHolder, jump);
				valueSum += oneCoinValue;
			}
			valueSum = value - valueSum;
			if (valueSum > 0)
			{
				GloryThing.GetGloryThing(valueSum, xx, yy, _coinsHolder, jump);
			}
		}		
		
		private function OnEventMonsterKilled(event: L_HEventMonsterKilled): void 
		{
			if (_monster)
			{
				_smokeCreator.CreateSmoke();
				_monster.DieAnimation();
				CreateCoins();
				_dieAnimationEndTimeout = setTimeout(OnDieAnimationEnd, 1000);
			}
		}
		
		private function OnChangeLevel():void 
		{
			_monster.DieAnimation();
			_dieAnimationEndTimeout = setTimeout(OnDieAnimationEnd, 1000);
		}
				
		private function OnDieAnimationEnd():void 
		{
			//_monster.Destroy();
			_monster.Die();
			_monster = null;			
		}
		
		private function OnTimeUp(event: Event): void 
		{
			_timeUpImage.alpha = 1;
			_timeUpImage.visible = true;
			var tl: TimelineLite = new TimelineLite;
			tl.from(_timeUpImage, TIME_UP_IMAGE_APPEAR_TIME, { y: TIME_UP_IMAGE_SLIDE_FROM_Y, alpha: 0, ease: ElasticOut } );
			tl.to(_timeUpImage, TIME_UP_IMAGE_DISAPPEAR_TIME, { alpha: 0, onComplete: function ():void 
			{
				_timeUpImage.visible = false;				
			} }, "+="+TIME_UP_IMAGE_REMAIN_TIME );
			_smokeCreator.CreateSmoke();
			//_monster.Destroy();
			_monster.Die();
			_monster = null;
		}			
		
		private function OnEventNewMonster(event: Event): void
		{
			if (_monster !== null)
			{
				clearTimeout(_dieAnimationEndTimeout);
				OnDieAnimationEnd();
			}
			_glory = parseInt(BigInt.bigInt2str(MonsterData(event.data.monsterData).glory,10),10);
			_monster = MonsterSprite.Create(MonsterData(event.data.monsterData));
			//_monster.alpha = 0;
			//TweenLite.to(_monster, 1, { alpha: 1 } );
			_coinsData = event.data.monsterData.coinsData;
			if (_monster.newlyCreated)
				_monsterHolder.addChild(_monster);
			if (World.SHOW_TUTORIAL && _tutorial.currentPhase === 0)
			{
				_tutorial.PointOnto(_monster);
				_tutorial.StartCurrentPhase();
			}
			//_bottomestHolder.addChild(_monster);
		}
	
		private function OnCreateFlyingText(event: Event):void 
		{
			var timeToDieInMs: Number = 1000;
			var fontSize: Number = -1;
			var color: uint = Color.WHITE;
			var font: String = "damageFont";
			if (event.data.timeToDieInMs !== undefined)
				timeToDieInMs = event.data.timeToDieInMs;
			if (event.data.fontSize !== undefined)
				fontSize = event.data.fontSize;
			if (event.data.color !== undefined)
				color = event.data.color;
			if (event.data.font !== undefined)
				font = event.data.font;
			var flyingText: FlyingText = FlyingText.GetFlyingText(event.data.text, timeToDieInMs, font, color);
			if (event.data.jumping !== undefined)
				flyingText.MakeJumping();
			flyingText.x = event.data.x;
			flyingText.y = event.data.y;
			_toppestHolder.addChild(flyingText);
		}
		
		private function OnGroupWindowDungeonsButtonPressed(event: Event): void 
		{
			Analytics.TrackPage("/game_screen/dungeons_window");
			MoveDungeonsWindowDown();
		}
		
		private function MoveDungeonsWindowDown(): void
		{
			Analytics.TrackPage("/game_screen/main_window");
			MoveWindowOut(_window);
			MoveWindowIn(_dungeonsWindow);		
		}		
		
		private function OnDungeonsWindowDungeonsButtonPressed(event: Event): void 
		{
			MoveDungeonsWindowUp();
		}
		
		private function MoveDungeonsWindowUp(): void
		{
			Analytics.TrackPage("/game_screen/main_window");
			MoveWindowIn(_window);
			MoveWindowOut(_dungeonsWindow);			
		}
		
		public function MoveWindowOut(window: HEntity, makeInvisible: Boolean = true, makeUntouchable: Boolean = true, extrude: Number = 0): void 
		{			
			if (_windowMoveTime == -1)
			{
				_windowMoveTime = 1;
				HSounds.PlaySound("WindowMove");
				//var result: Array = null;
				//result = HSounds.PlaySound("WindowMove");
				//if (result)
					//_windowMoveTime = result[0].lengthInSecs;
				//else
					//_windowMoveTime = 1;
			}
			if (makeUntouchable)
				window.touchable = false;
			if (makeInvisible)
				Tweens.TweenTo(window, _windowMoveTime, window.x, -window.height + extrude, function() : void { window.visible = false; _windowMoveTime = -1;  } );
			else
				Tweens.TweenTo(window, _windowMoveTime, window.x, -window.height + extrude);
		}
		
		public function MoveWindowIn(window: HEntity): void 
		{						
			if (_windowMoveTime == -1)
			{
				_windowMoveTime = 1;
				HSounds.PlaySound("WindowMove");
				
				//var result: Array = null;
				//result = HSounds.PlaySound("WindowMove");
				//if (result)
					//_windowMoveTime  = result[0].lengthInSecs;
				//else
					//_windowMoveTime = 1;
			}
			window.touchable = true;
			window.visible = true;
			Tweens.TweenTo(window, _windowMoveTime, window.x, 0, function () : void { _windowMoveTime = -1;  } );
		}
		
		public function CreateFlyingImage(texture: Texture, fromX: Number, fromY: Number, toX: Number, toY: Number, directed: Boolean = false):void 
		{
			var image: Image = new Image(texture);
			Misc.CenterObject(image, fromX, fromY);
			_toppestHolder.addChild(image);
			//var rndTime: Number = Misc.Random(600, 900);
			//setTimeout(function (): void { if (image) image.visible = false; }, rndTime);
			var rndTime: Number = Misc.Random(0.5, 1.2);
			TweenLite.to(image, rndTime, { x: toX, y: toY, onComplete: function() : void { image.removeFromParent(true); image = null; } } );
			if (directed)
			{
				var angle = Math.atan2(toY - fromY, toX - fromX);
				image.rotation = Misc.PI/2+angle;
			}
			//return image;
		}
		
		//public function CreateAttackAnim(heroType: int, fromX: Number, fromY: Number): void 
		//{
//
		//}		
		
	}

}