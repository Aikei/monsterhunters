package view.particles 
{
	import eng.particle.ParticleData;
	import eng.particle.ParticleSystem;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HypnotizedSwordParticleSystem extends ParticleSystem 
	{
		
		public function HypnotizedSwordParticleSystem() 
		{
			var particleData: ParticleData = new ParticleData;
			particleData.minDirX = 0;
			particleData.maxDirX = 0;
			particleData.minDirY = -1;
			particleData.maxDirY = -1;
			particleData.effectQuietlyDisappear = true;
			particleData.minDieWithin = 0.4;
			particleData.maxDieWithin = 0.45;
			particleData.speed = 75;
			particleData.effectShake = true;
			//particleData.shakeTime = 0.1;
			//particleData.shakeStrength = 6;
			super(Assets.interfaceAtlas.getTexture("sword_hypnotized_particle"),particleData);
			startSpreadX = 30;
			startSpreadY = 0;
		}
		
		override public function Start():void 
		{
			CycleCreateParticles(1, 0.15, 0.25, 0.5);
		}
		
	}

}