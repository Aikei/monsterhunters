package view.particles 
{
	import eng.particle.ParticleData;
	import eng.particle.ParticleSystem;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class StunSwordParticleSystem extends ParticleSystem 
	{
		
		public function StunSwordParticleSystem() 
		{
			
			var particleData: ParticleData = new ParticleData;
			particleData.effectQuietlyDisappear = true;
			particleData.minDieWithin = 0.9;
			particleData.maxDieWithin = 0.9;
			particleData.speed = 100;
			this.creationType = TYPE_RADIAL;
			super(Assets.interfaceAtlas.getTexture("stun_particle"), particleData);
			startSpreadX = 0;
			startSpreadY = 0;
		}
		
		override public function Start():void 
		{
			CreateParticles(8);
			//CycleCreateParticles(8, 0.3, 0.3, 0.6);
		}		
		
	}

}