package view 
{
	import d_hentity.HEntity;
	import starling.display.Quad;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class InvisibleTooltipHolder extends HEntity 
	{
		private var _quad: Quad;
		
		public function InvisibleTooltipHolder(x: Number, y: Number, w: Number, h: Number, text: String) 
		{
			super(false, true);
			this.x = x;
			this.y = y;
			showTooltipOnHover = true;
			tooltipText = text;
			_quad = new Quad(w, h);
			this.alpha = 0;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function SetQuadSize(w: Number, h: Number):void 
		{
			_quad.width = w;
			_quad.height = h;
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_quad);
		}
		
	}

}