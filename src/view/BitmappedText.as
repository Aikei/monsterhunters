package view 
{
	import d_hentity.HEntity;
	import feathers.events.FeathersEventType;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	
	/**
	 * ...
	 * @author Aikei
	 */
	

	 
	public class BitmappedText extends HEntity 
	{
		public static const ADDITIONAL_HEIGHT: Number = 4;
		public static const ADDITIONAL_WIDTH: Number = 4;
	
		private var _textSprite: Sprite = null;
		private var _currentText: String = "";
		private var _textField: TextField = null;
		private var _center: Boolean = false;
		
		private var _arguments: Array = null;
					
		private var _centerx: Number;
		private var _centery: Number;
		
		
		static public function Reset(text: String, obj: BitmappedText): BitmappedText
		{
			var n: BitmappedText = new BitmappedText();
			obj.args[0] = text;
			n.Init.apply(null, obj.args);
			n.x = obj.x;
			n.y = obj.y;
			obj.parent.addChild(n);
			obj.removeFromParent(true);
			return n;
		}			
		
		public function BitmappedText()  
		{
			super();
			
		}				
		
		public function Init(text: String, fontName: String, fontColor: uint = 0xffffff,  width: Number = 0, height: Number = 0, hAlign: String = "center", vAlign: String = "center", autoSize = "bothDirections"):void 
		{
			_arguments = arguments;
			_currentText = text;
			_textField = new TextField(width, height, "", fontName, -1, fontColor);
			_textField.hAlign = hAlign;
			_textField.vAlign = vAlign;
			_textField.autoSize = autoSize;
			_textField.text = text;
			AddToStage();
			//addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			//dispatchEventWith("feathersResize");
		}
		
		private function AddToStage():void 
		{
			if (_textSprite != null)
				_textSprite.removeFromParent(true);
			addChild(_textField);			
			_textSprite = TextField.getBitmapFont(_textField.fontName).createSprite(_textField.width+ADDITIONAL_WIDTH, _textField.height+ADDITIONAL_HEIGHT, _textField.text, -1, _textField.color, _textField.hAlign, _textField.vAlign, false);
			addChild(_textSprite);
			_textField.dispose();
			_textField = null;
			if (_center)
				Center();
			dispatchEventWith("feathersResize");
			//flatten();
		}
		
		public function ChangeText(text: String):void 
		{
			//unflatten();
			_arguments[0] = text;
			Init.apply(null, _arguments);
			//AddToStage();
		}
		
		public function SetTextBeforeAddingToStage(text: String):void 
		{
			_textField.text = text;
		}
				
		private function addedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			AddToStage();
		}		
		
		public function get currentText():String 
		{
			return _currentText;
		}
		
		public function get args(): Array 
		{
			return _arguments;
		}
		
		public function SetCenter(x: Number, y: Number):void 
		{
			_centerx = x;
			_centery = y;
			_center = true;
			Center();
		}
		
		public function Center():void 
		{
			this.x = Math.round(_centerx - this.width / 2 - ADDITIONAL_WIDTH/2);
			this.y = Math.round(_centery - this.height / 2 - ADDITIONAL_HEIGHT/2);
		}
		
		
	}

}