package view.gameviewelements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import feathers.text.BitmapFontTextFormat;
	import starling.display.Image;
	import view.gameviewelements.OldPanelButton;
	
	public class OldBuyUpgradeButton extends OldPanelButton 
	{
		
		public function OldBuyUpgradeButton() 
		{
			super();
			defaultSkin = new Image(Assets.interfaceAtlas.getTexture("upgrade_button"));
			hoverSkin = new Image(Assets.interfaceAtlas.getTexture("upgrade_button_hover"));
			disabledSkin = new Image(Assets.interfaceAtlas.getTexture("upgrade_button_inactive"));
			this.defaultLabelProperties.textFormat = new BitmapFontTextFormat("bbold11", 11, 0x7c3a12);
			this.disabledLabelProperties.textFormat = new BitmapFontTextFormat("bbold11", 11, 0x6d5548);
		}
		
		override public function set label(text: String): void 
		{
			super.label = text.toUpperCase();
		}
	}

}