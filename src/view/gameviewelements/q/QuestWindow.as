package view.gameviewelements.q
{
	import com.greensock.easing.*;
	import com.greensock.TweenLite;
	import d_hentity.HEntity;
	import feathers.text.BitmapFontTextFormat;
	import flash.geom.Rectangle;
	import logic.Quest;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextFieldAutoSize;
	import starling.textures.Texture;
	import system.Analytics;
	import system.HSounds;
	import view.GameView;
	import view.gameviewelements.HButton;
	import view.gameviewelements.HText;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class QuestWindow extends HEntity 
	{
		public static var REWARDS_FALL_Y: Number = 125;
		public static var REWARDS_FALL_X: Number = 215;
		public static var REWARDS_SPREAD_X: Number = 80;
		
		public static var WINDOW_MOVE_TIME: Number = 0.75;
		public static var QUEST_WINDOW_X: Number = 70;
		
		public static var EXTRUDE_Y: Number = 50;
		
		public static var SLIDE_TO_Y: Number = -10;
		public static var HIDE_Y: Number;
		public static var SLIDE_FROM_Y: Number;
		
		public static var ACCEPT_BUTTON_X: Number = 20;
		public static var ACCEPT_BUTTON_Y: Number = 306;
		
		public static var REWARD_TEXT_CENTER_X: Number = 132;
		public static var REWARD_TEXT_CENTER_Y: Number = 236;
		
		public static var GOLD_REWARD_TEXT_X: Number = 64;
		public static var GOLD_REWARD_TEXT_Y: Number = 260;
		
		public static var GLORY_REWARD_TEXT_X: Number = 178;
		public static var GLORY_REWARD_TEXT_Y: Number = 260;
		
		public static var REFUSE_BUTTON_CENTER_X: Number = 132;
		public static var REFUSE_BUTTON_CENTER_Y: Number = 362;
		
		public static var PROGRESS_DESC_TEXT_CENTER_X: Number = 129;
		public static var PROGRESS_DESC_TEXT_CENTER_Y: Number = 348;
		
		public static var PROGRESS_TEXT_CENTER_X: Number = 131;
		public static var PROGRESS_TEXT_CENTER_Y: Number = 364;
		
		public static var QUEST_NAME_X: Number = 50;
		public static var QUEST_NAME_Y: Number = 40;		
		public static var QUEST_NAME_WIDTH: Number = 165;
		
		public static var QUEST_DESCRIPTION_X: Number = 30;
		public static var QUEST_DESCRIPTION_Y: Number = 96;
		public static var QUEST_DESCRIPTION_Y_FROM_QUEST_NAME: Number = 20;
		public static var QUEST_DESCRIPTION_WIDTH: Number = 205;
		public static var QUEST_DESCRIPTION_HEIGHT: Number = 90;
		
		public static var MINIMIZE_BUTTON_X: Number = 111;
		public static var MINIMIZE_BUTTON_Y: Number = 337;
		
		public static var CANCEL_BUTTON_X: Number = 54;
		public static var CANCEL_BUTTON_Y: Number = 197;
		
		public static const STATE_NONE: int = 0;
		public static const STATE_NEW: int = 1;
		public static const STATE_OLD: int = 2;
		public static const STATE_MINIMIZED: int = 3;
		
		private var _image: Image = new Image(Assets.interfaceAtlas.getTexture("quest_window"));
		private var _acceptButton: HButton = new HButton;
		private var _refuseButton: HButton = new HButton;
		private var _rewardCommentText: HText = new HText(Language.GetString("Reward").toUpperCase(),"bbold12", -1, 0x8c430d)
		private var _goldRewardText: HText = new HText("", "bbold18", -1, 0x8c430d);
		private var _gloryRewardText: HText = new HText("", "bbold18", -1, 0x8c430d);
		private var _questNameText: HText = new HText("", "bbold18", -1, 0x8c430d, TextFieldAutoSize.VERTICAL);
		private var _questDescriptionText: HText = new HText("", "bregular13quests", -1, 0x8c430d, TextFieldAutoSize.VERTICAL);
		private var _cancelButton: HButton = new HButton;
		private var _minimizeButton: HButton = new HButton;
		private var _progressDescriptionText: HText = new HText("", "bbold12", -1, 0x8c430d);
		private var _progressText: HText = new HText("", "bbold12", -1, 0x8c430d);
		private var _moveDownButton: HButton = new HButton;
		private var _quest: Quest;
		
		private var _state: int = STATE_NONE;
		
		public function QuestWindow() 
		{
			_questNameText.x = QUEST_NAME_X;
			_questNameText.y = QUEST_NAME_Y;
			_questNameText.textField.width = QUEST_NAME_WIDTH;
			
			_questDescriptionText.x = QUEST_DESCRIPTION_X;
			if (QUEST_DESCRIPTION_Y_FROM_QUEST_NAME > 0)
				_questDescriptionText.y = QUEST_NAME_Y + _questNameText.textField.height + QUEST_DESCRIPTION_Y_FROM_QUEST_NAME;
			else
				_questDescriptionText.y = QUEST_DESCRIPTION_Y;
			_questDescriptionText.textField.width = QUEST_DESCRIPTION_WIDTH;
			_questDescriptionText.textField.height = QUEST_DESCRIPTION_HEIGHT;
			
			HIDE_Y = -_image.texture.height;
			SLIDE_FROM_Y = HIDE_Y + EXTRUDE_Y;
			this.x = QUEST_WINDOW_X;
			this.y = HIDE_Y;
			
			var moveDownButtonTexture: Texture = Texture.fromTexture(Assets.interfaceAtlas.getTexture("quest_window"), new Rectangle(0, 336, _image.texture.width, 50));
			_moveDownButton.defaultSkin = new Image(moveDownButtonTexture);
			_moveDownButton.hoverSkin = new Image(moveDownButtonTexture);
			_moveDownButton.hoverSkin.filter = Filters.highlightFilter;
			_moveDownButton.addEventListener(Event.TRIGGERED, OnMoveDownButtonTriggered);
			_moveDownButton.validate();
			_moveDownButton.y = _image.texture.height - _moveDownButton.height;
			_moveDownButton.visible = false;
			
			
			_minimizeButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("quest_minimize_btn"));
			_minimizeButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("quest_minimize_btn"));
			_minimizeButton.hoverSkin.filter = Filters.highlightFilter;
			_minimizeButton.addEventListener(Event.TRIGGERED, OnMinimizeButtonTriggered);					
			_minimizeButton.x = MINIMIZE_BUTTON_X;
			_minimizeButton.y = MINIMIZE_BUTTON_Y;
			_minimizeButton.visible = false;
			
			_goldRewardText.x = GOLD_REWARD_TEXT_X;
			_goldRewardText.y = GOLD_REWARD_TEXT_Y;
			
			_gloryRewardText.x = GLORY_REWARD_TEXT_X;
			_gloryRewardText.y = GLORY_REWARD_TEXT_Y;
			
			_cancelButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("quest_cancel_btn"));
			_cancelButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("quest_cancel_btn"));
			_cancelButton.hoverSkin.filter = Filters.highlightFilter;
			_cancelButton.label = Language.GetString("CancelQuest");
			_cancelButton.defaultLabelProperties.textFormat = new BitmapFontTextFormat("bbold12buttons", 12);
			_cancelButton.defaultLabelProperties.filter = null;
			_cancelButton.hoverLabelProperties.textFormat = new BitmapFontTextFormat("bbold12buttons", 12);
			_cancelButton.hoverLabelProperties.filter = Filters.highlightFilter;
			_cancelButton.addEventListener(Event.TRIGGERED, OnCancelButtonTriggered);	
			_cancelButton.x = CANCEL_BUTTON_X;
			_cancelButton.y = CANCEL_BUTTON_Y;
			_cancelButton.visible = false;
			
			//_questNameText.x = QUEST_NAME_X;
			//_questNameText.y = QUEST_NAME_Y;
			//_questNameText.width = QUEST_NAME_WIDTH;
			
			//_questDescriptionText.x = QUEST_DESCRIPTION_X;
			//_questDescriptionText.y = QUEST_DESCRIPTION_Y;
			//_questDescriptionText.width = QUEST_DESCRIPTION_WIDTH;
			//_questDescriptionText.height = 90;
			
			_acceptButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("quest_accept_button"));
			_acceptButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("quest_accept_button"));
			_acceptButton.hoverSkin.filter = Filters.highlightFilter;			
			_acceptButton.x = ACCEPT_BUTTON_X;
			_acceptButton.y = ACCEPT_BUTTON_Y;
			_acceptButton.defaultLabelProperties.textFormat = new BitmapFontTextFormat("bbold16buttons", 16);
			_acceptButton.defaultLabelProperties.filter = null;
			_acceptButton.hoverLabelProperties.textFormat = new BitmapFontTextFormat("bbold16buttons", 16);
			_acceptButton.hoverLabelProperties.filter = Filters.highlightFilter;			
			_acceptButton.label = Language.GetString("AcceptQuest");
			_acceptButton.addEventListener(Event.TRIGGERED, OnAcceptButtonTriggered);
			_acceptButton.visible = false;
			
			_refuseButton.label = Language.GetString("RefuseQuest");
			_refuseButton.defaultLabelProperties.textFormat = new BitmapFontTextFormat("bbold12", 12, 0x8c430d);
			_refuseButton.defaultLabelProperties.filter = null;
			_refuseButton.hoverLabelProperties.textFormat = new BitmapFontTextFormat("bbold12", 12, 0x8c430d);
			_refuseButton.hoverLabelProperties.filter = Filters.highlightFilter;
			_refuseButton.validate();
			_refuseButton.visible = false;
			
			_progressDescriptionText.touchable = false;
			_progressDescriptionText.visible = false;
			_progressText.touchable = false;
			_progressText.visible = false;
			
			Misc.CenterObject(_refuseButton, REFUSE_BUTTON_CENTER_X, REFUSE_BUTTON_CENTER_Y);
			_refuseButton.addEventListener(Event.TRIGGERED, OnRefuseButtonTriggered);
								
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			World.gWorld.addEventListener("newQuest", OnNewQuest);
			World.gWorld.addEventListener("questProgress", OnQuestProgress);
		}
		
		private function OnQuestProgress(e:Event):void 
		{
			UpdateProgress(Number(e.data));
		}
		
		private function OnNewQuest(e:Event):void 
		{
			_quest = Quest(e.data);
			InitQuest(_quest);
		}
		
		private function OnMoveDownButtonTriggered(e:Event):void 
		{
			if (_quest)
			{
				MoveDown(STATE_OLD);
			}
		}
		
		private function OnMinimizeButtonTriggered(e:Event): void 
		{
			MoveUp();
		}
		
		private function OnAcceptButtonTriggered(e:Event): void 
		{
			Analytics.TrackEvent("Quest", "Accepted");
			World.gameController.AcceptQuest(_quest);
			MoveUp();
		}		
		
		private function OnCancelButtonTriggered(e:Event): void 
		{
			World.gameController.RefuseQuest();
			_quest = null;
			SetState(STATE_NONE);
			MoveUp();			
		}
		
		private function OnRefuseButtonTriggered(e:Event): void 
		{		
			World.gameController.RefuseQuest();
			_quest = null;
			SetState(STATE_NONE);
			MoveUp();
		}		
		
		public function SetState(state: int): void 
		{
			if (_state === STATE_OLD)
			{
				_minimizeButton.visible = false;
				_cancelButton.visible = false;
				_image.texture = Assets.interfaceAtlas.getTexture("quest_window");
			}
			else if (_state === STATE_NEW)
			{
				_acceptButton.visible = false;
				_refuseButton.visible = false;			
			}
			else if (_state === STATE_MINIMIZED)
			{
				_progressDescriptionText.visible = false;
				_progressText.visible = false;
			}
			
			if (state === STATE_OLD)
			{
				_minimizeButton.visible = true;
				_cancelButton.visible = true;
				_image.texture = Assets.interfaceAtlas.getTexture("quest_window_old");
			}
			else if (state === STATE_NEW)
			{
				_acceptButton.visible = true;
				_refuseButton.visible = true;				
			}
			else if (state === STATE_MINIMIZED)
			{
				_progressDescriptionText.visible = true;
				if (_quest.showProgress)
					_progressText.visible = true;
				else
					_progressText.visible = false;
			}
			else if (state === STATE_NONE)
			{
				_moveDownButton.visible = false;
			}
			
			_state = state;
		}
		
		public function MoveFromHiddenToMinimized():void 
		{
			SetState(STATE_MINIMIZED);
			_moveDownButton.visible = true;
			TweenLite.to(this, WINDOW_MOVE_TIME/2, { y : SLIDE_FROM_Y, ease: Back.easeOut.config(0.8), onComplete: function ():void 
			{

			} } );			
		}
		
		public function MoveDown(state: int): void 
		{
			HSounds.PlaySound("QuestMenuMove");
			SetState(state);
			_moveDownButton.visible = false;			
			TweenLite.to(this, WINDOW_MOVE_TIME, { y : SLIDE_TO_Y, ease: Back.easeOut.config(0.8), onComplete: function ():void 
			{

			} } );
		}
		
		public function MoveUp(): void 
		{
			HSounds.PlaySound("QuestMenuMove");
			var slideTo: Number = HIDE_Y;
			if (_state !== STATE_NONE)
			{
				slideTo = SLIDE_FROM_Y;
				SetState(STATE_MINIMIZED);
			}
			TweenLite.to(this, WINDOW_MOVE_TIME, { y : slideTo, ease: Back.easeOut.config(0.8), onComplete: function ():void 
			{
				if (_state !== STATE_NONE)
					_moveDownButton.visible = true;
			} } );
		}
		
		public function InitQuest(quest: Quest):void 
		{
			_quest = quest;
			_questNameText.text = quest.name.toUpperCase();
			_questDescriptionText.y = _questNameText.y + _questNameText.textField.height + QUEST_DESCRIPTION_Y_FROM_QUEST_NAME;
			_questDescriptionText.text = quest.description;
			_goldRewardText.text = BigInt.bigint2HString(quest.goldReward);
			_gloryRewardText.text = quest.gloryReward.toString();
			_progressDescriptionText.text = quest.shortDescription;
			Misc.CenterObject(_progressDescriptionText, PROGRESS_DESC_TEXT_CENTER_X, PROGRESS_DESC_TEXT_CENTER_Y);
			UpdateProgress(quest.progress);
			if (!quest.accepted)
			{
				MoveDown(STATE_NEW);
			}
			else
			{				
				MoveFromHiddenToMinimized();
			}
		}
		
		private function UpdateProgress(progress: Number)
		{
			if (progress === -1)
			{
				Analytics.TrackEvent("Quest", "Completed");
				HSounds.PlaySound("QuestDone",0,false);
				GameView.gView.CreateGloryThings(_quest.gloryReward, REWARDS_SPREAD_X, REWARDS_FALL_X, REWARDS_FALL_Y, false);
				if (BigInt.greater(_quest.goldReward, Misc.BIG_INT_ZERO))
				{
					GameView.gView.CreateBonusGold(_quest.goldReward, REWARDS_SPREAD_X, REWARDS_FALL_X, REWARDS_FALL_Y, false);
				}
				SetState(STATE_NONE);
				MoveUp();
				return;
			}			
			_progressText.text = "(" + progress + "/" + _quest.progressGoal + ")";
			Misc.CenterObject(_progressText, PROGRESS_TEXT_CENTER_X, PROGRESS_TEXT_CENTER_Y);
		}		
			
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_image);
			addChild(_moveDownButton);
			addChild(_acceptButton);
			addChild(_refuseButton);
			addChild(_rewardCommentText);
			addChild(_goldRewardText);
			addChild(_gloryRewardText);
			addChild(_questNameText);
			addChild(_questDescriptionText);
			addChild(_cancelButton);
			addChild(_minimizeButton);
			addChild(_progressDescriptionText);
			addChild(_progressText);
			
			SetState(STATE_NONE);
			Misc.CenterObject(_rewardCommentText, REWARD_TEXT_CENTER_X, REWARD_TEXT_CENTER_Y);
		}
		
	}

}