package view.gameviewelements.custom 
{
	import feathers.layout.ILayout;
	import feathers.layout.IVirtualLayout;
	import starling.events.EventDispatcher;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class MyVerticalLayout extends EventDispatcher implements IVirtualLayout
	{
		
    import feathers.core.IFeathersControl;
    import feathers.layout.ILayout;
    import feathers.layout.LayoutBoundsResult;
    import feathers.layout.ViewPortBounds;
 
    import flash.geom.Point;
 
    import starling.display.DisplayObject;
    import starling.events.Event;
    import starling.events.EventDispatcher;
 
    public class MyVerticalLayout extends EventDispatcher implements ILayout
    {
        public function MyVerticalLayout()
        {
        }
 
        protected var _gap:Number = 0;
 
        public function get gap():Number
        {
            return this._gap;
        }
 
        public function set gap(value:Number):void
        {
            if(this._gap == value)
            {
                return;
            }
            this._gap = value;
            this.dispatchEventWith(Event.CHANGE);
        }
 
        public function layout(items:Vector.<DisplayObject>, viewPortBounds:ViewPortBounds = null, result:LayoutBoundsResult = null):LayoutBoundsResult
        {
            // initialize the view port's position and dimensions
            var startX:Number = 0;
            var startY:Number = 0;
            var explicitWidth:Number = NaN;
            var explicitHeight:Number = NaN;
            var minWidth:Number = 0;
            var minHeight:Number = 0;
            var maxWidth:Number = Number.POSITIVE_INFINITY;
            var maxHeight:Number = Number.POSITIVE_INFINITY;
            if(viewPortBounds)
            {
                startX = viewPortBounds.x;
                startY = viewPortBounds.y;
                explicitWidth = viewPortBounds.explicitWidth;
                explicitHeight = viewPortBounds.explicitHeight;
                minWidth = viewPortBounds.minWidth;
                minHeight = viewPortBounds.minHeight;
                maxWidth = viewPortBounds.maxWidth;
                maxHeight = viewPortBounds.maxHeight;
            }
 
            // loop through the items and position them
            var positionY:Number = startY;
            var maxItemWidth:Number = 0;
            var itemCount:int = items.length;
            for(var i:int = 0; i < itemCount; i++)
            {
                var item:DisplayObject = items[i];
                // skip items that aren't included in the layout
                var layoutItem:ILayoutDisplayObject = item as ILayoutDisplayObject;
                if(layoutItem && !layoutItem.includeInLayout)
                {
                    continue;
                }
                // special case for Feathers components
                if(item is IFeathersControl)
                {
                    IFeathersControl(item).validate();
                }
                item.x = startX;
                item.y = positionY;
                positionY += item.height + this._gap;
 
                // used for the final content width below
                maxItemWidth = Math.max(maxItemWidth, item.width);
            }
 
            // used for the final content height below
            positionY -= (startY + this._gap);
 
            // prepare the result object and return it
            if(!result)
            {
                result = new LayoutBoundsResult();
            }
            var viewPortWidth:Number = explicitWidth;
            var viewPortHeight:Number = explicitHeight;
            if(isNaN(viewPortWidth))
            {
                viewPortWidth = Math.max(minWidth, Math.min(maxWidth, maxItemWidth));
            }
            if(isNaN(explicitHeight))
            {
                viewPortHeight = Math.max(minHeight, Math.min(maxHeight, positionY));
            }
            var contentWidth:Number = Math.max(maxItemWidth, viewPortWidth);
            var contentHeight:Number = Math.max(positionY, viewPortHeight);
            result.viewPortWidth = viewPortWidth;
            result.viewPortHeight = viewPortHeight;
            result.contentWidth = contentWidth;
            result.contentHeight = contentHeight;
            return result;
        }
 
        public function getScrollPositionForIndex(index:int, items:Vector.<DisplayObject>, x:Number, y:Number, viewPortWidth:Number, viewPortHeight:Number, result:Point = null):Point
        {
            // loop through the items to calculate the scroll position
            var positionY:Number = 0;
            for(var i:int = 0; i < index; i++)
            {
                var item:DisplayObject = items[i];
                var layoutItem:ILayoutDisplayObject = item as ILayoutDisplayObject;
                if(layoutItem && !layoutItem.includeInLayout)
                {
                    continue;
                }
                if(item is IFeathersControl)
                {
                    IFeathersControl(item).validate();
                }
                positionY += item.height + this._gap;
            }
 
            // prepare the result object and return it
            if(!result)
            {
                result = new Point();
            }
            result.x = 0;
            result.y = positionY;
            return result;
        }
		
		public function measureViewPort(itemCount:int, viewPortBounds:ViewPortBounds = null, result:Point = null):Point
		{
			var sz: Point = new Point;
			
		}
		
		public function  getVisibleIndicesAtScrollPosition(scrollX:Number, scrollY:Number, width:Number, height:Number, itemCount:int, result:Vector.<int> = null):Vector.<int>
		{
			
		}
    }
		
	}

}