package view.gameviewelements.custom 
{
	import com.greensock.TweenLite;
	import flash.geom.Point;
	import flash.utils.clearInterval;
	import flash.utils.clearTimeout;
	import flash.utils.setTimeout;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import system.HCursor;
	import system.HSounds;
	import view.gameviewelements.MessageWindow.MessageWindow;
	import view.gameviewelements.MessageWindow.TooltipWindow;
	/**
	 * ...
	 * @author Aikei
	 */

	public class HoverChecker extends Sprite
	{
		static public var TIP_DELAY: Number = 0;
		static public var TIP_APPEAR_TIME: Number = 1.0;
		static public var TIP_BASE_ALPHA: Number = 0.98;
		
		private static var sPoint: Point = new Point;
		
		private var _displayObject: DisplayObject;
		private var _messageWindow: MessageWindow = null;
		private var _touchInited: Boolean = false;
		private var _hoverTime: Number = 0;
		private var _timeOut: uint = 0;
		
		public var showTooltipOnHover: Boolean = false;
		public var tooltipText: String = "";
		public var tooltipTitleText: String = "";
		public var hovered: Boolean = false;
		
		
		public function HoverChecker(displayObject: DisplayObject) 
		{
			_displayObject = displayObject;
			_displayObject.addEventListener(TouchEvent.TOUCH, OnTouch);
		}
		
		private function OnTouch(e:TouchEvent):void 
		{
			var touch: Touch = e.getTouch(_displayObject);
			if (touch)
			{
				if (touch.phase === TouchPhase.HOVER)
				{
					OnHover();						
				}
				else
				{
					_displayObject.globalToLocal(new Point(touch.globalX, touch.globalY), sPoint);
					if (sPoint.x < 0 || sPoint.x >= _displayObject.width || sPoint.y < 0 || sPoint.y >= _displayObject.height)
					{
						OnStopHover();
					}
					
					if (touch.phase === TouchPhase.BEGAN)
					{
						OnTouchPhaseBegan();
						OnStopHover();
					}
					else if (touch.phase === TouchPhase.ENDED)
					{
						OnTouchPhaseEnded();
						OnStopHover();
					}
					
					ContinueHover();
				}
			}
			else
			{
				OnStopHover();					
			}
		}
		
		protected function ContinueHover():void 
		{
			if (hovered)
				MaybeShowTip();
		}
		
		public function OnTouchPhaseEnded():void 
		{
			dispatchEventWith("H_TouchBegan");
		}
		
		public function OnTouchPhaseBegan():void 
		{
			dispatchEventWith("H_TouchEnded");
		}
		
		public function OnHover():void 
		{
			if (!hovered)
			{
				_hoverTime = Misc.GetServerTime();
				hovered = true;
				_timeOut = setTimeout(MaybeShowTip, TIP_DELAY * 1000);
				dispatchEventWith("H_Hover");									
			}
			MaybeShowTip();
		}
		
		public function MaybeShowTip():void 
		{
			if (Misc.GetServerTime() - _hoverTime >= TIP_DELAY)
			{
				clearTimeout(_timeOut);
				_timeOut = 0;					
				if (hovered && showTooltipOnHover)
				{			
					if (_messageWindow === null)
					{
						if (tooltipTitleText.length > 0)
							_messageWindow = new TooltipWindow(tooltipText,tooltipTitleText);
						else if (Language.HasTitle(tooltipText))
							_messageWindow = new TooltipWindow(tooltipText);
						else if (Language.HasEntry(tooltipText))
							_messageWindow = new MessageWindow(Language.GetString(tooltipText));
						else
							_messageWindow = new MessageWindow(tooltipText);
						_messageWindow.alpha = TIP_BASE_ALPHA;
						World.gWorld.addChild(_messageWindow);
						if (TIP_APPEAR_TIME > 0)
						{
							_messageWindow.alpha = 0;
							TweenLite.to(_messageWindow, TIP_APPEAR_TIME, { alpha: TIP_BASE_ALPHA } );
						}						
					}
					_messageWindow.x = HCursor.GetCursor().x - _messageWindow.width / 2;
					_messageWindow.y = HCursor.GetCursor().y + 60;
					
					if (_messageWindow.y + _messageWindow.height >= Preferences.SCREEN_HEIGHT)
					{
						_messageWindow.y = HCursor.GetCursor().y - _messageWindow.height - 5;
					}					
				}
			}
		}
		
		public function OnStopHover():void 
		{
			if (hovered)
			{
				hovered = false;
				if (_messageWindow)
				{
					_messageWindow.Destroy();
					_messageWindow = null;			
				}
				dispatchEventWith("H_StopHover");
			}			
		}
		
		public function Destroy():void 
		{
			_displayObject.removeEventListener(TouchEvent.TOUCH, OnTouch);
			_displayObject.removeFromParent(true);
		}
		
	}

}