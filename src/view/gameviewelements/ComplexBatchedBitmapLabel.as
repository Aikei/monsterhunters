package view.gameviewelements 
{
	import d_hentity.HEntity;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class ComplexBatchedBitmapLabel extends HEntity 
	{
		public var label: BatchedBitmapLabel;
		
		public function ComplexBatchedBitmapLabel(fontName: String, fontSize: int, fontColor: uint=0xffffff) 
		{
			super(false, false);
			label = new BatchedBitmapLabel(fontName, fontSize, fontColor);
			addChild(label);
		}
		
		public function set text(t: String):void 
		{
			label.text = t;
		}
		
		public function get text(): String 
		{
			return label.text;
		}		
		
		public function set color(c: uint):void 
		{
			label.color = c;
		}
		
		public function get color(): uint 
		{
			return label.color;
		}
		
	}

}