package view.gameviewelements 
{
	import d_hentity.HEntity;
	import flash.events.TimerEvent;
	import starling.display.Image;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.text.TextFieldAutoSize;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class LevelDot extends HEntity 
	{
		static private var _pool: Vector.<LevelDot> = new Vector.<LevelDot>;
		
		protected var _image: Image = new Image(Assets.interfaceAtlas.getTexture("map_level_locked"));	
		//protected var _levelText: HText;
		protected var _active: Boolean = false;
		protected var _visited: Boolean = false;
		protected var _alive: Boolean = true;
		protected var _current: Boolean = false;
		protected var _levelText: HText;
		protected var _level: int;
		protected var _inMove: Boolean = false;
		
		public var position: int;
		
		public function get inMove(): Boolean 
		{
			return _inMove;
		}
		
		public function set inMove(i: Boolean):void 
		{
			this._inMove = i;
		}
		
		public function get visited(): Boolean 
		{
			return _visited;
		}
		
		public function get level(): int 
		{
			return _level;
		}
		
		public function get alive(): Boolean 
		{
			return _alive;
		}
		
		public function LevelDot(position: int, level: int, dotX: Number) 
		{
			super(true,true);

			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			alignPivot();
			_image.alignPivot();
			_levelText = new HText("", "bbold12", 12, 0xa1a2a3);
			_levelText.touchable = false;
			Init(position,level,dotX);
		}
		
		private function Init(position: int,level: int,dotX: Number): void 
		{
			_alive = true;
			visible = true;
			_current = false;
			_visited = false;
			this.position = position;
			_level = level;
			_levelText.text = level.toString();
			_levelText.x += 1;
			//_levelText.y -= 1;
			this.x = dotX;
			_levelText.x = Math.round(1 - _levelText.textField.width / 2);
			_levelText.y = Math.round(-1 - _levelText.textField.height / 2);
			
			FrameCheck();
			//CheckVisited();
			CheckCurrent();
			RoundPosition();
		}
		
		public function RoundPosition():void 
		{
			Misc.RoundPosition(this);
			Misc.RoundPosition(_levelText);			
		}
		
		static public function Get(position: int, level: int, dotX: Number): LevelDot 
		{
			var dot: LevelDot;
			if (_pool.length === 0)
			{
				dot = new LevelDot(position,level,dotX);
			}
			else
			{
				dot = _pool.pop();
				dot.Init(position,level,dotX);
			}
			return dot;			
		}
		
		public function Die():void 
		{
			_alive = false;
			visible = false;
			_pool.push(this);
		}
		
		override public function OnEnterFrame(event: EnterFrameEvent): void 
		{
			if (_alive)
			{
				super.OnEnterFrame(event);
				FrameCheck();
			}
		}
		
		override protected function OnTouchPhaseEnded():void 
		{
			if (_inMove)
				return;
			if (_visited && !_current)
				World.gameController.ChangeLevel(_level);
		}
		
		override protected function OnHover():void 
		{
			if (!_hovered && _visited && !_current)
			{
				_image.filter = Filters.highlightFilter;
			}
			super.OnHover();
		}
		
		override protected function OnStopHover():void 
		{
			if (_hovered)
			{
				_image.filter = null;
			}
			super.OnStopHover();
		}
		
		protected function FrameCheck():void 
		{
			if (_alive)
			{
				if (_inMove)
				{
					if (x < 0 || x > LevelPanel.WIDTH)
						visible = false;
					else
						visible = true;
				}
				//if (!visible && x <= LevelPanel.WIDTH)
					//visible = true;
				//if (!_visited)
				//{
					//if (position < LevelPanel.CENTER_POSITION)
						//SetVisited(true);
				//}
				//else
				//{
					//if (position > LevelPanel.CENTER_POSITION)
						//SetVisited(false);
				//}
				//if (position == LevelPanel.CENTER_POSITION)
					//SetCurrent(true);
				//if (_current && position != LevelPanel.CENTER_POSITION)
					//SetCurrent(false);
			}			
		}
		
		public function MaybeDie():void 
		{
			var a: int = 1;
			if (x < 0 || x > LevelPanel.WIDTH)
			{
				Die();
			}			
		}
		
		protected function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);			
			addChild(_image);
			addChild(_levelText);
			//addChild(_levelText);
		}
		
		public function SetCurrent(c: Boolean = true):void 
		{			
			if (c != _current)
			{
				_current = c;
				CheckCurrent();
			}
		}
		
		public function CheckCurrent():void 
		{
			if (_current)
			{
				_image.texture = Assets.interfaceAtlas.getTexture("map_level_current");
				_levelText.visible = true;
			}
			else
			{
				CheckVisited();
			}			
		}
		
		public function SetVisited(v: Boolean = true): void 
		{			
			if (v !== _visited)
			{
				_visited = v;
				CheckVisited();
			}
		}		
		
		private function CheckVisited():void 
		{
			if (_visited)
			{
				//_levelText.textField.color = 0x6b6b6b;
				_image.texture = Assets.interfaceAtlas.getTexture("map_level_visited");
				_levelText.visible = true;
			}
			else
			{
				//_levelText.textField.color = 0xa1a2a3;
				_image.texture = Assets.interfaceAtlas.getTexture("map_level_locked");
				_levelText.visible = false;
			}			
		}
		

		
	}

}