package view.gameviewelements 
{
	import d_hentity.HEntity;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.text.TextFieldAutoSize;
	import flash.events.TimerEvent;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class LevelDotText extends HEntity 
	{
		static private var _pool: Vector.<LevelDotText> = new Vector.<LevelDotText>;
		
		//private var _text: BatchedBitmapLabel = new BatchedBitmapLabel("bbold13", 13, 0xa1a2a3);
		private var _text: HText = new HText("","bbold13", -1, 0xa1a2a3);
		protected var _alive: Boolean = true;
		protected var _visited: Boolean = false;
		
		public function LevelDotText(level: int, textX: Number, textY: Number) 
		{
			super(true);
			Init(level, textX, textY);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		static public function Get(level: int, textX: Number, textY: Number): LevelDotText 
		{
			var dotText: LevelDotText;
			if (_pool.length === 0)
			{
				dotText = new LevelDotText(level, textX, textY);
			}
			else
			{
				dotText = _pool.pop();
				dotText.Init(level,textX,textY);
			}
			return dotText;			
		}
		
		public function Die():void 
		{
			_alive = false;
			visible = false;
			_pool.push(this);
		}
		
		private function Init(level: int, textX: Number, textY: Number):void 
		{
			_alive = true;
			//visible = true;
			x = textX;
			y = textY;
			_text.color = 0xa1a2a3;
			_text.text = String(level);
			//Misc.CenterObject(_text, _text.x, _text.y);
			alignPivot();
			_text.textField.alignPivot();
			
			if (x < LevelPanel.firstSpacing + ((LevelPanel.CENTER_POSITION+1) * LevelPanel.interSpacing))
				SetVisited(true);
			else
				SetVisited(false);
			CheckVisited();
		}
		
		override public function OnEnterFrame(event: EnterFrameEvent): void 
		{
			if (_alive)
			{
				super.OnEnterFrame(event);
				//if (x < LevelPanel.firstSpacing + LevelPanel.CENTER_POSITION * LevelPanel.interSpacing)
					//SetVisited(true);		
				//else 
				
				if (x < LevelPanel.firstSpacing + ((LevelPanel.CENTER_POSITION+1) * LevelPanel.interSpacing))
					SetVisited(true);
				else
					SetVisited(false);
					
				//if (x < LevelPanel.firstSpacing + LevelPanel.CENTER_POSITION * LevelPanel.interSpacing)
					//_text.color = 0x6b6b6b;					
				//else if (x < LevelPanel.firstSpacing + ((LevelPanel.CENTER_POSITION+1) * LevelPanel.interSpacing))
					//_text.color = Color.WHITE;
			
				//if (!visible && x <= LevelPanel.WIDTH)
					//visible = true;
					
				if (x < 0)
				{
					Die();
				}
			}
		}
		
		private function SetVisited(v: Boolean):void 
		{
			if (v != _visited)
			{
				_visited = v;
				CheckVisited();
			}
		}
		
		private function CheckVisited():void 
		{
			if (_visited)
			{
				this.visible = true;
			}
			else
			{
				this.visible = false;
			}
		}
		
		//override public function OnTimerTick(event: TimerEvent): void 
		//{
			//if (_alive)
			//{
				//super.OnTimerTick(event);
				//if (!visible && x <= LevelPanel.WIDTH)
					//visible = true;
				//if (x < 0)
				//{
					//Die();
				//}
			//}
		//}		
		
		protected function OnAddedToStage(event: Event): void 
		{
			addChild(_text);
		}
		
	}

}