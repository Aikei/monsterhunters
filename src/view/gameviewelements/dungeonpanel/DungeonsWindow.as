package view.gameviewelements.dungeonpanel 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import starling.display.Image;
	import feathers.controls.Button;
	import starling.display.Quad;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import view.gameviewelements.HButton;
	import view.gameviewelements.PanelButton;
	import d_hentity.HEntity;
	import starling.filters.ColorMatrixFilter;
	
	public class DungeonsWindow extends HEntity 
	{
		public static var WINDOW_X: int = 380;
		public static var WINDOW_Y: int = 0;
		
		//protected var _backGround: Image = new Image(Assets.interfaceAtlas.getTexture("dungeons_window_bg"));	
		protected var _backGround: Quad = new Quad(403, 420, 0x000000);
		protected var _image: Image = new Image(Assets.interfaceAtlas.getTexture("dungeons_window"));
		protected var _scrollBg: Image = new Image(Assets.interfaceAtlas.getTexture("dungeons_scroll"));
		protected var _dungeonsButton: HButton = new HButton;		
		protected var _dungeonsPanel: DungeonsPanel = new DungeonsPanel;
		
		public function DungeonsWindow() 
		{
			super();
			_backGround.touchable = false;
			_image.touchable = false;
			_scrollBg.touchable = false;
			
			x = WINDOW_X;
			y = -_image.height;
			
			_scrollBg.x = 447;
			_scrollBg.y = 163;
			
			_backGround.x = 47;
			_backGround.y = 163;			
			
			_dungeonsButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("close_button"));
			_dungeonsButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("close_button"));
			
			_dungeonsButton.hoverSkin.filter = Filters.highlightFilter;
			
			_dungeonsButton.x = 234;
			_dungeonsButton.y = 568;
			_dungeonsButton.addEventListener(Event.TRIGGERED, OnDungeonsButtonTriggered);
			
			_image.touchable = false;
			this.visible = false;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		protected function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);			
			addChild(_backGround);
			addChild(_scrollBg);
			addChild(_dungeonsPanel);
			addChild(_image);			
			addChild(_dungeonsButton);	
		}
		
		public function OnDungeonsButtonTriggered(event: Event): void 
		{
			World.gWorld.dispatchEvent(new Event("dungeonsWindowDungeonsButtonPressed"));
		}
		
		public function OnGlobalEnterFrame(event: EnterFrameEvent):void 
		{
			_dungeonsPanel.OnGlobalEnterFrame(event);
		}
		
		public function OnGlobalTouch(event: TouchEvent):void 
		{
			_dungeonsPanel.OnGlobalTouch(event);
		}		
		
	}

}