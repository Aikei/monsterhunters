package view.gameviewelements.dungeonpanel {
	/**
	 * ...
	 * @author Aikei
	 */
	import logic.Dungeon;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import hevents.*;
	import starling.events.TouchEvent;
	import view.gameviewelements.BaseVerticalScrollContainer;
	
	public class DungeonsPanel extends BaseVerticalScrollContainer 
	{
		public static var DUNGEONS_PANEL_X: Number = 44;
		public static var DUNGEONS_PANEL_Y: Number = 164;
		public static var DUNGEONS_PANEL_WIDTH: Number = 414;
		public static var DUNGEONS_PANEL_HEIGHT: Number = 414;
		
		protected var _panelItems: Vector.<DungeonsPanelItem>;
		
		public function DungeonsPanel() 
		{
			super();
			x = DUNGEONS_PANEL_X;
			y = DUNGEONS_PANEL_Y;
			this.width = DUNGEONS_PANEL_WIDTH;
			this.height = DUNGEONS_PANEL_HEIGHT;
			
			World.gWorld.addEventListener(L_HEventDungeonLevelSet.L_HEVENT_DUNGEON_LEVEL_SET, OnDungeonLevelSet);
			World.gWorld.addEventListener(L_HEventDungeonsListInitialized.L_HEVENT_DUNGEONS_LIST_INITIALIZED, OnDungeonsListInitialized);
			World.gWorld.addEventListener("dungeonsUpdated", OnDungeonsUpdated);
			//World.gWorld.addEventListener("N_DungeonsData", OnDungeonsData);
		}				
		
		private function OnDungeonsUpdated(e:Event):void 
		{
			UpdateDungeons(Vector.<Dungeon>(e.data));
		}
		
		override protected function OnAddedToStage(event: Event): void 
		{
			super.OnAddedToStage(event);
		}
		
		protected function OnDungeonsListInitialized(event: L_HEventDungeonsListInitialized): void 
		{
			InitDungeons(event.dungeonsList);
		}
		
		public function InitDungeons(dungeonsList: Object):void 
		{
			_panelItems = new Vector.<DungeonsPanelItem>;
			removeChildren();
			for (var i: int = 0; i < dungeonsList.length; i++)
			{
				var item: DungeonsPanelItem = new DungeonsPanelItem;
				item.ParseDungeon(dungeonsList[i]);
				_panelItems.push(item);
				addChild(item);
			}
			for (i = 0; i < 3; i++)
			{
				addChild(new EmptyDungeon);
			}			
		}
		
		public function UpdateDungeons(dungeonsList: Vector.<Dungeon>):void 
		{
			for (var i: int = 0; i < dungeonsList.length; i++)
			{
				_panelItems[i].ParseDungeon(dungeonsList[i]);
			}
		}
		
		public function OnGlobalEnterFrame(event: EnterFrameEvent):void 
		{
			//for (var i: int = 0; i < _panelItems.length; i++)
			//{
				//_panelItems[i].OnEnterFrame(event);
			//}
		}
		
		public function OnGlobalTouch(event: TouchEvent):void 
		{
			for (var i: int = 0; i < _panelItems.length; i++)
			{
				_panelItems[i].OnTouch(event);
			}
		}		
		
		protected function OnDungeonLevelSet(event: L_HEventDungeonLevelSet): void 
		{
			for (var i: int = 0; i < _panelItems.length; i++)
			{
				if (_panelItems[i].dungeon.type !== event.dungeon.type)
					_panelItems[i].dungeon.current = false;
				else
					_panelItems[i].dungeon.current = true;
				_panelItems[i].CheckCurrent();
			}
		}		
		
		
	}

}