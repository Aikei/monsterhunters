package view.gameviewelements.dungeonpanel 
{
	import d_hentity.HEntity;
	import starling.display.Image;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class EmptyDungeon extends HEntity 
	{
		private var _image: Image = new Image(Assets.interfaceAtlas.getTexture("dungeon_beta"));
		public function EmptyDungeon() 
		{
			super(false, true);
			showTooltipOnHover = true;
			tooltipText = "NotAvailableInBeta";
			_image.readjustSize();
			addChild(_image);
			//addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		//private function OnAddedToStage(e:Event):void 
		//{
			//removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			//
		//}
		
	}

}