package view.gameviewelements.dungeonpanel 
{
	import feathers.text.BitmapFontTextFormat;
	import hevents.HEventNextLevel;
	import hevents.L_HEventDungeonLevelSet;
	import logic.Dungeon;
	import logic.DungeonEffect;
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.events.Touch;
	import starling.events.TouchPhase;
	import d_hentity.HEntity;
	import starling.utils.HAlign;
	import view.gameviewelements.HButton;
	import view.gameviewelements.HText;
	import view.gameviewelements.SimpleHButton;
	/**
	 * ...
	 * @author Aikei
	 */
	public class DungeonsPanelItem extends HEntity 
	{
		public static var START_X: Number = 84;
		
		public static var DUNGEON_NAME_TEXT_X: Number = START_X;		
		public static var DUNGEON_NAME_TEXT_Y: Number = 16;
		
		public static var DUNGEON_EFFECT_COMMENT_X: Number = START_X;		
		public static var DUNGEON_EFFECT_COMMENT_Y: Number = DUNGEON_NAME_TEXT_Y+20;
		
		public static var DUNGEON_EFFECT_DESCRIPTION_TEXT_X: Number = START_X;
		public static var DUNGEON_EFFECT_DESCRIPTION_TEXT_Y: Number = DUNGEON_EFFECT_COMMENT_Y+15;
		
		public static var GO_TO_BUTTON_X: Number = 85;
		public static var GO_TO_BUTTON_Y: Number = 99;
		
		public static var LEVEL_TEXT_CENTER_X: Number = 346;
		public static var LEVEL_TEXT_Y: Number = 20;
		
		public static var DUNGEON_ICON_CENTER_X: Number = 45;
		public static var DUNGEON_ICON_CENTER_Y: Number = 52;
		
		protected var _dungeon: Dungeon;
		//protected var _bottomLayer: HEntity = new HEntity;
		protected var _backgroundImage: Image = new Image(Assets.interfaceAtlas.getTexture("dungeon"));
		protected var _dungeonNameText: HText = new HText("", "bbold15", -1, 0xffd39b);
		protected var _levelText: HText  = new HText("", "bbold12", -1, 0x9797aa);
		protected var _dungeonEffectCommentText: HText = new HText(Language.GetString("EffectOfTheDay")+":", "bbold12", -1, 0x9797aa);
		protected var _dungeonEffectText: HText = new HText("", "bregular12", -1, 0x9797aa);
		protected var _dungeonIcon: Image = null;
		protected var _dungeonIconHolder: HEntity = new HEntity;
		protected var _gotoButton: SimpleHButton;
		
		
		
		public function get dungeon(): Dungeon 
		{
			return _dungeon;
			//_dungeonIconHolder.InitTouch();
			//_dungeonIconHolder.showTooltipOnHover = true;
		}		
					
		public function DungeonsPanelItem() 
		{
			super();
			_useWorldsPassedTime = true;
			//InitTouch();
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);	
			
			_gotoButton = new SimpleHButton(Assets.interfaceAtlas.getTexture("into_the_dungeon_button"));
			_gotoButton.disabledSkin = new Image(Assets.interfaceAtlas.getTexture("into_the_dungeon_button_inactive"));
			_gotoButton.defaultLabelProperties.textFormat = new BitmapFontTextFormat("bbold12", 12, 0x300b06);
			_gotoButton.disabledLabelProperties.textFormat = new BitmapFontTextFormat("bbold12", 12, 0x6a6a7d);
			//_gotoButton.label = Language.GetString("IntoTheDungeonButtonLabel");
			
			_gotoButton.x = GO_TO_BUTTON_X;
			_gotoButton.y = GO_TO_BUTTON_Y;
			_gotoButton.addEventListener(Event.TRIGGERED, OnGoToButtonTriggered);
			
			_dungeonNameText.x = DUNGEON_NAME_TEXT_X;
			_dungeonNameText.y = DUNGEON_NAME_TEXT_Y;
			//_dungeonNameText.InitTouch();
			//_dungeonNameText.showTooltipOnHover = true;
			
			//_levelText.x = LEVEL_TEXT_X;
			_levelText.y = LEVEL_TEXT_Y;
			//_levelText.InitTouch();
			//_levelText.showTooltipOnHover = true;
			
			_dungeonEffectCommentText.x = DUNGEON_EFFECT_COMMENT_X;
			_dungeonEffectCommentText.y = DUNGEON_EFFECT_COMMENT_Y;
			
			_dungeonEffectText.textField.autoSize = TextFieldAutoSize.VERTICAL;
			_dungeonEffectText.textField.width = _backgroundImage.texture.width - DUNGEON_EFFECT_DESCRIPTION_TEXT_X - 4;
			_dungeonEffectText.textField.hAlign = HAlign.LEFT;			
			_dungeonEffectText.x = DUNGEON_EFFECT_DESCRIPTION_TEXT_X;
			_dungeonEffectText.y = DUNGEON_EFFECT_DESCRIPTION_TEXT_Y;
			_dungeonEffectText.InitTouch();
			
			//_dungeonEffectText.showTooltipOnHover = true;
			//World.gWorld.addEventListener("N_DungeonsData", OnDungeonsData);
			//addEventListener(TouchEvent.TOUCH, OnTouch);
		}
		
		private function OnGoToButtonTriggered(e:Event):void 
		{
			if (!dungeon.current)
				World.gameController.ChangeDungeon(dungeon.type);			
		}
		
		//override protected function OnHover():void 
		//{
			//if (!_hovered)
			//{
				//filter = Filters.highlightFilter;
			//}
			//super.OnHover();
		//}
		//
		//override protected function OnStopHover():void 
		//{
			//if (_hovered)
				//filter = null;
			//super.OnStopHover();			
		//}		
		
		//private function OnDungeonsData(e: Event):void 
		//{
			//
		//}
		
		public function CheckCurrent(): void 
		{
			_levelText.text = Language.GetString("Level") + " " + dungeon.level;
			_levelText.x = Math.round(LEVEL_TEXT_CENTER_X - _levelText.width / 2);
			if (!dungeon.current)
			{
				//_dungeonNameText.textField.color = 0xffd39b;
				//_levelText.textField.color = 0x9797aa;
				//_levelText.textField.text = Language.GetString("Level") + " " + dungeon.level;
				_gotoButton.isEnabled = true;
				_gotoButton.label = Language.GetString("IntoTheDungeonButtonLabel").toUpperCase()+"  ";
			}
			else
			{
				//_dungeonNameText.textField.color = Color.WHITE;
				//_backgroundImage.texture = Assets.interfaceAtlas.getTexture("dungeon_current");		
				//_levelText.textField.color = Color.WHITE;
				//_levelText.textField.text = Language.GetString("YouAreHere");
				_gotoButton.isEnabled = false;
				_gotoButton.label = Language.GetString("YouAreHere").toUpperCase();
				//_dungeonEffectCommentText.text = Language.GetString("YouAreHere");
			}
		}			
		
		//override protected function OnTouchPhaseBegan():void 
		//{
			//if (!dungeon.current)
				//World.gameController.ChangeDungeon(dungeon.type);
		//}
		
		//protected function OnTouch(event: TouchEvent):void 
		//{
			//var touch:Touch = event.getTouch(this, TouchPhase.BEGAN);
			//if (touch && !dungeon.current)
			//{
				//World.gameController.ChangeDungeon(dungeon.type);
			//}
		//}
		
		protected function OnAddedToStage(event: Event):void 
		{
			addChild(_dungeonIconHolder);
			addChild(_backgroundImage);
			addChild(_dungeonNameText);
			addChild(_levelText);
			addChild(_dungeonEffectText);
			addChild(_dungeonEffectCommentText);
			addChild(_gotoButton);
		}
		
		public function ParseDungeon(dungeon: Dungeon): void 
		{
			//var s: String = Language.GetString("MonstersPossibleInDungeon");
			//var s: String = Language.GetString("PlayersInThisDungeon")+dungeon.numberOfPlayers.toString();
			//_dungeonNameText.tooltipText = s;
			//_levelText.tooltipText = s;
			var t: Texture = Assets.interfaceAtlas.getTexture("dungeon_icon_" + dungeon.type);
			if (t != null)
			{
				if (_dungeonIcon == null)
				{
					_dungeonIcon = new Image(t);
					_dungeonIconHolder.addChild(_dungeonIcon);
				}
				//_dungeonIconHolder.InitTouch();
				//_dungeonIconHolder.tooltipText = s;

				_dungeonIcon.x = DUNGEON_ICON_CENTER_X;
				_dungeonIcon.y = DUNGEON_ICON_CENTER_Y;
				_dungeonIcon.alignPivot();
			}
			_dungeonNameText.textField.text = dungeon.name;
			//if (dungeon.current)
				//_dungeonNameText.textField.color = Color.WHITE;
			//else
				//_dungeonNameText.textField.color = 0xffd39b;
			_dungeonNameText.textField.x = Math.round(_dungeonNameText.textField.x);
			_dungeonNameText.textField.y = Math.round(_dungeonNameText.textField.y);				
			_dungeonEffectText.textField.text = "";
			//_dungeonEffectText.tooltipText = "";
			for (var i: int = 0; i < dungeon.effects.length; i++)
			{				
				_dungeonEffectText.textField.text += dungeon.effects[i].explanation;
				//if (dungeon.effects[i].type === DungeonEffect.EFFECT_MONSTERS_WITH_TRAITS)
					//_dungeonEffectText.tooltipText += Language.GetString("ExplainTrait" + dungeon.effects[i].args[0]);
				if (i + 1 < dungeon.effects.length)
				{
					//_dungeonEffectText.tooltipText += "; ";
					_dungeonEffectText.textField.text += "; ";
				}
			}
			_dungeonEffectText.textField.x = Math.round(_dungeonEffectText.textField.x);
			_dungeonEffectText.textField.y = Math.round(_dungeonEffectText.textField.y);
			_dungeon = dungeon;
			CheckCurrent();
		}
		
	}

}