package view.gameviewelements 
{
	import feathers.events.FeathersEventType;
	import feathers.text.BitmapFontTextFormat;
	import starling.display.Image;
	import starling.events.Event;
	import starling.utils.VAlign;
	import view.gameviewelements.PanelButton;
	import feathers.controls.ScrollContainer;
	import feathers.layout.HorizontalLayout;
	import starling.utils.Color;
	import view.gameviewelements.OldPanelButton;
	/**
	 * ...
	 * @author Aikei
	 */
	public class PanelPickButtons extends ScrollContainer 
	{
		//1
		
		//public var weaponsButton: OldPanelButton = new OldPanelButton();
		//public var heroesButton: OldPanelButton = new OldPanelButton();
		//public var skillsButton: OldPanelButton = new OldPanelButton();
		
		//2
		
		//public var weaponsButton: PanelButton = new PanelButton();
		//public var heroesButton: PanelButton = new PanelButton();
		//public var skillsButton: PanelButton = new PanelButton();		
		
		public var weaponsButton: HToggleButton = new HToggleButton;
		public var heroesButton: HToggleButton = new HToggleButton;
		public var skillsButton: HToggleButton = new HToggleButton;
		
		private var pressedButtonTextFormat: BitmapFontTextFormat = new BitmapFontTextFormat("bbold15", 15, 0x071218);
		private var unpressedButtonTextFormat: BitmapFontTextFormat = new BitmapFontTextFormat("bbold15", 15, 0x1D3A4B);
		private var skillsUnpickedIcon: SkillsAvailableIcon = new SkillsAvailableIcon;
		
		public function PanelPickButtons() 
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			
			weaponsButton.label = Language.GetString("Weapons").toLocaleUpperCase();			
			weaponsButton.defaultLabelProperties.textFormat = unpressedButtonTextFormat; //new BitmapFontTextFormat("bbold15", 15, 0x071218);
			weaponsButton.defaultLabelProperties.filter = null;
			weaponsButton.hoverLabelProperties.textFormat = unpressedButtonTextFormat;
			weaponsButton.hoverLabelProperties.filter = Filters.highlightFilter;
			weaponsButton.defaultSelectedLabelProperties.textFormat = pressedButtonTextFormat;
			
			heroesButton.label = Language.GetString("Heroes").toLocaleUpperCase();			
			heroesButton.defaultLabelProperties.textFormat = unpressedButtonTextFormat; //new BitmapFontTextFormat("bbold15", 15, 0x071218);
			heroesButton.defaultLabelProperties.filter = null;
			heroesButton.hoverLabelProperties.textFormat = unpressedButtonTextFormat;
			heroesButton.hoverLabelProperties.filter = Filters.highlightFilter;
			heroesButton.defaultSelectedLabelProperties.textFormat = pressedButtonTextFormat;
			
			skillsButton.label = Language.GetString("Skills").toLocaleUpperCase();			
			skillsButton.defaultLabelProperties.textFormat = unpressedButtonTextFormat; //new BitmapFontTextFormat("bbold15", 15, 0x071218);
			skillsButton.defaultLabelProperties.filter = null;
			skillsButton.hoverLabelProperties.textFormat = unpressedButtonTextFormat;
			skillsButton.hoverLabelProperties.filter = Filters.highlightFilter;
			skillsButton.defaultSelectedLabelProperties.textFormat = pressedButtonTextFormat;
			
			weaponsButton.isSelected = true;
			heroesButton.isSelected = false;
			skillsButton.isSelected = false;
			weaponsButton.isToggle = false;
			//1
			
			//weaponsButton.SetActiveLabel(Language.GetString("Weapons").toLocaleUpperCase(), "bbold15", 0x071218);			
			//weaponsButton.SetInactiveLabel(Language.GetString("Weapons").toLocaleUpperCase(), "bbold15", 0x1D3A4B);
			//weaponsButton.SetHoveredInactiveBitmapLabel(Language.GetString("Weapons").toLocaleUpperCase(), "bbold15", 0x1D3A4B, Filters.highlightFilter);
			//
			//heroesButton.SetActiveLabel(Language.GetString("Heroes").toLocaleUpperCase(), "bbold15", 0x071218);
			//heroesButton.SetInactiveLabel(Language.GetString("Heroes").toLocaleUpperCase(), "bbold15", 0x1D3A4B);
			//heroesButton.SetHoveredInactiveBitmapLabel(Language.GetString("Heroes").toLocaleUpperCase(), "bbold15", 0x1D3A4B, Filters.highlightFilter);
			//
			//skillsButton.SetActiveLabel(Language.GetString("Skills").toLocaleUpperCase(), "bbold15", 0x071218);
			//skillsButton.SetInactiveLabel(Language.GetString("Skills").toLocaleUpperCase(), "bbold15", 0x1D3A4B);
			//skillsButton.SetHoveredInactiveBitmapLabel(Language.GetString("Skills").toLocaleUpperCase(), "bbold15", 0x1D3A4B, Filters.highlightFilter);
			//
			//weaponsButton.SetActive(true);
			//heroesButton.SetActive(false);
			//skillsButton.SetActive(false);
			
			//2
			
			//weaponsButton.defaultLabelProperties.textFormat = pressedButtonTextFormat;
			//weaponsButton.label = Language.GetString("Weapons").toLocaleUpperCase();
			//
			//heroesButton.defaultLabelProperties.textFormat = unpressedButtonTextFormat;
			//heroesButton.label = Language.GetString("Heroes").toLocaleUpperCase();
						////
			//skillsButton.defaultLabelProperties.textFormat = unpressedButtonTextFormat;
			//skillsButton.label = Language.GetString("Skills").toLocaleUpperCase();
			
			var layout: HorizontalLayout = new HorizontalLayout;
			layout.gap = 25;
			layout.lastGap = 5;
			layout.verticalAlign = VAlign.CENTER;
			this.layout = layout;
			this.x = 44;
			this.y = 160;
			this.width = 278;
			this.height = 30;
		}
				
		protected function OnWeaponsButtonTriggered(event: Event): void 
		{
			if (weaponsButton.isSelected)
			{
				weaponsButton.isToggle = false;
				heroesButton.isSelected = false;
				heroesButton.isToggle = true;
				skillsButton.isSelected = false;
				skillsButton.isToggle = true;
			}
			
			//2
			//weaponsButton.SetActive(true);
			//heroesButton.SetActive(false);
			//skillsButton.SetActive(false);
			
			//1
			//weaponsButton.defaultLabelProperties.textFormat = pressedButtonTextFormat;
			//heroesButton.defaultLabelProperties.textFormat = unpressedButtonTextFormat;
			//skillsButton.defaultLabelProperties.textFormat = unpressedButtonTextFormat;
		}
		
		protected function OnHeroesButtonTriggered(event: Event): void 
		{
			
			if (heroesButton.isSelected)
			{
				heroesButton.isToggle = false;
				weaponsButton.isSelected = false;
				weaponsButton.isToggle = true;
				skillsButton.isSelected = false;
				skillsButton.isToggle = true;
			}			
			
			//2
			//weaponsButton.SetActive(false);
			//heroesButton.SetActive(true);
			//skillsButton.SetActive(false);
			
			//1
			//weaponsButton.defaultLabelProperties.textFormat = unpressedButtonTextFormat;
			//heroesButton.defaultLabelProperties.textFormat = pressedButtonTextFormat;
			//skillsButton.defaultLabelProperties.textFormat = unpressedButtonTextFormat;
		}
		
		protected function OnSkillsButtonTriggered(event: Event): void 
		{
			if (skillsButton.isSelected)
			{
				skillsButton.isToggle = false;
				heroesButton.isSelected = false;
				heroesButton.isToggle = true;
				weaponsButton.isSelected = false;
				weaponsButton.isToggle = true;
			}			
			
			//2
			//weaponsButton.SetActive(false);
			//heroesButton.SetActive(false);
			//skillsButton.SetActive(true);
			
			//1
			//weaponsButton.defaultLabelProperties.textFormat = unpressedButtonTextFormat;
			//heroesButton.defaultLabelProperties.textFormat = unpressedButtonTextFormat;
			//skillsButton.defaultLabelProperties.textFormat = pressedButtonTextFormat;
		}		
		
		protected function OnAddedToStage(event: Event): void 
		{
			addChild(weaponsButton);
			addChild(heroesButton);
			addChild(skillsButton);
			addChild(skillsUnpickedIcon);
						
			weaponsButton.addEventListener(Event.CHANGE, OnWeaponsButtonTriggered);
			heroesButton.addEventListener(Event.CHANGE, OnHeroesButtonTriggered);			
			skillsButton.addEventListener(Event.CHANGE, OnSkillsButtonTriggered);
					
		}		
		
	}

}