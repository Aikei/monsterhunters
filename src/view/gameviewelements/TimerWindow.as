package view.gameviewelements 
{
	import com.greensock.TweenLite;
	import d_hentity.HEntity;
	import starling.display.Image;
	import starling.events.Event;
	import logic.MonsterData;
	import starling.events.EnterFrameEvent;
	import hevents.L_HEventMonsterKilled;
	import view.MonsterSprite;
	/**
	 * ...
	 * @author Aikei
	 */
	public class TimerWindow extends HEntity 
	{
		private var _timerHolder: HEntity = new HEntity;
		private var _timerImage: Image = new Image(Assets.interfaceAtlas.getTexture("timer"));
		private var _timeUpImage: Image = new Image(Assets.interfaceAtlas.getTexture("timesup"));
		private var _timerText: HText = new HText("", "timerFont", -1);
		private var _creationTime: Number = 0;
		private var _timerDuration: Number = 30;
		private var _currentTimer: Number;
		
		public function TimerWindow() 
		{
			super(true);
			this.x = 140;
			this.y = 130;
			
			_timeUpImage.x = 80;
			_timeUpImage.y = 100;
			
			_timerText.y = 14;
			
			_timerText.x = _timerImage.x + _timerImage.width + 10;
			_timerHolder.visible = false;
			_timerHolder.InitTouch();
			_timerHolder.tooltipText = "ExplainTimer";
			_timerHolder.showTooltipOnHover = true;
			
			_timeUpImage.alignPivot();
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			World.gWorld.addEventListener("N_NewMonster", OnNewMonster);
			World.gWorld.addEventListener(L_HEventMonsterKilled.H_EVENT_MONSTER_KILLED, OnMonsterKilled);
		}
		
		protected function OnMonsterKilled(event: L_HEventMonsterKilled):void 
		{
			_timerHolder.visible = false;
		}
		
		protected function OnAddedToStage(event: Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_timerHolder);
			_timerHolder.addChild(_timerImage);
			_timerHolder.addChild(_timerText);
			addChild(_timeUpImage);		
		}
		
		protected function OnNewMonster(event: Event): void 
		{			
			_timeUpImage.visible = false;
			//TweenLite.to(_timeUpImage, 0.5, { alpha: 0, onComplete: function() { _timeUpImage.visible = false; } } );
			_timerHolder.visible = true;
			_creationTime = MonsterData(event.data.monsterData).creationTime;
			_timerDuration = MonsterData(event.data.monsterData).timerDuration;
			_timerText.textField.text = Number(_timerDuration - (Misc.GetServerTime() - _creationTime)).toFixed(1);
		}
		
		override protected function OnEnterFrame(event: EnterFrameEvent): void 
		{			
			if (_timerHolder.visible)
			{
				_currentTimer = _timerDuration - (Misc.GetServerTime() - _creationTime);
				if (_currentTimer <= 0)
				{
					World.gameController.RunMonster();
					_timerHolder.visible = false;
					_timeUpImage.visible = true;	
					TweenLite.from(_timeUpImage, 0.5, { y: 300, alpha: 0 } );
				}
				else
					_timerText.textField.text = Language.SetDecimalDivider(_currentTimer.toFixed(1));
			}
		}
	}

}