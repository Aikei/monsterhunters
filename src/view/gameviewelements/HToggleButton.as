package view.gameviewelements 
{
	import feathers.controls.ToggleButton;
	import starling.events.Event;
	import view.gameviewelements.custom.HoverChecker;
	import system.HSounds;
	/**
	 * ...
	 * @author Aikei
	 */
	public class HToggleButton extends ToggleButton 
	{
		public var hoverChecker: HoverChecker = new HoverChecker(this);
		
		public function HToggleButton() 
		{
			super();
			hoverChecker.addEventListener("H_Hover", OnHover);
			hoverChecker.addEventListener("H_StopHover", OnStopHover);
			this.addEventListener(Event.TRIGGERED, OnTriggered);				
		}
		
		protected function OnTriggered(e:Event):void 
		{
			if (isToggle)
				HSounds.PlaySound("ClickSound");
		}
		
		protected function OnStopHover(e:Event):void 
		{
			
		}
		
		protected function OnHover(e:Event):void 
		{
			if (isToggle)
				HSounds.PlaySound("HoverSound");
		}
				
		public function Destroy():void 
		{
			hoverChecker.Destroy();
			removeFromParent(true);
		}		
		
	}

}