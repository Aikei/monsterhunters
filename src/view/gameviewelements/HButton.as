package view.gameviewelements 
{
	import starling.events.Event;
	import feathers.controls.Button;
	import system.HSounds;
	import view.gameviewelements.custom.HoverChecker;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HButton extends Button 
	{
		public var hoverChecker: HoverChecker = new HoverChecker(this);
		
		public function HButton() 
		{
			super();
			hoverChecker.addEventListener("H_Hover", OnHover);
			hoverChecker.addEventListener("H_StopHover", OnStopHover);	
			this.addEventListener(Event.TRIGGERED, OnTriggered);
		}
		
		private function OnTriggered(e:Event):void 
		{
			HSounds.PlaySound("ClickSound");
		}
		
		private function OnStopHover(e:Event):void 
		{
			
		}
		
		private function OnHover(e:Event):void 
		{
			HSounds.PlaySound("HoverSound");
		}
		
		public function Destroy():void 
		{
			hoverChecker.Destroy();
			removeFromParent(true);
		}			
	}

}