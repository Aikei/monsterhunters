package view.gameviewelements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import logic.Weapon;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.events.Event;
	import starling.text.TextField;
	import hevents.*;
	import starling.text.TextFieldAutoSize;
	import starling.utils.Color;
	import com.greensock.TweenLite;
	import com.greensock.TimelineLite;
	import system.Analytics;
	import system.HSounds;
	import view.BitmappedText;
	
	public class WeaponPanelItem extends ScrollContainerItem 
	{
		public static var DAMAGE_COMMENT_TEXT_Y: Number = 42;
		public static var DAMAGE_VALUE_TEXT_Y: Number = 60;
		
		protected var _weaponType: int;
		protected var _old: Boolean = false;
		//protected var _damageCommentText: BatchedBitmapLabel;
		//protected var _damageValueText: BatchedBitmapLabel;
		//protected var _levelText: BatchedBitmapLabel;
		
		protected var _damageCommentText: HText;
		protected var _damageValueText: HText;
		protected var _levelText: HText;		
		
		protected var _weapon: Weapon = null;
		protected var _triggered: Boolean = false;
		protected var _weaponBought: Boolean = false;
		
		public function WeaponPanelItem(weaponType: int)
		{
			super();
			_upgradeImage.texture = Assets.interfaceAtlas.getTexture("upgrade_weapon");
			_image.texture = Assets.interfaceAtlas.getTexture("weapon_item");
			_weaponType = weaponType;
			if (!World.data.weapons[_weaponType].animated)
				_itemPortrait = new Image(Assets.interfaceAtlas.getTexture("weapon_" + weaponType));
			else
			{
				_itemAnimatedPortrait = new MovieClip(Assets.miscAtlas.getTextures("weapon" + weaponType+"_"));								
			}
			//_itemPortrait.x = ITEM_PORTRAIT_X + _itemPortrait.width / 2;
			//_itemPortrait.y = ITEM_PORTRAIT_Y + _itemPortrait.height / 2;
			itemPortraitX += 4;
			itemPortraitY += 4;
			_itemPortraitHolder.InitTouch();
			_itemPortraitHolder.showTooltipOnHover = true;
			//_itemNameText.SetTextBeforeAddingToStage(Language.GetString("Weapon" + _weaponType.toString()));
			
			_itemNameText.text = Language.GetString("Weapon" + _weaponType.toString());
			
			//_levelText = new BatchedBitmapLabel("bbold16", 16, 0xd2b5a6);
			_levelText = new HText("", "bbold16", 16, 0xd2b5a6);
			_levelText.x = _itemNameText.x+_itemNameText.width;
			_levelText.y = ITEM_NAME_TEXT_Y;
			_levelText.touchable = false;			
			
			//_damageValueText = new BatchedBitmapLabel("bbold16", 16, 0xd2b6a6);
			_damageValueText = new HText("","bbold16", 16, 0xd2b6a6);
			_damageValueText.x = _itemNameText.x;
			_damageValueText.y = DAMAGE_VALUE_TEXT_Y;
			_damageValueText.touchable = false;
			
			//_damageCommentText = new BitmappedText(Language.GetString("Damage"), "bbold12", 0x977761, 0, 0, "left");
			//_damageCommentText.x = 124;
			//_damageCommentText.y = 46;
			//_damageCommentText.touchable = false;			
			
			//_damageCommentText = new BatchedBitmapLabel("bbold12", 12, 0x977761);
			//_damageCommentText.text = Language.GetString("Damage");
			_damageCommentText = new HText(Language.GetString("DamagePerClick"), "bbold12", 12, 0x977761);
			_damageCommentText.x = _itemNameText.x;
			_damageCommentText.y = DAMAGE_COMMENT_TEXT_Y;
			_damageCommentText.touchable = false;			
			
			//_buyButton.bitmaplabel = Language.GetString("Buy");
			
			World.gWorld.addEventListener("untriggerButton", OnUntrigger);
			World.gWorld.addEventListener(L_HEventWeaponSet.L_HEVENT_WEAPON_SET, OnWeaponSet);
			World.gWorld.addEventListener(L_HEventWeaponUpgraded.L_HEVENT_WEAPON_UPGRADED, OnWeaponUpgraded);
			
			//_buyButton.InitTouch();
			_buyButton.showTooltipOnHover = true;						
		}
		
		override public function Destroy():void 
		{			
			World.gWorld.removeEventListener("untriggerButton", OnUntrigger);
			World.gWorld.removeEventListener(L_HEventWeaponSet.L_HEVENT_WEAPON_SET, OnWeaponSet);
			World.gWorld.removeEventListener(L_HEventWeaponUpgraded.L_HEVENT_WEAPON_UPGRADED, OnWeaponUpgraded);
			super.Destroy();
		}
		
		public function SetOld():void 
		{
			_old = true;
			_buyButton.visible = false;
			_itemCostText.visible = false;
			_itemPortrait.filter = Filters.greyScaleFiler;
			_image.texture = Assets.interfaceAtlas.getTexture("weapon_item_inactive");
			if (_notBought)
			{
				_notBought.removeFromParent(true);
				_notBought = null;				
			}
			World.gWorld.removeEventListener("N_CoinsNow", OnCoinsNow);
		}
		
		public function SetBought():void 
		{
			_weaponBought = true;
		}
		
		//override protected function OnAddedToStage(event: Event):void 
		//{
			//super.OnAddedToStage(event);
			//
			////_itemNameText = BitmappedText.ResetText(Language.GetString("Weapon" + String(_weaponType)), _itemNameText);
			////_itemNameText.SetTextBeforeAddingToStage(Language.GetString("Weapon" + String(_weaponType)));
			////_itemNameText.text = Language.GetString("Weapon" + String(_weaponType));
			//
			//addChild(_levelText);						
			//addChild(_damageCommentText);						
			//addChild(_damageValueText);						
		//}
		
		override protected function AddImagesToStage():void 
		{
			super.AddImagesToStage();
		}
		
		override protected function AddTextToStage():void 
		{
			super.AddTextToStage();
			addChild(_levelText);
			addChild(_damageCommentText);
			addChild(_damageValueText);				
		}
		
		public function Init(weapon: Weapon, gameData: Object): void 
		{
			_weapon = weapon;
			UpdateWeaponData();
			var coins: BigInt = BigInt.shStr2BigInt(gameData.coins);
			_itemPortraitHolder.tooltipText = "ExplainWeapon" + weapon.type;
			AddChildrenToStage();
			CheckCost(coins);						
		}
		
		protected function OnUntrigger():void 
		{
			_triggered = false;
		}		
		
		override protected function OnBuyButtonTriggered(event: Event): void 
		{
			_triggered = true;
			if (!_weaponBought)
			{
				//Analytics.TrackEvent("Buy", "Weapon " + _weaponType); 
				World.gWorld.dispatchEventWith("weaponBuyButtonClicked",false,_weaponType);
			}
			else
			{
				World.gWorld.dispatchEventWith("weaponUpgradeButtonClicked",false,_weaponType);
			}			
		}
		
		protected function OnWeaponUpgraded(event: L_HEventWeaponUpgraded): void 
		{
			_triggered = false;
			//Analytics.TrackEvent("Upgrade", "Weapon " + _weaponType, "new damage: "+BigInt.bigint2HString(_weapon.damage.value));
			if (_weaponBought && _weaponType === event.weapon.type)
			{
				if (event.startAnimation)
					FireUpgradeAnimation();
				_weapon = event.weapon;
				UpdateWeaponData();
			}			
		}
		
		protected function OnWeaponSet(event: L_HEventWeaponSet): void 
		{
			if (_weaponType === event.weapon.type)
			{
				if (event.startAnimation)
					FireUpgradeAnimation();
				_triggered = false;
				_weaponBought = true;								
				_buyButton.label = Language.GetString("Upgrade");
				_weapon = event.weapon;
				UpdateWeaponData();
			}
			else if (_weaponType < event.weapon.type)
			{
				SetOld();
				//Destroy();
			}
		}
		
		protected function UpdateWeaponData(): void 
		{
			if (_weaponBought)
			{
				_notBought.removeFromParent(true);
				if (!World.data.weapons[_weaponType].animated)
					_itemPortrait.texture = Assets.interfaceAtlas.getTexture("weapon_" + _weaponType);	
				else
				{
					if (_itemAnimatedPortrait)
					{
						_itemAnimatedPortrait.stop();
						Starling.juggler.remove(_itemAnimatedPortrait);
						_itemAnimatedPortrait.removeFromParent(true);
						_itemAnimatedPortrait = null
					}
					_itemAnimatedPortrait = new MovieClip(Assets.miscAtlas.getTextures("weapon" + _weaponType+"_"));
					Starling.juggler.add(_itemAnimatedPortrait);
					_itemAnimatedPortrait.play();
					_itemPortraitHolder.addChild(_itemAnimatedPortrait);
				}
				_damageValueText.text = BigInt.bigint2HString(_weapon.damage.value);
				//_levelText.x = _itemNameText.x+_itemNameText.width;
				_levelText.text = " " + Language.GetString("Lv") + " " + BigInt.bigint2HString(_weapon.level);
				_levelText.x = _itemNameText.x+_itemNameText.width;			
				_itemCostText.text = BigInt.bigint2HString(_weapon.levelUpCost);
				_cost = _weapon.levelUpCost;
				_buyButton.tooltipText = Language.GetString("DamageAtLevel") + BigInt.bigint2HString(_weapon.nextLevelWeapon.damage.value);
				_buyButtonTooltipHolder.tooltipText = _buyButton.tooltipText;
				//_buyButton.tooltipText = _buyButton.tooltipText.replace("$l", BigInt.bigint2HString(_weapon.nextLevelWeapon.level));				
			}
			else
			{
				_cost = _weapon.buyCost;
				_buyButton.tooltipText = Language.GetString("DamageAtLevel") + BigInt.bigint2HString(_weapon.damage.value);
				_buyButtonTooltipHolder.tooltipText = _buyButton.tooltipText;
				_damageValueText.text = BigInt.bigint2HString(_weapon.damage.value);
				_itemCostText.text = BigInt.bigint2HString(_weapon.buyCost);
			}
		}
		
	}

}