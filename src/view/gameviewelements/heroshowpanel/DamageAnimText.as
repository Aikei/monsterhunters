package view.gameviewelements.heroshowpanel 
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import d_hentity.HEntity;
	import eng.particle.ParticleSystem;
	import flash.geom.Rectangle;
	import starling.display.Image;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import system.HSounds;
	import view.BitmappedText;
	import view.gameviewelements.BatchedBitmapLabel;
	import view.gameviewelements.HText;
	import view.particles.HypnotizedSwordParticleSystem;
	import view.particles.StunSwordParticleSystem;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class DamageAnimText extends HEntity 
	{
		static public var MOVE_IN_TIME: Number = 0.1;		//! may be set in script
		static public var DISAPPEAR_TIME: Number = 0.9;		//! may be set in script
		static private var _pool: Vector.<DamageAnimText> = new Vector.<DamageAnimText>;
		
		private var _bottomEffect: Image = new Image(Assets.interfaceAtlas.getTexture("sword_stun_glow"));
		private var _particlesHolder: HEntity = new HEntity;
		private var _sword: Image;
		private var _text: HText;
		private var _particles: ParticleSystem = null;
		
		public var newlyCreated: Boolean = true;
		
		public function DamageAnimText() 
		{			
			super(false, false);
			
			_useWorldsPassedTime = true;
			this.touchable = false;
			_sword = new Image(Assets.interfaceAtlas.getTexture("sword_red"));
			this.clipRect = new Rectangle( -200, -200, _sword.texture.width + 400, _sword.texture.height + 400);
			_text = new HText("", "bbold12", -1, 0xffffff);
			addChild(_bottomEffect);
			_bottomEffect.x = _sword.width / 2;
			_bottomEffect.y = _sword.height / 2;
			_bottomEffect.alignPivot();
			addChild(_particlesHolder);
			addChild(_sword);
			addChild(_text);
		}
		
		override public function get width():Number 
		{
			return _sword.width;
		}
		
		override public function get height():Number 
		{
			return _sword.height;
		}
		
		private function Init(eventData: Object):void 
		{
			_bottomEffect.visible = false;
			this.alpha = 1;
			if (_particles)
			{
				_particles.Destroy();
				_particles = null;
			}
			if (eventData.effectsData.wasHealingLastTime)
			{
				_sword.texture = Assets.interfaceAtlas.getTexture("sword_hypnotized");
				_particles = new HypnotizedSwordParticleSystem;
				_particles.x = _sword.texture.width / 2;
				addChild(_particles);
				_particles.Start();
			}
			else if (eventData.effectsData.wasStunningLastTime)
			{
				_sword.texture = Assets.interfaceAtlas.getTexture("sword_stun");
				_bottomEffect.texture = Assets.interfaceAtlas.getTexture("sword_stun_glow");
				_bottomEffect.visible = true;
				_particles = new StunSwordParticleSystem;
				_particles.x = _sword.texture.width / 2;
				_particles.y = _sword.texture.height / 2;
				_particlesHolder.addChild(_particles);
				_particles.Start();
			}
			else if (eventData.effectsData.wasEffectiveLastTime)
				_sword.texture = Assets.interfaceAtlas.getTexture("sword_red");
			else
				_sword.texture = Assets.interfaceAtlas.getTexture("sword_gray");
			_text.text = eventData.text;
			//addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			Misc.CenterObject(this, eventData.x, eventData.y);
			Misc.CenterObject(_text.textField, _sword.width / 2, _sword.height / 2 - 2);
			var time: Number = MOVE_IN_TIME;
			var r: Array = HSounds.PlaySound("HeroAttack");
			//if (r)
			//{
				//time = r[0].lengthInSecs;
			//}
			var tl: TimelineLite = new TimelineLite;
			tl.from(this, time, { y : this.y + 30 } );
			tl.to(this, DISAPPEAR_TIME, { alpha : 0, onComplete : Die }, "+="+time);
		}
		
		//private function OnAddedToStage(e:Event):void 
		//{
			//removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			//
		//}
		
		public function Die():void 
		{
			this.visible = false;
			OnEnterFrame
			//this.removeFromParent();
		}
		
		override public function OnEnterFrame(event: EnterFrameEvent): void 
		{
			if (_particles)
				_particles.OnGlobalEnterFrame(event);
		}
		
		static public function Get(eventData: Object): DamageAnimText
		{
			var newAnimText: DamageAnimText;
			if (_pool.length == 0)
				newAnimText = new DamageAnimText;
			else
			{
				newAnimText = _pool.pop();
				newAnimText.visible = true;
				newAnimText.newlyCreated = false;
			}
			newAnimText.Init(eventData);
			return newAnimText;
		}
		
	}

}