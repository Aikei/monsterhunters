package view.gameviewelements.heroshowpanel.visualizers 
{
	import com.greensock.TimelineLite;
	import com.greensock.TweenLite;
	import d_hentity.HEntity;
	import starling.display.Image;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HealVisualizer extends HEntity 
	{
		private var _icon: Image = new Image(Assets.interfaceAtlas.getTexture("heal_effect_icon"));
		private var _light: Image = new Image(Assets.interfaceAtlas.getTexture("heal_effect_light"));
		
		public function HealVisualizer() 
		{			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		override public function get width(): Number 
		{
			return _light.width;
		}
		
		override public function get height(): Number 
		{
			return _light.height;
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_light);
			addChild(_icon);
			_light.alignPivot();
			_icon.alignPivot();
			Start();
		}
		
		public function Start():void 
		{
			var tl: TimelineLite = new TimelineLite;
			tl.to(_light, 1.0, { rotation : Misc.PI } );
			tl.to(this, 0.5, { alpha: 0, onComplete: this.Destroy }, "-=0.5");
		}
		
	}

}