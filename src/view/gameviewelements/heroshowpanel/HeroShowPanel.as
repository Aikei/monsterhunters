package view.gameviewelements.heroshowpanel 
{
	import com.greensock.TweenLite;
	import d_hentity.HEntity;
	import feathers.controls.ScrollContainer;
	import feathers.layout.HorizontalLayout;
	import flash.geom.Rectangle;
	import logic.Affliction;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import hevents.L_HEventHeroBought;
	import logic.local.ActualHero;
	import view.GameView;

	/**
	 * ...
	 * @author Aikei
	 */
	public class HeroShowPanel extends ScrollContainer 
	{
		public static var PANEX_X: int = 10;
		public static var PANEL_Y: int = 620;
		
		//private var _heroSquares: Vector.<Image> = new Vector.<Image>;
		private var lastHeroBought: uint = 0;
		
		public function HeroShowPanel() 
		{
			super();
			var layout: HorizontalLayout = new HorizontalLayout;
			layout.gap = 6;
			this.snapScrollPositionsToPixels = true;
			this.layout = layout;
			this.width = Assets.interfaceAtlas.getTexture("barbarian_small").width*6+layout.gap*5;
			this.height = Assets.interfaceAtlas.getTexture("barbarian_small").height*2;
			this.x = PANEX_X;
			this.y = PANEL_Y;
			this.clipRect = new Rectangle( -200, -600, this.width+400, this.height+800);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			World.gWorld.addEventListener(L_HEventHeroBought.L_HEVENT_HERO_BOUGHT, OnHeroBought);
			World.gWorld.addEventListener("heroDamageInfo", OnHeroDamageInfo);
			World.gWorld.addEventListener("heroAnimStart", OnHeroAnimStart);
		}
		
		private function OnHeroAnimStart(e:Event):void 
		{
			var hero: ActualHero = e.data as ActualHero;
			var square: ShowPanelImageHolder = ShowPanelImageHolder(getChildAt(hero.id));
			//var square: ShowPanelImageHolder = ShowPanelImageHolder(getChildAt(int(e.data)));
			if (square.visible)
			{
				var animx: Number = square.x + square.image.width / 2;
				var animy: Number = square.y + square.image.height / 4;
				World.gWorld.dispatchEventWith("createAttackAnim", false, { x: PANEX_X + animx, y: PANEL_Y + animy, hero: hero } );
			}
		}
				
		private function OnHeroBought(event: L_HEventHeroBought): void
		{
			//var holder: ShowPanelImageHolder = ShowPanelImageHolder(getChildAt(event.hero.heroData.type));
			var holder: ShowPanelImageHolder = ShowPanelImageHolder(getChildAt(lastHeroBought));
			lastHeroBought++;
			holder.heroType = event.hero.heroData.type;
			holder.image.texture = Assets.interfaceAtlas.getTexture(event.hero.heroData.portraitName+"_small");
			holder.visible = true;
			TweenLite.from(holder, 1, { y: 200 } );
		}
		
		private function OnHeroDamageInfo(event: Event): void
		{
			if (GameView.SHOW_DAMAGE_TEXT)
			{
				var square: ShowPanelImageHolder = ShowPanelImageHolder(getChildAt(event.data.heroId));
				var textx: Number = square.x + square.image.width / 2;
				var texty: Number = square.y - 16;
				var text: String = BigInt.bigint2HString(event.data.damage);
				World.gWorld.dispatchEventWith("createDamageText", false, { x: PANEX_X + textx, y: PANEL_Y - 16, text: text, effectsData: event.data.effectsData } );
			}
		}		
		
		private function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			for (var i: int = 0; i < 6; i++)
			{
				var item: ShowPanelImageHolder = new ShowPanelImageHolder();
				addChild(item);
			} 
		}
		
	}

}