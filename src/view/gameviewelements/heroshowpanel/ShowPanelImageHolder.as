package view.gameviewelements.heroshowpanel 
{
	import com.greensock.TweenLite;
	import d_hentity.HEntity;
	import flash.geom.Rectangle;
	import logic.Affliction;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.textures.Texture;
	import view.gameviewelements.HText;
	import starling.events.EnterFrameEvent;
	/**
	 * ...
	 * @author Aikei
	 */
	public class ShowPanelImageHolder extends HEntity 
	{
		public static var AFFLICTION_TIME_TEXT_CENTER_X: Number = 32;
		public static var AFFLICTION_TIME_TEXT_CENTER_Y: Number = 8;
		
		public var image: Image = new Image(Assets.interfaceAtlas.getTexture("barbarian_small"));
		public var frame: Image = new Image(Assets.interfaceAtlas.getTexture("frame_basic"));
		public var overlayHolder: HEntity = new HEntity;
		public var overlay: Image = new Image(Assets.interfaceAtlas.getTexture("frame_basic"));		
		public var afflictionTimeText: HText = new HText("", "bbold12", 12, 0x1b1b1b);

		private var _affliction: Affliction = null;
		private var _heroType: int = -1;
		private var _timeTextPos: Vec2;
		private var _effectVisulizationHolder: HEntity = new HEntity;
		private var _effectVisualizaionObject: DisplayObject;
		
		private static var _temp: Number;
		
		public function get heroType(): int 
		{
			return _heroType;
		}
		
		public function set heroType(v: int): void 
		{
			_heroType = v;
		}		
		
		public function ShowPanelImageHolder() 
		{
			super();
			this.clipRect = new Rectangle( -100, -400, image.width + 200, image.height + 800);
			overlayHolder.visible = false;
			afflictionTimeText.visible = false;
			overlayHolder.InitTouch();
			overlayHolder.showTooltipOnHover = true;
			_timeTextPos = new Vec2(AFFLICTION_TIME_TEXT_CENTER_X, AFFLICTION_TIME_TEXT_CENTER_Y)
			Misc.CenterObject(afflictionTimeText, _timeTextPos.x, _timeTextPos.y);
			visible = false;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addEventListener(Event.ENTER_FRAME, OnEnterFrame);
			World.gWorld.addEventListener("addAffliction", OnAddAffliction);
			World.gWorld.addEventListener("removeAffliction", OnRemoveAffliction);
			World.gWorld.addEventListener("heroPortraitVisualize", OnVisualize);
		}
		
		private function OnVisualize(e:Event):void 
		{
			if (_heroType === int(e.data.heroType))
			{
				if (e.data.texture)
				{
					_effectVisualizaionObject = new Image(Texture(e.data.texture));
					TweenLite.to(_effectVisualizaionObject, 1.0, { alpha: 0, onComplete : function(): void { _effectVisualizaionObject.removeFromParent(true); } } );
				}
				else if (e.data.object)
				{
					_effectVisualizaionObject = e.data.object;
				}
				else
				{
					throw "wrong effect visualizer";
				}
				_effectVisulizationHolder.addChild(_effectVisualizaionObject);	
				_effectVisualizaionObject.x = image.x + image.width / 2;
				_effectVisualizaionObject.y = image.y + image.height / 2;
				//_effectVisualizaionObject.x = _effectVisualizaionObject.x - _effectVisualizaionObject.width / 2;
				//_effectVisualizaionObject.y = _effectVisualizaionObject.y - _effectVisualizaionObject.height / 2;							
			}
		}
		
		//override public function get width(): Number 
		//{
			//return image.width;
		//}
		//
		//override public function get height(): Number 
		//{
			//return image.height;
		//}		
		
		private function OnAddAffliction(event: Event): void 
		{
			if (event.data.heroType === _heroType)
				AddAfflictionOverlay(event.data.affliction);
		}
		
		private function OnRemoveAffliction(event: Event): void 
		{
			if (event.data.heroType === _heroType)
				RemoveAfflictionOverlay();
		}		
		
		override public function OnEnterFrame(event: EnterFrameEvent):void 
		{
			super.OnEnterFrame(event);
			if (overlayHolder.visible)
			{
				_temp = _affliction.duration - (Misc.GetServerTime() - _affliction.timeAfflicted);
				if (_temp <= 0)
				{
					World.gameController.RemoveAffliction(_affliction.type, _heroType);
				}
				afflictionTimeText.textField.text = Language.SetDecimalDivider(_temp.toFixed(1))+Language.GetString("SecondsAbbreviation");
				Misc.CenterObject(afflictionTimeText, _timeTextPos.x, _timeTextPos.y);
				//timeText.textField.alignPivot();
			}
		}
		
		public function AddAfflictionOverlay(a: Affliction):void 
		{
			if (a.iconName.length > 0)
			{
				overlayHolder.tooltipText = "ExplainAffliction" + a.type;
				_affliction = a;
				//timeText.textField.color = a.counterColor;
				overlayHolder.visible = true;
				afflictionTimeText.visible = true;
				overlay.texture = Assets.interfaceAtlas.getTexture(a.iconName);
				//overlay.x = image.x + image.width / 2 - overlay.texture.width / 2;
				//overlay.y = image.y + image.height / 2 - overlay.texture.height / 2;
				//overlay.alignPivot();
				//Misc.RoundPosition(overlay);
				//overlay.x += AFFLICTION_OVERLAY_ADD_X;
				//overlay.y += AFFLICTION_OVERLAY_ADD_Y;				
				//overlayHolder.addChild(overlay);
			}
		}
		
		//public function AddAfflictionOverlay(a: Affliction):void 
		//{
			//if (a.iconName.length > 0)
			//{
				//overlayHolder.tooltipText = Language.GetString("ExplainAffliction" + a.type);
				//_affliction = a;
				////timeText.textField.color = a.counterColor;
				//overlayHolder.visible = true;
				//afflictionTimeText.visible = true;
				//overlay.removeFromParent(true);
				//overlay = new Image(Assets.interfaceAtlas.getTexture(a.iconName));
				//var diffy: Number = (overlay.texture.height - image.texture.height) / 2;
				//var diffx: Number = (overlay.texture.width - image.texture.width)/2;
				//overlay.x = image.x - diffx;
				//overlay.y = image.y - diffy;
				//overlayHolder.addChild(overlay);
			//}
		//}		
		
		public function RemoveAfflictionOverlay():void 
		{
			overlayHolder.visible = false;
			afflictionTimeText.visible = false;
		}
		
		public function OnAddedToStage(event: Event): void 
		{
			addChild(image);
			addChild(frame);
			addChild(overlayHolder);
			overlayHolder.addChild(overlay);
			addChild(afflictionTimeText);
			addChild(_effectVisulizationHolder);
			//_quad = new Quad(width, height);
			//_quad.alpha = 0.5;
			//_quad.x = x;
			//_quad.y = y;
			//addChild(_quad);
		}
		
	}

}