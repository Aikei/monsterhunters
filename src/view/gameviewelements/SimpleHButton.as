package view.gameviewelements 
{
	import starling.display.Image;
	import starling.textures.Texture;
	/**
	 * ...
	 * @author Aikei
	 */
	public class SimpleHButton extends HButton 
	{
		
		public function SimpleHButton(texture: Texture) 
		{
			super();
			defaultSkin = new Image(texture);
			hoverSkin = new Image(texture);
			hoverSkin.filter = Filters.highlightFilter;
		}
		
	}

}