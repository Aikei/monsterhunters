package view.gameviewelements 
{
	import d_hentity.HEntity;
	import starling.events.Event;
	import starling.text.TextFieldAutoSize;
	import starling.text.TextField;
	import starling.utils.Color;
	
	/**
	 * ...
	 * @author Aikei
	 */
	
	
	public class HText extends HEntity 
	{
		public var textField: TextField;
		
		public function HText(text: String, font: String, fontSize: Number, fontColor: uint = Color.WHITE, autoSize = TextFieldAutoSize.BOTH_DIRECTIONS) 
		{
			super(false);
			this.textField = new TextField(0, 0, text, font, fontSize);
			this.textField.batchable = true;
			if (autoSize != TextFieldAutoSize.NONE)
				this.textField.autoSize = autoSize;
			this.textField.color = fontColor;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		override public function get width(): Number 
		{
			return textField.width;
		}
		
		override public function get height(): Number 
		{
			return textField.height;
		}
		
		public function AlignCenter(centerx: Number, centerx: Number):void 
		{
			this.textField.x = centerx - this.textField.width / 2;
			this.textField.y = centerx - this.textField.height / 2;
			Misc.RoundPosition(this.textField);
		}
		
		private function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(this.textField);
		}
		
		public function set color(c: uint):void 
		{
			textField.color = c;
		}
		
		public function get color(): uint 
		{
			return textField.color;
		}
		
		public function set text(t: String):void 
		{
			textField.text = t;
		}
		
		public function get text(): String 
		{
			return textField.text;
		}
		
	}

}