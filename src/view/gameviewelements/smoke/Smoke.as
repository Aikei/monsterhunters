package view.gameviewelements.smoke 
{
	import d_hentity.HEntity;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	import starling.filters.ColorMatrixFilter;
	import system.HColorFilter;
	import flash.utils.setTimeout;
	
	import com.greensock.*;		
	/**
	 * ...
	 * @author Aikei
	 */
	public class Smoke extends HEntity 
	{
		public static var MONSTER_SMOKE_TOTAL_DURATION: Number = 0.75;
		public static var MONSTER_SMOKE_DISAPPEAR_TIME: Number = 0.25;
		public static var MONSTER_SMOKE_SPEED: Number = 300;
		
		private var _image: Image = new Image(Assets.interfaceAtlas.getTexture("smoke"));
		private var _tweens: Vector.<TweenLite> = new Vector.<TweenLite>;
		
		public function Smoke() 
		{
			super(false);
			_useWorldsPassedTime = true;
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		private function addedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			setTimeout(Destroy, MONSTER_SMOKE_TOTAL_DURATION*1000);
			addChild(_image);
			_image.alignPivot();
			RandomlyResize();
			Launch();
			Disappear();
		}
		
		private function RandomlyResize():void 
		{
			var by: Number = Misc.Random(0.5, 2.0);
			_image.width *= by;
			_image.height *= by;
		}
		
		private function Launch():void 
		{
			var mmx: Number = Misc.RandomNoFloor(0, 2) - 1;
			var mmy: Number = Misc.RandomNoFloor(0, -1);
			ApplyRelativeVelocity(new Vec2(mmx,mmy), MONSTER_SMOKE_SPEED);
		}
		
		private function Disappear():void 
		{
			//var f: HColorFilter = new HColorFilter;
			//_image.filter = f;
			ApplyRotation(Misc.PI / 4 + Misc.Random(0, Misc.PI / 4));
			var tl: TimelineLite = new TimelineLite;
			tl.to(_image, MONSTER_SMOKE_TOTAL_DURATION, { width: _image.width / 4, height: _image.height / 4 } );
			tl.to(_image, MONSTER_SMOKE_DISAPPEAR_TIME, { alpha: 0 }, "-="+MONSTER_SMOKE_DISAPPEAR_TIME.toString());
		}
		
		//override public function Destroy(): void
		//{
			//
		//}
		
	}

}