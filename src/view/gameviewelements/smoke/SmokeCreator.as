package view.gameviewelements.smoke 
{
	import d_hentity.HEntity;
	import starling.events.EnterFrameEvent;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class SmokeCreator extends HEntity 
	{
		public static var NUMBER_OF_SMOKE_CIRCLES: int = 50;
		
		public function SmokeCreator() 
		{
			super(false, false);
			this.touchable = false;
		}
		
		public function CreateSmoke():void 
		{
			for (var i: int = 0; i < NUMBER_OF_SMOKE_CIRCLES; i++)
			{
				var smoke: Smoke = new Smoke;
				smoke.x = Misc.Random(0, 100) - 50;
				smoke.y = Misc.Random(0, 100) - 50;
				addChild(smoke);
			}
		}
		
		public function OnGlobalEnterFrame(event: EnterFrameEvent):void 
		{
			for (var i: int = 0; i < this.numChildren; i++)
			{
				Smoke(this.getChildAt(i)).OnEnterFrame(event);
			}
		}
		
	}

}