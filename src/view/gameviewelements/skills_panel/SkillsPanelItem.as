package view.gameviewelements.skills_panel 
{
	import d_hentity.HEntity;
	import feathers.controls.Label;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.ITextRenderer;
	import feathers.text.BitmapFontTextFormat;
	import logic.Skill;
	import starling.display.Image;
	import starling.events.Event;
	import feathers.controls.Button;
	import starling.text.BitmapFont;
	import starling.textures.TextureSmoothing;
	import view.BitmappedText;
	import view.gameviewelements.BatchedBitmapLabel;
	import view.gameviewelements.HText;
	import view.InvisibleTooltipHolder;
	/**
	 * ...
	 * @author Aikei
	 */
	public class SkillsPanelItem extends HEntity
	{
		public static var REQ_TEXT_X: Number = 201;
		public static var REQ_TEXT_Y: Number = 108;
		
		public static var FIRST_BUTTON_X: Number = 38;
		public static var FIRST_BUTTON_Y: Number = 34;
		public static var BUTTONS_SPACING: Number = 123;
		
		public static var NOT_ENOUGH_GLORY_TIP_X: Number = 170;
		public static var NOT_ENOUGH_GLORY_TIP_Y: Number = 102;
		public static var NOT_ENOUGH_GLORY_TIP_WIDTH: Number = 62;
		public static var NOT_ENOUGH_GLORY_TIP_HEIGHT: Number = 22;
		
		public var _notEnoughGloryTip: InvisibleTooltipHolder;
		
		protected var _skillButtons: Vector.<SkillButton> = new Vector.<SkillButton>;
		
		protected var _bg: Image = new Image(Assets.interfaceAtlas.getTexture("talents_locked"));
		protected var _reqText: BitmappedText = new BitmappedText;
		protected var _buttonsHolder: HEntity = new HEntity;
		
		protected var _req: BigInt;
		//protected var _num: int;
		protected var _available: Boolean = false;
		protected var _picked: Boolean = false;

		public function get picked(): Boolean 
		{
			return _picked;
		}
		
		public function set available(av: Boolean):void 
		{
			if (av == true)
			{
				_available = av;
				_notEnoughGloryTip.visible = false;
				_bg.texture = Assets.interfaceAtlas.getTexture("talents_bg");
				for (var i: int = 0; i < _skillButtons.length; i++)
				{
					_skillButtons[i].visible = true;
					_skillButtons[i].available = true;
					_skillButtons[i].active = true;
				}
				removeChild(_reqText);
				_reqText = null;
			}			
			_available = av;
		}
		
		public function get available(): Boolean 
		{
			return _available;
		}
		
		public function SkillsPanelItem(req: String) 
		{
			_notEnoughGloryTip = new InvisibleTooltipHolder(NOT_ENOUGH_GLORY_TIP_X, NOT_ENOUGH_GLORY_TIP_Y, NOT_ENOUGH_GLORY_TIP_WIDTH, NOT_ENOUGH_GLORY_TIP_HEIGHT, "ExplainNotEnoughGLory");
			_useWorldsPassedTime = true;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			_req = BigInt.shStr2BigInt(req);			
			_reqText.Init(req, "bbold15", 0xa7adb5);
			Misc.CenterObject(_reqText, REQ_TEXT_X, REQ_TEXT_Y);		
		}
						
		public function InitSkills(skillData: Object):void 
		{
			if (skillData.available && _reqText)
			{
				removeChild(_reqText);
				_reqText = null;
			}
			
			for (var i: int = 0; i < skillData.skills.length; i++)
			{
				var skill: Skill = Skill.CreateFromDataObejct(skillData.skills[i],skillData);
				skill.packNum = skillData.num;
				var button: SkillButton = new SkillButton(skill);
				_buttonsHolder.addChild(button);
				button.x = FIRST_BUTTON_X + BUTTONS_SPACING * i;
				button.y = FIRST_BUTTON_Y;
				button.addEventListener("skillButtonPushed", OnSkillButtonPushed);
				_skillButtons.push(button);
										
			}
			if (skillData.available)
				available = true;			
			if (skillData.picked != -1)
				PickSkill(skillData.skills[skillData.picked].type,skillData.skills[skillData.picked].subtype);
		}
		
		//override protected function OnHover():void 
		//{
			//super.OnHover();
		//}
		
		override protected function OnStopHover():void 
		{
			_bg.filter = null;
			super.OnStopHover();			
		}
		
		protected function OnSkillButtonPushed(event: Event): void 
		{
			if (_available)
			{
				PickSkill(Skill(event.data).type,Skill(event.data).subtype);
				for (var i: int = 0; i < _skillButtons.length; i++)
				{
					if (_skillButtons[i].skill.type === Skill(event.data).type && _skillButtons[i].skill.subtype === Skill(event.data).subtype)
						World.gameController.PickSkill(_req, _skillButtons[i].skill);
				}
			}
		}
		
		protected function PickSkill(type: int, subtype: int):void 
		{
			_picked = true;
			for (var i: int = 0; i < _skillButtons.length; i++)
			{
				_skillButtons[i].active = false;
				if (_skillButtons[i].skill.type == type && _skillButtons[i].skill.subtype == subtype)
					_skillButtons[i].bought = true;			
			}
		}
		
		protected function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			
			addChild(_bg);
			addChild(_buttonsHolder);
			if (_reqText)
				addChild(_reqText);
			addChild(_notEnoughGloryTip);
			
			//if (!_available)
				//_flag.texture = Assets.interfaceAtlas.getTexture("talents_flag_gray");
		}
		
	}

}


//package view.gameviewelements.skills_panel 
//{
	//import d_hentity.HEntity;
	//import feathers.controls.Label;
	//import feathers.controls.text.BitmapFontTextRenderer;
	//import feathers.core.ITextRenderer;
	//import feathers.text.BitmapFontTextFormat;
	//import logic.Skill;
	//import starling.display.Image;
	//import starling.events.Event;
	//import feathers.controls.Button;
	//import starling.text.BitmapFont;
	//import starling.textures.TextureSmoothing;
	//import view.BitmappedText;
	//import view.gameviewelements.BatchedBitmapLabel;
	//import view.gameviewelements.HText;
	///**
	 //* ...
	 //* @author Aikei
	 //*/
	//public class SkillsPanelItem extends HEntity
	//{
		//protected var _skillButtons: Vector.<SkillButton> = new Vector.<SkillButton>;
		//
		//protected var _bg: Image = new Image(Assets.interfaceAtlas.getTexture("talents_bg"));
		//protected var _flag: Image = new Image(Assets.interfaceAtlas.getTexture("talents_flag_gray"));
		////protected var _reqText: HText = new HText("", "bbold16", 16);
		//protected var _reqText: BitmappedText = new BitmappedText;
		////protected var _reqText: BatchedBitmapLabel = new BatchedBitmapLabel("bbold16",16,0xffffff);
		////protected var _reqText: Label = new Label;
		//protected var _imageHolder: HEntity = new HEntity;
		//protected var _priceHolder: HEntity = new HEntity;
		//protected var _buttonHolder: HEntity = new HEntity;
		//protected var _req: BigInt;
		//protected var _num: int;
		//protected var _available: Boolean = false;
		//protected var _picked: Boolean = false;
//
		//public function get picked(): Boolean 
		//{
			//return _picked;
		//}
		//
		//public function set available(av: Boolean):void 
		//{
			//if (av == true)
			//{
				//_available = av;
				//_flag.texture = Assets.interfaceAtlas.getTexture("talents_flag");
				////_flag.filter = null;
				//for (var i: int = 0; i < _skillButtons.length; i++)
				//{
					//_skillButtons[i].available = true;
					//_skillButtons[i].active = true;
				//}
				//for (i = 0; i < _priceHolder.numChildren; i++)
				//{
					//if (_priceHolder.getChildAt(i) is HText)
					//{
						//HText(_priceHolder.getChildAt(i)).textField.color = 0xffffff;
					//}
				//}
			//}
			//_available = av;
		//}
		//
		//public function get available(): Boolean 
		//{
			//return _available;
		//}
		//
		//public function SkillsPanelItem() 
		//{
			//_reqText.x = 42;
			//_reqText.y = 36;
			//_bg.touchable = false;
			//addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		//}
				//
		//public function InitSkills(skillData: Object):void 
		//{
			//_num = skillData.num;
			//_req = BigInt.shStr2BigInt(skillData.req);
			//
			////_reqText.text = BigInt.bigint2HString(_req);
			//
			//_reqText.Init(BigInt.bigint2HString(_req), "bbold16", 0xffffff);
			//Misc.CenterObject(_reqText, _reqText.x, _reqText.y);
			//
			//for (var i: int = 0; i < skillData.skills.length; i++)
			//{
				//var skill: Skill = Skill.CreateFromDataObejct(skillData.skills[i],skillData);
				//skill.packNum = skillData.num;
				//var button: SkillButton = new SkillButton(skill);
				//button.x = 145 + 97 * i;
				//button.y = 56;
				//button.addEventListener("skillButtonPushed", OnSkillButtonPushed);
				//_skillButtons.push(button);
				//_buttonHolder.addChild(button);
				//
				//var coin: Image = new Image(Assets.interfaceAtlas.getTexture("coin_small"));
				//_imageHolder.addChild(coin);
				//coin.x = 114 + 98 * i;
				//coin.y = 96;
				//
				//var priceText: HText = new HText(BigInt.bigint2HString(skill.price), "bbold14", -1, 0xc8ccda);
				//
				////var priceText: BatchedBitmapLabel = new BatchedBitmapLabel("bbold14", 14, 0xc8ccda);
				////priceText.text = BigInt.bigint2HString(skill.price);
				//
				////var priceText: BitmappedText = new BitmappedText;
				////priceText.Init(BigInt.bigint2HString(skill.price),"bbold14", 0xc8ccda);
				//
				//_priceHolder.addChild(priceText);
				//priceText.x = coin.x + 24;
				//priceText.y = 96;				
			//}
			//if (skillData.available)
				//available = true;			
			//if (skillData.picked != -1)
				//PickSkill(skillData.skills[skillData.picked].type);
		//}
		//
		//protected function OnSkillButtonPushed(event: Event): void 
		//{
			//if (_available)
			//{
				//PickSkill(Skill(event.data).type);
				//for (var i: int = 0; i < _skillButtons.length; i++)
				//{
					//if (_skillButtons[i].skill.type === Skill(event.data).type)
						//World.gameController.PickSkill(_req, _skillButtons[i].skill);
				//}
			//}
		//}
		//
		//protected function PickSkill(type: int):void 
		//{
			//_imageHolder.removeChildren(0, -1, true);
			//_priceHolder.removeChildren(0, -1, true);
			//_picked = true;
			//for (var i: int = 0; i < _skillButtons.length; i++)
			//{
				//var pickImage: Image;
				//if (_skillButtons[i].skill.type !== type)
				//{
					//pickImage = new Image(Assets.interfaceAtlas.getTexture("blocked"));				
				//}
				//else
				//{
					//_skillButtons[i].bought = true;
					//pickImage = new Image(Assets.interfaceAtlas.getTexture("selected"));
				//}
				//_skillButtons[i].active = false;
				//pickImage.x = _skillButtons[i].x;
				//pickImage.y = _skillButtons[i].y + 50;
				//pickImage.alignPivot();
				//_imageHolder.addChild(pickImage);
			//}
		//}
		//
		//protected function OnAddedToStage(event: Event): void 
		//{
			//removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			//
			//addChild(_bg);
			//addChild(_buttonHolder);
			//addChild(_flag);
			//addChild(_reqText);
			//addChild(_imageHolder);
			//addChild(_priceHolder);
			//
			//if (!_available)
				//_flag.texture = Assets.interfaceAtlas.getTexture("talents_flag_gray");
		//}
		//
	//}
//
//}