package view.gameviewelements.skills_panel 
{
	
	/**
	 * ...
	 * @author Aikei
	 */
	import starling.events.Event;
	import view.gameviewelements.BaseVerticalScrollContainer;
	
	public class SkillsPanel extends BaseVerticalScrollContainer 
	{
		private var _items: Vector.<SkillsPanelItem> = new Vector.<SkillsPanelItem>;
		private var _lastPickedSkillNumber: int = -1;
		private var _lastAvailableSkillNumber: int = -1;
		private var _skillsUnpicked: uint;
		
		public function SkillsPanel() 
		{
			super();
			visible = false;
			World.gWorld.addEventListener("nextSkillPackAvailable", OnNextSkillPackAvailable);
			World.gWorld.addEventListener("skillPicked", OnSkillPicked);
		}
		
		private function OnSkillPicked(e:Event):void 
		{
			_skillsUnpicked--;
			World.gWorld.dispatchEventWith("skillsUnpicked", false, _skillsUnpicked);
		}
		
		private function OnNextSkillPackAvailable(e:Event):void 
		{
			_items[e.data.num].available = true;
			CountUnpicked();
		}
		
		public function CountUnpicked():void 
		{
			_skillsUnpicked = 0;
			for (var i: int = 0; i < _items.length; i++)
			{
				if (_items[i].available && !_items[i].picked)
					_skillsUnpicked++;
			}
			World.gWorld.dispatchEventWith("skillsUnpicked", false, _skillsUnpicked);
		}
		
		public function AddSkills(skillData: Object):void 
		{
			var item: SkillsPanelItem = new SkillsPanelItem;
			item.InitSkills(skillData);
			addChild(item);
		}
		
		public function Init(gameData: Object): void 
		{
			for (var i: int = 0; i < gameData.skills.length; i++)
			{
				var item: SkillsPanelItem = new SkillsPanelItem;
				addChild(item);
				item.InitSkills(gameData.skills[i]);
				_items.push(item);								
			}
			CountUnpicked();
		}
		
	}

}