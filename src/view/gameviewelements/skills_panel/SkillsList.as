package view.gameviewelements.skills_panel 
{
	
	
	/**
	 * ...
	 * @author Aikei
	 */
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.renderers.IListItemRenderer;
	import feathers.data.ListCollection;
	import feathers.layout.VerticalLayout;
	import starling.display.DisplayObject;
	import starling.display.Quad;
	import starling.events.EnterFrameEvent;
	import starling.events.TouchEvent;
	import view.gameviewelements.list.BaseAccessoryOnlyList;
	import view.gameviewelements.list.BaseItemList;
	import starling.events.Event;
	
	public class SkillsList extends BaseAccessoryOnlyList 
	{		
		private var _items: Vector.<SkillsPanelItem> = new Vector.<SkillsPanelItem>;
		private var _lastPickedSkillNumber: int = -1;
		private var _lastAvailableSkillNumber: int = -1;
		private var _skillsUnpicked: uint;
		private var _nextAvailableSkillPackNumber: uint = 0;
		
		public function SkillsList() 
		{
			super();
			//itemRendererType = SkillsListItemRenderer;
			visible = false;
			
			//this.itemRendererFactory = function():IListItemRenderer
			//{
				//var renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
				//renderer.labelFunction = function ():String { return ""; }
				//renderer.itemHasIcon = false;
				//renderer.itemHasAccessory = true;
				//renderer.accessoryFunction = function (item: Object):DisplayObject 
				//{
					//return DisplayObject(item);
				//}
				//return renderer;
			//}			
			//this.isSelectable = false;
			//World.gWorld.addEventListener("nextSkillPackAvailable", OnNextSkillPackAvailable);
			World.gWorld.addEventListener("skillPicked", OnSkillPicked);			
			VerticalLayout(this.layout).typicalItem = new Quad(Assets.interfaceAtlas.getTexture("talents_bg").width, Assets.interfaceAtlas.getTexture("talents_bg").height);						
		}
		
		private function OnSkillPicked(e:Event):void 
		{
			_skillsUnpicked--;
			World.gWorld.dispatchEventWith("skillsUnpicked", false, _skillsUnpicked);			
		}
		
		//private function OnNextSkillPackAvailable(e:Event):void 
		//{
			//_items[e.data.num].available = true;
			//CountUnpicked();			
		//}
		
		public function OnGlobalEnterFrame(event: EnterFrameEvent):void 
		{
			//for (var i: int = 0; i < _items.length; i++)
			//{
				//if (_items[i].available)
					//_items[i].OnEnterFrame(event);
			//}			
		}
		
		public function OnGlobalTouch(event: TouchEvent):void 
		{
			for (var i: int = 0; i < _items.length; i++)
			{
				if (_items[i].available)
					_items[i].OnTouch(event);
			}			
		}		
		
		public function Init(gameData: Object) :void 
		{			
			for (var i: int = 0; i < Preferences.NUMBER_OF_SKILL_PACKS; i++)
			{
				var item: SkillsPanelItem = new SkillsPanelItem(gameData.skillsReqs[i].toString());
				if (i < gameData.skills.length)
				{
					item.InitSkills(gameData.skills[i]);
					_nextAvailableSkillPackNumber++;
				}
				_items.push(item);
			}
			var l: ListCollection = new ListCollection(_items);
			this.dataProvider = l;
			CountUnpicked();		
		}
		
		public function AddSkills(skillData: Object):void 
		{
			for (var i: int = 0; i < skillData.length; i++)
			{
				_items[_nextAvailableSkillPackNumber].InitSkills(skillData[i]);
				_nextAvailableSkillPackNumber++;
			}
			CountUnpicked();
		}		
		
		public function CountUnpicked():void 
		{
			_skillsUnpicked = 0;
			for (var i: int = 0; i < _items.length; i++)
			{
				if (_items[i].available && !_items[i].picked)
					_skillsUnpicked++;
			}
			World.gWorld.dispatchEventWith("skillsUnpicked", false, _skillsUnpicked);
		}		
		
	}

}