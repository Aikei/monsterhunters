package view.gameviewelements.skills_panel {
	import d_hentity.HEntity;
	import feathers.controls.Button;
	import flash.geom.Rectangle;
	import logic.Skill;
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	import system.HCursor;
	import system.HSounds;
	import view.gameviewelements.MessageWindow.MessageWindow;
	import starling.filters.ColorMatrixFilter;
	/**
	 * ...
	 * @author Aikei
	 */
	public class SkillButton extends HEntity 
	{
		//private static const OVERLAY_RECT: Rectangle = new Rectangle(34, 30, 88, 88);
		private var _baseOverlayTexture: Texture;
		protected var _image: Image;
		protected var _skill: Skill;
		protected var _active: Boolean = false;
		protected var _available: Boolean = false;
		protected var _overlayImage: Image;
		protected var _bought: Boolean = false;
			
		//protected var _hovered: Boolean = false;
		//protected var _messageWindow: MessageWindow = null;
	
		public function get available(): Boolean 
		{
			return _available;
		}
		
		public function set available(a: Boolean):void 
		{
			if (a && !_available)
			{
				_image.texture = Assets.interfaceAtlas.getTexture(_skill.iconName);
			}
			_available = a;
		}
		
		public function get active(): Boolean 
		{
			return _active;
		}
		
		public function set bought(b: Boolean): void 
		{
			if (!_bought && b)
			{
				_image.texture = Assets.interfaceAtlas.getTexture(_skill.iconName);
				_overlayImage.texture = Assets.interfaceAtlas.getTexture("talent_purchased");		
			}				
			_bought = b;	
		}
		
		public function set active(act: Boolean): void 
		{
			if (!act)
			{
				if (_active)
					OnStopHover();
				if (!_bought)
					_image.texture = Assets.interfaceAtlas.getTexture(_skill.iconName+"_bw");
			}
			if (act && !_active)
			{
				_image.texture = Assets.interfaceAtlas.getTexture(_skill.iconName);
			}
			_active = act;
		}
		
		public function get skill(): Skill 
		{
			return _skill;
		}
				
		public function SkillButton(skill: Skill) 
		{
			super();
			InitTouch();
			_skill = skill;
			this.visible = false;
			_baseOverlayTexture = Assets.interfaceAtlas.getTexture("talent_not_purchased"); //Texture.fromTexture(Assets.interfaceAtlas.getTexture("talents_bg"), OVERLAY_RECT);						
			_image = new Image(Assets.interfaceAtlas.getTexture(skill.iconName+"_bw"));
			_overlayImage = new Image(_baseOverlayTexture);
			_overlayImage.x = _image.x;
			_overlayImage.y = _image.y;
			_image.x += (_overlayImage.texture.height - _image.texture.height) / 2;
			_image.y += (_overlayImage.texture.height - _image.texture.height) / 2;
			showTooltipOnHover = true;
			tooltipText = _skill.info;
			tooltipTitleText = _skill.infoTitle;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);			
		}
		
		protected function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_image);
			addChild(_overlayImage);		
			if (!_available)
				_image.texture = Assets.interfaceAtlas.getTexture(skill.iconName+"_bw");
		}
		
		override protected function OnTouchPhaseEnded():void 
		{
			if (_active)
			{
				HSounds.PlaySound("ClickSound");
				HSounds.PlaySound("SkillPurchased");
				dispatchEventWith("skillButtonPushed", false, _skill);
			}
		}
		
		override protected function OnHover():void 
		{
			if (!_hovered)
			{
				_hovered = true;
				HSounds.PlaySound("HoverSound");
				if (_active && !_bought)
				{
					_overlayImage.filter = Filters.highlightFilter;
				}
			}			
			super.OnHover();
		}
		
		override protected function OnStopHover():void 
		{
			if (_active && !_bought)
				_overlayImage.filter = null;			
			super.OnStopHover();
		}		
		
		
	}

}