package view.gameviewelements.skills_panel 
{
	import d_hentity.HEntity;
	import logic.Skill;
	import view.BitmappedText;
	import view.gameviewelements.list.BaseListItemRenderer;
	import starling.display.Image;
	
	/**
	 * ...
	 * @author Aikei
	 */
	
	public class SkillsListItemRenderer extends BaseListItemRenderer 
	{
		protected var _item: SkillsPanelItem;		

		
		public function SkillsListItemRenderer() 
		{

		}
		
		//override protected function initialize():void
		//{
			//_item = new SkillsPanelItem;
			//addChild(_item);
		//}	
				
		override protected function autoSizeIfNeeded():Boolean
		{
			var needsWidth:Boolean = isNaN(this.explicitWidth);
			var needsHeight:Boolean = isNaN(this.explicitHeight);
			if(!needsWidth && !needsHeight)
			{
				return false;
			}
			//_item.width = explicitWidth - 2 * _padding;
			//_item.height = explicitHeight - 2 * _padding;
			//_item.validate();
			
			var newWidth:Number = explicitWidth;
			if(needsWidth)
			{
				newWidth = _item.width + 2 * _padding;
			}
			var newHeight:Number = explicitHeight;
			if(needsHeight)
			{
				newHeight = _item.height + 2 * _padding;
			}
			return this.setSizeInternal(newWidth, newHeight, false);			
		}
		
		override protected function layoutChildren():void
		{
			_item.x = _padding;
			_item.y = _padding;
			//_item.width = actualWidth - 2 * _padding;
			//_item.height = actualHeight - 2 * _padding;
		}
		
		//override protected function commitData():void
		//{
			//if (_data)
			//{
				//_item.InitSkills(_data);
			//}
		//}		
		
		override protected function commitData():void
		{
			if (_data)
			{
				_item = SkillsPanelItem(_data);
				addChild(_item);
			}
		}			
		
	}

}


//package view.gameviewelements.skills_panel 
//{
	//import d_hentity.HEntity;
	//import logic.Skill;
	//import view.BitmappedText;
	//import view.gameviewelements.BaseListItemRenderer;
	//import starling.display.Image;
	//
	///**
	 //* ...
	 //* @author Aikei
	 //*/
	//
	//public class SkillsListItemRenderer extends BaseListItemRenderer 
	//{
		//protected var _skillButtons: Vector.<SkillButton> = new Vector.<SkillButton>;
		//
		//protected var _bg: Image;
		//protected var _flag: Image;
		//protected var _reqText: BitmappedText;
		//protected var _topHolder: HEntity;
		//protected var _buttonHolder: HEntity;
		//protected var _req: BigInt;
		//protected var _num: int;
		//protected var _available: Boolean = false;
		//protected var _picked: Boolean = false;
		//
		//public function SkillsListItemRenderer() 
		//{
			//
		//}
		//
		//override protected function initialize():void
		//{
			//_bg = new Image(Assets.interfaceAtlas.getTexture("talents_bg"));
			//_flag = new Image(Assets.interfaceAtlas.getTexture("talents_flag"));
			//_reqText = new BitmappedText;
			//_topHolder = new HEntity;
			//_buttonHolder = new HEntity;
			//addChild(_bg);
			//addChild(_buttonHolder);
			//addChild(_flag);
			//addChild(_reqText);
			//addChild(_topHolder);
		//}	
		//
		//protected function commitData():void
		//{
			//if (_data)
			//{
				//_num = _data.num;
				//_req = BigInt.shStr2BigInt(_data.req);
				//_reqText.Init(BigInt.bigint2HString(_req), "bbold16", 0xffffff);
				//_reqText.SetCenter(_reqText.x, _reqText.y);
				//for (var i: int = 0; i < _data.skills.length; i++)
				//{
					//var skill: Skill = Skill.CreateFromDataObejct(_data.skills[i],_data);
					//skill.packNum = _data.num;
					//var button: SkillButton = new SkillButton(skill);
					//button.x = 145 + 97 * i;
					//button.y = 56;
					//button.addEventListener("skillButtonPushed", OnSkillButtonPushed);
					//_skillButtons.push(button);
					//_buttonHolder.addChild(button);
					//
					//var coin: Image = new Image(Assets.interfaceAtlas.getTexture("coin_small"));
					//_topHolder.addChild(coin);
					//coin.x = 114 + 98 * i;
					//coin.y = 96;
					//
					//var priceText: BitmappedText = new BitmappedText;
					//priceText.Init(BigInt.bigint2HString(skill.price), "bbold14", 0xc8ccda);
					//_topHolder.addChild(priceText);
					//priceText.x = coin.x + 25;
					//priceText.y = 96;				
				//}
				//if (_data.available)
					//available = true;			
				//if (_data.picked != -1)
					//PickSkill(skillData.skills[skillData.picked].type);				
			//}
			//else
			//{
				//
			//}
		//}			
		//
	//}
//
//}