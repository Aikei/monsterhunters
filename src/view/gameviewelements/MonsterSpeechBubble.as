package view.gameviewelements 
{
	import d_hentity.HEntity;
	import starling.display.Image;
	import starling.events.Event;
	import view.MonsterSprite;
	import com.greensock.TweenLite;
	import flash.utils.setTimeout;
	import flash.utils.clearTimeout;
	import starling.text.TextFieldAutoSize;
	
	import com.greensock.easing.*;
	/**
	 * ...
	 * @author Aikei
	 */
	public class MonsterSpeechBubble extends HEntity 
	{
		public static var BUBBLE_EDGE_SPACING: Number = 4;
		
		private var _image: Image = new Image(Assets.interfaceAtlas.getTexture("speech_bubble"));
		private var _text: HText = new HText("0","notoserif_bi",-1,0x7a7e84,TextFieldAutoSize.VERTICAL);
		private var cenX: Number;
		private var cenY: Number;
		private var _timeOutId: uint = 0;
		private var _tween: TweenLite = null;
		
		override public function get width(): Number 
		{
			return _image.width;
		}
		
		override public function get height(): Number 
		{
			return _image.height;
		}		
		
		public function MonsterSpeechBubble() 
		{
			this.touchable = false;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			cenX = Math.round(_image.width / 2);
			cenY = Math.round(_image.height / 2 - 3);
			_text.y = 10;
			_text.textField.width = _image.texture.width - BUBBLE_EDGE_SPACING*2;
			visible = false;
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_image);
			addChild(_text);
			//this.x = -_image.width;
			//pivotY = _image.height;
			//pivotX = -_image.width / 2;
		}
		
		public function StartSpeech(phrase: String):void 
		{
			if (_tween)
			{
				_tween.kill();
				_tween = null;
			}
			visible = true;
			alpha = 1;
			_text.textField.text = phrase;
			_text.x = Math.round(cenX - _text.textField.width / 2);
			if (_text.x < BUBBLE_EDGE_SPACING)
				_text.x = BUBBLE_EDGE_SPACING;
			_text.y = Math.round(cenY - _text.textField.height / 2);
			
			_tween = TweenLite.from(this, 1.0, { y: y + 60, alpha: 0.5, ease: ElasticOut.ease, onComplete: function() { _tween = null; } } );
			_timeOutId = setTimeout(function (bubble: MonsterSpeechBubble) 
			{ 
				bubble._timeOutId = 0;
				bubble._tween = TweenLite.to(bubble, 1.0, { alpha: 0, onComplete : function() { bubble.visible = false; } } );
			}, 2500, this);
		}
		
		override public function Destroy():void 
		{
			if (_tween)
			{
				_tween.seek(0);
				_tween.kill();
			}
			if (_timeOutId != 0)
				clearTimeout(_timeOutId);
			super.Destroy();
		}
		
	}

}