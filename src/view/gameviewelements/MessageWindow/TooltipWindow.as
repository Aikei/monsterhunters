package view.gameviewelements.MessageWindow 
{
	import starling.utils.HAlign;
	/**
	 * ...
	 * @author Aikei
	 */
	public class TooltipWindow extends TitledMessageWindow 
	{
		
		public function TooltipWindow(languageString: String, titleString: String = null) 
		{
			var lString: String = Language.GetString(languageString);
			if (!titleString)
				var tString: String = Language.GetString("Title" + languageString);
			else
				tString = Language.GetString(titleString);
			super(tString, lString, "bregular13", "bbold13", -1, -1, 0x878e96, 0xf4bc50);
			_titleText.textField.hAlign = HAlign.LEFT;
		}
		
	}

}