package view.gameviewelements.MessageWindow 
{
	import com.greensock.TweenLite;
	import d_hentity.HEntity;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.utils.HAlign;
	import view.gameviewelements.HText;
	import starling.text.TextFieldAutoSize;
	import starling.utils.Color;
	import starling.textures.TextureSmoothing;
	import flash.utils.setTimeout;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class MessageWindow extends HEntity 
	{
		protected var _text: HText;
		protected var _topImage: Image = new Image(Assets.interfaceAtlas.getTexture("help_top"));
		protected var _bg: Image = new Image(Assets.interfaceAtlas.getTexture("help_middle"));
		protected var _bottomImage: Image = new Image(Assets.interfaceAtlas.getTexture("help_bottom"));
		
		public var dieIn: Number = 0;
		public var dieGently: Boolean = false;
		
		public function MessageWindow(text: String, font: String = "bregular13", fontSize: Number = -1, fontColor: uint = 0x878e96) 
		{
			super(false);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			touchable = false;
			_text = new HText(text, font, fontSize, fontColor, TextFieldAutoSize.VERTICAL);
			_text.textField.hAlign = HAlign.LEFT;
			_text.textField.width = 175;
			_text.x = 20;
			_text.y = 20-_topImage.height-6;
			_bg.smoothing = TextureSmoothing.NONE;
		}
		
		public function SetBgAlpha(alpha: Number):void 
		{
			_bg.alpha = alpha;
			_topImage.alpha = alpha;
			_bottomImage.alpha = alpha;
		}
		
		override public function get width(): Number 
		{
			return _bg.width;
		}
		
		override public function get height(): Number 
		{
			return _bg.height+_topImage.height+_bottomImage.height;
		}		
		
		override public function set x(newX: Number):void 
		{
			super.x = newX;
			MoveIfNotOnScreenX();
		}
		
		override public function set y(newY: Number):void 
		{
			super.y = newY;
			MoveIfNotOnScreenX();
		}
		
		public function MoveIfNotOnScreenX():void 
		{
			if (this.x < 0)
				this.x = 0;
			if (this.x + this.width > Misc.SCREEN_WIDTH)
				this.x = Misc.SCREEN_WIDTH - this.width;				
		}
		
		public function MoveIfNotOnScreenY():void 
		{
			if (this.y-_topImage.height < 0)
				this.y = _topImage.height;
			if (this.y + this.height > Misc.SCREEN_HEIGHT)
				this.y = Misc.SCREEN_HEIGHT - this.height;					
		}		
		
		public function MoveIfNotOnScreen(): void
		{

	
		}
		
		public function DieGently():void 
		{
			TweenLite.to(this, 0.5, { alpha: 0, onComplete: this.Destroy } );
		}
		
		protected function GetHeight(): Number 
		{
			return _text.textField.height;
		}
		
		public function OnAddedToStage(event: Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			if (dieIn > 0)
			{
				if (dieGently)
					setTimeout(DieGently, dieIn*1000);
				else
					setTimeout(Destroy, dieIn*1000);
			}
			addChild(_topImage);
			addChild(_bg);
			addChild(_bottomImage);
			addChild(_text);
			
			//var quad: Quad = new Quad(20, 20);
			//quad.y = -_topImage.height;
			//quad.x = 0;
			//quad.alpha = 0.5;			
			//addChild(quad);
			
			_bg.height = GetHeight();
			_topImage.y = -_topImage.height;
			_bottomImage.y = _bg.height;			
		}
		
		//public function Destroy():void 
		//{
			//removeFromParent(true);
		//}
		
	}

}