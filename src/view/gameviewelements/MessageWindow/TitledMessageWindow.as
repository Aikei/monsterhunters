package view.gameviewelements.MessageWindow 
{
	import starling.events.Event;
	import view.gameviewelements.HText;
	import starling.text.TextFieldAutoSize;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class TitledMessageWindow extends MessageWindow 
	{
		public static var TITLE_TO_TEXT_SPACING: Number = 5;
		
		protected var _titleText: HText;
		
		public function TitledMessageWindow(title: String, text:String, font:String="bregular13", titleFont: String="bbold13", fontSize:Number = -1, titleFontSize:Number = -1, fontColor:uint=0x878e96, titleFontColor:uint=0x878e96) 
		{
			super(text, font, fontSize, fontColor);
			_titleText = new HText(title, titleFont, titleFontSize, titleFontColor, TextFieldAutoSize.VERTICAL);
			_titleText.textField.width = 175;
			_titleText.x = _text.x;
			_titleText.y = _text.y;
			_text.y += _titleText.textField.height+TITLE_TO_TEXT_SPACING;
		}
		
		override public function OnAddedToStage(event: Event):void 
		{
			super.OnAddedToStage(event);
			addChild(_titleText);
		}
		
		override protected function GetHeight(): Number 
		{
			return super.GetHeight()+_titleText.height+5;
		}		
	}

}