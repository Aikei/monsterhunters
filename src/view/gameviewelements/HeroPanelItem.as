package view.gameviewelements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import d_hentity.HEntity;
	import feathers.controls.supportClasses.TextFieldViewPort;
	import hevents.HEventHeroUpgraded;
	import hevents.L_HEventHeroBought;
	import logic.HeroData;
	import logic.local.ActualHero;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import system.Analytics;
	import system.HSounds;
	import view.BitmappedText;
	import view.gameviewelements.PanelButton;
	import starling.utils.Color;
	import com.greensock.TweenLite;
	import com.greensock.TimelineLite;
	
	public class HeroPanelItem extends ScrollContainerItem 
	{
		public static var DPS_COMMENT_TEXT_X: Number = 124;
		public static var DPS_COMMENT_TEXT_Y: Number = 46;
		public static var DPS_VALUE_TEXT_X: Number = 124;
		public static var DPS_VALUE_TEXT_Y: Number = 64;		
		public static var DAMAGE_TYPE_PORTRAIT_X: Number = 81;
		public static var DAMAGE_TYPE_PORTRAIT_Y: Number = 52;
		//private var _heroLevelText: BatchedBitmapLabel;
		//private var _dpsCommentText: BatchedBitmapLabel;
		//private var _dpsValueText: BatchedBitmapLabel;
		
		private var _heroLevelText: HText;
		private var _dpsCommentText: HText;
		private var _dpsValueText: HText;		
		
		private var _damageTypeHolder: HEntity = new HEntity;
		private var _damageTypePortrait: Image;
		private var _triggered: Boolean = false;
		private var _heroBought: Boolean = false;
		private var _actualHero: ActualHero = null;
		private var _heroType: int;
				
		public function HeroPanelItem(heroType: int) 
		{
			super();
			_heroType = heroType;
			
			//_itemNameText.SetTextBeforeAddingToStage(Language.GetString("Hero" + String(_heroType)));
			
			_itemNameText.text = Language.GetString("Hero" + String(_heroType));
			
			World.gWorld.addEventListener(L_HEventHeroBought.L_HEVENT_HERO_BOUGHT, OnHeroBought);
			World.gWorld.addEventListener(HEventHeroUpgraded.H_EVENT_HERO_UPGRADED, OnHeroUpgraded);
			World.gWorld.addEventListener("untriggerButton", OnUntrigger);			
		}
		
		public function Init(gameData: Object):void 
		{
			_itemPortrait = new Image(Assets.interfaceAtlas.getTexture(World.data.heroes[_heroType].portraitName));
			//_itemPortrait.x = ITEM_PORTRAIT_X;
			//_itemPortrait.y = ITEM_PORTRAIT_Y;
			//_itemPortrait.touchable = false;
			_itemPortraitHolder.InitTouch();
			_itemPortraitHolder.showTooltipOnHover = true;
			_itemPortraitHolder.tooltipText = "ExplainHero" + _heroType;
			
			_damageTypePortrait = new Image(Assets.interfaceAtlas.getTexture(World.data.heroes[_heroType].damage.portraitTextureName));
			_damageTypePortrait.x = DAMAGE_TYPE_PORTRAIT_X;
			_damageTypePortrait.y = DAMAGE_TYPE_PORTRAIT_Y;
			
			_damageTypeHolder.InitTouch();
			_damageTypeHolder.showTooltipOnHover = true;
			_damageTypeHolder.tooltipText = "ExplainDamageType" + World.data.heroes[_heroType].damage.type;
									
								//								
			//var args: Array = _itemNameText.args;
			//_itemNameText.removeFromParent(true);			
			//args[0] = Language.GetString("Hero" + String(_heroType));
			//_itemNameText = new BitmappedText();
			//_itemNameText.Init.apply(null, args);
								//
			//_itemNameText.text = Language.GetString("Hero" + String(_heroType));			
			
			//_heroLevelText = new BatchedBitmapLabel("bbold16", 16, 0xd2b5a6);
			_heroLevelText = new HText("", "bbold16", -1, 0xd2b5a6);
			_heroLevelText.x = _itemNameText.x+_itemNameText.textField.width;
			_heroLevelText.y = ITEM_NAME_TEXT_Y;
			_heroLevelText.touchable = false;
						
			//_dpsCommentText = new BatchedBitmapLabel("bbold12", -1, 0x977761);
			//_dpsCommentText.text = Language.GetString("Dps");
			_dpsCommentText = new HText(Language.GetString("Dps"), "bbold12", -1, 0x977761);			
			_dpsCommentText.x = DPS_COMMENT_TEXT_X;
			_dpsCommentText.y = DPS_COMMENT_TEXT_Y;
			_dpsCommentText.touchable = false;
						
			//_dpsValueText = new BatchedBitmapLabel("bbold16", 16, 0xd2b6a6);
			_dpsValueText = new HText(BigInt.bigint2HString(World.data.heroes[_heroType].dps.value),"bbold16", -1, 0xd2b6a6);
			_dpsValueText.x = DPS_VALUE_TEXT_X;
			_dpsValueText.y = DPS_VALUE_TEXT_Y;
			_dpsValueText.touchable = false;
									
			ParseHeroData(World.data.heroes[_heroType]);
			//_itemCostText.text = BigInt.bigint2HString(World.data.heroes[_heroType].buyCost);
			
			//_buyButton.InitTouch();
			//_buyButton.showTooltipOnHover = true;
			//_buyButton.tooltipText = Language.GetString("DamageAtLevel") + BigInt.bigint2HString(World.data.heroes[_heroType].dps.value);
			
			_cost = World.data.heroes[_heroType].buyCost;
			var coins: BigInt = BigInt.shStr2BigInt(gameData.coins);
			
			AddChildrenToStage();
			if (_heroBought)
				_notBought.removeFromParent(true);
			CheckCost(coins);						
		}
		
		//override protected function OnAddedToStage(event: Event):void 
		//{
			//super.OnAddedToStage(event);
			//addChild(_heroLevelText);
			//addChild(_dpsCommentText);
			//addChild(_dpsValueText);
		//}
		
		override protected function AddImagesToStage():void 
		{
			super.AddImagesToStage();
			//addChild(_itemPortrait);			
			addChild(_damageTypeHolder);
			_damageTypeHolder.addChild(_damageTypePortrait);
			removeChild(_buyButton);
			addChild(_buyButton);
			_buyButton.label = Language.GetString("Hire");
		}
		
		override protected function AddTextToStage():void 
		{
			super.AddTextToStage();
			addChild(_heroLevelText);
			addChild(_dpsCommentText);
			addChild(_dpsValueText);
			
		}		
		
		private function ParseHeroData(heroData: HeroData): void 
		{
			_dpsValueText.text = BigInt.bigint2HString(heroData.dps.value);			
			if (_heroBought)
				_cost = heroData.levelUpCost;
			else
				_cost = heroData.buyCost;
			_itemCostText.text = BigInt.bigint2HString(_cost);
			_heroLevelText.text = " " + Language.GetString("Lv") + " " + BigInt.bigint2HString(heroData.level);
			_buyButton.showTooltipOnHover = true;
			_buyButton.tooltipText = Language.GetString("DamageAtLevel") + BigInt.bigint2HString(heroData.nextLevelHero.dps.value);
			_buyButtonTooltipHolder.tooltipText  = _buyButton.tooltipText;
			//_buyButton.tooltipText = _buyButton.tooltipText.replace("$l", BigInt.bigint2HString(heroData.nextLevelHero.level));
		}
		
		protected function OnUntrigger():void 
		{
			_triggered = false;
		}
		
		override protected function OnBuyButtonTriggered(event: Event): void 
		{
			_triggered = true;
			if (!_heroBought)
			{
				World.gWorld.dispatchEventWith("heroBuyButtonClicked",false,_heroType);
			}
			else
			{
				World.gWorld.dispatchEventWith("heroUpgradeButtonClicked",false,_actualHero.id);
			}
		}
		
		private function OnHeroBought(event: L_HEventHeroBought): void 
		{
			if (event.hero.heroData.type === _heroType)
			{
				
				//Analytics.TrackEvent("Buy", "Hero " + Assets.heroNames[_heroType]); 
				if (event.startAnimation)
					FireUpgradeAnimation();
				_notBought.removeFromParent(true);
				_triggered = false;
				_heroBought = true;				
				//_itemNameText.text = event.hero.name;
				_buyButton.label = Language.GetString("Upgrade");
				_actualHero = event.hero;
				ParseHeroData(_actualHero.heroData);
			}
		}				
		
		private function OnHeroUpgraded(event: HEventHeroUpgraded):void 
		{
			//Analytics.TrackEvent("Upgrade", "Hero " + Assets.heroNames[_heroType], "new dps: "+BigInt.bigint2HString(_actualHero.heroData.dps.value));
			_triggered = false;
			if (_heroBought && _actualHero.id === event.ah.id)
			{
				if (event.startAnimation)
					FireUpgradeAnimation();
				ParseHeroData(event.heroData);
			}
		}
		
	}

}