package view.gameviewelements.rename 
{
	import com.greensock.TweenLite;
	import d_hentity.HEntity;
	import feathers.controls.Button;
	import feathers.controls.text.BitmapFontTextEditor;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.controls.text.StageTextTextEditor;
	import feathers.controls.text.TextFieldTextEditor;
	import feathers.controls.TextInput;
	import feathers.core.ITextEditor;
	import feathers.core.ITextRenderer;
	import feathers.events.FeathersEventType;
	import feathers.text.BitmapFontTextFormat;
	import flash.text.TextFormat;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.BitmapFont;
	import starling.textures.Texture;
	import system.HSounds;
	import view.gameviewelements.HButton;
	import view.gameviewelements.HText;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class RenamePanel extends HEntity 
	{
		private var _bg: Image = new Image(Assets.interfaceAtlas.getTexture("rename"));
		
		public static var ERROR_TEXT_X: Number = 20;					//! may be set in script
		public static var ERROR_TEXT_Y: Number = 16;					//! may be set in script
		public static var ERROR_SLIDER_X: Number = 4;					//! may be set in script
		public static var ERROR_SLIDER_Y: Number;
		public static var ERROR_SLIDER_Y_PLUS: Number = 0;
		public static var RENAME_BUTTON_X: Number = 336;				//! may be set in script
		public static var RENAME_BUTTON_Y: Number = 43;					//! may be set in script
		public static var INPUT_X: Number = 14;							//! may be set in script
		public static var INPUT_Y: Number = 46;							//! may be set in script
		public static var MAX_CHARS: uint = 20;							//! may be set in script
		public static var COMMENT_TEXT_X: Number = 160;					//! may be set in script
		public static var COMMENT_TEXT_Y: Number = 4;					//! may be set in script
		public static var SLIDE_BY: Number = 32;
		public static var WINDOW_PLUS_X: Number = 0;
		public static var WINDOW_PLUS_Y: Number = 0;
		
		private var _renameHolder: HEntity = new HEntity;
		private var _renameErrorSlider: HEntity = new HEntity;
		private var _errorSliderImage: Image;
		private var _errorSliderText: HText;
		
		
		private var _renameButton: HButton = new HButton;
		private var _renameButtonPreloader: MovieClip;
		private var _input: TextInput = new TextInput;
		//private var _actualText: HText = new HText("", "bbold24", -1, 0xdbdbdb);
		private var _commentText: HText = new HText(Language.GetString("NewSquadName"), "bbold15", -1, 0xb5b5b5);
		//private var _quad: Quad = new Quad(Preferences.SCREEN_WIDTH, Preferences.SCREEN_HEIGHT, 0x000000);
		private var _shadyBg: Image = new Image(Texture.fromEmbeddedAsset(Assets.SHADY_BG_TEXTURE));
		//private var _reactOnShadyTouch: Boolean;
		private var _oldName: String;
		
		public function RenamePanel() 
		{
			super(false, false);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
									
			_errorSliderImage = new Image(Assets.interfaceAtlas.getTexture("rename_error"));
			ERROR_SLIDER_Y = _bg.texture.height - _errorSliderImage.texture.height + ERROR_SLIDER_Y_PLUS;
			_renameErrorSlider.addChild(_errorSliderImage);			
						
			_errorSliderText = new HText(Language.GetString("NameTaken"), "bbold15", -1, 0xffd39b);
			_renameErrorSlider.addChild(_errorSliderText);
			_errorSliderText.x = ERROR_TEXT_X;
			_errorSliderText.y = ERROR_TEXT_Y;			
			
			_renameErrorSlider.x = ERROR_SLIDER_X;
			_renameErrorSlider.y = ERROR_SLIDER_Y; //- _errorSliderImage.texture.height;			
			_renameErrorSlider.visible = false;
			
			_renameHolder.addChild(_renameErrorSlider);			
			
			_renameHolder.addChild(_bg);			
			_renameHolder.x = Math.round(Preferences.SCREEN_WIDTH / 2 - _bg.texture.width / 2)+WINDOW_PLUS_X;
			_renameHolder.y = Math.round(Preferences.SCREEN_HEIGHT / 2 - _bg.texture.height / 2)+WINDOW_PLUS_Y;
			
			_renameButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("rename_btn"));
			_renameButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("rename_btn"));
			_renameButton.hoverSkin.filter = Filters.highlightFilter;
			_renameHolder.addChild(_renameButton);
			_renameButton.x = RENAME_BUTTON_X;
			_renameButton.y = RENAME_BUTTON_Y;
			_renameButton.addEventListener(Event.TRIGGERED, OnButtonTriggered);
			
			_renameButtonPreloader = new MovieClip(Assets.miscAtlas.getTextures("rename_button_preloader_"),15);
			_renameButtonPreloader.visible = false;
			_renameButtonPreloader.x = _renameButton.x;
			_renameButtonPreloader.y = _renameButton.y;
			_renameHolder.addChild(_renameButtonPreloader);
						
			_commentText.x = COMMENT_TEXT_X;
			_commentText.y = COMMENT_TEXT_Y;
			_renameHolder.addChild(_commentText);
			_shadyBg.addEventListener(TouchEvent.TOUCH, OnShadyBgTouch);
			
			_input.backgroundSkin = new Quad(306, 40);
			_input.backgroundSkin.alpha = 0;
			_input.promptFactory = function(): ITextRenderer
			{
				var textRenderer:BitmapFontTextRenderer = new BitmapFontTextRenderer();
			 
				// customize properties and styleshere
				textRenderer.textFormat = new BitmapFontTextFormat(Assets.bbold24,-1,0xdbdbdb);
				return textRenderer;
			}
			
			_input.textEditorFactory = function(): ITextEditor
			{
				var editor:StageTextTextEditor = new StageTextTextEditor;
				editor.fontFamily = "Lora";
				editor.fontSize = 24;
				editor.color = 0xdbdbdb;
				//editor.textFormat = new TextFormat("Lora", 24, 0xdbdbdb);
				return editor;
			}			
			_renameHolder.addChild(_input);
			_input.maxChars = MAX_CHARS;
			_input.x = INPUT_X;
			_input.y = INPUT_Y;
			
			this.visible = false;
			//_quad.alpha = 0.5;
			
			//_renameHolder.addChild(_actualText);
			//_actualText.x = 14;
			//_actualText.y = 46;
			
			_input.addEventListener(FeathersEventType.ENTER, input_enterHandler);
			World.gWorld.addEventListener("renameResponse", OnRenameResponseReceived);
			//_input.addEventListener(Event.CHANGE, OnTextChanged);
		}
		
		private function OnShadyBgTouch(e:TouchEvent):void 
		{
			var touch: Touch = e.getTouch(_shadyBg, TouchPhase.ENDED);
			if (touch)
				Show(false);
		}
		
		private function OnRenameResponseReceived(e:Event):void 
		{
			PreloadStarted(false);
			if (Boolean(e.data) === true)
			{
				World.gWorld.dispatchEventWith("squadNameChanged",false,_input.text);
			}
			else
			{
				_renameErrorSlider.visible = true;
				TweenLite.to(_renameErrorSlider, 0.5, { y: ERROR_SLIDER_Y + SLIDE_BY } );
			}
		}
		
		//private function OnTextChanged(e:Event):void 
		//{
			//_actualText.text = _input.text;
		//}
		
		private function input_enterHandler(e:Event):void 
		{			
			OnNameEntered();
		}
		
		private function OnButtonTriggered(e:Event):void 
		{
			OnNameEntered();
		}
		
		private function PreloadStarted(started: Boolean): void
		{
			if (started)
			{
				_renameButton.visible = false;
				Starling.juggler.add(_renameButtonPreloader);
				_renameButtonPreloader.visible = true;
				_renameButtonPreloader.play();
				_input.isEditable = false;
			}
			else
			{
				_renameButton.visible = true;
				Starling.juggler.remove(_renameButtonPreloader);
				_renameButtonPreloader.visible = false;
				_renameButtonPreloader.stop();
				_input.isEditable = true;				
			}
		}
		
		private function OnNameEntered():void 
		{
			if (_input.text.length < 3)
				return;
			if (_input.text != _oldName)
			{
				PreloadStarted(true);
				World.gameController.ChangeSquadName(_input.text);
			}
			else
				Show(false);
		}
		
		//private function input_focusInHandler(e:Event):void 
		//{
			//MConsole.CmdWrite("Focus!");
		//}
		
		public function Show(show: Boolean, name: String=""):void 
		{
			if (show)
			{
				HSounds.PlaySound("FadeIn");
				visible = true;
				alpha = 0;
				TweenLite.to(this, 0.5, { alpha : 1, onComplete: function ():void 
				{
					
				} } );
				_oldName = name;
				_input.text = name;
				_input.selectRange(_input.text.length);
				_input.setFocus();
				_renameErrorSlider.y = ERROR_SLIDER_Y;
			}
			else
			{
				HSounds.PlaySound("FadeOut");
				_renameErrorSlider.visible = false;
				TweenLite.to(this, 0.5, { alpha : 0, onComplete: function (obj: HEntity):void 
				{
					obj.visible = false;
				}, 
				onCompleteParams: [this] }
				);
			}
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_shadyBg);
			addChild(_renameHolder);
		}
		
	}

}