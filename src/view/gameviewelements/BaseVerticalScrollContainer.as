package view.gameviewelements 
{
	import feathers.controls.ScrollBar;
	import feathers.controls.ScrollContainer;
	import feathers.display.Scale3Image;
	import feathers.layout.VerticalLayout;
	import feathers.controls.Button;
	import feathers.textures.Scale3Textures;
	import starling.display.Image;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class BaseVerticalScrollContainer extends ScrollContainer
	{
		public static const SCROLL_CONTAINER_WIDTH: int = 412;
		public static const SCROLL_CONTAINER_HEIGHT: int = 336;
		public static const SCROLL_CONTAINER_X: int = 44;
		public static const SCROLL_CONTAINER_Y: int = 200;	
		
		public function BaseVerticalScrollContainer() 
		{
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);				
			var layout: VerticalLayout = new VerticalLayout;
			layout.gap = 0;
			this.layout = layout;
			this.width = SCROLL_CONTAINER_WIDTH;
			this.height = SCROLL_CONTAINER_HEIGHT;
			this.x = SCROLL_CONTAINER_X;
			this.y = SCROLL_CONTAINER_Y;
			this.interactionMode = INTERACTION_MODE_MOUSE;
			this.scrollBarDisplayMode = SCROLL_BAR_DISPLAY_MODE_FIXED;
			this.snapScrollPositionsToPixels = true;
			this.verticalScrollBarFactory = function(): ScrollBar
			{
				var scrollBar: ScrollBar = new ScrollBar;
				scrollBar.direction = ScrollBar.DIRECTION_VERTICAL;
				scrollBar.trackLayoutMode = ScrollBar.TRACK_LAYOUT_MODE_SINGLE;
				scrollBar.paddingBottom = 10;
				scrollBar.paddingTop = 10;
				
				scrollBar.thumbFactory = function (): Button 
				{
					var button: HButton = new HButton;
					var textures: Scale3Textures = new Scale3Textures(Assets.interfaceAtlas.getTexture("handle"), 8, 56, Scale3Textures.DIRECTION_VERTICAL);					
					button.defaultSkin = new Scale3Image(textures);
					button.hoverSkin = new Scale3Image(textures);
					button.hoverSkin.filter = Filters.highlightFilter;
					return button;
				}
				
				return scrollBar;
			}
			
		}
		
		public function AddChildrenToStage():void 
		{
			for (var i: int = 1; i < numChildren; i++)
			{
				ScrollContainerItem(getChildAt(i)).AddChildrenToStage();
			}
		}
		
		protected function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);	
		}
		
	}

}