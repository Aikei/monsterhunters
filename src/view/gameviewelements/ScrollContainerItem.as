
package view.gameviewelements 
{
	import flash.geom.Rectangle;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import starling.utils.Color;
	import d_hentity.HEntity;
	import system.HSounds;
	import view.BitmappedText;
	import view.gameviewelements.OldBuyUpgradeButton;
	import view.InvisibleTooltipHolder;
	
	import com.greensock.TweenLite;
	import com.greensock.TimelineLite;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class ScrollContainerItem extends HEntity 
	{
		public static const BASE_ITEM_PORTRAIT_WIDTH: Number = 48;
		public static const BASE_ITEM_PORTRAIT_HEIGHT: Number = 48;
		
		public static var NOT_BOUGHT_FRAME_X: Number = 14;
		public static var NOT_BOUGHT_FRAME_Y: Number = 28;
		
		public static var BUTTON_X: int = 304.75;
		public static var BUTTON_Y: int = 53;
		public static var ITEM_NAME_TEXT_X: int = 77;
		public static var ITEM_NAME_TEXT_Y: int = 20;
		public static var ITEM_DAMAGE_TEXT_X: int = 88;
		public static var ITEM_DAMAGE_TEXT_Y: int = 54;
		public static var ITEM_COST_TEXT_X: int = 325;
		public static var ITEM_COST_TEXT_Y: int = 26;		
		public static var ITEM_PORTRAIT_X: int = 14;
		public static var ITEM_PORTRAIT_Y: int = 28;
		
		public var itemPortraitX: Number = ITEM_PORTRAIT_X;
		public var itemPortraitY: Number = ITEM_PORTRAIT_Y;
		
		protected var _inactive: Boolean = false;
		protected var _image: Image;
		protected var _upgradeImage: Image = new Image(Assets.interfaceAtlas.getTexture("upgrade"));
		protected var _buyButton: OldBuyUpgradeButton = new OldBuyUpgradeButton;
		protected var _itemPortrait: Image = null;
		protected var _itemAnimatedPortrait: MovieClip = null;
		protected var _cost: BigInt;
		protected var _notBought: Image = new Image(Assets.interfaceAtlas.getTexture("not_owned"));
		protected var _itemNameText: HText;		
		protected var _itemDamageText: HText;
		protected var _itemCostText: HText;
		protected var _itemPortraitHolder: HEntity = new HEntity;
		protected var _buyButtonTooltipHolder: InvisibleTooltipHolder = new InvisibleTooltipHolder(BUTTON_X, BUTTON_Y, 1, 1, "");
		//protected var _itemNameText: BatchedBitmapLabel;		
		//protected var _itemDamageText: BatchedBitmapLabel;
		//protected var _itemCostText: BatchedBitmapLabel;			
		
		public function ScrollContainerItem() 
		{
			super();
			
			_image = new Image(Assets.interfaceAtlas.getTexture("hero_item"));
			_image.touchable = false;
			_upgradeImage.touchable = false;
			_upgradeImage.visible = false;
			_notBought.touchable = false;
			//_itemNameText = new BitmappedText();
			//_itemNameText.Init("", "bbold16", 0xd2b5a6, 0, 0);
			//_itemNameText.x = ITEM_NAME_TEXT_X;
			//_itemNameText.y = ITEM_NAME_TEXT_Y;
			//_itemNameText.touchable = false;
			
			//_itemCostText = new BitmappedText();
			//_itemCostText.x = ITEM_COST_TEXT_X;
			//_itemCostText.y = ITEM_COST_TEXT_Y;
			//_itemCostText.touchable = false;
			
			//_itemNameText = new BatchedBitmapLabel("bbold16", 16, 0xd2b5a6);
			
			_itemNameText = new HText("","bbold16", -1, 0xd2b5a6);
			//_itemNameText.batchable = true;
			//_itemNameText.autoSize = TextFieldAutoSize.HORIZONTAL;
			
			_itemNameText.x = ITEM_NAME_TEXT_X;
			_itemNameText.y = ITEM_NAME_TEXT_Y;
			_itemNameText.touchable = false;			
			
			//_itemPortraitHolder.x = ITEM_PORTRAIT_X;
			//_itemPortraitHolder.y = ITEM_PORTRAIT_Y;
			
			//_itemCostText = new BatchedBitmapLabel("bbold14", 14, 0xd2b5a6);
			
			_itemCostText = new HText("","bbold14", -1, 0xd2b5a6);
			//_itemCostText.batchable = true;
			//_itemCostText.autoSize = TextFieldAutoSize.HORIZONTAL;
			_itemCostText.x = ITEM_COST_TEXT_X;
			_itemCostText.y = ITEM_COST_TEXT_Y;
			_itemCostText.touchable = false;			
			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			World.gWorld.addEventListener("N_CoinsNow", OnCoinsNow);
		}
		
		protected function OnCoinsNow(e: Event):void 
		{
			CheckCost(e.data as BigInt);
		}
		
		public function SetCost(newCost: BigInt):void 
		{
			_cost = BigInt.dup(newCost);
		}
		
		public function CheckCost(coins: BigInt):void 
		{
			if (BigInt.greater(_cost, coins))
			{
				_buyButton.SetEnabled(false);
				_buyButtonTooltipHolder.visible = true;
				_itemCostText.color = 0x9f897d;
			}
			else
			{
				_buyButton.SetEnabled(true);
				_buyButtonTooltipHolder.visible = false;
				_itemCostText.color = 0xffffff;
			}
		}			
		
		protected function OnAddedToStage(event: Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function AddChildrenToStage():void 
		{
			AddImagesToStage();
			AddTextToStage();			
		}
		
		protected function AddImagesToStage():void 
		{
			addChild(_image);
			addChild(_upgradeImage);			
			if (_itemPortrait || _itemAnimatedPortrait)
			{
				if (_itemPortrait)
					_itemPortraitHolder.addChild(_itemPortrait);
				else
				{
					_itemPortraitHolder.clipRect = new Rectangle(0, 0, 48, 48);
					_itemPortraitHolder.addChild(_itemAnimatedPortrait);
					Starling.juggler.add(_itemAnimatedPortrait);
					_itemAnimatedPortrait.play();
				}
				_itemPortraitHolder.x = itemPortraitX;
				_itemPortraitHolder.y = itemPortraitY;				
			}
			addChild(_buyButton);
			addChild(_itemPortraitHolder);
			if (_notBought)
			{
				addChild(_notBought);
				_notBought.x = NOT_BOUGHT_FRAME_X;
				_notBought.y = NOT_BOUGHT_FRAME_Y;
			}			
			
			_buyButton.x = BUTTON_X;
			_buyButton.y = BUTTON_Y;
			_buyButtonTooltipHolder.SetQuadSize(_buyButton.defaultSkin.width, _buyButton.defaultSkin.height);
			_buyButtonTooltipHolder.tooltipText = _buyButton.tooltipText;
			
			addChild(_buyButtonTooltipHolder);
			_buyButton.label = Language.GetString("Buy");
			_buyButton.addEventListener(Event.TRIGGERED, OnBuyButtonTriggered);				
		}
		
		protected function AddTextToStage():void 
		{
			addChild(_itemNameText);						
			addChild(_itemCostText);			
		}		
		
		protected function FireUpgradeAnimation():void 
		{
			HSounds.PlaySound("Purchase");
			_upgradeImage.visible = true;
			_upgradeImage.alpha = 0;
			var timeLine: TimelineLite = new TimelineLite();
			timeLine.to(_upgradeImage, 0.25, { alpha: 1 } );
			timeLine.to(_upgradeImage, 0.25, { alpha: 0, onComplete: function ():void { _upgradeImage.visible = false; } });
		}
		
		protected function OnBuyButtonTriggered(event: Event): void 
		{

		}
		
		override public function Destroy():void 
		{
			World.gWorld.removeEventListener("N_CoinsNow", OnCoinsNow);
			_buyButton.removeEventListener(Event.TRIGGERED, OnBuyButtonTriggered);
			super.Destroy();
		}
		
	}

}