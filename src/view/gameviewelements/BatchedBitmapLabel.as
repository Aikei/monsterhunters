package view.gameviewelements 
{
	import feathers.controls.Label;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.ITextRenderer;
	import feathers.text.BitmapFontTextFormat;
	import starling.textures.TextureSmoothing;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class BatchedBitmapLabel extends Label 
	{
		private var mFont: String;
		private var mFontSize: int;
		private var mFontColor: uint;
		
		public function BatchedBitmapLabel(font: String = "bbold11", fontSize: int = 11, fontColor: uint = 0xffffff) 
		{
			super();
			mFont = font;
			mFontSize = fontSize;
			mFontColor = fontColor;
			
			this.textRendererFactory = 	function (): ITextRenderer 
			{
				var textRenderer: BitmapFontTextRenderer = new BitmapFontTextRenderer();
				//textRenderer.textFormat = new BitmapFontTextFormat(font, fontSize, fontColor);
				textRenderer.smoothing = TextureSmoothing.NONE;
				textRenderer.useSeparateBatch = false;
				//textRenderer.embedFonts = true;
				return textRenderer;
			}
			
			ReadjustTextFormat();
		}
		
		private function ReadjustTextFormat():void 
		{
			this.textRendererProperties.textFormat = new BitmapFontTextFormat(mFont, mFontSize, mFontColor);
		}
		
		public function SetFont(font: String):void 
		{
			mFont = font;
			ReadjustTextFormat();
		}
		
		public function SetFontSize(fontSize: int):void 
		{
			mFontSize = fontSize;
			ReadjustTextFormat();
		}
		
		public function SetFontColor(c: uint):void 
		{
			mFontColor = c;
			ReadjustTextFormat();
		}
		
		public function set color(c: uint):void 
		{
			SetFontColor(c);
		}
		
		public function get color(): uint 
		{
			return mFontColor;
		}
		
		public function set font(f: String):void 
		{
			SetFont(f);
		}
		
		public function get font(): String 
		{
			return mFont;
		}
		
		public function set fontSize(sz: int): void 
		{
			SetFontSize(sz);
		}
		
		public function get fontSize(): int 
		{
			return mFontSize;
		}
		
		public function SetTextFormat(font: String = "bbold11", fontSize: int=11, fontColor: uint = 0xffffff):void 
		{
			this.textRendererProperties.textFormat = new BitmapFontTextFormat(font, fontSize, fontColor);
		}
		
		override public function set text(t: String):void 
		{
			super.text = t;
			validate();
		}
		
	}

}