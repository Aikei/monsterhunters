package view.gameviewelements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import starling.display.Image;
	import starling.events.Event;
	import view.BitmappedText;
	
	public class BuyUpgradeButton extends PanelButton 
	{
				
		public function BuyUpgradeButton() 
		{
			super(true);
			_button.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("upgrade_button"));
			_button.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("upgrade_button_hover"));
			_button.disabledSkin = new Image(Assets.interfaceAtlas.getTexture("upgrade_button_inactive"));
			World.gWorld.addEventListener(Event.ADDED_TO_STAGE, gWorld_addedToStage);
		}
		
		private function gWorld_addedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, gWorld_addedToStage);
			//SetActiveLabel(
		}
					
		override public function set label(text: String): void 
		{
			super.label = text.toUpperCase();
		}
		
		override public function set bitmaplabel(text: String): void 
		{
			super.bitmaplabel = text.toUpperCase();
		}		
	}

}