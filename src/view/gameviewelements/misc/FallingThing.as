package view.gameviewelements.misc 
{	
	/**
	 * ...
	 * @author Aikei
	 */
	import d_hentity.HEntity;
	import flash.geom.Point;
	import hevents.L_HEventCoinsReceived;
	import starling.display.Image;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import flash.utils.*;
	import starling.textures.Texture;
	import starling.utils.Color;
	import system.HCursor;
	import system.HSounds;
	import view.GameView;
	import view.gameviewelements.MonsterHpPanel;
	/**
	 * ...
	 * @author Aikei
	 */
	public class FallingThing extends HEntity 
	{
		public static var GROUND_Y: Number = MonsterHpPanel.HP_PANEL_Y + 30;
		
		public var jumpBeforeFall: Boolean = true;
		
		protected var _image: Image;
		//private var _number: BigInt;
		protected var _timeOut: uint = 0;
		protected var _alive: Boolean;
		protected var _inDieAnim: Boolean = false;
		
		protected var _collectSoundName: String = "";
		protected var _sendFlyingText: String = "";
		protected var _deathAnimTime: Number = 0;
		protected var _fontName: String = "goldFont";
		protected var _dieWithin: Number = 10;
		protected var _invulnerable: Boolean = false;
		protected var _invulnerableDuringFall: Boolean = false;
		//protected static var _fallingThingsPool: Vector.<FallingThing> = new Vector.<FallingThing>;		
		//static private const COIN_WIDTH: Number = Assets.interfaceAtlas.getTexture("coin").width;
		//static private const COIN_HEIGHT: Number = Assets.interfaceAtlas.getTexture("coin").height;
		
		public function FallingThing(texture: Texture, x: int, y: int) 
		{
			super(false);
			_useWorldsPassedTime = true;
			InitImage(texture);
			//InitFallingThing(x, y);

			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			//addEventListener(TouchEvent.TOUCH, OnTouch);
		}
		
		protected function InitImage(texture: Texture):void 
		{
			if (!_image)
			{
				_image = new Image(texture);
			}
			else
			{
				_image.texture = texture;
				_image.width = texture.width;
				_image.height = texture.height;
			}			
		}
		
		public function InitFallingThing(x: int, y: int): void 
		{
			StopMyMovementAndAcceleration();

			this.x = x;
			this.y = y;
			visible = true;
			_alive = true;
			_inDieAnim = false;
			if (_dieWithin > 0)
				_timeOut = setTimeout(StartDieAnim, _dieWithin*1000);
			ApplyGravity(1500, GROUND_Y);
			if (jumpBeforeFall)
			{
				var dir: Vec2 = new Vec2;
				dir.x = (Misc.Random(0.0, 8.0) - 4.0)/10.0;
				dir.y = -(Misc.Random(5.0, 10.0)/10.0);			
				ApplyRelativeVelocity(dir, 300);
			}
		}
						
		//public static function GetFallingThing(x: int, y: int): FallingThing 
		//{
			//var thing: FallingThing;
			//if (_pool.length === 0)
			//{
				//thing = new FallingThing(x, y);
			//}
			//else
			//{
				//thing = _pool.pop();
				//thing.Init(x, y);
			//}
			//return thing;
		//}
		
		protected function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_image);
			alignPivot();			
		}
		
		//override public function OnTouch(event: TouchEvent): void 
		//{
			//if (_inDieAnim)
				//return;
			//var touch: Touch = event.getTouch(this, null);
			//if (touch && (touch.phase === TouchPhase.HOVER || touch.phase === TouchPhase.BEGAN))
			//{				
				//StartDieAnim();
			//}
		//}
		
		override public function OnEnterFrame(event: EnterFrameEvent): void
		{
			super.OnEnterFrame(event);
			if (!_inDieAnim && _image.bounds.intersects(HCursor.cursor.getBounds(this)))
			{
				OnCursorCollision();
			}
		}
		
		override protected function OnFall():void
		{
			if (_invulnerableDuringFall)
				_invulnerable = false;
		}		
		
		public function OnCursorCollision():void 
		{
			if (!_invulnerable)
				StartDieAnim();
		}
		
		protected function StartDieAnim(): void 
		{
			_inDieAnim = true;
			if (_collectSoundName.length > 0)
				HSounds.PlaySound(_collectSoundName);
			if (_timeOut > 0)
				clearTimeout(_timeOut);
			if (_sendFlyingText.length > 0)
				World.gWorld.dispatchEventWith("createFlyingText", false, { x: x, y: y, text: _sendFlyingText, font: _fontName, color: Color.WHITE } );
			StopMyMovementAndAcceleration();
			setTimeout(Die, _deathAnimTime*1000);
		}
		
		protected function Die():void 
		{
			alpha = 1;
			visible = false;
			_alive = false;				
		}
			
		//private function StartDieAnim(): void 
		//{
			//_inDieAnim = true;
			//HSounds.PlaySound("Collect");
			//clearTimeout(_timeOut);
			//World.gameController.AddCoins(_numberOfCoins);
			//World.gWorld.dispatchEventWith("createFlyingText", false, { x: x, y: y, text: BigInt.bigint2HString(_numberOfCoins), font: "timerFont", color: Color.YELLOW } );
			//StopMyMovementAndAcceleration();
			//var p: Point = GameView.gView.GetCoinCounterCenter();
			//Tweens.TweenToLinearInOut(this, 0.5, p.x, p.y);
			//setTimeout(Die, 500);
		//}
		
		//override public function Destroy():void 
		//{
			//clearTimeout(_timeOut);
			//removeEventListener(TouchEvent.TOUCH, OnTouch);
			////if (_moving)
			////{
				////_numberOfCoins = BigInt.add(_numberOfCoins,BigInt.divide(_numberOfCoins, BigInt.shStr2BigInt("2")));
			////}
			//World.gameController.AddCoins(_numberOfCoins);			
			//World.gWorld.dispatchEvent(new Event("createFlyingText", false, { x: x, y: y, text: BigInt.bigint2HString(_numberOfCoins) } ));
			//super.Destroy();
		//}
		
	}

}