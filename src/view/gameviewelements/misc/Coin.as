package view.gameviewelements.misc 
{
	import d_hentity.HEntity;
	import flash.geom.Point;
	import hevents.L_HEventCoinsReceived;
	import starling.display.DisplayObject;
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import flash.utils.*;
	import starling.textures.Texture;
	import starling.utils.Color;
	import system.HSounds;
	import view.GameView;
	/**
	 * ...
	 * @author Aikei
	 */
	public class Coin extends FallingThing 
	{
		private var _numberOfCoins: BigInt;
		private static var _pool: Vector.<Coin> = new Vector.<Coin>;
		
		//static private const COIN_WIDTH: Number = Assets.interfaceAtlas.getTexture("coin").width;
		//static private const COIN_HEIGHT: Number = Assets.interfaceAtlas.getTexture("coin").height;
		
		public function Coin(numberOfCoins: BigInt, x: int, y: int) 
		{
			super(Assets.interfaceAtlas.getTexture("coin"), x, y);
			_fontName = "goldFont";
			_collectSoundName = "Collect";			
			_deathAnimTime = 0.5;
			InitCoin(numberOfCoins, x, y);
		}
		
		protected function InitCoin(numberOfCoins: BigInt, x: int, y: int):void 
		{
			super.InitFallingThing(x, y);
			_numberOfCoins = numberOfCoins;
			_sendFlyingText = BigInt.bigint2HString(_numberOfCoins);
		}
		
		override protected function OnFall():void
		{
			super.OnFall();
			HSounds.PlaySound("Drop");
		}
		
		public static function GetCoin(numberOfCoins: BigInt, x: int, y: int, newParent: DisplayObjectContainer = null, jump: Boolean = true): Coin 
		{
			var coin: Coin;
			if (_pool.length === 0)
			{
				coin = new Coin(numberOfCoins, x, y);
				if (newParent)
					newParent.addChild(coin);				
			}
			else
			{
				coin = _pool.pop();
				coin.jumpBeforeFall = jump;
				coin.InitCoin(numberOfCoins, x, y);
			}
			return coin;
		}		
		
		override protected function StartDieAnim():void 
		{
			super.StartDieAnim();
			World.gameController.AddCoins(_numberOfCoins);
			var p: Point = GameView.gView.GetCoinCounterCenter();
			Tweens.TweenToLinearInOut(this, _deathAnimTime, p.x, p.y);
		}
		
		override protected function Die():void 
		{
			super.Die();
			_pool.push(this);
		}		
		
	}

}