package view.gameviewelements.misc 
{
	import com.greensock.TweenLite;
	import starling.display.DisplayObjectContainer;
	import starling.textures.Texture;
	import system.HSounds;
	import view.GameView;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class GloryThing extends FallingThing 
	{		
		private var _value: uint;
		private static var _pool: Vector.<GloryThing> = new Vector.<GloryThing>;
		
		
		public function GloryThing(value: uint, x: int, y: int, jump: Boolean=true) 
		{			
			jumpBeforeFall = jump;
			super(Assets.interfaceAtlas.getTexture("renown"), x, y);
			_fontName = "gloryFont";
			_collectSoundName = "Collect";
			//_sendFlyingText = "+" + value+" @";
			_sendFlyingText = "+"+value+" "+Language.GetString("Glory").toLowerCase();
			_deathAnimTime = 0.75;
			InitGloryThing(value, x, y);
		}
		
		protected function InitGloryThing(value: uint, x: int, y: int):void 
		{
			super.InitFallingThing(x, y);			
			_value = value;
		}
		
		override protected function OnFall():void
		{
			HSounds.PlaySound("Drop");
		}
		
		public static function GetGloryThing(value: uint, x: int, y: int, newParent: DisplayObjectContainer = null, jump: Boolean = true): GloryThing 
		{
			var thing: GloryThing;
			if (_pool.length === 0)
			{
				thing = new GloryThing(value, x, y, jump);
				if (newParent)
					newParent.addChild(thing);				
			}
			else
			{
				thing = _pool.pop();
				thing.jumpBeforeFall = jump;
				thing.InitGloryThing(value, x, y);
			}
			
			return thing;
		}		
		
		override protected function StartDieAnim():void 
		{
			super.StartDieAnim();
			World.gameController.AddGlory(_value);
			this.alpha = 0;
			//TweenLite.to(this, _deathAnimTime, { y: y - 200, alpha: 0 } );
		}
		
		override protected function Die():void 
		{
			super.Die();
			visible = false;
			_alive = false;			
			_pool.push(this);
		}		
		
	}

}