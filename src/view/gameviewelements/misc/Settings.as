package view.gameviewelements.misc 
{
	import com.greensock.easing.Back;
	import com.greensock.TweenLite;
	import d_hentity.HEntity;
	import starling.display.Image;
	import starling.events.Event;
	import system.HSounds;
	import view.gameviewelements.HToggleButton;
	
	/**
	 * ...
	 * @author Aikei
	 */
	
	public class Settings extends HEntity
	{
		public static var SLIDE_FROM_Y: Number = -70;
		public static var SLIDE_TO_Y: Number = -10;
		public static var SETTINGS_X: Number = 15;
		public static var SETTINGS_SLIDE_TIME: Number = 0.33;
		
		private var _image: Image = new Image(Assets.interfaceAtlas.getTexture("settings_panel"));
		private var _muteMusicButton: HToggleButton = new HToggleButton;
		private var _muteSoundsButton: HToggleButton = new HToggleButton;
		private var _settingsButton: HToggleButton = new HToggleButton;
			
		public function Settings() 
		{		
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			this.y = SLIDE_FROM_Y;
			this.x = SETTINGS_X;
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_image);
			
			_muteMusicButton.x = 7;
			_muteMusicButton.y = 9;
			_muteMusicButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("music_on"));
			_muteMusicButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("music_on"));
			_muteMusicButton.hoverSkin.filter = Filters.highlightFilter;			
			_muteMusicButton.defaultSelectedSkin = new Image(Assets.interfaceAtlas.getTexture("music_off"));
			_muteMusicButton.selectedHoverSkin = new Image(Assets.interfaceAtlas.getTexture("music_off"));
			_muteMusicButton.selectedHoverSkin.filter = Filters.highlightFilter;
			_muteMusicButton.validate();
			_muteMusicButton.addEventListener(Event.CHANGE, OnMuteMusicButtonChange);
			addChild(_muteMusicButton);
			
			_muteSoundsButton.x = 7;
			_muteSoundsButton.y = _muteMusicButton.y + _muteMusicButton.height + 1;
			_muteSoundsButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("sounds_on"));
			_muteSoundsButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("sounds_on"));
			_muteSoundsButton.hoverSkin.filter = Filters.highlightFilter;			
			_muteSoundsButton.defaultSelectedSkin = new Image(Assets.interfaceAtlas.getTexture("sounds_off"));
			_muteSoundsButton.selectedHoverSkin = new Image(Assets.interfaceAtlas.getTexture("sounds_off"));
			_muteSoundsButton.selectedHoverSkin.filter = Filters.highlightFilter;
			_muteSoundsButton.validate();
			_muteSoundsButton.addEventListener(Event.CHANGE, OnMuteSoundsButtonChange);
			addChild(_muteSoundsButton);
			
			_settingsButton.x = 7;
			_settingsButton.y = _muteSoundsButton.y + _muteSoundsButton.height + 1;
			_settingsButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("settings_open_btn"));
			_settingsButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("settings_open_btn"));
			_settingsButton.hoverSkin.filter = Filters.highlightFilter;				
			_settingsButton.defaultSelectedSkin = new Image(Assets.interfaceAtlas.getTexture("settings_close_btn"));
			_settingsButton.selectedHoverSkin = new Image(Assets.interfaceAtlas.getTexture("settings_close_btn"));
			_settingsButton.selectedHoverSkin.filter = Filters.highlightFilter;
			_settingsButton.addEventListener(Event.TRIGGERED, OnSettingsButtonTriggered);
			addChild(_settingsButton);
			
		}
		
		private function OnMuteSoundsButtonChange(e:Event):void 
		{
			HSounds.muteSounds = _muteSoundsButton.isSelected;
		}
		
		private function OnMuteMusicButtonChange(e:Event):void 
		{
			HSounds.muteMusic = _muteMusicButton.isSelected;
		}
		
		private function OnSettingsButtonTriggered(e:Event):void 
		{
			if (!_settingsButton.isSelected)
				TweenLite.to(this, SETTINGS_SLIDE_TIME, { y : SLIDE_TO_Y, ease: Back.easeOut.config(0.9) } );
			else
				TweenLite.to(this, SETTINGS_SLIDE_TIME, { y : SLIDE_FROM_Y, ease: Back.easeOut.config(0.9) } );
		}
		
	}

}