package view.gameviewelements.misc 
{
	import com.greensock.TweenLite;
	import flash.utils.setTimeout;
	import logic.Quest;
	import starling.events.Event;
	import starling.utils.HAlign;
	import view.GameView;
	import view.gameviewelements.HText;
	import view.gameviewelements.MessageWindow.MessageWindow;
	import view.gameviewelements.MessageWindow.TitledMessageWindow;
	import starling.text.TextFieldAutoSize;
	/**
	 * ...
	 * @author Aikei
	 */
	public class QuestWindowOld extends TitledMessageWindow 
	{
		public static var WINDOW_X: Number = 100;
		public static var WINDOW_Y: Number = 30;
		
		public static var REWARDS_FALL_Y: Number = 125;
		public static var REWARDS_FALL_X: Number = 215;
		public static var REWARDS_SPREAD_X: Number = 80;
		
		protected var _progressWord: String;
		protected var _progressText: HText;
		protected var _progressGoal: int;
		protected var _quest: Quest;
		
		public function QuestWindowOld(quest: Quest) 
		{
			_quest = quest;
			var questDescription: String = Language.GetQuestDescription(quest);
			var title: String = Language.GetString("Quest");			
			super(title, questDescription, "bregular13", "bbold13",-1,-1,0xffffff, 0xffffff);
			_topImage.visible = false;
			_bg.visible = false;
			_bottomImage.visible = false;

			_progressWord = Language.GetString("QuestProgress");
			_progressGoal = quest.progressGoal;
			_progressText = new HText(_progressWord + ": " + quest.progress + " / " + _progressGoal, "bbold13", -1,0xffffff, TextFieldAutoSize.VERTICAL);
			_progressText.textField.width = 175;
			_progressText.textField.hAlign = HAlign.LEFT;
			_progressText.x = _text.x;
			_progressText.y = _text.y + _text.height + 10;
			this.x = WINDOW_X;
			this.y = WINDOW_Y;
			World.gWorld.addEventListener("questProgress", OnQuestProgress);
		}
		
		protected function OnQuestDone()
		{
			_text.textField.hAlign = HAlign.CENTER;
			World.gWorld.dispatchEventWith("showBigCenterText", false, { text: Language.GetString("QuestComplete"), showDuration: 3.0, disappearDuration: 1.0 });
			_text.visible = false;
			_titleText.visible = false;
			_progressText.visible = false;
			GameView.gView.CreateGloryThings(_quest.gloryReward, REWARDS_SPREAD_X, REWARDS_FALL_X, REWARDS_FALL_Y, false);
			TweenLite.to(this, 3.0, { alpha: 0 } );
			setTimeout(Destroy,3000);
		}
		
		private function OnQuestProgress(e:Event):void 
		{
			if (int(e.data) === -1)
			{
				OnQuestDone();
				return;
			}
			_progressText.text = _progressWord + " " + int(e.data) + " / " + _progressGoal;
		}
		
		override public function OnAddedToStage(event: Event):void 
		{
			super.OnAddedToStage(event);
			addChild(_progressText);
		}
		
		override public function Destroy():void 
		{
			World.gWorld.removeEventListener("questProgress", OnQuestProgress);
			super.Destroy();
		}
		
		override protected function GetHeight(): Number 
		{
			return super.GetHeight()+_progressText.height+10;
		}			
		
	}

}