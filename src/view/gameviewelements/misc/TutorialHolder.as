package view.gameviewelements.misc 
{
	import com.greensock.easing.Back;
	import com.greensock.TweenMax;
	import d_hentity.HEntity;
	import hevents.L_HEventMonsterKilled;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import view.gameviewelements.HText;
	import starling.text.TextFieldAutoSize;
	/**
	 * ...
	 * @author Aikei
	 */
	public class TutorialHolder extends HEntity 
	{		
		public static var TEXT_CENTER_X: Number = 208;
		public static var TEXT_Y: Number = 20;
		public static var TEXT_WIDTH: Number = 246;
		
		public static var ARROW_X: Number = 178;
		public static var ARROW_Y: Number = 200;
		
		private var _arrow: Image = new Image(Assets.interfaceAtlas.getTexture("tutorial_arrow"));
		private var _text: HText = new HText("", "tutorialFont", -1, 0xfed648, TextFieldAutoSize.VERTICAL);
		private var _tween: TweenMax = null;
		private var _currentPhase: int = 0;
		private var _numerOfPhases: int = 1;
		
		public function get currentPhase(): int 
		{
			return _currentPhase;
		}
		
		public function TutorialHolder() 
		{
			_text.textField.width = TEXT_WIDTH;
			_text.x = TEXT_CENTER_X - TEXT_WIDTH / 2;
			_text.y = TEXT_Y;
			_arrow.x = ARROW_X;
			_arrow.y = ARROW_Y;
			visible = false;
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			World.gWorld.addEventListener("tutorialPhaseDone", OnTutorialPhaseDone);			
		}
		
		private function OnMonsterKilled(e:Event):void 
		{
			World.gWorld.removeEventListener(L_HEventMonsterKilled.H_EVENT_MONSTER_KILLED, OnMonsterKilled);
			EndPhase();		
		}
		
		private function OnTutorialPhaseDone(e:Event):void 
		{
			EndPhase();
		}
		
		public function StartCurrentPhase():void 
		{
			if (!visible)
			{
				_text.text = Language.GetString("Tutorial" + _currentPhase);
				if (_currentPhase == 0)
				{
					_text.y = _arrow.y - _text.height - 20;
					World.gWorld.addEventListener(L_HEventMonsterKilled.H_EVENT_MONSTER_KILLED, OnMonsterKilled);
				}
				_tween = TweenMax.to(_arrow, 0.5, { y: _arrow.y - 15, ease: Back.easeIn.config(0.5), yoyo: true, repeat: -1 } );
				visible = true;
			}
		}
		
		public function PointOnto(obj: DisplayObject):void 
		{
			_arrow.x = Math.round(obj.x + obj.width / 2 - _arrow.width / 2);
			_arrow.y = obj.y - _arrow.height - 30;
		}
		
		
		public function EndPhase():void 
		{
			if (_tween)
			{
				_tween.kill()
				_tween = null;
			}
			visible = false;
			_currentPhase++;
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_text);
			addChild(_arrow);
		}
		
	}

}