package view.gameviewelements.misc 
{
	import com.greensock.TweenLite;
	import d_hentity.HEntity;
	import flash.utils.setTimeout;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class DisappearingParticle extends HEntity 
	{
		private var _image: Image;
		
		public function DisappearingParticle(time: Number, texture: Texture, w: Number, h: Number, center_x: Number, center_y: Number) 
		{
			super(false, false);
			_image = new Image(texture);
			_image.width = w;
			_image.height = h;
			this.x = Math.round(center_x - w / 2);
			this.y = Math.round(center_y - h / 2);
			TweenLite.to(this, 1, { alpha: 0 } );
			setTimeout(Destroy, time * 1500);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		private function Destroy():void 
		{
			removeFromParent(true);
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			addChild(_image);
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
	}

}