package view.gameviewelements.misc 
{
	import d_hentity.HEntity;
	import starling.display.Quad;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class EmptyTipHolder extends HEntity 
	{
		private var _quad: Quad;
		
		public function EmptyTipHolder(tip: String, w: Number, h: Number, initTouch: Boolean = true) 
		{
			super(false, initTouch);
			showTooltipOnHover = true;
			tooltipText = tip;
			_quad = new Quad(w, h);
			_quad.alpha = 0;
			addChild(_quad);
		}
		
	}

}