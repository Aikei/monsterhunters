package view.gameviewelements.misc 
{
	import d_hentity.HEntity;
	import flash.geom.Rectangle;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	import view.gameviewelements.HText;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class RibbonImage extends HEntity 
	{
		public static var TEXT_CENTER_X: int = 146;
		public static var TEXT_CENTER_Y: int = 21;
		
		private var _image: Image;
		private var _text: HText;
		private var _topText: HText;
		
		private var _centerX: Number;
		private var _centerY: Number;
		
		public function get image(): Image 
		{
			return _image;
		}
		
		public function RibbonImage(texture: Texture, text: String="",centerX: Number = 146, centerY: Number = 21) 
		{
			super(false, false);
			_image = new Image(texture);
			_centerX = centerX;
			_centerY = centerY;
			this.clipRect = new Rectangle( -200, -200, this._image.texture.width + 400, this._image.texture.height + 400);
			_text = new HText(text, "bbold16", -1, 0xffffff);
			Misc.CenterObject(_text, centerX, centerY);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);		
		}
		
		public function set text(t: String):void 
		{
			_text.text = t;
			Misc.CenterObject(_text, _centerX, _centerY);				
		}
		
		public function set topText(t: String):void 
		{
			if (!_topText)
			{
				_topText = new HText(t, "oopsFont", -1);
				addChild(_topText);
			}
			else
			{
				_topText.text = t;				
			}
			Misc.CenterObject(_topText, _image.width / 2, -_topText.height+4);
		}
		
		//public function set topText(t: String):void 
		//{
			//if (!_topText)
			//{
				//_topText = new HText(t, "bbold24", -1, 0xffffff);
			//}
		//}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_image);
			addChild(_text);
		}
		
	}

}