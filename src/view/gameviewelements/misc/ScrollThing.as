package view.gameviewelements.misc 
{
	import com.greensock.easing.Back;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import eng.particle.ParticleData;
	import eng.particle.ParticleSystem;
	import flash.geom.Rectangle;
	import logic.Quest;
	import starling.display.Image;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	import system.HSounds;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class ScrollThing extends FallingThing 
	{
		private var _particles: ParticleSystem;
		private var _quest: Quest;
		private var _exclamationMark: Image = new Image(Assets.interfaceAtlas.getTexture("exclamation_mark"));
		private var _tween: TweenMax;
			
		public function ScrollThing(quest: Quest, x:int, y:int) 
		{
			super(Assets.interfaceAtlas.getTexture("quest_scroll"), x, y);
			clipRect = new Rectangle( -100, -100, 200, 200);
			_dieWithin = 0;
			
			_quest = quest;
			
			_exclamationMark.x = _image.texture.width/2 - _exclamationMark.texture.width/2;
			_exclamationMark.y = -_exclamationMark.texture.height - 4;
			_exclamationMark.visible = false;
			
			_invulnerable = true;
			_invulnerableDuringFall = true;
			
			var particleData: ParticleData = new ParticleData;
			particleData.resizeOnStart = true;
			particleData.resizeOnStartMin = 0.2;
			particleData.resizeOnStartMax = 0.33;
			particleData.minDieWithin = 0.75;
			particleData.maxDieWithin = 0.75;
			particleData.effectQuietlyDisappear = true;
			particleData.speed = 100;
			_deathAnimTime = 0.75;
			
			_particles = new ParticleSystem(Assets.interfaceAtlas.getTexture("smoke_gold"), particleData);
			_particles.startSpreadX = 30;
			_particles.startSpreadY = 30;
			
			_particles.alignPivot();
		}
		
		override protected function OnFall():void
		{
			super.OnFall();
			HSounds.PlaySound("ScrollDrop");
			_exclamationMark.visible = true;
			_tween = TweenMax.to(_exclamationMark, 0.5, { y: _exclamationMark.y - 10, ease: Back.easeIn, yoyo: true, repeat: -1 } );
		}
		
		//override public function OnCursorCollision():void 
		//{			
			//if (!_invulnerable)
			//{
				//_tween.kill();
				//StartDieAnim();
			//}
		//}		
		
		override public function OnTouch(event: TouchEvent): void
		{
			super.OnTouch(event);
			if (_invulnerable)
				return;
			var touch: Touch = event.getTouch(this, null);
			if (touch && touch.phase === TouchPhase.ENDED)
			{
				_tween.kill();
				StartDieAnim();
			}
		}
		
		override protected function OnHover(): void
		{
			_image.filter = Filters.highlight2Filter;
		}
		
		override protected function OnStopHover():void 
		{
			_image.filter = null;
		}
		
		override public function OnCursorCollision(): void 
		{			

		}		
		
		override protected function OnAddedToStage(event: Event): void
		{
			super.OnAddedToStage(event);			
			addChild(_exclamationMark);
			addChild(_particles);
		}
		
		override protected function StartDieAnim(): void 
		{
			super.StartDieAnim();
			HSounds.PlaySound("QuestReceived");
			_image.visible = false;
			_exclamationMark.visible = false;
			_particles.CreateParticles(15);
			World.gWorld.dispatchEventWith("newQuest", false, _quest);
		}
		
		override public function OnEnterFrame(event: EnterFrameEvent): void
		{
			super.OnEnterFrame(event);
			_particles.OnGlobalEnterFrame(event);
		}
		
	}

}