package view.gameviewelements 
{
	import d_hentity.HEntity;
	import feathers.controls.Button;
	import feathers.events.FeathersEventType;
	import feathers.text.BitmapFontTextFormat;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.ITextRenderer;
	import starling.display.Quad;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.filters.ColorMatrixFilter;
	import view.BitmappedText;
	import view.gameviewelements.MessageWindow.MessageWindow;
	import system.HCursor;
	import starling.events.TouchPhase;
	import starling.events.Touch;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class PanelButton extends HEntity 
	{
		private var _touchInited: Boolean = false;
		private var _hovered: Boolean = false;
		private var _messageWindow: MessageWindow = null;
		protected var _button: HButton;
		protected var _emptyButton: Boolean = true;
		//public var showTooltipOnHover: Boolean = false;
		//public var tooltipText: String = "";
		
		private var _activeBitmappedLabel: BitmappedText = null;
		private var _inactiveBitmappedLabel: BitmappedText = null;
		private var _hoveredActiveBitmapLabel: BitmappedText = null;
		private var _hoveredInactiveBitmapLabel: BitmappedText = null;
		
		private var _useStandardBitmapLabel: Boolean = false;
		
		static private var activeTextFormat: BitmapFontTextFormat = new BitmapFontTextFormat("bbold11", 11, 0x7c3a12);
		static private var inactiveTextFormat: BitmapFontTextFormat = new BitmapFontTextFormat("bbold11", 11, 0x6d5548);
		
		public function get button(): HButton 
		{
			return _button;
		}
		
		public function SetActiveLabel(text: String, font: String, color: uint):void 
		{
			_activeBitmappedLabel = new BitmappedText;
			_activeBitmappedLabel.addEventListener("feathersResize", OnFeathersResize);
			_activeBitmappedLabel.Init(text, font, color);
			_activeBitmappedLabel.touchable = false;
			if (!_emptyButton)
			{
				_activeBitmappedLabel.SetCenter(_button.defaultSkin.width / 2, _button.defaultSkin.height / 2);
				_activeBitmappedLabel.y -= 1;
			}					
		}
		
		public function SetInactiveLabel(text: String, font: String, color: uint):void 
		{
			_inactiveBitmappedLabel = new BitmappedText;
			_inactiveBitmappedLabel.addEventListener("feathersResize", OnFeathersResize);
			_inactiveBitmappedLabel.Init(text, font, color);
			_inactiveBitmappedLabel.touchable = false;
			if (!_emptyButton)
			{
				_inactiveBitmappedLabel.SetCenter(_button.defaultSkin.width / 2, _button.defaultSkin.height / 2);
				_inactiveBitmappedLabel.y -= 1;
				
			}
			_inactiveBitmappedLabel.visible = false;			
		}
		
		public function SetHoveredActiveBitmapLabel(text: String, font: String, color: uint, filter: ColorMatrixFilter = null):void 
		{
			_hoveredActiveBitmapLabel = new BitmappedText;
			_hoveredActiveBitmapLabel.addEventListener("feathersResize", OnFeathersResize);
			_hoveredActiveBitmapLabel.Init(text, font, color);
			_hoveredActiveBitmapLabel.touchable = false;
			if (!_emptyButton)
			{
				_hoveredActiveBitmapLabel.SetCenter(_button.defaultSkin.width / 2, _button.defaultSkin.height / 2);				
			}
			_hoveredActiveBitmapLabel.visible = false;
			if (filter)
				_hoveredActiveBitmapLabel.filter = filter;
		}
		
		public function SetHoveredInactiveBitmapLabel(text: String, font: String, color: uint, filter: ColorMatrixFilter = null):void 
		{
			_hoveredInactiveBitmapLabel = new BitmappedText;
			_hoveredInactiveBitmapLabel.addEventListener("feathersResize", OnFeathersResize);
			_hoveredInactiveBitmapLabel.Init(text, font, color);
			_hoveredInactiveBitmapLabel.touchable = false;
			if (!_emptyButton)
			{
				_hoveredInactiveBitmapLabel.SetCenter(_button.defaultSkin.width / 2, _button.defaultSkin.height / 2);
				
			}
			_hoveredInactiveBitmapLabel.visible = false;
			if (filter)
				_hoveredInactiveBitmapLabel.filter = filter;			
		}		
		
		public function SetActive(active: Boolean):void 
		{
			_activeBitmappedLabel.visible = active;
			_inactiveBitmappedLabel.visible = !active;
		}			
		
		public function PanelButton(useStandardBitmapLabel: Boolean = false) 
		{
			super(false,true);

			_button = new HButton;
			_button.addEventListener(FeathersEventType.RESIZE, OnFeathersResize);
			//_button.defaultSkin = new Quad(80, 30);
			//_button.defaultSkin.alpha = 0;
			_useStandardBitmapLabel = useStandardBitmapLabel;
			if (_useStandardBitmapLabel)
			{
				_button.labelFactory = function (): ITextRenderer 
				{
					var textRenderer: BitmapFontTextRenderer = new BitmapFontTextRenderer();
					textRenderer.textFormat = new BitmapFontTextFormat("bbold11buttons", 11, 0x7c3a12);
					//textRenderer.embedFonts = true;
					return textRenderer;
				}
				_button.disabledLabelProperties.textFormat = inactiveTextFormat;
				_button.defaultLabelProperties.textFormat = activeTextFormat;
			}
			_button.addEventListener(Event.TRIGGERED, button_triggered);
			//_button.addEventListener(TouchEvent.TOUCH, OnButtonTouch);
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		override protected function OnHover():void 
		{
			if (_activeBitmappedLabel.visible && _hoveredActiveBitmapLabel)
			{
				_activeBitmappedLabel.visible = false;
				_hoveredActiveBitmapLabel.visible = true;
			}
			if (_inactiveBitmappedLabel.visible && _hoveredInactiveBitmapLabel)
			{
				_inactiveBitmappedLabel.visible = false;
				_hoveredInactiveBitmapLabel.visible = true;				
			}				
		}
		
		override protected function OnStopHover():void 
		{
			if (_hoveredActiveBitmapLabel && _hoveredActiveBitmapLabel.visible)
			{
				_activeBitmappedLabel.visible = true;
				_hoveredActiveBitmapLabel.visible = false;
			}
			if (_hoveredInactiveBitmapLabel && _hoveredInactiveBitmapLabel.visible)
			{
				_inactiveBitmappedLabel.visible = true;
				_hoveredInactiveBitmapLabel.visible = false;				
			}		
		}
		
		private function OnFeathersResize(e:Event):void 
		{
			if (_emptyButton && _button.defaultSkin)
			{
				_button.defaultSkin.width = _activeBitmappedLabel.width;
				_button.defaultSkin.height = _activeBitmappedLabel.height;
			}				
			dispatchEventWith(FeathersEventType.RESIZE);
		}
		
		private function button_triggered(e:Event):void 
		{
			dispatchEventWith(Event.TRIGGERED);
		}
				
		public function set bitmaplabel(text: String): void 
		{
			if (_activeBitmappedLabel != null)
			{
				_activeBitmappedLabel.ChangeText(text);
				_inactiveBitmappedLabel.ChangeText(text);
			}
			else
			{							
				_activeBitmappedLabel = new BitmappedText;
				_activeBitmappedLabel.addEventListener("feathersResize", OnFeathersResize);
				_activeBitmappedLabel.Init(text, "bbold11", 0x7c3a12, 0, 0, "center", "center");				
				_activeBitmappedLabel.SetCenter(_button.defaultSkin.width / 2, _button.defaultSkin.height / 2);
				//_activeBitmappedLabel.y -= 100;
				_activeBitmappedLabel.touchable = false;
								
				_inactiveBitmappedLabel = new BitmappedText;
				_inactiveBitmappedLabel.addEventListener("feathersResize", OnFeathersResize);
				_inactiveBitmappedLabel.Init(text, "bbold11", 0x6d5548, 0, 0, "center", "center");										
				_inactiveBitmappedLabel.SetCenter(_button.defaultSkin.width / 2, _button.defaultSkin.height / 2);
				//_inactiveBitmappedLabel.y -= 100;
				_inactiveBitmappedLabel.touchable = false;
				
				
				if (stage)
				{
					addChild(_activeBitmappedLabel);
					addChild(_inactiveBitmappedLabel);									
				}
					
			}
		}
		
		public function get bitmaplabel(): String
		{
			return _activeBitmappedLabel.currentText;
		}
		
		private function addedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			addChild(_button);			
			if (!_useStandardBitmapLabel && _activeBitmappedLabel)
			{
				addChild(_activeBitmappedLabel);
				addChild(_inactiveBitmappedLabel);
			}
			if (_emptyButton)
			{
				_button.defaultSkin = new Quad(_activeBitmappedLabel.width-BitmappedText.ADDITIONAL_WIDTH, _activeBitmappedLabel.height-BitmappedText.ADDITIONAL_HEIGHT);
				_button.defaultSkin.alpha = 0;
			}			
		}
		
		public function set label(text: String): void
		{
			if (!_useStandardBitmapLabel)
				bitmaplabel = text;
			else
				_button.label = text;
		}		
		
		public function get label():String 
		{
			if (_activeBitmappedLabel)
				return _activeBitmappedLabel.currentText;
			else 
				return _button.label;
		}
		
		//override public function get width(): Number 
		//{
			//return _button.width;
		//}
		//
		//override public function get height(): Number 
		//{
			//return _button.height;
		//}
				
		//protected function OnTouch(event: TouchEvent): void
		//{
			//var touch: Touch = event.getTouch(this);
			//if (touch != null)
			//{
				//if (touch.phase === TouchPhase.HOVER)
				//{
					//_hovered = true;
					//OnHover();
				//}
				//else if (touch.phase === TouchPhase.BEGAN)
				//{
					//OnTouchPhaseBegan();
				//}
				//else if (touch.phase === TouchPhase.ENDED)
				//{
					//OnTouchPhaseEnded();
				//}
			//}
			//else
			//{
				//if (_hovered)
				//{
					//_hovered = false;
					//OnStopHover();
				//}
			//}
		//}
		
		public function SetEnabled(enabled: Boolean):void 
		{
			if (!enabled)
			{
				_activeBitmappedLabel.visible = false;
				_inactiveBitmappedLabel.visible = true;
				OnStopHover();
			}
			else
			{
				_activeBitmappedLabel.visible = true;
				_inactiveBitmappedLabel.visible = false;
			}
			_button.isEnabled = enabled;
		}
		
		//protected function OnHover():void 
		//{
			//if (showTooltipOnHover)
			//{
				//if (_messageWindow === null)
				//{
					//_messageWindow = new MessageWindow(tooltipText);
					//_messageWindow.alpha = 0.9;
					//World.gWorld.addChild(_messageWindow);
				//}
				//_messageWindow.x = HCursor.GetCursor().x - _messageWindow.width / 2;
				//_messageWindow.y = HCursor.GetCursor().y - _messageWindow.height - 4;				
			//}
		//}			
		
	}

}