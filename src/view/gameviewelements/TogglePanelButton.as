package view.gameviewelements 
{
	import feathers.controls.Button;
	import feathers.text.BitmapFontTextFormat;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.ITextRenderer;
	import starling.events.TouchEvent;
	import view.gameviewelements.MessageWindow.MessageWindow;
	import system.HCursor;
	import starling.events.TouchPhase;
	import starling.events.Touch;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class TogglePanelButton extends HToggleButton 
	{
		
		public function OldPanelButton() 
		{
			super();
			//useHandCursor = true;
			this.labelFactory = function (): ITextRenderer 
			{
				var textRenderer: BitmapFontTextRenderer = new BitmapFontTextRenderer();
				textRenderer.textFormat = new BitmapFontTextFormat("bbold11", 11, 0x7c3a12);
				textRenderer.useSeparateBatch = true;
				//textRenderer.embedFonts = true;
				return textRenderer;
			}		
		}		
			
		public function SetEnabled(enable: Boolean):void 
		{
			if (!enable && _hovered)
			{
				hoverChecker.OnStopHover();
			}
			isEnabled = enable;
		}
	}
}