package view.gameviewelements 
{
	import feathers.controls.Button;
	import flash.geom.Point;
	import logic.Quest;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import system.Analytics;
	import view.GameView;
	import view.gameviewelements.BaseVerticalScrollContainer;
	import view.gameviewelements.misc.EmptyTipHolder;
	import view.gameviewelements.ScrollContainerItem;
	import view.gameviewelements.HeroPanelItem;
	import starling.utils.Color;
	import starling.text.TextFieldAutoSize;
	import hevents.*;
	import logic.Dungeon;
	import view.gameviewelements.skills_panel.SkillsList;
	import view.gameviewelements.skills_panel.SkillsPanel;
	import view.gameviewelements.HButton;
	import view.InvisibleTooltipHolder;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	import com.greensock.plugins.*;
	
	import d_hentity.HEntity;
	/**
	 * ...
	 * @author Aikei
	 */
	
	public class Window extends HEntity 
	{
		//! may be set in script
		public static var WINDOW_X: int = 380;		
		public static var WINDOW_Y: int = 0;		
		
		public static var DUNGEON_NAME_X: Number = 268;
		public static var DUNGEON_NAME_Y: Number = 568;
		
		public static var GLORY_VALUE_TEXT_X: Number = 406;
		public static var GLORY_VALUE_TEXT_Y: Number = 90;
		
		public static var EXHAUSTION_IMAGE_X: Number = 105;
		public static var EXHAUSTION_IMAGE_Y: Number = 580;
		
		public static var CHANGE_SQUAD_NAME_BUTTON_PLUS_X: Number = 0;
		public static var CHANGE_SQUAD_NAME_BUTTON_PLUS_Y: Number = 0;
		
		public static var DUNGEON_BUTTON_VFX_X_PLUS: Number = 0;
		public static var DUNGEON_BUTTON_VFX_Y_PLUS: Number = 0;	
		
		public static var COINS_TEXT_X: Number = 385;
		public static var COINS_TEXT_Y: Number = 160;
		
		public static var NUMBER_OF_MONSTERS_TEXT_X: Number = 425;
		public static var NUMBER_OF_MONSTERS_TEXT_Y: Number = 570;
		
		public static var GOLD_TIP_X: Number = 350;
		public static var GOLD_TIP_Y: Number = 157;	
		public static var GOLD_TIP_WIDTH: Number = 32;
		public static var GOLD_TIP_HEIGHT: Number = 35;
		
		public static var GLORY_TIP_X: Number = 374;
		public static var GLORY_TIP_Y: Number = 58;		
		public static var GLORY_TIP_WIDTH: Number = 69;
		public static var GLORY_TIP_HEIGHT: Number = 84;
		
		public static var GOLD_LIMIT_TEXT_CENTER_X: Number = 98;
		public static var GOLD_LIMIT_TEXT_CENTER_Y: Number = 581;
		
		public static var EXHAUSTION_BAR_X: Number = 65;
		public static var EXHAUSTION_BAR_Y: Number = 571;
		
		protected var _backGround: starling.display.Quad = new starling.display.Quad(424, 340, 0x000000);
		private var _exhaustionImage: Image = new Image(Assets.interfaceAtlas.getTexture("exhausted_4"));
		private var _exhaustionHolder: HEntity = new HEntity;
		private var _exhaustionNumber: uint = 4;
		
		private var _image: Image;
		private var _exhaustionBar: Image;
		private var _scrollBg: Image = new Image(Assets.interfaceAtlas.getTexture("scroll"));
		
		private var _heroPanel: HeroPanel = new HeroPanel;
		private var _weaponsPanel: WeaponsPanel = new WeaponsPanel;
		//private var _skillsPanel: SkillsPanel = new SkillsPanel;
		private var _skillsList: SkillsList = new SkillsList;
		
		private var _panelPickButtons: PanelPickButtons = new PanelPickButtons;

		private var _dungeonsButton: HButton;
		private var _levelPanel: LevelPanel = new LevelPanel;
		private var _currentDuneonName: String;
		private var _goldTipHolder: EmptyTipHolder;
		private var _gloryTipHolder: EmptyTipHolder;
		private var _topTextHolder: HEntity = new HEntity;
		
		private var _memberElements: Vector.<HEntity> = new Vector.<HEntity>;
		
		//private var _exhaustionText: ComplexBatchedBitmapLabel;		
		//private var _coinsText: BatchedBitmapLabel;		
		//private var _numberOfMonstersText: BatchedBitmapLabel;
		//private var _dungeonNameLevelText: BatchedBitmapLabel;		
		//private var _groupCommentText: BatchedBitmapLabel;
		//private var _groupNameText: BatchedBitmapLabel;
		//private var _leaderCommentText: BatchedBitmapLabel;
		//private var _leaderNameText: BatchedBitmapLabel;		
		//private var _gloryCommentText: BatchedBitmapLabel;
		//private var _gloryValueText: BatchedBitmapLabel;
		
		private var _exhaustionText: HText;
		private var _coinsText: HText;		
		private var _numberOfMonstersText: HText;
		private var _dungeonNameLevelText: HText;		
		private var _groupCommentText: HText;
		private var _groupNameText: HText;
		private var _leaderCommentText: HText;
		private var _leaderNameText: HText;		
		private var _gloryValueText: HText;		
		private var _buttonVfx: HEntity;
		private var _goldLimitText: HText;
		private var _goldLimit: BigInt;
		private var _changeGroupNameButton: HButton = new HButton;
		
		
		private static var _coinCounterCenter: Point = new Point(368, 174);
		
		private var _monstersKilledTipHolder: InvisibleTooltipHolder;
		public static var MONSTERS_KILLED_TIP_X: Number = 397;
		public static var MONSTERS_KILLED_TIP_Y: Number = 570;
		public static var MONSTERS_KILLED_TIP_WIDTH: Number = 72;
		public static var MONSTERS_KILLED_TIP_HEIGHT: Number = 23;				
		
		private var _dungeonsButtonTip: InvisibleTooltipHolder;
		public static var DUNGEONS_BUTTON_TIP_X: Number = 224;
		public static var DUNGEONS_BUTTON_TIP_Y: Number = 510;
		public static var DUNGEONS_BUTTON_TIP_WIDTH: Number = 61;
		public static var DUNGEONS_BUTTON_TIP_HEIGHT: Number = 64;	
		
		//private var _skillsAvailableIcon: Image = new Image(Assets.interfaceAtlas.getTexture("skills_available_icon"));
		//private var _skillsAvailableText: HText = new HText("0", "bbold12o", -1, 0x4b0000);		
		
		
		public function Window() 
		{
			_dungeonsButtonTip = new InvisibleTooltipHolder(DUNGEONS_BUTTON_TIP_X, DUNGEONS_BUTTON_TIP_Y, DUNGEONS_BUTTON_TIP_WIDTH, DUNGEONS_BUTTON_TIP_HEIGHT, "");
			_dungeonsButtonTip.tooltipTitleText = "TitleExplainDungeonsButton";
			_monstersKilledTipHolder = new InvisibleTooltipHolder(MONSTERS_KILLED_TIP_X, MONSTERS_KILLED_TIP_Y, MONSTERS_KILLED_TIP_WIDTH, MONSTERS_KILLED_TIP_HEIGHT, "");
					
			_goldTipHolder = new EmptyTipHolder(Language.GetOrdersHelp(), GOLD_TIP_WIDTH, GOLD_TIP_HEIGHT, false);
			_goldTipHolder.tooltipTitleText = "TitleBigNumbers";
			_goldTipHolder.x = GOLD_TIP_X;
			_goldTipHolder.y = GOLD_TIP_Y;
			_memberElements.push(_goldTipHolder);
			
			_gloryTipHolder = new EmptyTipHolder("ExplainGlory", GLORY_TIP_WIDTH, GLORY_TIP_HEIGHT);
			_gloryTipHolder.x = GLORY_TIP_X;
			_gloryTipHolder.y = GLORY_TIP_Y;
			_memberElements.push(_gloryTipHolder);
			
			_exhaustionText = new HText(Language.GetString("Exhausted"),"bbold13", -1);
			_groupCommentText = new HText(Language.GetString("Group") + ":", "bbold14", -1, 0x764e38);
			_groupNameText = new HText("","bbold18", -1, 0x8b160d);			
			_leaderCommentText = new HText(Language.GetString("Leader") + ": ","bbold14", -1, 0x764e38);
			_leaderNameText = new HText("","bbold14", -1, 0x8b160d);		
			//_gloryCommentText = new HText(Language.GetString("Glory"),"bregular14", -1, 0xffffff);
			_gloryValueText = new HText("", "bbold16", -1, 0xffffff);	
			
			_goldLimitText = new HText("", "bbold14", -1);
			
			_scrollBg.touchable = false;			
			
			x = WINDOW_X;
			y = WINDOW_Y;
			_scrollBg.x = 446;
			_scrollBg.y = 200;
			//_backGround = new Image(Assets.interfaceAtlas.getTexture("group_window_bg"));
			_backGround.touchable = false;
			_backGround.x = 44;
			_backGround.y = 198;
			_image = new Image(Assets.interfaceAtlas.getTexture("group_window"));
			_image.touchable = false;
			
			_exhaustionBar = new Image(Assets.interfaceAtlas.getTexture("exhaustion_bar_100"));
			_exhaustionBar.touchable = false;
			_exhaustionBar.x = EXHAUSTION_BAR_X;
			_exhaustionBar.y = EXHAUSTION_BAR_Y;
			
			_panelPickButtons.weaponsButton.addEventListener(Event.TRIGGERED, OnWeaponsButtonTriggered);
			_panelPickButtons.heroesButton.addEventListener(Event.TRIGGERED, OnHeroesButtonTriggered);
			_panelPickButtons.skillsButton.addEventListener(Event.TRIGGERED, OnSkillsButtonTriggered);
			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
						
			//_coinsText = new BatchedBitmapLabel("bbold16", 16, Color.WHITE);
			_coinsText = new HText("","bbold16", -1, Color.WHITE);
			_coinsText.x = COINS_TEXT_X;
			_coinsText.y = COINS_TEXT_Y;
			_coinsText.touchable = false;
			
			//_numberOfMonstersText = new BatchedBitmapLabel("bbold15", 15, 0xa7adb5);
			//_numberOfMonstersText.text = "0 / " + Dungeon.MONSTERS_MAX.toString();
			_numberOfMonstersText = new HText("0 / " + Dungeon.MONSTERS_MAX.toString(), "bbold15", -1, 0xa7adb5);
			_numberOfMonstersText.x = NUMBER_OF_MONSTERS_TEXT_X;
			_numberOfMonstersText.y = NUMBER_OF_MONSTERS_TEXT_Y;
			_numberOfMonstersText.touchable = false;
			
			_dungeonsButton = new HButton;
			_dungeonsButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("dungeons_button"));
			_dungeonsButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("dungeons_button_hover"));
			_dungeonsButton.x = 233;
			_dungeonsButton.y = 519;
			_dungeonsButton.validate();
			_dungeonsButton.addEventListener(Event.TRIGGERED, OnDungeonsButtonTriggered);
			_dungeonsButton.hoverChecker.showTooltipOnHover = true;
			_dungeonsButton.hoverChecker.tooltipTitleText = "TitleExplainDungeonsButton";
			
			_buttonVfx = new HEntity(true);
			var image: Image = new Image(Assets.interfaceAtlas.getTexture("dungeon_button_vfx"));
			_buttonVfx.addChild(image);
			_buttonVfx.visible = false;
			_buttonVfx.x = _dungeonsButton.x + _dungeonsButton.width / 2 + DUNGEON_BUTTON_VFX_X_PLUS;
			_buttonVfx.y = _dungeonsButton.y + _dungeonsButton.height / 2 + DUNGEON_BUTTON_VFX_Y_PLUS;
			_buttonVfx.alignPivot();

			//_dungeonNameLevelText = new BatchedBitmapLabel("bbold15", 15, 0xa7adb5);
			_dungeonNameLevelText = new HText("","bbold15", -1, 0xa7adb5);
			_dungeonNameLevelText.x = DUNGEON_NAME_X;
			_dungeonNameLevelText.y = DUNGEON_NAME_Y;
			_dungeonNameLevelText.touchable = false;
					
			_topTextHolder.addChild(_coinsText);			
			_topTextHolder.addChild(_numberOfMonstersText);
			_topTextHolder.addChild(_dungeonNameLevelText);	
			
			//_gloryCommentText.x = Math.round(GLORY_VALUE_TEXT_X - _gloryCommentText.textField.width / 2);
			//_gloryCommentText.y = 70;
			//_topTextHolder.addChild(_gloryCommentText);
			
			_gloryValueText.x = Math.round(GLORY_VALUE_TEXT_X - _gloryValueText.width / 2);
			_gloryValueText.y = GLORY_VALUE_TEXT_Y;
			_gloryValueText.touchable = false;
			_topTextHolder.addChild(_gloryValueText);
			
			_groupCommentText.x = 62;
			_groupCommentText.y = 72;
			_groupCommentText.touchable = false;
			_topTextHolder.addChild(_groupCommentText);
			
			_groupNameText.x = 62;
			_groupNameText.y = _groupCommentText.y + 16;
			_groupNameText.touchable = false;
			_topTextHolder.addChild(_groupNameText);
			
			_changeGroupNameButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("rename_icon"));
			_changeGroupNameButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("rename_icon"));
			_changeGroupNameButton.hoverSkin.filter = Filters.highlightFilter;
			_changeGroupNameButton.addEventListener(Event.TRIGGERED, OnChangeGroupNameButtonTriggered);
			_changeGroupNameButton.validate();
			
			_leaderCommentText.x = 62;
			_leaderCommentText.y = _groupNameText.y + 26;
			_leaderCommentText.touchable = false;
			_topTextHolder.addChild(_leaderCommentText);
			
			_leaderNameText.x = _leaderCommentText.x + _leaderCommentText.width;
			_leaderNameText.y = _leaderCommentText.y;
			_leaderNameText.touchable = false;
			_topTextHolder.addChild(_leaderNameText);
			
			Misc.CenterObject(_exhaustionImage, EXHAUSTION_IMAGE_X, EXHAUSTION_IMAGE_Y);
			_exhaustionHolder.addChild(_exhaustionImage);
			_exhaustionImage.visible = false;
			
			_exhaustionHolder.InitTouch();
			_exhaustionHolder.showTooltipOnHover = true;
			_exhaustionHolder.tooltipText = "ExplainExhaustion";
			
			_exhaustionText.x = EXHAUSTION_IMAGE_X - _exhaustionText.width / 2;
			_exhaustionText.y = EXHAUSTION_IMAGE_Y - _exhaustionText.height / 2;
			Misc.RoundPosition(_exhaustionText);
			_exhaustionText.visible = false;
			_exhaustionText.touchable = false;
			_exhaustionText.InitTouch();
			_exhaustionText.showTooltipOnHover = true;
			_exhaustionText.tooltipText = "ExplainExhausted";
			
			World.gWorld.addEventListener("N_CoinsNow", OnCoinsNow);
			World.gWorld.addEventListener("N_CoinsReceived", OnCoinsReceived);
			World.gWorld.addEventListener("N_NewMonster", OnNewMonster);
			World.gWorld.addEventListener(L_HEventMonsterKilled.H_EVENT_MONSTER_KILLED, OnMonsterKilled);
			World.gWorld.addEventListener(L_HEventDungeonLevelSet.L_HEVENT_DUNGEON_LEVEL_SET, OnDungeonLevelSet);
			World.gWorld.addEventListener(HEventDungeonChanged.H_EVENT_DUNGEON_CHANGED, OnDungeonChanged);
			World.gWorld.addEventListener(HEventNextLevel.HEVENT_NEXT_LEVEL, OnNextLevel);
			World.gWorld.addEventListener("changeLevel", OnChangeLevel);
			World.gWorld.addEventListener("N_GloryChanged", OnGloryChanged);
			World.gWorld.addEventListener("newSkillPackReceived", OnNewSkillPackReceived);
			World.gWorld.addEventListener("N_DungeonsData", OnDungeonsData);
			World.gWorld.addEventListener("squadNameChanged", OnSquadNameChanged);
			World.gWorld.addEventListener("questNotInThisDungeon", OnQuestNotInThisDungeon);
			//World.gWorld.addEventListener("enteredQuestDungeon", OnEnteredQuestDungeon);
			Analytics.TrackPage("/game_screen/main_window");
		}
		
		private function OnDungeonChanged(e:HEventDungeonChanged):void 
		{
			_goldLimit = e.goldLimit;
			UpdateGoldLimit();
		}
		
		private function OnCoinsReceived(e:Event):void 
		{
			var received: BigInt = BigInt(e.data);
			if (BigInt.greater(_goldLimit, received))
				_goldLimit = BigInt.sub(_goldLimit, received);
			else
				_goldLimit = Misc.BIG_INT_ZERO;
			UpdateGoldLimit();
		}
		
		//private function OnEnteredQuestDungeon(e:Event):void 
		//{
			//ShowDungeonButtonEffect(false);
		//}
		
		private function OnQuestNotInThisDungeon(e:Event):void 
		{
			if (World.SHOW_TUTORIAL)
				ShowDungeonButtonEffect(true);
		}
		
		public function ShowDungeonButtonEffect(show: Boolean):void 
		{
			if (show)
			{
				_buttonVfx.visible = true;
				_buttonVfx.ApplyRotation(Misc.PI);
			}
			else
			{
				_buttonVfx.visible = false;
				_buttonVfx.StopMyRotation();
			}
		}
		
		private function OnSquadNameChanged(e:Event):void 
		{
			SetGroupName(e.data as String);
		}
		
		private function OnChangeGroupNameButtonTriggered(e:Event):void 
		{		
			World.gWorld.dispatchEventWith("changeGroupNameButtonTriggered",false,_groupNameText.textField.text);
		}
		
		//private function OnSkillsUnpicked(e:Event):void 
		//{
			//_skillsAvailableText.textField.text = String(e.data);
			//_skillsAvailableText.x = _skillsAvailableIcon.x + _skillsAvailableIcon.width / 2 - _skillsAvailableText.textField.width / 2;
		//}
		
		public function OnGlobalEnterFrame(event: EnterFrameEvent):void 
		{
			if (_skillsList)
				_skillsList.OnGlobalEnterFrame(event);
			for (var i: int = 0; i < _memberElements.length; i++)
			{
				_memberElements[i].OnEnterFrame(event);
			}
		}
		
		public function OnGlobalTouch(event: TouchEvent):void 
		{
			if (_skillsList)
				_skillsList.OnGlobalTouch(event);
			//if (_levelPanel)
				//_levelPanel.OnTouch(event);
			for (var i: int = 0; i < _memberElements.length; i++)
			{
				_memberElements[i].OnTouch(event);
			}
		}		
		
		public function GetCoinCounterCenter(): Point 
		{
			var p: Point = localToGlobal(_coinCounterCenter);
			return p;
		}
		
		public function Init(game: Object):void 
		{
			SetGroupName(Language.ProcessSquadName(game.groupName));
			_leaderNameText.text = World.gWorld.socialProfile.GetName();
			SetGlory(BigInt.shStr2BigInt(game.glory));	
			_goldLimit = BigInt.shStr2BigInt(game.goldLimit);
			UpdateGoldLimit();
			_heroPanel.Init(game);
			_weaponsPanel.Init(game);
			_skillsList.Init(game);
		}		
		
		public function SetGroupName(name: String):void 
		{
			if (name == "Default")
				_groupNameText.text = Language.GetString("DefaultGroupName")
			else
				_groupNameText.text = name;
			_changeGroupNameButton.x = Math.round(_groupNameText.x + _groupNameText.textField.width+4) + CHANGE_SQUAD_NAME_BUTTON_PLUS_X;
			_changeGroupNameButton.y = Math.round(_groupNameText.y + _groupNameText.textField.height/2 - _changeGroupNameButton.height/2) + CHANGE_SQUAD_NAME_BUTTON_PLUS_Y;			
		}
		
		protected function SetGlory(glory: BigInt): void 
		{
			_gloryValueText.text = BigInt.bigint2HString(glory);
			_gloryValueText.x = GLORY_VALUE_TEXT_X - _gloryValueText.width / 2;
		}
		
		protected function OnDungeonLevelSet(event: L_HEventDungeonLevelSet): void 
		{
			_currentDuneonName = event.dungeon.name;			
			_numberOfMonstersText.text = event.dungeon.monstersKilled.toString() + " / " + Dungeon.MONSTERS_MAX.toString();
			_monstersKilledTipHolder.tooltipTitleText = event.dungeon.monstersKilled.toString() + " / " + Dungeon.MONSTERS_MAX.toString();
			_monstersKilledTipHolder.tooltipText = Language.GetString("ExplainMonstersKilled");
			_monstersKilledTipHolder.tooltipText = _monstersKilledTipHolder.tooltipText.replace("$n", String(Dungeon.MONSTERS_MAX - event.dungeon.monstersKilled));			
			_dungeonNameLevelText.text = event.dungeon.name; // + " (" + Language.GetString("Lv") + " " + event.dungeon.level.toString() + ")";
			_dungeonNameLevelText.x = DUNGEON_NAME_X - _dungeonNameLevelText.width / 2;
			Misc.RoundPosition(_dungeonNameLevelText);			
			//_dungeonNameLevelText.alignPivot();
		}
		
		protected function UpdateGoldLimit():void 
		{
			_goldLimitText.text = BigInt.bigint2HString(_goldLimit);
			_goldLimitText.x = GOLD_LIMIT_TEXT_CENTER_X - _goldLimitText.textField.width / 2;
			_goldLimitText.y = GOLD_LIMIT_TEXT_CENTER_Y - _goldLimitText.textField.height / 2;
			Misc.RoundPosition(_goldLimitText);			
		}
						
		protected function OnNextLevel(event: HEventNextLevel): void 
		{			
			_dungeonNameLevelText.text = _currentDuneonName; // + " (" + Language.GetString("Lv") + " " + event.level.toString() + ")";
			_dungeonNameLevelText.x = DUNGEON_NAME_X - _dungeonNameLevelText.width / 2;
			Misc.RoundPosition(_dungeonNameLevelText);
			_goldLimit = event.goldLimit;
			UpdateGoldLimit();
		}
		
		protected function OnChangeLevel(event: Event): void 
		{
			_numberOfMonstersText.text = event.data.monstersKilled.toString() + " / " + Dungeon.MONSTERS_MAX.toString();
			_dungeonNameLevelText.text = _currentDuneonName; // + " (" + Language.GetString("Lv") + " " + event.level.toString() + ")";
			_dungeonNameLevelText.x = DUNGEON_NAME_X - _dungeonNameLevelText.width / 2;
			Misc.RoundPosition(_dungeonNameLevelText);
			_goldLimit = event.data.goldLimit;
			UpdateGoldLimit();
		}		
		
		protected function OnNewSkillPackReceived(event: Event): void 
		{
			_skillsList.AddSkills(event.data);
			//_skillsPanel.AddSkills(event.data);
		}
		
		protected function OnWeaponsButtonTriggered(event: Event): void 
		{
			_weaponsPanel.visible = true;
			_heroPanel.visible = false;
			//_skillsPanel.visible = false;
			_skillsList.visible = false;
		}
		
		protected function OnSkillsButtonTriggered(event: Event):void 
		{
			_weaponsPanel.visible = false;
			_heroPanel.visible = false;
			//_skillsPanel.visible = true;
			_skillsList.visible = true;
		} 
		
		protected function OnHeroesButtonTriggered(event: Event): void 
		{
			_weaponsPanel.visible = false;
			_heroPanel.visible = true;
			//_skillsPanel.visible = false;
			_skillsList.visible = false;
		}				
		
		protected function OnCoinsNow(event: Event): void 
		{
			_coinsText.text = BigInt.bigint2HString(event.data as BigInt);
		}
		
		protected function OnDungeonsButtonTriggered(event: Event): void 
		{
			if (World.DUNGEONS_LOADED)
			{
				if (World.SHOW_TUTORIAL && _buttonVfx.visible)
				{
					ShowDungeonButtonEffect(false);
					World.gameController.TutorialDone();
				}
				World.gWorld.dispatchEventWith("groupWindowDungeonsButtonPressed");
			}
			else
			{
				GameView.gView.ShowMessageWindow(Language.GetString("DungeonsNotLoaded"),5);
			}
		}
		
		protected function OnNewMonster(event: Event):void 
		{
			if (event.data.monstersKilled === 0)
			{
				_monstersKilledTipHolder.tooltipTitleText = "0 / " + Dungeon.MONSTERS_MAX.toString();
				_numberOfMonstersText.text = "0 / " + Dungeon.MONSTERS_MAX.toString();
				_monstersKilledTipHolder.tooltipText = Language.GetString("ExplainMonstersKilled");
				_monstersKilledTipHolder.tooltipText = _monstersKilledTipHolder.tooltipText.replace("$n", Dungeon.MONSTERS_MAX.toString());
			}
		}
		
		protected function OnMonsterKilled(event: L_HEventMonsterKilled): void 
		{
			_numberOfMonstersText.text = event.monstersKilledTotal.toString() + " / " + Dungeon.MONSTERS_MAX.toString();
			_monstersKilledTipHolder.tooltipTitleText = event.monstersKilledTotal.toString() + " / " + Dungeon.MONSTERS_MAX.toString();
			_monstersKilledTipHolder.tooltipText = Language.GetString("ExplainMonstersKilled");
			_monstersKilledTipHolder.tooltipText = _monstersKilledTipHolder.tooltipText.replace("$n", String(Dungeon.MONSTERS_MAX - event.monstersKilledTotal));
		}
		
		protected function OnGloryChanged(event: Event):void 
		{
			SetGlory(event.data as BigInt);
		}
				
		protected function OnDungeonsData(e: Event):void 
		{
			for (var i: int = 0; i < e.data.d.length; i++)
			{
				if (e.data.sh[i].current)
				{
					var newExhaustion: uint;
					if (e.data.d[i].exhaustion < 25)
						newExhaustion = 4;
					else if (e.data.d[i].exhaustion < 50)
						newExhaustion = 3;
					else if (e.data.d[i].exhaustion < 75)
						newExhaustion = 2;
					else if (e.data.d[i] < 100)
						newExhaustion = 1;
					else
						newExhaustion = 0;
					if (_exhaustionNumber != newExhaustion)
					{
						if (_exhaustionNumber == 0)
							_exhaustionText.visible = false;
						_exhaustionNumber = newExhaustion;
						if (newExhaustion == 0)
						{
							_exhaustionText.visible = true;
							_exhaustionImage.visible = false;
						}
						else
						{
							_exhaustionImage.texture = Assets.interfaceAtlas.getTexture("exhausted_" + _exhaustionNumber);
						}
					}
					break;
				}
			}
		}
		
		private function OnAddedToStage(event: Event): void
		{			
			addChild(_backGround);			
			addChild(_scrollBg);
			addChild(_heroPanel);
			addChild(_weaponsPanel);
			addChild(_skillsList);
			
			//addChild(_skillsPanel);
			addChild(_image);
			addChild(_exhaustionBar);
			addChild(_goldTipHolder);
			addChild(_gloryTipHolder);
			addChild(_topTextHolder);
			addChild(_exhaustionText);
			addChild(_goldLimitText);
			addChild(_levelPanel);
			addChild(_panelPickButtons);	
			addChild(_exhaustionHolder);																							
			addChild(_changeGroupNameButton);
			addChild(_buttonVfx);
			//addChild(_dungeonsButtonTip);
			addChild(_dungeonsButton);
			
			addChild(_monstersKilledTipHolder);
		}
		
	}

}