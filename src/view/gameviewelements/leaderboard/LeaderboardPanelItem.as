package view.gameviewelements.leaderboard 
{
	import d_hentity.HEntity;
	import starling.display.Image;
	import starling.events.Event;
	import starling.text.TextFieldAutoSize;
	import view.gameviewelements.BatchedBitmapLabel;
	import view.gameviewelements.HText;
	/**
	 * ...
	 * @author Aikei
	 */
	public class LeaderboardPanelItem extends HEntity
	{
		//! set in script
		static public var POSITION_TEXT_X: Number = 34;
		static public var POSITION_TEXT_Y: Number = 46;
		static public var SQUAD_NAME_TEXT_X: Number = 88;
		static public var SQUAD_NAME_TEXT_Y: Number = 30;
		static public var LEADER_COMMENT_TEXT_X: Number = 88;
		static public var LEADER_COMMENT_TEXT_Y: Number = 50;
		static public var LEADER_NAME_TEXT_X: Number = 0;
		static public var LEADER_NAME_TEXT_Y: Number = 50;
		static public var GLORY_COMMENT_TEXT_X: Number = 362;
		static public var GLORY_COMMENT_TEXT_Y: Number = 24;
		static public var GLORY_TEXT_X: Number = 366;
		static public var GLORY_TEXT_Y: Number = 53;
		static public var LEADERS_IMAGE_X: Number = 8;
		static public var LEADERS_IMAGE_Y: Number = 23;
		
		private var _background: Image = new Image(Assets.interfaceAtlas.getTexture("leaderboard_item"));		
		
		private var _positionText: HText = new HText("","bbold14", -1, 0xa192ac);
		private var _squadNameText: HText = new HText("","bbold16", -1, 0xffffff);
		private var _leaderCommentText: HText = new HText(Language.GetString("Leader") + ": ","bbold14", -1, 0x766982);
		private var _leaderNameText: HText = new HText("","bbold14", -1, 0xa192ac);
		//private var _gloryCommentText: HText = new HText(Language.GetString("Glory"),"bregular14", -1, 0xa192ac);
		private var _gloryText: HText = new HText("","bbold14", -1);		
		private var _leadersImage: Image = new Image(Assets.interfaceAtlas.getTexture("leaderboard_item_first"));
		
		private var _self: Boolean = false;
		private var _perDayGlory: Boolean = false;
		//private var _playerBackground: Image = new Image(Assets.interfaceAtlas.getTexture("leaderboard_player"));
		
		public function LeaderboardPanelItem(perDayGlory: Boolean) 
		{
			//_leaderCommentText.text = Language.GetString("Leader") + ": ";
			//_gloryCommentText.text = Language.GetString("Glory");

			_perDayGlory = perDayGlory;
			_positionText.x = POSITION_TEXT_X;
			_positionText.y = POSITION_TEXT_Y;
			_squadNameText.x = SQUAD_NAME_TEXT_X;
			_squadNameText.y = SQUAD_NAME_TEXT_Y;
			_leaderCommentText.x = LEADER_COMMENT_TEXT_X;
			_leaderCommentText.y = LEADER_COMMENT_TEXT_Y;
			_leaderNameText.x = _leaderCommentText.x + _leaderCommentText.textField.width;
			_leaderNameText.y = LEADER_NAME_TEXT_Y;
			_leadersImage.x = LEADERS_IMAGE_X;
			_leadersImage.y = LEADERS_IMAGE_Y;
			_leadersImage.visible = false;
			//_gloryCommentText.x = GLORY_COMMENT_TEXT_X - _gloryCommentText.textField.width/2;
			//_gloryCommentText.y = GLORY_COMMENT_TEXT_Y - _gloryCommentText.textField.height/2;
			//Misc.RoundPosition(_gloryCommentText);
			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public function SetNewSquadName(name: String):void 
		{
			_squadNameText.text = Language.ProcessSquadName(name);
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_background);
			addChild(_leadersImage);
			addChild(_positionText)
			addChild(_squadNameText);
			addChild(_leaderCommentText);
			addChild(_leaderNameText);
			//addChild(_gloryCommentText);
			addChild(_gloryText);			
		}
		
		public function Init(leaderboardEntry: Object):void 
		{
			if (_self)
			{
				_background.texture = Assets.interfaceAtlas.getTexture("leaderboard_item");
				_self = false;
			}
			if (!_perDayGlory)
				_gloryText.text = BigInt.str2HString(String(leaderboardEntry.g));
			else
				_gloryText.text = BigInt.str2HString(String(leaderboardEntry.dg));
			_gloryText.x = GLORY_TEXT_X;
			_gloryText.y = GLORY_TEXT_Y;
			Misc.RoundPosition(_gloryText);
			
			_squadNameText.text = Language.ProcessSquadName(leaderboardEntry.gn);
			_leaderNameText.text = Language.ProcessPlayerName(leaderboardEntry.n); //Language.ProcessPlayerName(leaderboardEntry.n, leaderboardEntry.id);
			var pos: int = -1;
			if (!_perDayGlory)
				pos = leaderboardEntry.p + 1;
			else
				pos = leaderboardEntry.dp + 1;
			if (pos <= 3)
			{
				_leadersImage.visible = true;
				_positionText.visible = false;
				if (pos === 1)
				{
					_leadersImage.texture = Assets.interfaceAtlas.getTexture("leaderboard_item_first");
				}
				else if (pos === 2)
				{
					_leadersImage.texture = Assets.interfaceAtlas.getTexture("leaderboard_item_second");
				}
				else if (pos === 3)
				{
					_leadersImage.texture = Assets.interfaceAtlas.getTexture("leaderboard_item_third");
				}				
			}
			else
			{
				_leadersImage.visible = false;
				_positionText.visible = true;
				_positionText.text = pos.toString();
				_positionText.x = Math.round(POSITION_TEXT_X - _positionText.textField.width/2);
				_positionText.y = Math.round(POSITION_TEXT_Y - _positionText.textField.height/2);
				Misc.RoundPosition(_positionText);
			}
		}
		
		public function SetSelf(name: String = ""):void 
		{
			if (!_self)
			{
				_background.texture = Assets.interfaceAtlas.getTexture("leaderboard_player");
				_self = true;
				if (name.length > 0)
					SetNewSquadName(name);
			}
		}		
		
	}

}