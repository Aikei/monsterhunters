package view.gameviewelements.leaderboard 
{
	import d_hentity.HEntity;
	import feathers.controls.Button;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.controls.ToggleButton;
	import feathers.core.ITextRenderer;
	import feathers.text.BitmapFontTextFormat;
	import flash.geom.Rectangle;
	import flash.utils.clearTimeout;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.events.TouchPhase;
	import starling.text.BitmapFont;
	import starling.textures.Texture;
	import view.BitmappedText;
	import view.gameviewelements.BatchedBitmapLabel;
	import view.gameviewelements.HButton;
	import view.gameviewelements.HText;
	import starling.events.TouchEvent;
	import starling.events.Touch;
	import starling.events.Event;
	import flash.utils.setTimeout;
	import view.gameviewelements.HToggleButton;
	
	/**
	 * ...
	 * @author Aikei	
	 */
	public class LeaderboardWindow extends HEntity 
	{
		public static var WINDOW_X: Number = 402;
		public static var WINDOW_Y: Number = 0;		
		
		private var _visibilityHolder: HEntity = new HEntity;
		private var _background: Quad = new Quad(410, 440, 0x000000);
		private var _windowImage: Image;
		private var _scrollBg: Image = new Image(Assets.interfaceAtlas.getTexture("leaderboard_scroll"));
		private var _closeButton: HButton = new HButton;
		private var _topText: BatchedBitmapLabel;
		private var _moveInButton: HButton = new HButton;
		//private var _moveInButton: Quad = new Quad(80, 30, 0xffffff);
		private var _requestLeaderboardTimeoutId: uint = 0;
		
		private var _totalGloryLeaderboardPanel: LeaderBoardList = new LeaderBoardList(false);
		private var _perDayGloryLeaderboardPanel: LeaderBoardList = new LeaderBoardList(true);		
		
		//private var _totalGloryLeaderboardPanel: LeaderboardPanel = new LeaderboardPanel(false);
		//private var _perDayGloryLeaderboardPanel: LeaderboardPanel = new LeaderboardPanel(true);
		
		private var _showPerDayGloryButton: HToggleButton = new HToggleButton;
		private var _showTotalGloryButton: HToggleButton = new HToggleButton;
		
		private var _showTotalLeaderboard: Boolean = false;
		
		private var _windowOut: Boolean = true;
		private var _requestLeaderboardTimeout: uint = 0;
		
		
		
		public static var EXTRUDE_Y: Number = 37;
		
		public function get windowOut(): Boolean 
		{
			return _windowOut;
		}
		
		static public function GetLabelFactory(): Function 
		{
			return function (): ITextRenderer 
			{
				var textRenderer: BitmapFontTextRenderer = new BitmapFontTextRenderer();
				//textRenderer.textFormat = new BitmapFontTextFormat(fontName, fontSize, fontColor);
				//textRenderer.embedFonts = true;
				return textRenderer;
			}				
		}
		
		public function LeaderboardWindow() 
		{
			super(false, false);
			
			var windowTexture: Texture = Assets.interfaceAtlas.getTexture("leaderboard_window");			
			_windowImage = new Image(windowTexture);
			_windowImage.touchable = false;
			_totalGloryLeaderboardPanel.visible = false;
			this.x = WINDOW_X;
			this.y = WINDOW_Y-_windowImage.height + EXTRUDE_Y;
			//Texture.fromTexture
			_background.x = 22;
			_background.y = 152;
			_scrollBg.x = 424;
			_scrollBg.y = 152;
			
			_showPerDayGloryButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("inactive_leaderboard_button"));
			_showPerDayGloryButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("inactive_leaderboard_button"));
			_showPerDayGloryButton.hoverSkin.filter = Filters.highlightFilter;			
			_showPerDayGloryButton.defaultSelectedSkin = new Image(Assets.interfaceAtlas.getTexture("active_leaderboard_button"));
			_showPerDayGloryButton.isToggle = true;
			_showPerDayGloryButton.x = 21;
			_showPerDayGloryButton.y = 56;
			_showPerDayGloryButton.isSelected = true;
			_showPerDayGloryButton.isToggle = false;
			_showPerDayGloryButton.addEventListener(Event.CHANGE, OnPerDayGloryButtonActivated);
			_showPerDayGloryButton.width = _showPerDayGloryButton.defaultSkin.width+1;
			_showPerDayGloryButton.labelFactory = GetLabelFactory();
			_showPerDayGloryButton.defaultLabelProperties.textFormat = new BitmapFontTextFormat("bbold12", NaN, 0x8d98a0, "center");
			_showPerDayGloryButton.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat("bbold12", NaN, 0x7b4c02, "center");
			_showPerDayGloryButton.label = Language.GetString("GloryPerDay").toUpperCase();
			
			var images: Vector.<Image> = new Vector.<Image>;
			images.push(new Image(Assets.interfaceAtlas.getTexture("inactive_leaderboard_button")));
			images.push(new Image(Assets.interfaceAtlas.getTexture("inactive_leaderboard_button")));
			images.push(new Image(Assets.interfaceAtlas.getTexture("active_leaderboard_button")));
			
			for (var i: int = 0; i < images.length; i++)
			{
				images[i].x = images[i].texture.width / 2;
				images[i].y = images[i].texture.height / 2;				
				images[i].pivotX = images[i].texture.width / 2;
				images[i].pivotY = images[i].texture.height / 2;
				images[i].rotation = Misc.PI;
				images[i].pivotX = images[i].texture.width;
				images[i].pivotY = images[i].texture.height;
			}
			
			_showTotalGloryButton.defaultSkin = images[0];
			_showTotalGloryButton.hoverSkin = images[1];
			_showTotalGloryButton.hoverSkin.filter = Filters.highlightFilter;
			_showTotalGloryButton.defaultSelectedSkin = images[2];			
			_showTotalGloryButton.isToggle = false;
			_showTotalGloryButton.x = 278;
			_showTotalGloryButton.y = 56;
			_showTotalGloryButton.width = images[0].width+1;
			//_showTotalGloryButton.pivotX = _showTotalGloryButton.defaultSkin.width / 2;
			//_showTotalGloryButton.pivotY = _showTotalGloryButton.defaultSkin.height / 2;
			//_showTotalGloryButton.rotation = Misc.PI;
			//_showTotalGloryButton.x += _showTotalGloryButton.defaultSkin.width / 2;
			//_showTotalGloryButton.y += _showTotalGloryButton.defaultSkin.height / 2;
			
			Misc.RoundPosition(_showTotalGloryButton);
			_showTotalGloryButton.isSelected = false;
			_showTotalGloryButton.isToggle = true;
			_showTotalGloryButton.addEventListener(Event.CHANGE, OnTotalGloryButtonActivated);
			
			_showTotalGloryButton.labelFactory = GetLabelFactory();
			
			_showTotalGloryButton.defaultLabelProperties.textFormat = new BitmapFontTextFormat("bbold12", NaN, 0x8d98a0, "center");
			_showTotalGloryButton.defaultSelectedLabelProperties.textFormat = new BitmapFontTextFormat("bbold12", NaN, 0x7b4c02, "center");
			_showTotalGloryButton.label = Language.GetString("GloryTotal").toUpperCase();
			
			_closeButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("close_button"));
			_closeButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("close_button"));
			_closeButton.hoverSkin.filter = Filters.highlightFilter;
			
			_closeButton.x = 211;
			_closeButton.y = 577;
			
			_moveInButton.defaultSkin = new Image(Texture.fromTexture(windowTexture, new Rectangle(192, 657, 84, 40)));
			_moveInButton.hoverSkin = new Image(Texture.fromTexture(windowTexture, new Rectangle(192, 657, 84, 40)));
			_moveInButton.hoverSkin.filter = Filters.highlightFilter;
			_moveInButton.x = 192;
			_moveInButton.y = 657;
			_moveInButton.addEventListener(Event.TRIGGERED, OnMoveInButtonTriggered);
			
			_topText = new BatchedBitmapLabel("bbold14", 14, 0xffffff);
			_topText.text = Language.GetString("TOP");
			Misc.CenterObject(_topText, 232, 672);
			//_topText.x = 226;
			//_topText.y = 656;					
			_topText.touchable = false;			
			
			_closeButton.addEventListener(Event.TRIGGERED, OnCloseButtonTriggered);
			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			World.gWorld.addEventListener("leaderboardArrived", OnLeaderboardArrived);
		}
		
		override public function set visible(v: Boolean):void 
		{
			_visibilityHolder.visible = v;
			if (v)
			{
				_moveInButton.visible = false;
				if (!_showTotalLeaderboard)
				{
					_totalGloryLeaderboardPanel.visible = false;
					_perDayGloryLeaderboardPanel.ScrollToSelf();
				}
				else
				{
					_perDayGloryLeaderboardPanel.visible = false;
					_totalGloryLeaderboardPanel.ScrollToSelf();
				}
			}
			else
			{
				_moveInButton.visible = true;
			}
		}
		
		override public function get visible(): Boolean 
		{
			return super.visible;
		}
		
		private function OnLeaderboardArrived(e:Event): void 
		{
			_perDayGloryLeaderboardPanel.Init(e.data);
			_totalGloryLeaderboardPanel.Init(e.data);			
		}
		
		private function OnTotalGloryButtonActivated(e:Event):void 
		{
			if (_showTotalGloryButton.isSelected)
			{
				_showTotalGloryButton.isToggle = false;
				_showPerDayGloryButton.isSelected = false;
				_showPerDayGloryButton.isToggle = true;
				_perDayGloryLeaderboardPanel.visible = false;
				_totalGloryLeaderboardPanel.visible = true;
				_showTotalLeaderboard = true;
			}			
		}
		
		private function OnPerDayGloryButtonActivated(e:Event):void 
		{
			if (_showPerDayGloryButton.isSelected)
			{
				_showPerDayGloryButton.isToggle = false;
				_showTotalGloryButton.isSelected = false;
				_showTotalGloryButton.isToggle = true;
				_perDayGloryLeaderboardPanel.visible = true;
				_totalGloryLeaderboardPanel.visible = false;
				_showTotalLeaderboard = false;
			}
		}
		
		public function Init(leaderboardObject: Object): void 
		{
			_perDayGloryLeaderboardPanel.Init(leaderboardObject);
			_totalGloryLeaderboardPanel.Init(leaderboardObject);
		}
		
		private function OnCloseButtonTriggered(e:Event):void 
		{
			_windowOut = true;
			clearTimeout(_requestLeaderboardTimeout);
			dispatchEventWith("moveLeaderboardWindowOut");			
		}
		
		private function OnMoveInButtonTriggered(e:Event):void 
		{			
			if (_windowOut)
			{
				_windowOut = false;
				World.gameController.RequestLeaderboard(true);
				_requestLeaderboardTimeout = setTimeout(RequestLeaderboard, 30000);
				dispatchEventWith("moveLeaderboardWindowIn");					
			}
		}
		
		private function RequestLeaderboard():void 
		{
			World.gameController.RequestLeaderboard(false);
			if (!_windowOut)
				setTimeout(RequestLeaderboard, 30000);
		}
				
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_visibilityHolder);
			_visibilityHolder.addChild(_background);
			_visibilityHolder.addChild(_scrollBg);
			_visibilityHolder.addChild(_totalGloryLeaderboardPanel);
			_visibilityHolder.addChild(_perDayGloryLeaderboardPanel);
			_visibilityHolder.addChild(_windowImage);			
			_visibilityHolder.addChild(_showPerDayGloryButton);
			_visibilityHolder.addChild(_showTotalGloryButton);
			_visibilityHolder.addChild(_closeButton);							
			
			addChild(_moveInButton);				
			addChild(_topText);
			this.visible = false;
		}
		
	}

}