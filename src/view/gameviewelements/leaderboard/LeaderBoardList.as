package view.gameviewelements.leaderboard 
{
	import feathers.data.ListCollection;
	import view.gameviewelements.list.BaseAccessoryOnlyList;
	import starling.display.Image;
	import starling.events.Event;	
	/**
	 * ...
	 * @author Aikei
	 */
	public class LeaderBoardList extends BaseAccessoryOnlyList 
	{
		
		private var _items: Vector.<LeaderboardPanelItem> = new Vector.<LeaderboardPanelItem>;
		private var _perDayPanel: Boolean = false;
		private var _selfIndex: int = -1;
		private var _selfName: String = "";
		
		public function LeaderBoardList(perDay: Boolean) 
		{
			super();
			_perDayPanel = perDay;
			this.x = 22;
			this.y = 152;
			this.width = 413;
			this.height = 434;
			World.gWorld.addEventListener("squadNameChanged", OnSquadNameChanged);
		}
		
		private function OnSquadNameChanged(e:Event):void 
		{
			if (_selfIndex != -1)
			{
				_selfName = e.data as String;
			}
		}
		
		public function Init(leaderboardData: Object): void 
		{
			var mainLb: Array;
			if (!_perDayPanel)
				mainLb = leaderboardData.l;
			else
				mainLb = leaderboardData.dl;

			for (var i = 0; i < mainLb.length; i++)
			{
				if (_items.length < i + 1)
				{
					_items.push(new LeaderboardPanelItem(_perDayPanel));
					//addChild(_items[i]);
				}
				_items[i].Init(mainLb[i]);
				if ((!_perDayPanel && leaderboardData.u.p == i) || (_perDayPanel && leaderboardData.u.dp == i))
				{
					_items[i].SetSelf(_selfName);
					_selfIndex = i;
					_selfName = "";
				}
			}
			if (leaderboardData.u.position >= 100)
			{
				_items.push(new LeaderboardPanelItem(_perDayPanel));
				//addChild(_items[i]);
				_items[_items.length-1].Init(leaderboardData.u);
				_items[_items.length-1].SetSelf(_selfName);
				_selfIndex = _items.length - 1;
				_selfName = "";
			}
			var l: ListCollection = new ListCollection(_items);
			this.dataProvider = l;
			ScrollToSelf();
		}
		
		public function ScrollToSelf():void 
		{
			this.scrollToDisplayIndex(_selfIndex,0.75);
		}
		
		override public function set visible(v: Boolean):void 
		{
			super.visible = v;
			if (v)
				ScrollToSelf();
		}
		
		
		
	}

}