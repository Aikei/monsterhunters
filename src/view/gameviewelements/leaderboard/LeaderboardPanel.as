package view.gameviewelements.leaderboard 
{
	import starling.display.Image;
	import starling.events.Event;
	import view.gameviewelements.BaseVerticalScrollContainer;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class LeaderboardPanel extends BaseVerticalScrollContainer 
	{

		private var _items: Vector.<LeaderboardPanelItem> = new Vector.<LeaderboardPanelItem>;
		private var _perDayPanel: Boolean = false;
		
		public function LeaderboardPanel(perDay: Boolean) 
		{
			super();
			_perDayPanel = perDay;
			this.x = 22;
			this.y = 152;
			this.width = 413;
			this.height = 434;
		}
		
		//override protected function AddImagesToStage():void 
		//{
			//super.AddImagesToStage();
		//}
		//
		//override protected function AddTextToStage():void 
		//{
			//super.AddTextToStage();											
		//}
		
		public function Init(leaderboardData: Object): void 
		{
			var mainLb: Array;
			if (!_perDayPanel)
				mainLb = leaderboardData.l;
			else
				mainLb = leaderboardData.dl;

			for (var i = 0; i < mainLb.length; i++)
			{
				if (_items.length < i + 1)
				{
					_items.push(new LeaderboardPanelItem(_perDayPanel));
					addChild(_items[i]);
				}
				_items[i].Init(mainLb[i]);
				if ((!_perDayPanel && leaderboardData.u.p == i) || (_perDayPanel && leaderboardData.u.dp == i))
					_items[i].SetSelf();				
			}
			if (leaderboardData.u.position >= 100)
			{
				_items.push(new LeaderboardPanelItem(_perDayPanel));
				addChild(_items[i]);
				_items[i].Init(leaderboardData.u);
				_items[i].SetSelf();
			}
		}
		
		
		//protected function OnAddedToStage(event: Event): void 
		//{
			//removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			//
		//}		
	}

}