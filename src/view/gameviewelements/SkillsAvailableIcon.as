package view.gameviewelements 
{
	import d_hentity.HEntity;
	import starling.events.Event;
	import starling.display.Image;
	/**
	 * ...
	 * @author Aikei
	 */
	public class SkillsAvailableIcon extends HEntity 
	{
		private var _skillsAvailableIcon: Image = new Image(Assets.interfaceAtlas.getTexture("skills_available_icon"));
		private var _skillsAvailableText: HText = new HText("0", "bbold12", -1, 0xffffff);
		
		public function SkillsAvailableIcon() 
		{
			super();
			this.touchable = false;
			_skillsAvailableIcon.y = 4;
			_skillsAvailableText.y = 3;
			addChild(_skillsAvailableIcon);
			addChild(_skillsAvailableText);		
			World.gWorld.addEventListener("skillsUnpicked", OnSkillsUnpicked);
		}
		
		private function OnSkillsUnpicked(e:Event):void 
		{
			if (uint(e.data) == 0)
			{
				visible = false;
			}
			else
			{
				visible = true;
				_skillsAvailableText.textField.text = String(e.data);
				_skillsAvailableText.x = Math.round(_skillsAvailableIcon.width / 2 - _skillsAvailableText.textField.width / 2 + 1);
			}
		}
		
	}

}