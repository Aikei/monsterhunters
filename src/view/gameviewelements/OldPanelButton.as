package view.gameviewelements 
{
	import feathers.controls.Button;
	import feathers.text.BitmapFontTextFormat;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.ITextRenderer;
	import starling.events.TouchEvent;
	import view.gameviewelements.MessageWindow.MessageWindow;
	import system.HCursor;
	import starling.events.TouchPhase;
	import starling.events.Touch;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class OldPanelButton extends HButton 
	{
		
		
		public function set showTooltipOnHover(show: Boolean): void
		{
			hoverChecker.showTooltipOnHover = show;
		}
		
		public function get showTooltipOnHover(): Boolean
		{
			return hoverChecker.showTooltipOnHover;
		}
		
		public function set tooltipText(txt: String): void
		{
			hoverChecker.tooltipText = txt;
		}
		
		public function get tooltipText(): String
		{
			return hoverChecker.tooltipText;
		}			
		
		public function OldPanelButton() 
		{
			super();
			//useHandCursor = true;
			this.labelFactory = function (): ITextRenderer 
			{
				var textRenderer: BitmapFontTextRenderer = new BitmapFontTextRenderer();
				textRenderer.textFormat = new BitmapFontTextFormat("bbold11", 11, 0x7c3a12);
				textRenderer.useSeparateBatch = false;
				//textRenderer.embedFonts = true;
				return textRenderer;
			}
		}				
		
		public function SetEnabled(enable: Boolean):void 
		{
			if (!enable && hoverChecker.hovered)
			{
				hoverChecker.OnStopHover();
			}
			isEnabled = enable;
		}
	}

}

//package view.gameviewelements 
//{
	//import feathers.controls.Button;
	//import feathers.text.BitmapFontTextFormat;
	//import feathers.controls.text.BitmapFontTextRenderer;
	//import feathers.core.ITextRenderer;
	//import starling.events.TouchEvent;
	//import view.gameviewelements.MessageWindow.MessageWindow;
	//import system.HCursor;
	//import starling.events.TouchPhase;
	//import starling.events.Touch;
	//
	///**
	 //* ...
	 //* @author Aikei
	 //*/
	//public class OldPanelButton extends HButton 
	//{
		//private var _touchInited: Boolean = false;
		//private var _hovered: Boolean = false;
		//private var _messageWindow: MessageWindow = null;
		//
		//public var showTooltipOnHover: Boolean = false;
		//public var tooltipText: String = "";
		//
		//public function OldPanelButton() 
		//{
			//super();
			////useHandCursor = true;
			//this.labelFactory = function (): ITextRenderer 
			//{
				//var textRenderer: BitmapFontTextRenderer = new BitmapFontTextRenderer();
				//textRenderer.textFormat = new BitmapFontTextFormat("bbold11", 11, 0x7c3a12);
				//textRenderer.useSeparateBatch = false;
				////textRenderer.embedFonts = true;
				//return textRenderer;
			//}
		//}
		//
		//public function InitTouch():void 
		//{
			//_touchInited = true;
			//addEventListener(TouchEvent.TOUCH, OnTouch);
		//}
		//
		//protected function OnTouch(event: TouchEvent): void
		//{
			//var touch: Touch = event.getTouch(this);
			//if (touch != null)
			//{
				//if (touch.phase === TouchPhase.HOVER || touch.phase === TouchPhase.STATIONARY)
				//{					
					//OnHover();
				//}
				//else if (touch.phase === TouchPhase.BEGAN)
				//{
					//OnTouchPhaseBegan();
					////OnStopHover();
				//}
				//else if (touch.phase === TouchPhase.ENDED)
				//{
					//OnTouchPhaseEnded();
					////OnStopHover();
				//}
			//}
			//else
			//{		
				//OnStopHover();
			//}
		//}
		//
		//protected function OnHover():void 
		//{
			//_hovered = true;
			//if (showTooltipOnHover)
			//{
				//if (_messageWindow === null)
				//{
					//_messageWindow = new MessageWindow(tooltipText);
					//_messageWindow.alpha = 0.98;
					//World.gWorld.addChild(_messageWindow);
				//}
				//_messageWindow.x = HCursor.GetCursor().x - _messageWindow.width / 2;
				//_messageWindow.y = HCursor.GetCursor().y - _messageWindow.height - 4;				
			//}
		//}
		//
		//protected function OnStopHover():void 
		//{
			//if (_hovered)
			//{
				//_hovered = false;
				//if (_messageWindow)
				//{
					//_messageWindow.Destroy();
					//_messageWindow = null;			
				//}
			//}
		//}
		//
		//protected function OnTouchPhaseBegan():void 
		//{
			//
		//}
		//
		//protected function OnTouchPhaseEnded():void 
		//{
			//
		//}				
		//
		//override public function Destroy():void 
		//{
			//super.Destroy();
			//removeEventListener(TouchEvent.TOUCH, OnTouch);
			//removeFromParent(true);
		//}
		//
		//public function SetEnabled(enable: Boolean):void 
		//{
			//if (!enable && _hovered)
			//{
				//OnStopHover();
			//}
			//isEnabled = enable;
		//}
	//}
//
//}