package view.gameviewelements 
{
	import com.greensock.TweenLite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import logic.ClientLogic;
	import logic.MonsterData;
	import starling.display.Image;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import hevents.*;
	import starling.filters.ColorMatrixFilter;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import d_hentity.HEntity;
	import system.HColorFilter;
	import system.HSounds;
	import view.InvisibleTooltipHolder;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class MonsterHpPanel extends HEntity 
	{
		public static var HP_PANEL_X: int = 206;
		public static var HP_PANEL_Y: int = 690;		
		public static var NAME_TEXT_X: Number = 202;
		public static var NAME_TEXT_Y: Number = 86;		
		public static var TRAIT_TEXT_X: Number = 202;
		public static var TRAIT_TEXT_Y: Number = 106;		
		public static var HP_TEXT_X: Number = 106;
		public static var HP_TEXT_Y: Number = 126;		
		public static var ARMOR_COMMENT_TEXT_X: Number = 288;
		public static var ARMOR_COMMENT_TEXT_Y: Number = 143;		
		public static var ARMOR_TEXT_X: Number = 312;
		public static var ARMOR_TEXT_Y: Number = 136;	
		public static var TIME_BAR_X: Number = 98;
		public static var TIME_BAR_Y: Number = 152;		
		public static var HEALTH_BAR_X: Number = 94;
		public static var HEALTH_BAR_Y: Number = 128;			
		public static var TIME_TEXT_X: Number = 73;
		public static var TIME_TEXT_Y: Number = 141;
		public static var STUN_TIMER_IMAGE_CENTER_X: Number = 72.3;
		public static var STUN_TIMER_IMAGE_CENTER_Y: Number = 144;
		
		public static var ARMOR_TIP_X: Number = 279;
		public static var ARMOR_TIP_Y: Number =	128;
		public static var ARMOR_TIP_WIDTH: Number = 64;
		public static var ARMOR_TIP_HEIGHT: Number = 32;
		
		public static var TIMER_TIP_X: Number = 50;
		public static var TIMER_TIP_Y: Number = 122;
		public static var TIMER_TIP_WIDTH: Number = 44;
		public static var TIMER_TIP_HEIGHT: Number = 44;					
		
		//public static const 
		
		private var _image: Image = new Image(Assets.interfaceAtlas.getTexture("no_armor"));
		private var _currentHealthBarTexture: Texture;
		private var _healthBarTexture: Texture = Assets.interfaceAtlas.getTexture("health_bar");
		private var _healthBarTextureRegion: Rectangle = new Rectangle(0,0,0,0);
		private var _healthBar: Image = new Image(_healthBarTexture);	
		private var _additionalHealthBar: Image = new Image(_healthBarTexture);

		//private var _nameText: ComplexBatchedBitmapLabel;
		//private var _traitText: ComplexBatchedBitmapLabel;
		//private var _rarityText: ComplexBatchedBitmapLabel;
		//private var _armorCommentText: ComplexBatchedBitmapLabel;
		//
		//private var _hpText: BatchedBitmapLabel;		
		//private var _timeText: BatchedBitmapLabel;
		
		private var _nameText: HText;
		private var _traitTexts: Vector.<HText> = new Vector.<HText>;
		private var _rarityText: HText;
		private var _armorCommentText: HText;
		
		private var _hpText: HText;		
		private var _timeText: HText;		
		
		private var _currentHp: BigInt;
		private var _maxHp: BigInt;				
		private var _timeBar: Image;
		private var _creationTime: Number = 0;
		private var _timerDuration: Number = 30;
		private var _currentTimer: Number;
		private var _stunTimerHolder: HEntity = new HEntity(true); 
		private var _armorPanel: HEntity = new HEntity();
		
		private var _armorText: HText;
		private var _filter: HColorFilter;
		
		private var _stunned: Boolean = false;
		private var _stunTime: Number;
		private var _stunDuration: Number;
		
		private var _armorTypeTooltipHolder: InvisibleTooltipHolder = new InvisibleTooltipHolder(178, 36, 45, 45, "");
		private var _armorTipHolder: InvisibleTooltipHolder;
		private var _timerTipHolder: InvisibleTooltipHolder;
		
		
		public function MonsterHpPanel() 
		{
			super();

			_armorTipHolder = new InvisibleTooltipHolder(ARMOR_TIP_X, ARMOR_TIP_Y, ARMOR_TIP_WIDTH, ARMOR_TIP_HEIGHT, "ExplainArmor");
			_timerTipHolder = new InvisibleTooltipHolder(TIMER_TIP_X, TIMER_TIP_Y, TIMER_TIP_WIDTH, TIMER_TIP_HEIGHT, "ExplainTimer");
			
			HP_PANEL_Y = Preferences.SCREEN_HEIGHT - _image.height;
			_filter = new HColorFilter;
			this.filter = _filter;
			//visible = false;
			_image.touchable = false;
			x = Math.round(HP_PANEL_X -_image.width / 2);
			y = HP_PANEL_Y;
			//_hpText = new BatchedBitmapLabel("bbold14",14);
			_hpText = new HText("","bbold14", -1);
			_hpText.x = HP_TEXT_X;
			_hpText.y = HP_TEXT_Y;
									
			//_nameText = new ComplexBatchedBitmapLabel("bbold16", 16);
			_nameText = new HText("","bbold16", -1);
			_nameText.InitTouch();
			//_nameText.showTooltipOnHover = true;
			_nameText.x = NAME_TEXT_X;
			_nameText.y = NAME_TEXT_Y;
			
			//_rarityText = new ComplexBatchedBitmapLabel("bregular12", 12, 0xa7aab5);
			_rarityText = new HText("","bregular12", -1, 0xa7aab5);
			_rarityText.x = TRAIT_TEXT_X;
			_rarityText.y = TRAIT_TEXT_Y;
			_rarityText.InitTouch();
			_rarityText.showTooltipOnHover = true;
			
			//_traitText = new ComplexBatchedBitmapLabel("bregular12", 12, 0xE4CE83);
			for (var i: int = 0; i < 3; i++)
			{
				_traitTexts[i] = new HText("", "bregular12", -1, 0xE4CE83);
				_traitTexts[i].x = TRAIT_TEXT_X;
				_traitTexts[i].y = TRAIT_TEXT_Y;
				_traitTexts[i].InitTouch();
				_traitTexts[i].showTooltipOnHover = true;
			}
			
			
			//_armorPanel.InitTouch();
			//_armorPanel.x = 188;
			//_armorPanel.y = 124;
			//_armorPanel.showTooltipOnHover = true;
			//_armorPanel.tooltipText = "ExplainArmor";
			
			//_armorText = new ComplexBatchedBitmapLabel("bbold12o", 12);
			_armorText = new HText("","bbold12o", -1);
			//_armorText.InitTouch();
			//_armorText.showTooltipOnHover = true;
			//_armorText.tooltipText = "ExplainArmor";
			Misc.CenterObject(_armorText, ARMOR_TEXT_X, ARMOR_TEXT_Y);
			_armorPanel.addChild(_armorText);
			
			//_armorCommentText = new ComplexBatchedBitmapLabel("bbold11", 11, Color.BLACK);
			_armorCommentText = new HText("", "bbold11", -1, Color.BLACK);
			_armorCommentText.text = Language.GetString("Armor").toUpperCase();
			_armorCommentText.InitTouch();
			_armorCommentText.showTooltipOnHover = true;
			_armorCommentText.tooltipText = "ExplainArmor";			
			_armorCommentText.x = ARMOR_COMMENT_TEXT_X;
			_armorCommentText.y = ARMOR_COMMENT_TEXT_Y;
			

			
			//Misc.CenterObject(_armorCommentText, ARMOR_COMMENT_TEXT_X, ARMOR_COMMENT_TEXT_Y);
			
			//_armorPanel.addChild(_armorCommentText);		
															
			_healthBar.x = 94;
			_healthBar.y = 128;	
			
			//_additionalHealthBar.x = 60;
			//_additionalHealthBar.y = 99;
			//_additionalHealthBar.color = 0xB4B400;
			
			_timeBar = new Image(Assets.interfaceAtlas.getTexture("time"));
			_timeBar.x = TIME_BAR_X;
			_timeBar.y = TIME_BAR_Y;
			
			_timeText = new HText("","bbold16", 16, 0xffdd40);
			//_timeText = new BatchedBitmapLabel("bbold16", 16, 0xffdd40);
			//_timeText.x = TIME_TEXT_X;
			//_timeText.y = TIME_TEXT_Y;
			
			var stunTimerImage: Image = new Image(Assets.interfaceAtlas.getTexture("stun_timer"));
			_stunTimerHolder.addChild(stunTimerImage);
			_stunTimerHolder.x = STUN_TIMER_IMAGE_CENTER_X;
			_stunTimerHolder.y = STUN_TIMER_IMAGE_CENTER_Y;
			_stunTimerHolder.alignPivot();
			//Misc.CenterObject(_stunTimerHolder, TIME_TEXT_X, TIME_TEXT_Y);
			_stunTimerHolder.visible = false;
			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			World.gWorld.addEventListener("N_NewMonster", OnEventNewMonster);
			World.gWorld.addEventListener(L_HEventMonsterKilled.H_EVENT_MONSTER_KILLED, OnEventMonsterKilled);
			World.gWorld.addEventListener("N_TimeUp", OnTimeUp);
			World.gWorld.addEventListener(L_HEventMonsterHpChanged.H_EVENT_MONSTER_HP_CHANGED, OnEventMonsterHpChanged);
			World.gWorld.addEventListener(EnterFrameEvent.ENTER_FRAME, OnEnterFrame);
			World.gWorld.addEventListener("stunMonster", OnMonsterStunned);
		}
		
		private function OnMonsterStunned(e:Event):void 
		{
			HSounds.PlaySound("Stun");
			Tweens.StunShake(this);
			_stunTimerHolder.visible = true;
			_stunTimerHolder.ApplyRotation(Misc.PI);
			_stunTime = Misc.GetServerTime();
			_stunDuration = e.data as Number;
			_stunned = true;
			_timeText.visible = false;
		}
		
		public function OnAddedToStage(event: Event): void 
		{
			addChild(_image);
			addChild(_healthBar);
			//addChild(_additionalHealthBar);
			addChild(_hpText);
			addChild(_nameText);
			addChild(_rarityText);
			for (var i: int = 0; i < _traitTexts.length; i++)
				addChild(_traitTexts[i]);
			
			addChild(_timeBar);
			addChild(_timeText);
			addChild(_stunTimerHolder);
			addChild(_armorCommentText);
			addChild(_armorPanel);
			
			addChild(_armorTypeTooltipHolder);
			addChild(_armorTipHolder);
			addChild(_timerTipHolder);
		}
		
		private function OnEventMonsterHpChanged(event: Event): void
		{
			SetNewHp(event.data as BigInt);
		}
		
		private function OnEventMonsterKilled(event: L_HEventMonsterKilled): void
		{
			//this.visible = false;
			if (_stunned)
				EndStun();
			TweenLite.to(_filter, 1, { brightness: -1 } );
			//TweenLite.to(this, 1, { alpha: 0.25 } );
			_creationTime = -1;
			SetNewHp(Misc.BIG_INT_ZERO);
		}
		
		private function OnTimeUp(event: Event): void
		{
			//this.visible = false;
			HSounds.PlaySound("Fail");
			TweenLite.to(_filter, 1, { brightness: -1 } );
			//TweenLite.to(this, 1, { alpha: 0.25 } );
			SetNewHp(Misc.BIG_INT_ZERO);
		}		
		
		private function OnEventNewMonster(event: Event): void
		{
			//this.visible = true;
			TweenLite.to(_filter, 0.5, { brightness: 0 } );
			//TweenLite.to(this, 1, { alpha: 1 } );
			if (MonsterData(event.data.monsterData).isQuestBoss)
				_nameText.text = ClientLogic.gLogic.quest.uniqueTargetName.GetString();
			else
				_nameText.text = Language.GetMonsterName(MonsterData(event.data.monsterData).type, MonsterData(event.data.monsterData).rarity);
			_nameText.x = NAME_TEXT_X - _nameText.width / 2;
			//_nameText.textField.alignPivot();
			Misc.RoundPosition(_nameText);	
			
			_armorTypeTooltipHolder.tooltipText = "ExplainArmorType" + MonsterData(event.data.monsterData).armorType.toString();
			
			//_nameText.tooltipText = Language.GetString("ExplainArmorType" + MonsterData(event.data.monsterData).armorType.toString());
			for (var i: int = 0; i < _traitTexts.length; i++)
			{			
				_traitTexts[i].text = "";
				_traitTexts[i].tooltipText = "";
				_traitTexts[i].textField.width = 0;
			}
			
			_rarityText.tooltipText = "";
			_rarityText.text = Language.GetString("Rarity" + MonsterData(event.data.monsterData).rarity.toString()).toLowerCase();			
			
			if (MonsterData(event.data.monsterData).rarity == MonsterData.RARITY_COMMON)
			{
				for (i = 0; i < _traitTexts.length; i++)
					_traitTexts[i].color = 0xa7aab5;
				_rarityText.color = 0xa7aab5;
				_rarityText.tooltipText = "ExplainCommonMonster";
			}
			else if (MonsterData(event.data.monsterData).rarity == MonsterData.RARITY_RARE)
			{
				for (i = 0; i < _traitTexts.length; i++)
					_traitTexts[i].color = 0xe4ce83;
				_rarityText.color = 0xe4ce83;
				_rarityText.tooltipText = "ExplainRareMonster";
				_rarityText.tooltipText = _rarityText.tooltipText.replace("$rarity", Language.GetString("Rarity" + MonsterData(event.data.monsterData).rarity).toLocaleLowerCase());
			}
			else if (MonsterData(event.data.monsterData).rarity == MonsterData.RARITY_ELITE)
			{
				for (i = 0; i < _traitTexts.length; i++)
					_traitTexts[i].color = 0xf36742;
				_rarityText.color = 0xf36742;
				_rarityText.tooltipText = "ExplainEliteMonster";
				_rarityText.tooltipText = _rarityText.tooltipText.replace("$rarity", Language.GetString("Rarity" + MonsterData(event.data.monsterData).rarity).toLocaleLowerCase());				
			}			
						
			if (MonsterData(event.data.monsterData).traits.length > 0)
			{
				for (i = 0; i < MonsterData(event.data.monsterData).traits.length; i++ )
				{
					_traitTexts[i].tooltipText = Language.GetString("ExplainTrait" + MonsterData(event.data.monsterData).traits[i].type.toString());
					_traitTexts[i].tooltipTitleText = "TitleExplainTrait" + MonsterData(event.data.monsterData).traits[i].type.toString();
					_traitTexts[i].text = Language.GetString("Trait" + MonsterData(event.data.monsterData).traits[i].type.toString()).toLowerCase();
					if (i + 1 < MonsterData(event.data.monsterData).traits.length)
					{
						_traitTexts[i].text += " • ";
					}
				}
			}
			
			if (_traitTexts[0].text != "")
				_rarityText.text += " • ";
			
			var overallWidth: Number = _rarityText.width;
			for (i = 0; i < _traitTexts.length; i++)
			{
				if (_traitTexts[i].text.length > 0)
					overallWidth += _traitTexts[i].width;
			}
			
			_rarityText.x = TRAIT_TEXT_X - overallWidth / 2;
			Misc.RoundPosition(_rarityText);
			_traitTexts[0].x = _rarityText.x + _rarityText.width;
			for (i = 1; i < _traitTexts.length; i++)
			{
				_traitTexts[i].x = _traitTexts[i - 1].x + _traitTexts[i - 1].textField.width;
			}
			Misc.RoundPosition(_traitTexts[0]);
						
			_maxHp = MonsterData(event.data.monsterData).hp;
			_image.texture = Assets.GetArmorTexture(MonsterData(event.data.monsterData).armorType);
			SetNewHp(_maxHp);
			SetNewArmor(MonsterData(event.data.monsterData).armor);
			
			_creationTime = MonsterData(event.data.monsterData).creationTime;
			_timerDuration = MonsterData(event.data.monsterData).timerDuration;
			_timeText.text = Number(_timerDuration - (Misc.GetServerTime() - _creationTime)).toFixed(0);			
		}
		
		private function EndStun():void 
		{
			_creationTime += _stunDuration;
			_stunTime = 0;
			_stunDuration = 0;
			_stunned = false;
			_stunTimerHolder.visible = false;
			_stunTimerHolder.StopMyRotation();
			_timeText.visible = true;
			World.gWorld.dispatchEventWith("stunEnded");
		}
		
		override public function OnEnterFrame(event: EnterFrameEvent): void 
		{
			if (_creationTime != -1)
			{
				if (_stunned)
				{
					if (Misc.GetServerTime() - _stunTime >= _stunDuration)
					{
						EndStun();
					}
				}
				else
				{
					_currentTimer = _timerDuration - (Misc.GetServerTime() - _creationTime);
					if (_currentTimer <= 3)
					{
						HSounds.PlayRunningOutOfTimeSound();
					}					
					if (_currentTimer <= 0)
					{
						_creationTime = -1;
						World.gameController.RunMonster();
					}
					else
					{
						_timeText.text = _currentTimer.toFixed(0);
						Misc.CenterObject(_timeText, TIME_TEXT_X, TIME_TEXT_Y);
					}
					var portion: Number = Misc.RoundToDecimalPlaces(_currentTimer / _timerDuration,2);
					_timeBar.width = _timeBar.texture.width * portion;
					_timeBar.setTexCoordsTo(1, portion, 0);
					_timeBar.setTexCoordsTo(3, portion, 1);					
				}
			}
		}		
		
		private function SetNewArmor(armor: BigInt):void 
		{
			_armorText.text = BigInt.bigint2HString(armor);
			Misc.CenterObject(_armorText, ARMOR_TEXT_X, ARMOR_TEXT_Y);
			//_armorText.x = ARMOR_COMMENT_TEXT_X-_armorText.width / 2;
			//_armorText.x = Math.round(_armorText.x);
			//_armorText.alignPivot();
			
			//_armorIcon.x = _armorText.x - _armorIcon.width - 4;			
		}
		
		private function SetNewHp(newHp: BigInt):void 
		{
			_currentHp = newHp;
			_hpText.text = BigInt.bigint2HString(newHp);
			var maxHpString: String = BigInt.bigInt2str(_maxHp, 10);
			var newHpString: String = BigInt.bigInt2str(newHp, 10);
			if (maxHpString.length > 9)
				maxHpString = maxHpString.slice(0, 9);
			if (newHpString.length > 9)
				newHpString = newHpString.slice(0, 9);
			var portion: Number = Misc.RoundToDecimalPlaces(Number(newHpString) / Number(maxHpString),2);
			_healthBar.width = _healthBarTexture.width * portion;
			_healthBar.setTexCoordsTo(1, portion, 0);
			_healthBar.setTexCoordsTo(3, portion, 1);
		}
		
	}

}