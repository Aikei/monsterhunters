package view.gameviewelements 
{
	import com.greensock.TweenLite;
	import d_hentity.HEntity;
	import feathers.text.BitmapFontTextFormat;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextFieldAutoSize;
	import starling.textures.Texture;
	import system.Analytics;
	import system.HSounds;
	import view.GameView;
	/**
	 * ...
	 * @author Aikei
	 */
	public class BackgroundProgressWindow extends HEntity 
	{
		//! may be set in script
		public static var COIN_AND_GLORY_ICONS_Y: Number = 386;		
		public static var COIN_ICON_X: Number = 84;					
		public static var GLORY_ICON_X: Number = 214;				
		public static var GOLD_VALUE_TEXT_X: Number = 128;
		public static var GOLD_VALUE_TEXT_Y: Number = 386;
		public static var GLORY_VALUE_TEXT_X: Number = 255;
		public static var GLORY_VALUE_TEXT_Y: Number = 386;		
		public static var SIR_TEXT_CENTER_X: Number = 186;
		public static var SIR_TEXT_CENTER_Y: Number = 248;
		public static var SHARE_BUTTON_X: Number = 66;
		public static var SHARE_BUTTON_Y: Number = 458;
		public static var COMMENT_TEXT_X: Number = 49;
		public static var COMMENT_TEXT_Y: Number = 300;
		public static var CLOSE_BUTTON_X: Number = 330;
		public static var CLOSE_BUTTON_Y: Number = 6;
		public static var COIN_MICRO_X: Number = 156;
		public static var COIN_MICRO_Y: Number = 520;
		public static var BONUS_VALUE_TEXT_X: Number = 181;
		public static var BONUS_VALUE_TEXT_Y: Number = 516;
		public static var TIME_LEFT_TEXT_CENTER_X: Number = 183;
		public static var TIME_LEFT_TEXT_CENTER_Y: Number = 529;
		public static var SHARE_ALLOWED_PERIOD: Number = Misc.DAY_IN_SECONDS;
		
		public static var WINDOW_CENTER_X: Number = Preferences.SCREEN_WIDTH/2;
		public static var WINDOW_CENTER_Y: Number = Preferences.SCREEN_HEIGHT/2;		
		
		public static var TIME_CHECK_INTERVAL: Number = 800;
		
		private static const SHOW_PHASE_BONUS_VALUE: int = 0;
		private static const SHOW_PHASE_TIME_LEFT: int = 1;
		
		private var _showPhase: int = 0;
		
		private var _holder: HEntity = new HEntity;
		private var _nonInteractiveHolder: HEntity = new HEntity;
		
		private var _image: Image = new Image(Assets.interfaceAtlas.getTexture("bg_report_body"));
		private var _shareButton: HButton = new HButton;
		private var _closeButton: HButton = new HButton;
		private var _sirText: HText = new HText("", "bbold24", -1);
		private var _commentText: HText = new HText("", "bbold15bgProgress", -1, 0xffffff, TextFieldAutoSize.VERTICAL);
		private var _coinBig: Image = new Image(Assets.interfaceAtlas.getTexture("coin_big"));
		//private var _gloryBig: Image = new Image(Assets.interfaceAtlas.getTexture("renown_big"));
		private var _goldValueText: HText = new HText("", "bbold24", -1);
		//private var _gloryValueText: HText = new HText("", "bbold24", -1);						
		private var _shadyBg: Image = new Image(Texture.fromEmbeddedAsset(Assets.SHADY_BG_TEXTURE));		
		private var _shareBonusCoinIcon: Image = new Image(Assets.interfaceAtlas.getTexture("coin_micro"));
		private var _shareBonusValueText: HText = new HText("", "bbold15", -1, 0xffffff);
		private var _shareBonus: BigInt;
		private var _timeLeftText: HText = new HText("", "bbold15", -1, 0x818080);
		private var _shareTime: Number = 0;
		private var _shared: Boolean = false;
		private var _timeLeft: Number = 0;
		private var _timeCheckInterval: uint = 0;
		private var _goldReceived: BigInt = null;
		//private var _gloryReceived: uint = 0;
		private var _canGetBonus: Boolean = true;
		private var _squadName: String;
		
		public function BackgroundProgressWindow() 
		{
			super(false, false);
			this.visible = false;
			_holder.addChild(_nonInteractiveHolder);
			_nonInteractiveHolder.touchable = false;
			_holder.x = Math.round(WINDOW_CENTER_X - _image.texture.width / 2);
			_holder.y = Math.round(WINDOW_CENTER_Y - _image.texture.height / 2);
			
			_sirText.x = SIR_TEXT_CENTER_X;
			_sirText.y = SIR_TEXT_CENTER_Y;
			
			_shareButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("bg_report_button"));
			_shareButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("bg_report_button"));
			_shareButton.hoverSkin.filter = Filters.highlightFilter;
			//_shareButton.labelFactory = 
			_shareButton.defaultLabelProperties.textFormat = new BitmapFontTextFormat("bbold24buttons", 24, 0x7c3a12);
			_shareButton.label = Language.GetString("Share").toUpperCase();
			_shareButton.x = SHARE_BUTTON_X;
			_shareButton.y = SHARE_BUTTON_Y;
			_shareButton.addEventListener(Event.TRIGGERED, OnShareButtonTriggered);
			
			_closeButton.defaultSkin = new Image(Assets.interfaceAtlas.getTexture("close_button"));
			_closeButton.hoverSkin = new Image(Assets.interfaceAtlas.getTexture("close_button"));
			_closeButton.hoverSkin.filter = Filters.highlightFilter;	
			_closeButton.x = CLOSE_BUTTON_X;
			_closeButton.y = CLOSE_BUTTON_Y;
			_closeButton.addEventListener(Event.TRIGGERED, OnCloseButtonTriggered);
			
			_commentText.x = COMMENT_TEXT_X;
			_commentText.y = COMMENT_TEXT_Y;
			_commentText.textField.width = 276;
			
			_coinBig.x = COIN_ICON_X;
			_coinBig.y = COIN_AND_GLORY_ICONS_Y;
			
			//_gloryBig.x = GLORY_ICON_X;
			//_gloryBig.y = COIN_AND_GLORY_ICONS_Y;			
			
			_goldValueText.x = GOLD_VALUE_TEXT_X;
			_goldValueText.y = GOLD_VALUE_TEXT_Y;
			
			//_gloryValueText.x = GLORY_VALUE_TEXT_X;
			//_gloryValueText.y = GLORY_VALUE_TEXT_Y;
			
			_shareBonusCoinIcon.x = COIN_MICRO_X;
			_shareBonusCoinIcon.y = COIN_MICRO_Y;
			
			_shareBonusValueText.x = BONUS_VALUE_TEXT_X;
			_shareBonusValueText.y = BONUS_VALUE_TEXT_Y;
			
			_timeLeftText.visible = false;
			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			//_shadyBg.touchable = false;
		}
		
		private function SetShowPhase(phase: int):void 
		{
			if (phase !== _showPhase)
			{
				_showPhase = phase;
				if (_showPhase === SHOW_PHASE_TIME_LEFT)
				{
					//_shareButton.isEnabled = false;
					_canGetBonus = false;
					_timeLeftText.visible = true;
					_shareBonusCoinIcon.visible = false;
					_shareBonusValueText.visible = false;
				}
				else if (_showPhase === SHOW_PHASE_BONUS_VALUE)
				{
					//_shareButton.isEnabled = true;
					_canGetBonus = true;
					_timeLeftText.visible = false;
					_shareBonusCoinIcon.visible = true;
					_shareBonusValueText.visible = true;					
				}
			}
		}
		
		private function ProcessTimeLeft():void 
		{
			_timeLeft = SHARE_ALLOWED_PERIOD - (Misc.GetServerTime() - _shareTime);
			if (_shareTime !== 0 && _timeLeft > 0)
			{
				SetShowPhase(SHOW_PHASE_TIME_LEFT);
				var timeObject: Object = Misc.ParseTime(_timeLeft);
				var str: String = Language.ParsedTimeObjectToString(timeObject);
				_timeLeftText.text = str;
				Misc.CenterObject(_timeLeftText, TIME_LEFT_TEXT_CENTER_X, TIME_LEFT_TEXT_CENTER_Y);
			}
			else
			{
				SetShowPhase(SHOW_PHASE_BONUS_VALUE);
				clearInterval(_timeCheckInterval);				
			}
		}
		
		//private function OnShadyBgTouched(e:TouchEvent):void 
		//{
			//var touch: Touch = e.getTouch(_shadyBg);
			//if (touch && touch.phase === TouchPhase.ENDED)
			//{
				//CloseWindow();
			//}
		//}
		
		public function Init(gameDataObject: Object):void 
		{
			if (gameDataObject.bgp === null)
				return;
			if (World.gWorld.socialProfile.sex === "m")
				_sirText.text = Language.GetString("Sir")+"!";
			else
				_sirText.text = Language.GetString("Dame") + "!";
			_sirText.x = SIR_TEXT_CENTER_X - _sirText.width / 2;
			var str: String = Language.GetString("BgReportComment");
			_squadName = Language.ProcessSquadName(gameDataObject.groupName);
			str = str.replace("$sq", _squadName);
			//str = str.replace("$num", BigInt.str2HString(gameDataObject.bgp.killed));
			_commentText.text = str;
			_goldReceived = BigInt.shStr2BigInt(gameDataObject.bgp.gold);
			//_gloryReceived = uint(gameDataObject.bgp.glory);
			//World.gameController.AddGlory(glory);
			//World.gameController.AddCoins(gold);
			//_goldValueText.text = "+" + BigInt.str2HString(gameDataObject.bgp.gold);
			//_gloryValueText.text = "+" + BigInt.str2HString(gameDataObject.bgp.glory);
			
			_shareTime = gameDataObject.lastShare;
			_shareBonus = BigInt.divide(BigInt.shStr2BigInt(gameDataObject.bgp.gold), BigInt.shStr2BigInt("10"));
			_shareBonusValueText.text = "+" + BigInt.bigint2HString(_shareBonus);
			_timeCheckInterval = setInterval(ProcessTimeLeft,TIME_CHECK_INTERVAL);
			this.visible = true;
			this.alpha = 0;
			TweenLite.to(this, 0.5, { alpha: 1 } );
			HSounds.PlaySound("FadeIn");
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_shadyBg);
			addChild(_holder);
			_nonInteractiveHolder.addChild(_image);
			_holder.addChild(_shareButton);
			_holder.addChild(_closeButton);
			_nonInteractiveHolder.addChild(_coinBig);
			//_nonInteractiveHolder.addChild(_gloryBig);
			_nonInteractiveHolder.addChild(_shareBonusCoinIcon);
			_nonInteractiveHolder.addChild(_sirText);
			_nonInteractiveHolder.addChild(_commentText);			
			_nonInteractiveHolder.addChild(_goldValueText);			
			//_nonInteractiveHolder.addChild(_gloryValueText);
			_nonInteractiveHolder.addChild(_shareBonusValueText);
			_nonInteractiveHolder.addChild(_timeLeftText);
		}
		
		private function OnCloseButtonTriggered(e:Event):void 
		{
			CloseWindow();
		}
		
		private function CloseWindow():void 
		{
			clearInterval(_timeCheckInterval);
			if (!_shared)
			{
				if (_timeLeft == 0)
					Analytics.TrackEvent("Background Report", "Closed Without Share");
				else
					Analytics.TrackEvent("Background Report", "Closed Without Share While Impossible to Share");
			}
				
			this.visible = false;
			//GameView.gView.CreateBonusGlory(_gloryReceived, 70, 175, 215, false);
			GameView.gView.CreateBonusGold(_goldReceived, 70, 175, 215, false);
		}
		
		private function MakeInactive(inactive: Boolean):void 
		{
			if (inactive)
				this.touchable = false;
			else
				this.touchable = true;
		}
		
		private function OnShareButtonTriggered(e:Event):void 
		{
			MakeInactive(true);
			World.gWorld.addEventListener("backgroundShareComplete", OnShareComplete);
			World.gWorld.addEventListener("backgroundShareFail", OnShareFail);
			World.gameController.ShareBackgroundProgressReport(_goldValueText.text, "", _squadName);
		}
		
		private function OnShareFail(e:Event):void 
		{
			World.gWorld.removeEventListener("backgroundShareComplete", OnShareComplete);
			World.gWorld.removeEventListener("backgroundShareFail", OnShareFail);			
			MakeInactive(false);
			_shareBonusCoinIcon.visible = false;
			_shareBonusValueText.visible = false;
			_timeLeftText.text = Language.GetString("ErrorOccured");
			_timeLeftText.visible = true;
			Misc.CenterObject(_timeLeftText, TIME_LEFT_TEXT_CENTER_X, TIME_LEFT_TEXT_CENTER_Y);			
		}
		
		private function OnShareComplete(e:Event):void 
		{
			World.gWorld.removeEventListener("backgroundShareComplete", OnShareComplete);
			World.gWorld.removeEventListener("backgroundShareFail", OnShareFail);
			_shared = true;
			if (_canGetBonus)
				World.gameController.AddCoins(_shareBonus);
			_shareTime = Number(e.data);
			MakeInactive(false);
			CloseWindow();
			//SetShowPhase(SHOW_PHASE_TIME_LEFT);
			//setInterval(ProcessTimeLeft,TIME_CHECK_INTERVAL);			
		}
		
	}

}