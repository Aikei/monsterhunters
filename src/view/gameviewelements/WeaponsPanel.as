package view.gameviewelements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import starling.events.Event;
	import logic.Weapon;
	import view.TipElement;
	
	public class WeaponsPanel extends BaseVerticalScrollContainer 
	{
		private var _weapons: Vector.<WeaponPanelItem> = new Vector.<WeaponPanelItem>;
		
		public function WeaponsPanel() 
		{
			super();
			//visible = false;
			
		}
		
		override protected function OnAddedToStage(event: Event): void 
		{
			super.OnAddedToStage(event);
			//var tip: TipElement = new TipElement(Language.GetString("TipWeapons"));
			//addChild(tip);			
			for (var i: int = 0; i < Weapon.NUMBER_OF_WEAPONS; i++)
			{
				_weapons.push(new WeaponPanelItem(i));
			}
		}
		
		public function Init(game: Object):void 
		{
			for (var i: int = 0; i < game.currentWeapon.type; i++)
				_weapons[i].SetOld();
				//_weapons[i].Destroy();
			//_weapons.splice(0, game.currentWeapon.type);
			for (i = 0; i < _weapons.length; i++)
			{
				_weapons[i].Init(World.data.weapons[i].Copy(),game);
				addChild(_weapons[i]);
			}
		}
		
	}

}