package view.gameviewelements 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import starling.events.Event;
	import logic.HeroData;
	import view.TipElement;
	
	public class HeroPanel extends BaseVerticalScrollContainer 
	{
		private var _items: Vector.<HeroPanelItem> = new Vector.<HeroPanelItem>;
		
		public function HeroPanel() 
		{
			super();
			visible = false;
		}
				
		override protected function OnAddedToStage(event: Event): void 
		{
			super.OnAddedToStage(event);
			var tip: TipElement = new TipElement(Language.GetString("TipHeroes"));
			addChild(tip);				
			for (var i: int = 0; i < HeroData.NUMBER_OF_HEROES; i++)
			{
				_items.push(new HeroPanelItem(i));
				addChild(_items[i]);
			}		
		}
		
		public function Init(game: Object):void 
		{		
			for (var i: int = 0; i < 6; i++)
			{
				_items[i].Init(game);
			}				
		}
				
	}

}