package view.gameviewelements 
{
	import d_hentity.HEntity;
	import flash.geom.Rectangle;
	import logic.Dungeon;
	import hevents.*;
	import starling.display.Image;
	import starling.events.Event;
	import flash.utils.setTimeout;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.text.TextFieldAutoSize;
	import com.greensock.*;
	/**
	 * ...
	 * @author Aikei
	 */
	public class LevelPanel extends HEntity 
	{
		public static const WIDTH: int = 428;
		public static const CENTER_POSITION: int = 3;
		
		public static var LEVEL_PANEL_X: Number = 39;
		public static var LEVEL_PANEL_Y: Number = 619;
		
		public static const firstSpacing: int = 40;
		public static const interSpacing: int = 59;		
		public static var rightEndX: Number;
		public static var currentMaxLevel: uint = 1;
		
		//public static const firstSpacing: int = 24;
		//public static const interSpacing: int = 48;
		
		protected var currentLevel: int;
		
		//protected var _groupImage: Image = new Image(Assets.interfaceAtlas.getTexture("map_group"));
		protected var _dotsLayer: HEntity = new HEntity;
		protected var _dotsTextLayer: HEntity = new HEntity;
		protected var _topLayer: HEntity = new HEntity;		
		
		public function LevelPanel() 
		{
			super();
			x = LEVEL_PANEL_X;
			y = LEVEL_PANEL_Y;
			rightEndX = 486 - LEVEL_PANEL_X;
			//this.clipRect = new Rectangle(0, -50, WIDTH, 100);
			World.gWorld.addEventListener(L_HEventDungeonLevelSet.L_HEVENT_DUNGEON_LEVEL_SET, OnDungeonLevelSet);
			World.gWorld.addEventListener(HEventNextLevel.HEVENT_NEXT_LEVEL, OnNextLevel);
			World.gWorld.addEventListener("changeLevel", OnChangeLevel);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		//override public function OnTouch(event: TouchEvent):void 
		//{
			//var t: Touch = event.getTouch(this);
			//for (var i: int = 0; i < _dotsLayer.numChildren; i++)
			//{
				//var dot: LevelDot = LevelDot(_dotsLayer.getChildAt(i));
				//if (dot.position < CENTER_POSITION)
					//dot.OnTouch(event);
			//}
		//}
		
		protected function CreateDot(dotX: Number, level: int, position: int, visible: Boolean = true): LevelDot 
		{
			if (level <= 0)
				return null;
			var dot: LevelDot = LevelDot.Get(position,level,dotX);
			//dot.visible = visible;
			_dotsLayer.addChild(dot);
			
			return dot;
			
			//var txt: LevelDotText = LevelDotText.Get(level, dotX, dot.y);
			//txt.visible = visible;
			//_dotsTextLayer.addChild(txt);
			//return { dot: dot, txt: txt };
		}
		
		protected function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_dotsLayer);
			addChild(_dotsTextLayer);
			addChild(_topLayer);
			//_groupImage.alignPivot();
			//_groupImage.x = firstSpacing + CENTER_POSITION * interSpacing;
			//_topLayer.addChild(_groupImage);
		}
		
		protected function OnNextLevel(event: HEventNextLevel): void 
		{
			currentLevel++;
			if (currentLevel > currentMaxLevel)
				currentMaxLevel = currentLevel;
			MoveDots(1);
			CheckVisited();
		}
		
		protected function OnChangeLevel(event: Event): void 
		{
			var diff: int = event.data.level - currentLevel;
			currentLevel = event.data.level;
			MoveDots(diff);
			CheckVisited();
		}
		
		protected function CheckVisited():void 
		{
			for (var i: int = 0; i < _dotsLayer.numChildren; i++)
			{
				var dot: LevelDot = LevelDot(_dotsLayer.getChildAt(i));
				if (dot.level <= currentMaxLevel)
					dot.SetVisited(true);
				else
					dot.SetVisited(false);
				if (dot.level == currentLevel)
				{
					dot.SetCurrent(true);
				}
				else
				{
					dot.SetCurrent(false);
				}
				dot.CheckCurrent();
			}
		}
		
		//protected function MoveDots():void 
		//{
			//var toX: Number;
			//for (var i: int = 0; i < _dotsLayer.numChildren; i++)
			//{
				//var dot: LevelDot = LevelDot(_dotsLayer.getChildAt(i));
				//dot.position--;
				//toX = dot.x - interSpacing;
				//TweenLite.to(dot, 1.0, { x: toX } );			
			//}
			//for (i = 0; i < _dotsTextLayer.numChildren; i++)
			//{
				//var txt: LevelDotText = LevelDotText(_dotsTextLayer.getChildAt(i));
				//toX = txt.x - interSpacing;
				//TweenLite.to(txt, 1.0, { x: toX } );			
			//}			
			//var obj: Object = CreateDot(firstSpacing + ((CENTER_POSITION * 2 + 1) * interSpacing), currentLevel + CENTER_POSITION, CENTER_POSITION * 2, false);	
			//toX = obj.dot.x - interSpacing;
			//TweenLite.to(obj.dot, 1.0, { x: toX } );
			//TweenLite.to(obj.txt, 1.0, { x: toX } );
		//}		
		
		protected function MoveDots(by: int):void 
		{
			var toX: Number;
			for (var i: int = 0; i < _dotsLayer.numChildren; i++)
			{
				var dot: LevelDot = LevelDot(_dotsLayer.getChildAt(i));
				if (!dot.alive)
					continue;
				dot.position -= by;
				toX = dot.x - interSpacing * by;
				dot.inMove = true;
				TweenLite.to(dot, 1.0, { x: toX, onComplete: 
					function(d: LevelDot) 
					{
						d.inMove = false;
						d.MaybeDie();
						Misc.RoundPosition(d);
					}
					, onCompleteParams: [dot] } );			
			}
			
			//for (i = 0; i < _dotsTextLayer.numChildren; i++)
			//{
				//var txt: LevelDotText = LevelDotText(_dotsTextLayer.getChildAt(i));
				//toX = txt.x - interSpacing;
				//TweenLite.to(txt, 1.0, { x: toX } );			
			//}
			var countTo: int = by;
			if (countTo < 0)
				countTo = -countTo;
			var j: int;
			for (i = 1, j = countTo; i <= countTo; i++, j--)
			{
				var ci: int = i;
				var cj: int = j;
				if (by < 0)
				{
					ci = -i;
					cj = -j;
				}
				
				if (by < 0)
				{
					dot = CreateDot(firstSpacing + (ci * interSpacing), currentLevel - by - CENTER_POSITION+ci, ci, false);
				}
				else
				{
					var pos: Number = (CENTER_POSITION * 2 + ci);
					dot = CreateDot(firstSpacing + (pos * interSpacing), currentLevel - by + CENTER_POSITION+ci, CENTER_POSITION * 2-1+ci, false);
				}
				if (dot)
				{
					toX = dot.x - interSpacing * by;
					dot.inMove = true;
					TweenLite.to(dot, 1.0, { x: toX, onComplete: 
						function(d: LevelDot) 
						{
							d.inMove = false;
							d.MaybeDie();
							Misc.RoundPosition(d);
						}
						, onCompleteParams: [dot] } );
				}
			}
			//TweenLite.to(obj.txt, 1.0, { x: toX } );
		}
		
		public function Init(dungeon: Dungeon): void 
		{
			currentMaxLevel = dungeon.maxLevel;
			for (var i: int = 0; i < _dotsLayer.numChildren; i++)
			{
				LevelDot(_dotsLayer.getChildAt(i)).Die();
			}
			for (i = 0; i < _dotsTextLayer.numChildren; i++)
			{
				LevelDotText(_dotsTextLayer.getChildAt(i)).Die();
			}			
			currentLevel = dungeon.level;
			for (i = CENTER_POSITION; i < CENTER_POSITION+4; i++)
			{
				CreateDot(firstSpacing + i * interSpacing, dungeon.level + i - CENTER_POSITION, i);
			}
			for (var j: int = 1, k: int = CENTER_POSITION-1; k >= 0; k--, j++)
			{
				if (dungeon.level - j <= 0)
					break;
				CreateDot(firstSpacing + k * interSpacing, dungeon.level - j, k);
			}
			CheckVisited();
		}
		
		protected function OnDungeonLevelSet(event: L_HEventDungeonLevelSet): void 
		{
			Init(event.dungeon);
		}
		
	}

}