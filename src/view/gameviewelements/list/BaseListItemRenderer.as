package view.gameviewelements.list 
{
	import feathers.controls.List;
	import feathers.core.FeathersControl;
	import feathers.controls.renderers.IListItemRenderer;
	import starling.events.Event;
	/**
	 * ...
	 * @author Aikei
	 */
	public class BaseListItemRenderer extends FeathersControl implements IListItemRenderer
	{
		protected var _index: int = -1;
		
		public function get index(): int 
		{
			return _index;
		}
		
		public function set index(value:int):void
		{
			if(_index == value)
			{
				return;
			}
			_index = value;
			invalidate(INVALIDATION_FLAG_DATA);
		}
		
		protected var _owner: List;
		 
		public function get owner(): List
		{
			return this._owner;
		}
		 
		public function set owner(value: List):void
		{
			if(_owner == value)
			{
				return;
			}
			_owner = value;
			invalidate(INVALIDATION_FLAG_DATA);
		}
		
		protected var _data:Object;
		 
		public function get data():Object
		{
			return _data;
		}
		 
		public function set data(value:Object):void
		{
			if(_data == value)
			{
				return;
			}
			_data = value;
			invalidate(INVALIDATION_FLAG_DATA);
		}
		
		protected var _isSelected:Boolean;
		 
		public function get isSelected():Boolean
		{
			return this._isSelected;
		}
		 
		public function set isSelected(value:Boolean):void
		{
			if(this._isSelected == value)
			{
				return;
			}
			_isSelected = value;
			invalidate(INVALIDATION_FLAG_SELECTED);
			dispatchEventWith(Event.CHANGE);
		}
		
		override protected function draw():void
		{
			var dataInvalid:Boolean = this.isInvalid(INVALIDATION_FLAG_DATA);
		 
			if(dataInvalid)
			{
				commitData();
			}
			
			autoSizeIfNeeded();
			layoutChildren();
		}
		
		protected var _padding:Number = 0;
		 
		public function get padding():Number
		{
			return _padding;
		}
		 
		public function set padding(value:Number):void
		{
			if(_padding == value)
			{
				return;
			}
			_padding = value;
			invalidate(INVALIDATION_FLAG_LAYOUT);
		}
						
		//override protected function initialize():void
		//{
			//this._label = new Label();
			//this.addChild(this._label);
		//}	
		
		protected function commitData():void
		{
			
		}
		
		//protected function commitData():void
		//{
			//if(this._data)
			//{
				//this._label.text = this._data.label;
			//}
			//else
			//{
				//this._label.text = null;
			//}
		//}		
		
		protected function autoSizeIfNeeded(): Boolean 
		{
			return false;
		}
		
		//protected function autoSizeIfNeeded():Boolean
		//{
			//var needsWidth:Boolean = isNaN(this.explicitWidth);
			//var needsHeight:Boolean = isNaN(this.explicitHeight);
			//if(!needsWidth && !needsHeight)
			//{
				//return false;
			//}
			//this._label.width = this.explicitWidth - 2 * this._padding;
			//this._label.height = this.explicitHeight - 2 * this._padding;
			//this._label.validate();
			//var newWidth:Number = this.explicitWidth;
			//if(needsWidth)
			//{
				//newWidth = this._label.width + 2 * this._padding;
			//}
			//var newHeight:Number = this.explicitHeight;
			//if(needsHeight)
			//{
				//newHeight = this._label.height + 2 * this._padding;
			//}
			//return this.setSizeInternal(newWidth, newHeight, false);			
		//}
		
		protected function layoutChildren():void
		{
			
		}
		
		//protected function layoutChildren():void
		//{
			//this._label.x = this._padding;
			//this._label.y = this._padding;
			//this._label.width = this.actualWidth - 2 * this._padding;
			//this._label.height = this.actualHeight - 2 * this._padding;
		//}		
	}

}