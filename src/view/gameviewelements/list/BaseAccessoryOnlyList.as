package view.gameviewelements.list 
{
	import starling.display.DisplayObject;
	import view.gameviewelements.list.BaseItemList;
	
	/**
	 * ...
	 * @author Aikei
	 */
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.renderers.IListItemRenderer;
	
	public class BaseAccessoryOnlyList extends BaseItemList 
	{
		
		public function BaseAccessoryOnlyList() 
		{
			super();
			this.itemRendererFactory = function():IListItemRenderer
			{
				var renderer:DefaultListItemRenderer = new DefaultListItemRenderer();
				renderer.labelFunction = function ():String { return ""; }
				renderer.itemHasIcon = false;
				renderer.itemHasAccessory = true;
				renderer.accessoryFunction = function (item: Object):DisplayObject 
				{
					return DisplayObject(item);
				}
				return renderer;
			}	
			this.isSelectable = false;			
		}
		
	}

}