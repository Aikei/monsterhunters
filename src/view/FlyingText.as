package view 
{
	import d_hentity.HEntity;
	import flash.utils.Dictionary;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.Color;
	import starling.text.TextFieldAutoSize;
	import flash.utils.setTimeout;
	import com.greensock.*;
	/**
	 * ...
	 * @author Aikei
	 */
	public class FlyingText extends HEntity 
	{
		private var _text: TextField;
		
		private static var _pools: Dictionary = new Dictionary;
		
		private var _timeToDieInMs: Number;
		private var _speed: Number = 250;
		private var _jumping: Boolean = false;
		private var _fontName: String;
		
		public function FlyingText(fontName: String)
		{
			super(true);
			_fontName = fontName;
			touchable = false;
			_text = new TextField(0, 0, "", fontName, -1, Color.WHITE);
			_text.batchable = true;
			_text.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
			alignPivot();
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		private function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_text);
		}
		
		public static function GetFlyingText(text: String, timeToDieInMs: Number, fontName: String, color: uint): FlyingText 
		{
			var flyingText: FlyingText;
			if (!_pools[fontName] || _pools[fontName].length === 0)
			{
				flyingText = new FlyingText(fontName);
			}
			else
			{
				flyingText = _pools[fontName].pop();				
			}
			flyingText.Init(text, timeToDieInMs, color);
			return flyingText;
		}
		
		public function SetSpeed(speed: Number): void 
		{
			_speed = speed;
		}
		
		public function MakeJumping(jumping: Boolean = true): void 
		{
			StopMyMovementAndAcceleration();
			ApplyGravity(1500, 2000);
			ApplyRelativeVelocity(new Vec2(0.2, -1), 700);	
		}
		
		public function MakeDisappearing():void 
		{
			StopMyMovementAndAcceleration();
			TweenLite.to(this, _timeToDieInMs, { alpha: 0 } );
		}
		
		private function Init(text: String, timeToDieInMs: Number, color: uint): void 
		{
			visible = true;
			touchable = false;
			
			//_text = new TextField(0, 0, text, font, fontSize, color);
			//_text.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
			
			_text.color = color;
			_text.text = text;
			
			_timeToDieInMs = timeToDieInMs;
			_text.alignPivot();
			ApplyRelativeVelocity(new Vec2(0, -1), _speed);
			setTimeout(Die, _timeToDieInMs);			
		}
		
		private function Die(): void 
		{
			StopMyMovementAndAcceleration();
			alpha = 1;
			_jumping = false;
			visible = false;
			if (!_pools[_fontName])
				_pools[_fontName] = new Vector.<FlyingText>;
			_pools[_fontName].push(this);
		}
		
	}

}