package view 
{
	import d_hentity.HEntity;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.text.TextFieldAutoSize;
	import view.gameviewelements.HText;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class TipElement extends HEntity 
	{
		private var _image: Image = new Image(Assets.interfaceAtlas.getTexture("tip"));
		private var _bitmapText: BitmappedText;
		//private var _text: HText;
		//private var _textSprite: Sprite;
		
		public function TipElement(text: String) 
		{
			super(false, false);
			
			_bitmapText = new BitmappedText();
			_bitmapText.x = 17;
			_bitmapText.y = 13;			
			_bitmapText.Init(text, "bregular13", 0xbf7566, _image.width - _bitmapText.x*2, 0, "left", "top", "vertical");

			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			//var t: TextField = new TextField(_image.width - 28, 0, "", "bregular13", -1, 0xbf7566);
			//t.autoSize = TextFieldAutoSize.BOTH_DIRECTIONS;
			//t.text = text;
			//_textSprite = Assets.bregular13.createSprite(t.width, t.height, text, -1, 0xbf7566, "left", "top", false);
			//_textSprite.x = 14;
			//_textSprite.y = 16;			
			//t.dispose();
			//t = null;
			
			//_text = new HText("", "bregular13", -1, 0xbf7566, "vertical");
			//_text.x = 14;
			//_text.y = 16;
			//_text.textField.width = _image.width - 28;
			//_text.textField.hAlign = "left";
			//_text.textField.text = text;
		}
		
		private function addedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			addChild(_image);
			addChild(_bitmapText);
			//addChild(_textSprite);
			flatten();
		}
		
	}

}