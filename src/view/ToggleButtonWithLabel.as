package view 
{
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.controls.ToggleButton;
	import feathers.core.ITextRenderer;
	import feathers.text.BitmapFontTextFormat;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class ToggleButtonWithLabel extends ToggleButton 
	{
		
		public function ToggleButtonWithLabel() 
		{
			super();

		}
		
		public function InitLabelFactory(fontName: String, fontSize: Number, fontColor: uint):void 
		{
			this.labelFactory = function (): ITextRenderer 
			{
				var textRenderer: BitmapFontTextRenderer = new BitmapFontTextRenderer();
				//textRenderer.textFormat = new BitmapFontTextFormat(fontName, fontSize, fontColor);
				//textRenderer.embedFonts = true;
				return textRenderer;
			}			
		}
		
	}

}