package view {
	/**
	 * ...
	 * @author Aikei
	 */
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import controller.lsevents.LS_HEventMonsterClickHit;
	import d_hentity.HEntity;
	import flash.ui.Mouse;
	import flash.utils.Dictionary;
	import hevents.L_HEventClickHit;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.events.*;
	import starling.core.Starling;
	import logic.MonsterData;
	import system.HColorFilter;
	import system.HCursor;
	import flash.utils.setTimeout;
	import flash.utils.clearTimeout;
	import starling.utils.Color;
	import system.HSounds;
	import view.gameviewelements.MonsterSpeechBubble;
	
	public class MonsterSprite extends HEntity
	{	
		public static var StunMovie: MovieClip;
		
		public static var _pool: Vector.<MonsterSprite> = new Vector.<MonsterSprite>;
		
		public static var SPEAK_CHANCE: Number = 50;
		public static var HARM_FPS_MULTIPLIER: Number = 2;
		public static var BASE_STAND_FPS: Number = 18;
		public static var HARM_FPS: Number = BASE_STAND_FPS * HARM_FPS_MULTIPLIER;
		public static var X6_MULTIPLIER: Number = 5;
		public static var X6_STAND_FPS: Number = BASE_STAND_FPS * X6_MULTIPLIER;				
		public static var START_X: Number = 210;
		public static var START_Y: Number = 484;		
		public static var STUN_APPEAR_DISAPPEAR_TIME: Number = 0.5;
		
		public static const STATE_STANDING: int = 0;
		public static const STATE_HARMED: int = 1;
		public static const NUMBER_OF_STATES: int = 2;

		private var _type: int;
		private var _hovered: Boolean = false;
		private var _state: int = STATE_STANDING;
		private var _alive: Boolean = true;
		
		private var _clips: Vector.<MovieClip> = new Vector.<MovieClip>;
		private var _reverseClips: Vector.<MovieClip> = new Vector.<MovieClip>;
		private var _lastHitTime: Number;
		private var _timeoutId: uint = 0;
		private var _modelName: String = "";
		private var _reverse: Boolean = false;
		private var _inTransition: Boolean = false;
		private var _baseWidth: Number = 0;
		private var _baseHeight: Number = 0;
		private var _rarity: int = 0;
		private var _monsterSpeechBubble: MonsterSpeechBubble = new MonsterSpeechBubble;
		private var _stunned: Boolean = false;
		private var _dieTween1: TweenLite;
		private var _dieTween2: TweenLite;
		private var _shakeTween: TweenMax = null;
		private var _tweens: Vector.<TweenLite> = new Vector.<TweenLite>;
		
		public var newlyCreated: Boolean = true;
		
		private var _quad: Quad;
		
		static public var sMonsterModelScales: Dictionary = new Dictionary;
		static public var sCommonMonsterScale: Number = 0.75;
		static public var sRareMonsterScale: Number = 1.0;
		static public var sEliteMonsterScale: Number = 1.0;
		
		public function get modelName(): String 
		{
			return _modelName;
		}
		
		public function get rarity(): int 
		{
			return _rarity;
		}
		
		override public function get width(): Number 
		{
			return _clips[0].width;
		}
		
		override public function get height(): Number 
		{
			return _clips[0].height;
		}		
		
		public function set reverse(r: Boolean):void 
		{
			if (!_stunned && r != _reverse)
			{
				_reverse = r;
				var v: Vector.<MovieClip> = _clips;
				_clips = _reverseClips;
				_reverseClips = v;
				for (var i: int = 0; i < _clips.length; i++)
				{
					_clips[i].stop();
					_clips[i].visible = false;
					_reverseClips[i].stop();
					_reverseClips[i].visible = false;
				}
				_clips[_state].visible = true;
				_clips[_state].play();
			}
		}
		
		private function PlayState(newState: int):void 
		{
			_clips[_state].stop();
			_clips[_state].visible = false;
			_clips[newState].play();
			_clips[newState].visible = true;
		}
		
		public function ChangeRarityScale(rarityScale: Number): void 
		{
			var scale: Number = sMonsterModelScales[_modelName]*rarityScale;			
			for (var i: int = 0; i < _clips.length; i++)
			{
				_clips[i].width = _baseWidth * scale;
				_clips[i].height = _baseHeight * scale;
				_reverseClips[i].width = _baseWidth * scale;
				_reverseClips[i].height = _baseHeight * scale;
			}
			x = START_X;
			y = START_Y;
			x -= _clips[0].width / 2;
			y -= _clips[0].height;			
		}
		
		public function ChangeModelScale(modelScale: Number): void 
		{
			var scale: Number;
			if (rarity == MonsterData.RARITY_COMMON)
				scale = sCommonMonsterScale;
			else if (rarity == MonsterData.RARITY_RARE)
				scale = sRareMonsterScale;
			else if (rarity == MonsterData.RARITY_ELITE)
				scale = sEliteMonsterScale;				
			scale *= modelScale;
			
			for (var i: int = 0; i < _clips.length; i++)
			{
				_clips[i].width = _baseWidth * scale;
				_clips[i].height = _baseHeight * scale;
				_reverseClips[i].width = _baseWidth * scale;
				_reverseClips[i].height = _baseHeight * scale;
			}
			x = START_X;
			y = START_Y;
			x -= _clips[0].width / 2;
			y -= _clips[0].height;			
		}		
		
		public function get reverse(): Boolean 
		{
			return _reverse;
		}
		
		public function get type(): int
		{
			return _type;
		}
		
		public function MonsterSprite() 
		{
			//addChild(_monsterSpeechBubble);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			World.gWorld.addEventListener("stunMonster", OnStunMonster);
			World.gWorld.addEventListener("stunEnded", OnStunEnded);
			InitTouch();
		}
		
		private function OnStunEnded(e:Event):void 
		{			
			StunMovie.stop();
			StunMovie.alpha = 1;
			_tweens.push(TweenLite.to(StunMovie, STUN_APPEAR_DISAPPEAR_TIME, { alpha: 0, onComplete: function ():void 
			{
				StunMovie.visible = false;
			} } ));
			
			_stunned = false;
			SetState(STATE_STANDING, 0);
			_clips[_state].play();
		}
		
		private function OnStunMonster(e:Event):void 
		{
			if (_shakeTween)
			{
				_shakeTween.kill();
				_shakeTween = null;
			}
			_shakeTween = Tweens.StunShake(this);			
			if (!_stunned)
			{
				SetState(STATE_HARMED, 3);
				_stunned = true;
				_clips[_state].pause();
				StunMovie.x = Math.round(this.x + _clips[0].width / 2 - StunMovie.width/2);
				StunMovie.y = Math.round(this.y - StunMovie.height);				
				StunMovie.visible = true;
				StunMovie.alpha = 0;
				_tweens.push(TweenLite.to(StunMovie, STUN_APPEAR_DISAPPEAR_TIME, { alpha: 1 } ));
				StunMovie.play();
			}
		}
		
		private function OnAddedToStage(event: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			//addChild(_monsterSpeechBubble);	
		}
		
		override protected function OnHover():void 
		{
			if (_alive)
			{
				super.OnHover();
				HCursor.ShowWeaponCursor();
			}
			else
				OnStopHover();					
		}
		
		override protected function OnStopHover():void 
		{
			super.OnStopHover();
			HCursor.ShowHandCursor();
		}
		
		override protected function OnTouchPhaseBegan():void 
		{
			
			if (!Preferences.clickingAllowed)
				return;
			HSounds.PlaySound("Hit");
			if (!_inTransition && !_stunned)
			{
				if (_state === STATE_STANDING)
				{
					_inTransition = true;
					if (_clips[_state].currentFrame > 10)
					{
						var f: int = _clips[_state].currentFrame;
						reverse = true;
						_clips[_state].currentFrame = _clips[_state].numFrames - 1 - f;					
					}
					_clips[_state].addEventListener(Event.COMPLETE, OnTransitionPhaseEnd);
					_clips[_state].fps = X6_STAND_FPS;
				}
				else if (_state === STATE_HARMED)
				{
					_clips[_state].currentFrame = 0;
					_clips[_state].addEventListener(Event.COMPLETE, OnHarmComplete);
				}
			}
			_lastHitTime = Misc.GetTimeMSecs();
			World.gWorld.dispatchEventWith("N_LocalClickHit");			
		}
		
		protected function OnTransitionPhaseEnd(event: Event):void 
		{
			_clips[_state].removeEventListener(Event.COMPLETE, OnTransitionPhaseEnd);
			_inTransition = false;
			_clips[_state].fps = BASE_STAND_FPS;
			if (reverse)
				reverse = false;			
			SetState(STATE_HARMED);			
			_clips[_state].addEventListener(Event.COMPLETE, OnHarmComplete);
		}
		
		//protected function OnTouch(event: TouchEvent): void
		//{
			//super.OnTouch(event);
			//var touch: Touch = event.getTouch(this);
			//if (touch == null)
			//{
				//if (_hovered)
				//{
					//HCursor.ShowHandCursor();
					//_hovered = false;
				//}
			//}
			//else
			//{
				//if (touch.phase == TouchPhase.BEGAN)
				//{					
					//_lastHitTime = Misc.GetTimeMSecs();
					//SetState(STATE_HARMED);
					//_clips[_state].addEventListener(Event.COMPLETE, OnHarmComplete);
					//World.gWorld.dispatchEventWith("N_LocalClickHit");
				//}
				//else if (touch.phase == TouchPhase.HOVER)
				//{
					//if (!_hovered)
					//{
						//_hovered = true;
						//HCursor.ShowWeaponCursor();
					//}
				//}
			//}
		//}
		
		private function OnHarmComplete(event: Event):void 
		{
			if (Misc.GetTimeMSecs() - _lastHitTime > 200)
			{
				_clips[_state].removeEventListener(Event.COMPLETE, OnHarmComplete);
				SetState(STATE_STANDING);
			}
		}
		
		//private function MaybeStopHarm():void 
		//{
			//if (Misc.GetTimeMSecs() - _lastHitTime < 190)
			//{
				//_timeoutId = setTimeout(MaybeStopHarm, 200);
			//}
			//else
			//{
				//_timeoutId = 0;
				//SetState(STATE_STANDING);
			//}
		//}
		
		private function SetState(newState: int, frame: int = -1): void 
		{
			if (!_stunned)
			{
				if (_state !== newState)
				{
					_clips[_state].stop();
					_clips[_state].visible = false;
					_clips[newState].play();
					_clips[newState].visible = true;			
				}				
				_state = newState;
				if (frame > -1 && frame !== _clips[_state].currentFrame)
					_clips[_state].currentFrame = frame;
			}
		}
		
		override public function Destroy():void 
		{
			World.gWorld.removeEventListener("stunMonster", OnStunMonster);
			World.gWorld.removeEventListener("stunEnded", OnStunEnded);
			_hovered = false;
			HCursor.ShowHandCursor();
			if (_timeoutId !== 0)
				clearTimeout(_timeoutId);
			_monsterSpeechBubble.Destroy();
			for (var i: int = 0; i < _clips.length; i++)
			{
				Starling.juggler.remove(_clips[i]);
				_clips[i].removeEventListener(Event.COMPLETE, OnHarmComplete);
				_clips[i].removeEventListener(Event.COMPLETE, OnTransitionPhaseEnd);				
				_clips[i].removeFromParent(true);

				Starling.juggler.remove(_reverseClips[i]);
				_reverseClips[i].removeEventListener(Event.COMPLETE, OnHarmComplete);
				_reverseClips[i].removeEventListener(Event.COMPLETE, OnTransitionPhaseEnd);					
				_reverseClips[i].removeFromParent(true);			
			}
			_reverseClips = null;
			_clips = null;
			//removeEventListener(TouchEvent.TOUCH, OnTouch);
			super.Destroy();
		}
		
		public function SetFps(fps: Number):void 
		{
			for (var i: int = 0; i < _clips.length; i++)
			{
				_clips[i].fps = fps;
			}
		}
		
		public function DieAnimation():void 
		{
			HSounds.PlaySound("Death");
			_alive = false;
			HCursor.ShowHandCursor();
			var f: HColorFilter = new HColorFilter;
			this.filter = f;
			_dieTween1 = TweenLite.to(this, 1, { alpha: 0 } );
			_dieTween2 = TweenLite.to(f, 1, { brightness: 1 } );
		}
		
		public function Tint(state: int, color: uint): void 
		{
			_clips[state].color = color;
		}
		
		public function Speak(text: String = null):void 
		{
			if (text === null)
				return;
			if (Misc.Random(0,100) < SPEAK_CHANCE)
			{
				_monsterSpeechBubble.y = -30;
				_monsterSpeechBubble.x = Math.round(_clips[0].width / 2 - _monsterSpeechBubble.width / 2);			
				_monsterSpeechBubble.StartSpeech(text);
			}
		}
		
		public static function GetMonsterModelNameByType(type: int): String 
		{
			switch (type) 
			{
				case MonsterData.MONSTER_DUMMY:
					return "dummy";
					break;
				case MonsterData.MONSTER_SLIME:
					return "slime";
					break;
				case MonsterData.MONSTER_SAPLING:
					return "sapling";
					break;
				case MonsterData.MONSTER_PLANT:
					return "plant";
					break;
					
				case MonsterData.MONSTER_BAT:
					return "bat";
					break;
				case MonsterData.MONSTER_DARK_DUMMY:
					return "dummy";
					break;
				case MonsterData.MONSTER_SHADOW:
					return "shadow";
					break;
				case MonsterData.MONSTER_LURKER:
					return "lurker";
					break;
				
				case MonsterData.MONSTER_SKELETON:
					return "skeleton";
					break;
				case MonsterData.MONSTER_SENTRY:
					return "sentry";
					break;
				case MonsterData.MONSTER_GOBLIN:
					return "goblin";
					break;
				case MonsterData.MONSTER_CRYPT_SLIME:
					return "slime";
					break;
					
				default:
					return "dummy";
					break;
			}			
		}
		
		public function Die():void 
		{
			_pool.push(this);
			visible = false;
			_monsterSpeechBubble.visible = false;
			for (var i: int = 0; i < _clips.length; i++)
			{
				Starling.juggler.remove(_clips[i]);
				_clips[i].removeEventListener(Event.COMPLETE, OnHarmComplete);
				_clips[i].removeEventListener(Event.COMPLETE, OnTransitionPhaseEnd);				
				_clips[i].removeFromParent(true);

				Starling.juggler.remove(_reverseClips[i]);
				_reverseClips[i].removeEventListener(Event.COMPLETE, OnHarmComplete);
				_reverseClips[i].removeEventListener(Event.COMPLETE, OnTransitionPhaseEnd);					
				_reverseClips[i].removeFromParent(true);			
			}
			removeChild(_monsterSpeechBubble);
		}
		
		private function ReInit():void 
		{
			if (_dieTween1)
			{
				_dieTween1.kill();
				_dieTween2.kill();
				_dieTween1 = null;
				_dieTween2 = null;
			}
			for (var i: int = 0; i < _tweens.length; i++)
			{
				_tweens[i].kill();
			}
			_tweens = new Vector.<TweenLite>;
			_baseWidth = 0;
			_baseHeight = 0;
			_clips = new Vector.<MovieClip>;
			_reverseClips = new Vector.<MovieClip>;			
			_reverse = false;
			_inTransition = false;
			_stunned = false;
			_hovered = false;
			_alive = true;
			_state = STATE_STANDING;
			filter = null;
			newlyCreated = false;
			visible = true;
								
			//addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		public static function Get(): MonsterSprite 
		{
			if (_pool.length == 0)
				return new MonsterSprite;
			var monster: MonsterSprite = _pool.pop();
			monster.ReInit();
			return monster;
		}
		
		public static function Create(monsterData: MonsterData): MonsterSprite
		{
			Mouse.hide();
			//var m: MonsterSprite = new MonsterSprite;
			var m: MonsterSprite = Get();
			m._type = monsterData.type;
			m._rarity = monsterData.rarity;
			m._modelName = GetMonsterModelNameByType(monsterData.type);
			for (var i: int = 0; i < NUMBER_OF_STATES; i++)
			{
				var clip: MovieClip = new MovieClip(Assets.GetMonsterAtlas(m._modelName, monsterData.rarity, i).getTextures(Assets.monsterStateNames[i].toLowerCase() + "_"), BASE_STAND_FPS);
				var reverseClip: MovieClip = new MovieClip(Assets.GetMonsterAtlas(m._modelName, monsterData.rarity, i).getTextures(Assets.monsterStateNames[i].toLowerCase() + "_").reverse(), BASE_STAND_FPS);
				if (m._baseWidth == 0)
				{
					m._baseWidth = clip.width;
					m._baseHeight = clip.height;
				}
				reverseClip.visible = false;
				var scale: Number = 1;
				//if (monsterData.rarity == MonsterData.RARITY_COMMON)
					//scale = sCommonMonsterScale;
				//else if (monsterData.rarity == MonsterData.RARITY_RARE)
					//scale = sRareMonsterScale;
				//else if (monsterData.rarity == MonsterData.RARITY_ELITE)
					//scale = sEliteMonsterScale;
				if (sMonsterModelScales[m._modelName] != undefined)
					scale *= sMonsterModelScales[m._modelName];
				clip.width = Math.round(clip.width*scale);
				clip.height = Math.round(clip.height * scale);
				reverseClip.width = Math.round(reverseClip.width*scale);
				reverseClip.height = Math.round(reverseClip.height*scale);					
				if (i === STATE_HARMED)
				{
					clip.fps = HARM_FPS;
					reverseClip.fps = HARM_FPS;
				}
				m.addChild(reverseClip);
				m.addChild(clip);				
				Starling.juggler.add(clip);
				Starling.juggler.add(reverseClip);
				if (i !== 0)
				{
					clip.stop();
					clip.visible = false;
				}
				m._clips.push(clip);
				m._reverseClips.push(reverseClip);
			}
			m.x = START_X;
			m.y = START_Y;
			m.x -= Math.round(m._clips[0].width / 2);
			m.y -= m._clips[0].height;
			m.addChild(m._monsterSpeechBubble);
			m._monsterSpeechBubble.y = m.y-30;
			m._monsterSpeechBubble.x = Math.round(m._clips[0].width / 2 - m._monsterSpeechBubble.width / 2);
			
			m.Speak(Language.GetMonsterPhrase(m.type));
			m.alpha = 0;
			TweenLite.to(m, 1, { alpha: 1 } );
			return m;
		}
		
	}

}