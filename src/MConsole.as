
package  
{
	/**
	 * ...
	 * @author Aikei
	 */
	import com.greensock.TweenLite;
	import flash.utils.Dictionary;
	import starling.display.Quad;
	import starling.events.*;
	import flash.ui.*;
	import starling.utils.HAlign;
	import starling.text.TextField;
	import starling.utils.Color;
	import system.TextEditor;
	import d_hentity.HEntity;
	import starling.core.Starling;
	import dbg.CommandParser;	
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class MConsole extends HEntity
	{
		public static const LIST_MODE_PREVIOUS_LINES: int = 0;
		public static const LIST_MODE_POSSIBLE_COMMANDS: int = 1;
		public static const LIST_MODE_POSSIBLE_VARIABLES: int = 2;
		
		public static const LIST_ADD_MODE_CLEAR_PREVIOUS_TEXT: int = 0;
		public static const LIST_ADD_MODE_ADD_TO_PREVIOUS_TEXT: int = 1;
		
		private var _texts: Vector.<TextField>;
		private static const TEXT_HEIGHT: Number = 20;
		private static const MAX_LINES: int = 20;
		private static const FONT_SIZE: int = 16;
		private static const FONT_NAME: String = "bregular12";
		private static const FONT_COLOR: uint = Color.BLACK;
		private static const LINE_HEIGHT: int = 25;
		private static const LINE_WIDTH: int = 1024;
		private static const TEXT_ALIGNMENT: String = HAlign.LEFT;
		private static const MAX_COMMANDS: int = 50;
		
		private var beginner: String = " : ";
		
		private var _command: TextField; 
		private var _beginner: TextField;
		//public static var showConsole : Boolean = false;
		
		private var _textEditor: TextEditor = new TextEditor;
		private var _cursorText: TextField;
		private var _previousCommands: Vector.<String> = new Vector.<String>;
		private var _listVector: Vector.<String>;
		private var _upArrowGetIndex: int = -1;	
		private var _listAddMode: int;
		private var _listedLastTime: Boolean = false;
		private var _listMode: int;
		private var _prevIndex: int = 0;
		private var _spaceAdded: Boolean = false;
		private var _quad: Quad = new Quad(LINE_WIDTH, LINE_HEIGHT * MAX_LINES, 0xffffff);
		
		static public var lastError: Error = null;
		static public var e: Boolean = true;
		
		public function get listMode(): int 
		{
			return _listMode;
		}
		
		public static function GetCurrentLineNumber(): int 
		{
			return _console._previousCommands.length;
		}
		
		public static function GetLineValue(lineNumber: int): String 
		{
			if (lineNumber > _console._previousCommands.length)
				return "WRONG LINE";
			else if (lineNumber === _console._previousCommands.length)
				return _console._command.text;
			else
				return _console._previousCommands[lineNumber];
		}
		
		public function set listMode(value: int): void 
		{
			if (_listMode !== value)
				_listedLastTime = false;
			_listMode = value;			
			if (_listMode === LIST_MODE_PREVIOUS_LINES)
			{
				_listAddMode = LIST_ADD_MODE_CLEAR_PREVIOUS_TEXT;
				_listVector = _previousCommands;
			}
			else if (_listMode === LIST_MODE_POSSIBLE_VARIABLES)
			{
				_listAddMode = LIST_ADD_MODE_ADD_TO_PREVIOUS_TEXT;
				_listVector = CommandParser.variablesVector;
			}
			else if (_listMode === LIST_MODE_POSSIBLE_COMMANDS)
			{
				_listAddMode = LIST_ADD_MODE_ADD_TO_PREVIOUS_TEXT;
				_listVector = CommandParser.commandsVector;
			}
			_upArrowGetIndex = -1;
		}
		
		private static var _console: MConsole = null;
		
		public static var logLevel: int = 2;
		
				
		public function MConsole() 
		{
			listMode = LIST_MODE_PREVIOUS_LINES;
			_command = new TextField(LINE_WIDTH,LINE_HEIGHT,"", FONT_NAME, FONT_SIZE, FONT_COLOR);
			_command.x = 25;
			_command.y = 15;
			
			_beginner = new TextField(LINE_WIDTH, LINE_HEIGHT, ">", FONT_NAME, FONT_SIZE, FONT_COLOR);
			_beginner.x = 15;
			_beginner.y = 15;
			
			_console = this;
			_texts = new Vector.<TextField>;
			_cursorText = CreateTextEntity(_textEditor.cursorChar, FONT_SIZE*2, 15);
			_command = CreateTextEntity("", FONT_SIZE*2, 15);
			_beginner = CreateTextEntity(">", 15, 15);
			
			visible = false;
			
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addEventListener(KeyboardEvent.KEY_DOWN, OnKeyDown);
			addEventListener(KeyboardEvent.KEY_UP, OnKeyUp);
			
			CommandParser.Init();
		}
		
		public function OnAddedToStage(ev: Event): void
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			addChild(_quad);
			addChild(_command);
			addChild(_beginner);
			addChild(_cursorText);			
		}		
		
		public static function get created():Boolean
		{
			return (_console != null);
		}		
		
		public static function set showConsole(show: Boolean): void
		{
			if (_console == null)
				return;
			_console.visible = show;
		}
		
		public static function get showConsole(): Boolean
		{
			if (_console == null)
				return false;
			return _console.visible;
		}
		
		public static function Write(... args): void
		{
			if (_console == null)
				return;
			var startI: int = 0;
			var str: String = new String;
			if (args[0] != "_i" && args[0] != "_ii" && args[0] != "##" && args[0] != "e" && logLevel < 3)
				return;
			if (args[0] != "_i" && args[0] != "##" && args[0] != "e" && logLevel < 2)
				return;
			if (args[0] == "_i" || args[0] == "_ii")
				args.splice(0, 1);
			if (args[0] == "w" || args[0] == "e" || args[0] == "##")
			{
				str += args[0];
				startI = 1;
			}
			str += ": ";
			for (var i : int = startI; i < args.length; i++)
			{
				str += String(args[i]);
				str += " ";
			}
			_console.AddText(str);			
		}
		
		public static function CmdWrite(... args): void
		{
			var allArgsArray: Array = new Array;
			allArgsArray.push("##");
			for (var i : int = 0; i < args.length; i++)
			{
				//if (args[i] is Vector.<Object>)
				//{
					//for (var j: int = 0; j < Vector.<Object>(args[i]).length; j++)
						//allArgsArray.push(args[i][j])
				//}
				if (args[i] is Dictionary)
				{
					for (var key: Object in Dictionary(args[i]))
						allArgsArray.push(String(key)+",")
				}				
				else
					allArgsArray.push(args[i].toString());
			}			
			Write.apply(null, allArgsArray);
			
			//if (_console == null)
				//return;
			//var str: String = ": ";
			//for (var i : int = 0; i < args.length; i++)
			//{
				//str += String(args[i]);
				//str += " ";
			//}
			//_console.AddText(str);			
		}		
		
		private function AddText(str: String): void
		{
			var txt: TextField = CreateTextEntity(str, 15, (TEXT_HEIGHT * 2) + _texts.length * TEXT_HEIGHT);
			txt.hAlign = TEXT_ALIGNMENT;
			addChild(txt);
			_texts.push(txt);
			if (_texts.length > MAX_LINES)
			{
				txt = _texts[0];
				_texts.splice(0, 1);
				txt.removeFromParent(true);
			}
			UpdateTextPositions();
		}
		
		private function CreateTextEntity(str: String, x: int, y: int): TextField
		{
			var txt: TextField = new TextField(LINE_WIDTH, LINE_HEIGHT, str, FONT_NAME, FONT_SIZE, FONT_COLOR);
			txt.x = x;
			txt.y = y;
			txt.autoScale = true;
			txt.hAlign = HAlign.LEFT;
			return txt;
		}
		
		private function UpdateTextPositions(): void
		{
			for (var i: int = 0, l:int = _texts.length-1; i <= l; i++)
			{
				_texts[l-i].y = i * TEXT_HEIGHT+(TEXT_HEIGHT*2);
			}
		}			
		
		public function OnKeyDown(ev: KeyboardEvent): Boolean
		{
			if (!visible && ev.keyCode !== Keyboard.TAB)
				return false;
			if (ev.keyCode === Keyboard.ENTER)
			{
				_previousCommands.push(_command.text);
				if (_previousCommands.length >= MAX_COMMANDS)
					_previousCommands.splice(0, 1);
				AddText(_beginner.text + _beginner.text + " " + _command.text);
				if (e)
				{
					try
					{
						CommandParser.ParseCommand(_command.text);
					}
					catch (err:Error)
					{
						MConsole.CmdWrite(err.message);
						lastError = err;
					}
				}
				_textEditor.Clear();
				_command.text = "";
				_cursorText.text = "";
				_upArrowGetIndex = -1;
				return true;
			}
			else if (ev.keyCode === Keyboard.UP)
			{
				if (_upArrowGetIndex === -1)
					_upArrowGetIndex = _listVector.length-1;
				else
					_upArrowGetIndex--;
				if (_upArrowGetIndex >= 0)
				{												
					if (_listAddMode === LIST_ADD_MODE_CLEAR_PREVIOUS_TEXT)
					{
						_command.text = _listVector[_upArrowGetIndex];
						_textEditor.Clear();
						_textEditor.SetText(_command.text);
					}
					else if (_listAddMode === LIST_ADD_MODE_ADD_TO_PREVIOUS_TEXT)
					{
						DeleteLastInsertedListTerm();
						//if (_listedLastTime)
						//{
							//var l: int = _command.text.length - _listVector[_prevIndex].length;
							//if (_spaceAdded)
								//l--;
							//_command.text = _command.text.substring(0,l);
							//_textEditor.SetText(_command.text);
						//}
						if (_command.text.length !== 0 && _command.text.charAt(_command.text.length - 1) !== " ")
						{
							_command.text += " ";
							_spaceAdded = true;
						}
						else
							_spaceAdded = false;
						_command.text += _listVector[_upArrowGetIndex];
						_textEditor.AddText(_command.text, _spaceAdded);
						_prevIndex = _upArrowGetIndex;
						_listedLastTime = true;
					}
				}					
			}
			else if (ev.keyCode === Keyboard.DOWN)
			{					
				if (_upArrowGetIndex === -1 || _upArrowGetIndex === _listVector.length-1)
					return true;
				_upArrowGetIndex++;
				if (_upArrowGetIndex >= 0)
				{
					if (_listAddMode === LIST_ADD_MODE_CLEAR_PREVIOUS_TEXT)
					{
						_command.text = _listVector[_upArrowGetIndex];
						_textEditor.Clear();
						_textEditor.SetText(_command.text);
					}
					else if (_listAddMode === LIST_ADD_MODE_ADD_TO_PREVIOUS_TEXT)
					{
						//if (_listedLastTime)
						//{
							//l = _command.text.length - _listVector[_prevIndex].length;
							//if (_spaceAdded)
								//l--;								
							//_command.text = _command.text.substring(0,l);
							//_textEditor.SetText(_command.text);
						//}
						DeleteLastInsertedListTerm();
						if (_command.text.length !== 0 && _command.text.charAt(_command.text.length - 1) !== " ")
						{
							_command.text += " ";
							_spaceAdded = true;
						}
						else
							_spaceAdded = false;													
						_command.text += _listVector[_upArrowGetIndex];
						_textEditor.AddText(_command.text,_spaceAdded);
						_prevIndex = _upArrowGetIndex;
						_listedLastTime = true;
					}						
				}
			}
			else if (ev.keyCode === Keyboard.TAB)
			{
				_command.text = "";
				_textEditor.Clear();					
				_listedLastTime = false;
				visible = !visible;
				
				//if (!visible)
				//{
					//visible = true;
					//y = -_quad.height;
					//TweenLite.to(this, 0.5, { y : 0 } );
				//}
				//else
				//{
					//TweenLite.to(this, 0.5, { y : -_quad.height, onComplete : function ():void 
					//{
						//this.visible = false;
					//}});
				//}
			}
			else if (ev.keyCode === Keyboard.DELETE)
			{
				if (_listedLastTime)
					DeleteLastInsertedListTerm();
			}
			else if (ev.keyCode === Keyboard.SHIFT)
			{
				if (listMode === LIST_MODE_PREVIOUS_LINES)
					listMode = LIST_MODE_POSSIBLE_COMMANDS;
			}
			else if (ev.keyCode === Keyboard.CONTROL)
			{
				if (listMode === LIST_MODE_PREVIOUS_LINES)
					listMode = LIST_MODE_POSSIBLE_VARIABLES;
			}			
			else
			{				
				if (_textEditor.OnKeyboardEvent(ev))
				{
					_listedLastTime = false;
					_command.text = _textEditor.text;
					_cursorText.text = _textEditor.cursorString;
					return true;
				}
			}
			return false;
		}
		
		private function DeleteLastInsertedListTerm(): void 
		{
			if (_listedLastTime)
			{
				var l: int = _command.text.length - _listVector[_prevIndex].length;
				if (_spaceAdded)
					l--;
				_command.text = _command.text.substring(0,l);
				_textEditor.SetText(_command.text);
				_listedLastTime = false;
			}
		}
		
		public function OnKeyUp(ev: KeyboardEvent): void
		{			
			if (ev.keyCode === Keyboard.SHIFT || ev.keyCode === Keyboard.CONTROL)
			{
				listMode = LIST_MODE_PREVIOUS_LINES;
			}
		}
		
		
	}

}