package system 
{
	/**
	 * ...
	 * @author Aikei
	 */
    import flash.net.SharedObject;

    /**
     * Flash 10.1 introduce a nasty bug that crash the FlashPlayer and the browser when a SharedObject is accessed consecutively
     * We work around this issue with a static accessor that make sure the SharedObject is accessed only once and kept in cache.
     * */
    public class SharedObjectManager
    { 
        private static var cache:Object = new Object();
		
        public static function getLocal(name:String, localPath:String = null, secure:Boolean = false): SharedObject 
		{
            if (cache.hasOwnProperty(name+":" + localPath + ":" + secure)) 
			{
                return cache[name+":"+localPath+":"+secure]
            } 
			else 
			{
                cache[name+":" + localPath + ":" + secure] = SharedObject.getLocal(name, localPath, secure);
            }
            return cache[name+":" + localPath + ":" + secure];
        }
    }
}