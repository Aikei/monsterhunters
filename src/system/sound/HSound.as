package system.sound 
{
	import flash.media.Sound;
	import flash.media.SoundLoaderContext;
	import flash.net.URLRequest;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HSound extends Sound 
	{
		public var loops;
		public var sid: String;
		public var n: int;
		public var bytesPosition: uint;
		public var lengthInSecs: Number;
		
		public function HSound(sid: String, n: int, lengthInSecs: int, loops: int = 0, stream:URLRequest=null, context:SoundLoaderContext=null) 
		{
			super(stream, context);
			this.sid = sid;
			this.n = n;
			this.loops = loops;
			this.lengthInSecs = lengthInSecs;
		}
		
	}

}