package system 
{
	import dbg.CommandParser;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	/**
	 * ...
	 * @author Aikei
	 */
	public class ScriptsInit 
	{
		static private var _urlLoader: URLLoader = new URLLoader;
		static private var _remotePath: String = "http://178.62.202.250:11500/data/";
		static private var _localPath: String = "../data/";
		static private var _path: String;
		static private var _tries: int = 0;
		
		static public function Init():void 
		{
			if (Preferences.REMOTE_SCRIPTS_LOAD)
				_path = _remotePath;
			else
				_path = _localPath;
			_urlLoader.addEventListener(Event.COMPLETE, OnFileLoaded);
			_urlLoader.addEventListener(IOErrorEvent.IO_ERROR, OnError);
			_urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, OnError);
			Load();
		}
		
		static private function Load():void 
		{
			try 
			{
				_urlLoader.load(new URLRequest(_path + 'client_scripts'));
			}
			catch (err:Error)
			{
				_urlLoader.load(new URLRequest(_path + 'client_scripts'));
			}
		}
		
		static private function OnError(e:Event):void 
		{
			_tries++;
			if (_tries < 5)
				Load();
			else
				throw "Script file cannot be found!";
		}
		
		static private function ProcessInitialization(allInit:Array ):void 
		{
			for (var k: int = 0; k < allInit.length; k++)
			{
				if (allInit[k].length > 1 && (allInit[k].charAt(0) != '/' || allInit[k].charAt(1) != '/'))
					CommandParser.ParseCommand(allInit[k]);
			}
		}
		
		static private function ProcessAliases(aliasesData: Array):void 
		{
			for (var k: int = 0; k < aliasesData.length; k++)
			{
				if (aliasesData[k].charAt(0) === '@')
				{
					var alias: String = aliasesData[k].slice(1);
					var meaning: Vector.<String> = new Vector.<String>;
					while (true) 
					{
						k++;
						if (k == aliasesData.length || aliasesData[k].charAt(0) === '@')
							break;
						if (aliasesData[k].length > 1 && (aliasesData[k].charAt(0) != '/' || aliasesData[k].charAt(1) != '/'))
							meaning.push(aliasesData[k]);
					}
					CommandParser.AddAlias(alias, meaning);
					if (k < aliasesData.length && aliasesData[k].charAt(0) === '@')
						k--;			
				}
			}
		}
		
		static private function OnFileLoaded(e:Event):void 
		{
			var strings: Array = String(_urlLoader.data).split('\n');
			var startI: int = 0;
			for (var i: int = 0; i < strings.length; i++)
			{
				if (strings[i].charAt(strings[i].length-1) === '\r')
					strings[i] = strings[i].slice(0, strings[i].length - 1);				
			}
			for (i = 0; i < strings.length; i++)
			{
				if (strings[i] === '<aliases>')
				{
					startI = i+1;
					do
					{						
						i++;
						if (i >= strings.length)
							return;							
					} while (strings[i] != '</aliases>');					
					var allAliases: Array = strings.slice(startI, i);
					ProcessAliases(allAliases);
				}
				else if (strings[i] === '<init>')
				{
					startI = i+1;
					do
					{						
						i++;
						if (i >= strings.length)
							return;							
					} while (strings[i] != '</init>');					
					var allInit: Array = strings.slice(startI, i);
					ProcessInitialization(allInit);					
				}
			}
			World.gWorld.dispatchEventWith("scriptsInitDone");
		}
		
	}

}