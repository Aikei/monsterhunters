package system 
{
	//import flash.display.Bitmap;
	//import flash.display.BitmapData;
	//import flash.display.Sprite;
	//import flash.events.Event;
	

	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.events.Touch;
	import flash.geom.Point;
	import hevents.*;
	import d_hentity.HEntity;
	import flash.ui.Mouse;
	import starling.core.Starling;
	import flash.events.MouseEvent;
	import starling.display.Image;
	import starling.display.MovieClip;
	
	/**
	 * ...
	 * @author Aikei
	 */
	
	//public class HCursor extends Sprite 
	//{
		//private var _weaponImage: Bitmap = null;
		//private var _handImage: Bitmap = null;
		//private var _weaponShown: Boolean = false;
		//private var _handShown: Boolean = false;
		//
		//static private var _cursor: HCursor = null;
		//
		//public function HCursor() 
		//{
			//super();
			//_cursor = this;
			//
			//Mouse.hide();
			//addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			////addEventListener(Event.ENTER_FRAME, OnEnterFrame);
			//World.gWorld.addEventListener(L_HEventWeaponSet.L_HEVENT_WEAPON_SET, OnWeaponSet);
			//Starling.current.nativeOverlay.addChild(this);
			////addEventListener(MouseEvent.MOUSE_DOWN, OnMouseClick);
			////Starling.current.nativeOverlay.addEventListener(MouseEvent.CLICK, OnFlashMouseMove);
			////World.gWorld.addEventListener(TouchEvent.TOUCH, OnTouch);
		//}
		//
		//public function Hide():void 
		//{
			//Mouse.hide();
		//}
		//
		//public function Show():void 
		//{
			//Mouse.show();
		//}
		//
		////public function OnMouseClick(event: MouseEvent): void 
		////{
			////var i: int = 1;
			////MConsole.Write("_ii", "FUCK!");
		////}		
		//
		//protected function OnAddedToStage(event: Event): void 
		//{
			//removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			////_handImage = new Bitmap(Misc.TextureToBitmapData(Assets.interfaceAtlas.getTexture("hand")));
			////_handImage = new Bitmap(Assets.GetBitmapData("CursorHand"));
			//_handImage = new Assets["CursorHand"] as Bitmap;
			//if (_weaponImage === null)
				//_weaponImage = new Assets["CursorWeapon0"] as Bitmap;
				////_weaponImage = new Bitmap(Misc.TextureToBitmapData(Assets.interfaceAtlas.getTexture("weapon_0")));
			//
			//addChild(_handImage);
			//addChild(_weaponImage);
			//_weaponImage.visible = false;
			//var p: Point = globalToLocal(new Point(mouseX, mouseY));
			//this.x = p.x;
			//this.y = p.y;
			//this.startDrag();
		//}		
		//
		//protected function OnWeaponSet(event: L_HEventWeaponSet): void 
		//{
			//if (_weaponImage === null)
			//{
				////_weaponImage = new Image(Assets.interfaceAtlas.getTexture("weapon_" + event.weapon.type));
				////_weaponImage = new Bitmap(Misc.TextureToBitmapData(Assets.interfaceAtlas.getTexture("weapon_" + event.weapon.type)));
				////_weaponImage = new Bitmap(Assets.GetBitmapData("CursorWeapon" + event.weapon.type.toString()));
				//_weaponImage = new Assets["CursorWeapon" + event.weapon.type.toString()] as Bitmap;
				//addChild(_weaponImage);
			//}
			//else
				//_weaponImage.bitmapData = Assets.GetBitmapData("CursorWeapon" + event.weapon.type.toString());
				////_weaponImage = new Assets["CursorWeapon" + event.weapon.type.toString()] as Bitmap;
				////_weaponImage = new Bitmap(Assets.GetBitmapData("CursorWeapon" + event.weapon.type.toString()));
				////_weaponImage.bitmapData = Misc.TextureToBitmapData(Assets.interfaceAtlas.getTexture("weapon_" + event.weapon.type));
		//}
		//
		//static public function ShowHandCursor(): void 
		//{
			//if (!_cursor._handShown)
			//{
				//_cursor._weaponShown = false;
				//_cursor._handShown = true;
				//_cursor._handImage.visible = true;
				//_cursor._weaponImage.visible = false;
			//}
		//}
		//
		//static public function ShowWeaponCursor(): void 
		//{
			//if (!_cursor._weaponShown && Preferences.clickingAllowed)
			//{
				//_cursor._weaponShown = true;
				//_cursor._handShown = false;
				//_cursor._handImage.visible = false;
				//_cursor._weaponImage.visible = true;
			//}
		//}
		//
		//static public function GetCursor(): HCursor 
		//{
			//return _cursor;
		//}
//
	//} 
	 
	 
	public class HCursor extends HEntity 
	{
		private var _weaponImage: Image = null;
		private var _weaponMovie: MovieClip = null;
		private var _handImage: Image = null;
		private var _weaponShown: Boolean = false;
		private var _handShown: Boolean = false;
		
		static private var _cursor: HCursor = null;
		static private var _sHelperPoint: Point = new Point;
		
		static public function get cursor(): HCursor 
		{
			return _cursor;
		}
		
		public function HCursor() 
		{
			super();
			_cursor = this;
			touchable = false;
			Mouse.hide();
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			World.gWorld.addEventListener(L_HEventWeaponSet.L_HEVENT_WEAPON_SET, OnWeaponSet)
			//addEventListener(TouchEvent.TOUCH, OnMTouch);
			//Starling.current.nativeOverlay.addEventListener(MouseEvent.CLICK, OnFlashMouseMove);
			//World.gWorld.addEventListener(TouchEvent.TOUCH, OnTouch);
			
		}
		
		protected function OnAddedToStage(event: Event): void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			stage.addEventListener(TouchEvent.TOUCH, OnStageTouch);
			_handImage = new Image(Assets.interfaceAtlas.getTexture("hand"));
			if (_weaponImage === null)
				_weaponImage = new Image(Assets.interfaceAtlas.getTexture("weapon_0"));
			addChild(_handImage);
			addChild(_weaponImage);
			_weaponImage.visible = false;
		}		
		
		protected function OnWeaponSet(event: L_HEventWeaponSet): void 
		{
			if (!event.weapon.animated)
			{
				if (_weaponImage === null)
				{					
					if (_weaponMovie)
					{
						_weaponMovie.stop();
						Starling.juggler.remove(_weaponMovie);
						_weaponMovie.removeFromParent(true);
						_weaponMovie = null;
					}
					_weaponImage = new Image(Assets.interfaceAtlas.getTexture("weapon_" + event.weapon.type));
					addChild(_weaponImage);	
				}
				else
					_weaponImage.texture = Assets.interfaceAtlas.getTexture("weapon_" + event.weapon.type);
			}
			else
			{
				if (_weaponMovie)
				{
					_weaponMovie.stop();
					Starling.juggler.remove(_weaponMovie);
					_weaponMovie.removeFromParent(true);
					_weaponMovie = null;
				}					
				if (_weaponImage)
				{
					_weaponImage.visible = false;
				}
				_weaponMovie = new MovieClip(Assets.miscAtlas.getTextures("weapon" + event.weapon.type+"_"));
				Starling.juggler.add(_weaponMovie);
				_weaponMovie.visible = false;
				addChild(_weaponMovie);
			}					
		}
		
		static public function ShowHandCursor(): void 
		{
			if (!_cursor._handShown)
			{
				_cursor._weaponShown = false;
				_cursor._handShown = true;
				_cursor._handImage.visible = true;					
				if (_cursor._weaponMovie)
				{
					_cursor._weaponMovie.visible = false;
					_cursor._weaponMovie.stop();
				}
				else
				{
					_cursor._weaponImage.visible = false;
				}
			}
		}
		
		static public function ShowWeaponCursor(): void 
		{
			if (!_cursor._weaponShown)
			{
				_cursor._weaponShown = true;
				_cursor._handShown = false;
				_cursor._handImage.visible = false;
				if (_cursor._weaponMovie)
				{
					_cursor._weaponMovie.visible = true;
					_cursor._weaponMovie.play();
				}
				else
					_cursor._weaponImage.visible = true;
			}
		}
		
		protected function OnFlashMouseMove(event: MouseEvent):void 
		{
			_cursor.x = event.stageX;
			_cursor.y = event.stageY;
		}
		
		protected function OnStageTouch(event: TouchEvent): void 
		{
			var touches: Vector.<Touch> = event.getTouches(stage);
			for (var i: int = 0; i < touches.length; i++)
			{
				touches[i].getLocation(this.parent, _sHelperPoint);
				this.x = _sHelperPoint.x;
				this.y = _sHelperPoint.y;
			}
			if (this.y >= Preferences.SCREEN_HEIGHT-10)
				Mouse.show();
			else
				Mouse.hide();
		}
		
		static public function GetCursor(): HCursor 
		{
			return _cursor;
		}		
		
	}

}