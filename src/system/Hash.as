package system 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import system.MurmurHash2;
	
	public class Hash 
	{
		
		static public function Hash(s: String, seed: uint): String 
		{
			return MurmurHash2.Hash(s, seed);
		}
		
	}

}