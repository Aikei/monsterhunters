package system 
{
	import com.jac.ogg.events.OggManagerEvent;
	import com.jac.ogg.OggManager;
	import flash.events.Event;
	import flash.events.SampleDataEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundCodec;
	import flash.media.SoundTransform;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import hevents.L_HEventMonsterKilled;
	import logic.Dungeon;
	import system.sound.HSound;
	/**
	 * ...
	 * @author Aikei
	 */
	public class HSounds 
	{
		static public var MAX_SOUND_CHANNELS: int = 10;
		
		static private var _muteSounds: Boolean = false;
		static private var _muteMusic: Boolean = false;
		static private var _runningOutOfTimeChannel:SoundChannel = null;
		
		static private const NUM_SAMPLES:int = 2048;
		static private const BYTES_PER_SAMPLE:int = 8;
		static private const BASE_FREQUENCY: Number = 44100;
		static private var _soundBytes: Dictionary = new Dictionary;
		static private var _sound
		static private var _dungeonThemeChannel: SoundChannel = null;
		static private var _dungeonSound: Sound = null;
		static private var _currentDungeonThmeNumber: int = 0;
		static private var _baseDungeonThemeVolume: Number = 0.5;
		static private var _loaded: Boolean = false;
		static private var _baseSoundTransform: SoundTransform = new SoundTransform;
		
		static private var _oggManager: OggManager = new OggManager;
		static private var _loadQueue: Vector.<Array> = new Vector.<Array>;
		static private var _loadIndex: int = 0;
		static private var _soundChannels: Vector.<SoundChannel> = new Vector.<SoundChannel>;
		
		//static public var Volumes: Object = new Object;
		
		public static var DungeonThemeVolume: Number = 1;					
		public static var WindowMoveVolume: Number = 1;			
		public static var PurchaseVolume: Number = 1;			
		public static var HitVolume: Number = 1;
		public static var DropVolume: Number = 1;			
		public static var CollectVolume: Number = 1;
		public static var DeathVolume: Number = 1;
		public static var PoisonVolume: Number = 1;			
		public static var HeroAttackVolume: Number = 1;			
		public static var DoorsCloseVolume: Number = 1;
		public static var DoorsOpenVolume: Number = 1;
		public static var ClickSoundVolume: Number = 1;
		public static var HoverSoundVolume: Number = 1;		
		public static var FailVolume: Number = 1;
		public static var LevelCompleteVolume: Number = 1;			
		public static var QuestReceivedVolume: Number = 1;
		public static var QuestMenuMoveVolume: Number = 1;
		public static var ScrollDropVolume: Number = 1;
		public static var QuestDoneVolume: Number = 1;		
		public static var RunningOutOfTimeVolume: Number = 1;
		public static var StunVolume: Number = 1;		
		public static var FadeInVolume: Number = 1;
		public static var FadeOutVolume: Number = 1;
		public static var SkillPurchasedVolume: Number = 1;
		public static var HypnosisVolume: Number = 1;
		public static var HealVolume: Number = 1;
		
		static public function get mute(): Boolean 
		{
			return (_muteMusic && _muteSounds);
		}
		
		static public function set mute(m: Boolean):void 
		{
			muteSounds = m;
			muteMusic = m;
		}
		
		static public function set muteSounds(m: Boolean):void 
		{
			_muteSounds = m;
		}
		
		static public function set muteMusic(m: Boolean):void 
		{
			if (m && !_muteMusic)
			{
				PauseTheme();
				_muteMusic = m;
			}
			else if (!m && _muteMusic)
			{
				_muteMusic = m;
				ContinueTheme();
			}				
		}
		
		static public function get muteSounds(): Boolean 
		{
			return _muteSounds;
		}
		
		static public function get muteMusic(): Boolean 
		{
			return _muteMusic;			
		}			
		
		static public function InitSounds() 
		{
			trace("Decoding sounds");
			_baseSoundTransform.volume = 0.33;
			_oggManager.addEventListener(OggManagerEvent.DECODE_COMPLETE, OnDecodeComplete);
										
			//for (var i: int = 0; i < Dungeon.NUMBER_OF_DUNGEONS; i++)
			//{				
				//if (i < 2)
				//{
					//_loadQueue.push(["DungeonTheme" + i,"DungeonTheme" + i, DungeonThemeVolume]);					
				//}
			//}	
			
			_loadQueue.push(["DungeonTheme0","DungeonTheme0", DungeonThemeVolume]);
			
			_loadQueue.push(["WindowMove", "WindowMove2", WindowMoveVolume]);
			
			_loadQueue.push(["Purchase","Purchase", PurchaseVolume]);			
			
			for (var i: int = 0; i < 3; i++)
			{
				_loadQueue.push(["Hit","Hit" + i, HitVolume]);
			}
			
			for (i = 0; i < 1; i++)
			{
				_loadQueue.push(["Drop","Drop" + i, DropVolume]);
			}				
			
			for (i = 0; i < 1; i++)
			{
				_loadQueue.push(["Collect","Collect" + i, CollectVolume]);
			}
			
			for (i = 0; i < 1; i++)
			{
				_loadQueue.push(["Death","Death" + i, DeathVolume]);
			}			
			
			_loadQueue.push(["Poison", "Poison", PoisonVolume]);			
			_loadQueue.push(["HeroAttack", "HeroAttack", HeroAttackVolume]);			
			_loadQueue.push(["DoorsClose", "DoorsClose", DoorsCloseVolume]);
			_loadQueue.push(["DoorsOpen", "DoorsOpen", DoorsOpenVolume]);
			_loadQueue.push(["ClickSound", "ClickSound", ClickSoundVolume]);
			_loadQueue.push(["HoverSound", "HoverSound", HoverSoundVolume]);	
			
			_loadQueue.push(["Fail", "Fail", FailVolume]);
			_loadQueue.push(["LevelComplete", "LevelComplete0", LevelCompleteVolume]);
			
			_loadQueue.push(["QuestReceived", "QuestReceived", QuestReceivedVolume]);
			_loadQueue.push(["QuestMenuMove", "QuestMenuMove", QuestMenuMoveVolume]);
			_loadQueue.push(["ScrollDrop", "ScrollDrop", ScrollDropVolume]);
			_loadQueue.push(["QuestDone", "QuestDone", QuestDoneVolume]);
			
			_loadQueue.push(["RunningOutOfTime", "RunningOutOfTime", RunningOutOfTimeVolume]);
			_loadQueue.push(["Stun", "Stun", StunVolume]);
			
			_loadQueue.push(["FadeIn", "Fade", FadeInVolume]);
			_loadQueue.push(["FadeOut", "Fade", FadeOutVolume]);			
			_loadQueue.push(["SkillPurchased", "SkillPurchased", SkillPurchasedVolume]);
			_loadQueue.push(["Hypnosis", "Hypnosis", HypnosisVolume]);
			
			_loadQueue.push(["Heal", "Heal1", HealVolume]);
			LoadNext();
			
			World.gWorld.addEventListener("N_TimeUp", OnTimeUp);
			World.gWorld.addEventListener(L_HEventMonsterKilled.H_EVENT_MONSTER_KILLED, OnEventMonsterKilled);			
		}
		
		static private function OnEventMonsterKilled(e:L_HEventMonsterKilled):void 
		{
			if (_runningOutOfTimeChannel)
			{
				_runningOutOfTimeChannel.stop();
				_runningOutOfTimeChannel = null;
			}
		}
		
		static private function OnTimeUp():void 
		{
			if (_runningOutOfTimeChannel)
			{
				_runningOutOfTimeChannel.stop();
				_runningOutOfTimeChannel = null;
			}			
		}
		
		static private function LoadNext():void 
		{
			World.gWorld.dispatchEventWith("soundsDecodingProgress", false, { loaded: _loadIndex, total: _loadQueue.length } );
			var b: ByteArray = new Assets[_loadQueue[_loadIndex][1]] as ByteArray;
			//if (_loadIndex < 2)
			//{
				//_soundBytes[_loadQueue[_loadIndex][0]] = new Vector.<Object>;
				//_soundBytes[_loadQueue[_loadIndex][0]].push(0,0);
				//_soundBytes[_loadQueue[_loadIndex][0]].push( { rawBytes: b, length: 0, decoded: false } );
				//IncreaseCounter();								
			//}
			//else
			//{
				_oggManager.decode(b, NUM_SAMPLES*8);
			//}
		}
		
		static private function OnDecodeComplete(e:OggManagerEvent):void 
		{
			var b: ByteArray = new ByteArray;
			b.writeBytes(_oggManager.decodedBytes);
			if (_soundBytes[_loadQueue[_loadIndex][0]] == undefined)
			{
				_soundBytes[_loadQueue[_loadIndex][0]] = new Vector.<Object>;
				_soundBytes[_loadQueue[_loadIndex][0]].push(_loadQueue[_loadIndex][2]);
				_soundBytes[_loadQueue[_loadIndex][0]].push(0);
			}
			_soundBytes[_loadQueue[_loadIndex][0]].push( { bytes: b, length: (b.length / BASE_FREQUENCY) / 8, decoded: true } );
			IncreaseCounter();
		}
		
		static private function IncreaseCounter():void 
		{
			_loadIndex++;
			if (_loadIndex < _loadQueue.length)
				LoadNext();
			else
			{
				World.gWorld.dispatchEventWith("soundsDecodingProgress", false, { loaded: _loadIndex, total: _loadQueue.length } );
				trace("_ii", "All sounds decoded");
				_loaded = true;
				World.gWorld.dispatchEventWith("soundsLoaded");
			}			
		}
		
		static public function PlayRunningOutOfTimeSound():void 
		{
			if (_runningOutOfTimeChannel)
				return;
			PlaySound("RunningOutOfTime");
			_runningOutOfTimeChannel = _soundChannels.pop();
		}
		
		static public function PlaySound(name: String, loops: int = 0, addToList: Boolean = true):  Array
		{
			if (!_loaded)
				return null;			
			if ((loops == -1 && _muteMusic) || (loops > -1 && _muteSounds))
				return null;
			var n: int = Misc.Random(2, _soundBytes[name].length);					//1st position is occupied by the record of the number of channels being played, 2nd - by the time when last track started
			//if (_soundBytes[name][0] > 2)
			//{
				//var timeDiff: Number = Misc.GetServerTime() - _soundBytes[name][1];
				//if (timeDiff < 0.3)
					//return null;
				//else if (timeDiff < 0.5)
				//{
					//if (Misc.Random(0, 100) < 50)
						//return null;
				//}
			//}
			var s: HSound = new HSound(name, n, _soundBytes[name][n].length, loops);
			if (_soundBytes[name][n].length == 0)
				_oggManager.initDecoder(_soundBytes[name][n].rawBytes);
			s.addEventListener(SampleDataEvent.SAMPLE_DATA, OnSampleData, false);
			//_soundBytes[name][0]++;
			_soundBytes[name][1] = Misc.GetServerTime();
			var soundChannel: SoundChannel = s.play();
			if (!soundChannel)
				return null;
			if (addToList && loops > -1)
				_soundChannels.push(soundChannel);
			if (_soundChannels.length > MAX_SOUND_CHANNELS)
			{
				_soundChannels[0].stop();
				_soundChannels.splice(0, 1);
			}
			var transform: SoundTransform = CopyTransform(_baseSoundTransform);
			transform.volume *= _soundBytes[name][0];
			soundChannel.soundTransform = transform;
			return [s, soundChannel];
		}
		
		static public function CopyTransform(transform: SoundTransform): SoundTransform 
		{
			var newTransform: SoundTransform = new SoundTransform;
			newTransform.volume = transform.volume;
			newTransform.rightToRight = transform.rightToRight;
			newTransform.rightToLeft = transform.rightToLeft;
			newTransform.pan = transform.pan;
			newTransform.leftToRight = transform.leftToRight;
			newTransform.leftToLeft = transform.leftToLeft;
			return newTransform;
		}
				
		static public function SetDungeonTheme(dungeonType: int):void 
		{	
			StopTheme();
			_currentDungeonThmeNumber = dungeonType;
			var soundName: String = "DungeonTheme" + dungeonType;
			if (_soundBytes[soundName] == undefined)
			{
				_currentDungeonThmeNumber = 0;
				soundName = "DungeonTheme0";
			}
			var r: Array = PlaySound(soundName, -1, false);
			if (r)
			{
				_dungeonSound = r[0];
				_dungeonThemeChannel = r[1];
			}
		}
		
		static public function StopTheme():void 
		{
			if (_dungeonThemeChannel)
			{	
				_dungeonThemeChannel.stop();
				_dungeonSound.removeEventListener(SampleDataEvent.SAMPLE_DATA, OnSampleData, false);
				_dungeonThemeChannel = null;
			}
		}
		
		static public function PauseTheme():void 
		{
			if (_dungeonThemeChannel)
			{	
				_dungeonThemeChannel.stop();
				_dungeonSound.removeEventListener(SampleDataEvent.SAMPLE_DATA, OnSampleData, false);
			}
		}
		
		static public function ContinueTheme():void 
		{
			if (!_loaded)
				return;
			if (_dungeonThemeChannel)
			{
				_dungeonSound.addEventListener(SampleDataEvent.SAMPLE_DATA, OnSampleData, false);
				_dungeonThemeChannel = _dungeonSound.play();
				var transform: SoundTransform = CopyTransform(_baseSoundTransform);
				transform.volume *= _baseDungeonThemeVolume;
				_dungeonThemeChannel.soundTransform = transform;				
			}
			else
			{
				var r = PlaySound("DungeonTheme" + _currentDungeonThmeNumber);
				if (r)
				{
					_dungeonSound = r[0];
					_dungeonThemeChannel = r[1];
				}				
			}
		}
		
		static private function OnSampleData(e:SampleDataEvent): void 
		{
			var hsound: HSound = HSound(e.target);
			var bytes: ByteArray = null;
			if (_soundBytes[hsound.sid][hsound.n].bytes == undefined)
				_soundBytes[hsound.sid][hsound.n].bytes = new ByteArray;
			bytes = _soundBytes[hsound.sid][hsound.n].bytes;
			
			if (_soundBytes[hsound.sid][hsound.n].decoded == false)
			{
				var tempBytes: ByteArray = new ByteArray;
				var result: Object = _oggManager.getSampleData(NUM_SAMPLES, tempBytes);
				if (tempBytes.length < NUM_SAMPLES * BYTES_PER_SAMPLE)
				{
					_soundBytes[hsound.sid][hsound.n].decoded = true;
					_soundBytes[hsound.sid][hsound.n].length = (_soundBytes[hsound.sid][hsound.n].bytes.length / BASE_FREQUENCY) / 8;
				}
				else
				{
					bytes.writeBytes(tempBytes);
				}
			}			
			
			for (var i:int = 0; i < NUM_SAMPLES; i++)
			{//feed sound
				bytes.position = hsound.bytesPosition;
				if (bytes.bytesAvailable < BYTES_PER_SAMPLE)
				{//loop
					hsound.bytesPosition = 0;
					bytes.position = 0;
					if (hsound.loops > -1)
					{
						hsound.loops--;
						if (hsound.loops < 0)
						{
							hsound.removeEventListener(SampleDataEvent.SAMPLE_DATA, OnSampleData);
							//_soundBytes[hsound.sid][0]--;
							return;
						}
					}
				}//loop
				
				//feed data
				e.data.writeFloat(bytes.readFloat());		//Left Channel
				e.data.writeFloat(bytes.readFloat());		//Right Channel
				hsound.bytesPosition = bytes.position;
			}//feed sound
		}
					
	}

}

//package system 
//{
	//import com.jac.ogg.events.OggManagerEvent;
	//import com.jac.ogg.OggManager;
	//import flash.events.Event;
	//import flash.events.SampleDataEvent;
	//import flash.media.Sound;
	//import flash.media.SoundChannel;
	//import flash.media.SoundTransform;
	//import flash.utils.ByteArray;
	//import flash.utils.Dictionary;
	//import logic.Dungeon;
	///**
	 //* ...
	 //* @author Aikei
	 //*/
	//public class HSounds 
	//{
		////static private var _dungeonTheme: Vector.<Class> = new Vector.<Class>;
		//static private const NUM_SAMPLES:int = 2048;
		//static private var _dungeonTheme: Vector.<ByteArray> = new Vector.<ByteArray>;
		//static private var _music: Sound;
		//static private var _hitSounds: Vector.<Sound> = new Vector.<Sound>;
		//static private var _coinDropSounds: Vector.<Sound> = new Vector.<Sound>;
		//static private var _coinCollectSounds: Vector.<Sound> = new Vector.<Sound>;
		//static private var _deathSounds: Vector.<Sound> = new Vector.<Sound>;
		//static private var _purchaseSound: Sound;
		//static private var _mainSoundTransform: SoundTransform = new SoundTransform(0.25);
		//static private var _musicChannel: SoundChannel = null;
		//static private var _baseSounds: Dictionary = new Dictionary;
		//static private var _currentDungeonTheme: int = -1;
		//static private var _dungeonSound: Sound = new Sound;
		////static private var 
		////static private var _coinDropChannel: SoundChannel = null;
		////static private var _coinCollectChannel: SoundChannel = null;
		////static private var _hitChannel: SoundChannel = null;
		////static private var _deathChannel: SoundChannel = null;
		//static private var _oggManager: OggManager = new OggManager;
		//
		//static public function InitSounds() 
		//{
			//_purchaseSound = new Assets.Purchase();
			//
			////for (var i: int = 0; i < Dungeon.NUMBER_OF_DUNGEONS; i++)
			////{
				////if (i < 2)
				////_dungeonTheme.push(Assets["DungeonTheme"+i]);
			////}
			//
			//_oggManager.addEventListener(OggManagerEvent.DECODE_COMPLETE, OnDecodeComplete);
						//
			//for (var i: int = 0; i < Dungeon.NUMBER_OF_DUNGEONS; i++)
			//{				
				//if (i < 1)
				//{
					//var b: ByteArray = new Assets["DungeonTheme" + i] as ByteArray;
					//_oggManager.decode(b, b.length);					
				//}
			//}			
			//
			//for (i = 0; i < 3; i++)
			//{
				//_hitSounds.push(new Assets["Hit" + i]);
			//}
			//
			//for (i = 0; i < 1; i++)
			//{
				//_coinDropSounds.push(new Assets["Drop" + i]);
			//}				
			//
			//for (i = 0; i < 1; i++)
			//{
				//_coinCollectSounds.push(new Assets["Collect" + i]);
			//}
			//
			//for (i = 0; i < 1; i++)
			//{
				//_deathSounds.push(new Assets["Death" + i]);
			//}
			//_baseSounds["poison"] = Assets.Poison;
			//
			//_dungeonSound.addEventListener(SampleDataEvent.SAMPLE_DATA, OnDungeonSampleData, false, 0, true);
		//}
		//
		//static public function SetDungeonTheme(dungeontType: int):void 
		//{
			//_currentDungeonTheme = dungeontType;
			//_dungeonSound.play();
		//}		
		//
		////static public function SetDungeonTheme(dungeontType: int):void 
		////{
			////if (_musicChannel)
			////{
				////_musicChannel.stop();
				////_musicChannel = null;
			////}
			////_music = new _dungeonTheme[dungeontType]();
			////_musicChannel = _music.play(0,99999);
			////_musicChannel.soundTransform = _mainSoundTransform;
		////}		
		//
		//static private function OnDungeonSampleData(e:SampleDataEvent):void 
		//{
			//for (var i:int = 0; i < NUM_SAMPLES; i++)
			//{//feed sound
				//if (_dungeonTheme[_currentDungeonTheme].bytesAvailable < 8)
				//{//loop
					//_dungeonTheme[_currentDungeonTheme].position = 0;
				//}//loop
				//
				////feed data
				//e.data.writeFloat(_dungeonTheme[_currentDungeonTheme].readFloat());		//Left Channel
				//e.data.writeFloat(_dungeonTheme[_currentDungeonTheme].readFloat());		//Right Channel
			//}//feed sound
		//}
		//
		//static private function OnDecodeComplete(e:OggManagerEvent):void 
		//{
			//_dungeonTheme.push(OggManager(e.target).decodedBytes);
		//}
		//
		//static public function PlayBaseSound(name: String):void 
		//{
			//if (_baseSounds[name] != undefined)
				//_baseSounds[name].play().soundTransform = _mainSoundTransform;
		//}
		//
		//static public function PlaySound(sound: Sound,soundTransform: SoundTransform = null): SoundChannel 
		//{
			//var channel: SoundChannel = sound.play();
			//if (!soundTransform)
				//channel.soundTransform = _mainSoundTransform;
			//else
				//channel.soundTransform = soundTransform;
			//return channel;
		//}			
					//
		//static private function DisposeChannel(channel: SoundChannel):void 
		//{
			//channel.stop();
		//}
						//
		//static public function PlayHitSound():void 
		//{
			//var num: int = Misc.Random(0, _hitSounds.length);
			//PlaySound(_hitSounds[num]);			
		//}		
		//
		//static public function PlayCollectSound():void 
		//{
			//var num: int = Misc.Random(0, _coinCollectSounds.length);
			//PlaySound(_coinCollectSounds[num]);
		//}
		//
		//static public function PlayDropSound():void 
		//{
			//var num: int = Misc.Random(0, _coinDropSounds.length);
			//PlaySound(_coinDropSounds[num]);		
		//}
		//
		//static public function PlayDeathSound():void 
		//{
			//PlaySound(_deathSounds[0]);
			////var num: int = Misc.Random(0, _deathSounds.length);
			////_deathSounds[num].play();			
		//}
		//
		//static public function PlayPurchaseSound():void 
		//{
			//PlaySound(_purchaseSound);		
		//}			
		//
		//
	//}
//
//}