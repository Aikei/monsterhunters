package system 
{
	import starling.filters.ColorMatrixFilter;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HColorFilter extends ColorMatrixFilter 
	{
		
		public function HColorFilter()
		{
			_brightness = 0;
			_hue = 0;
			_saturation = 0;
			_contrast = 0;
		}			
		
		private var _brightness:Number;			 			
	 
		public function get brightness(): Number { return _brightness; }
		
		public function set brightness(value:Number):void
		{
			_brightness = value;
			reset();
			adjustBrightness(value);
		}
		
		private var _hue: Number;
		
		public function get hue(): Number { return _hue; }
		
		public function set hue(value:Number):void
		{
			_hue = value;
			reset();
			adjustHue(value);
		}
		
		private var _saturation: Number;
		
		public function get saturation(): Number { return _saturation; }
		
		public function set saturation(value:Number):void
		{
			_saturation = value;
			reset();
			adjustSaturation(value);
		}
		
		private var _contrast: Number;
		
		public function get contrast(): Number { return _contrast; }
		
		public function set contrast(value:Number):void
		{
			_contrast = value;
			reset();
			adjustContrast(value);
		}		
		
	}

}