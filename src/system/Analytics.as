package system 
{
	/**
	 * ...
	 * @author Aikei
	 */
	import com.google.analytics.GATracker;
	import com.google.analytics.AnalyticsTracker;
	import flash.display.DisplayObject;
	import flash.utils.Dictionary;
	
	public class Analytics 
	{
		static private var _pages: Dictionary = new Dictionary;
		static private var _tracker: AnalyticsTracker;
		static private var _mainPage: String = "/game_screen";
		static private var _currentFirstPage: String = "";
		static private var _currentSecondaryPage: String = "";
		static private var _fullPageString: String = "";
		
		static public function Init(d: DisplayObject) 
		{
			_tracker = new GATracker(d, "UA-68010400-2", "AS3", false);
			_pages["main"] = "/main_window";
			_pages["dungeons"] = "/dungeons_window";
			_pages["leaderboard"] = "/leaderboard_window";
			_pages["weapons"] = "/weapons";
			_pages["heroes"] = "/heroes";
			_pages["skills"] = "/skills";
		}
		
		static public function SetPage(n: int, page: String):void 
		{
			
		}		
				
		static public function TrackPage(pageStr: String)
		{
			_tracker.trackPageview(pageStr);
		}
		
		static public function TrackEvent(type: String, action: String, label:String = null, optValue: Number = 0):void 
		{
			_tracker.trackEvent(type,action,label, optValue)
		}
	}

}