package system 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class MurmurHash2 
	{
		
		static public function Hash(s:String, seed:uint = 0):String
		{
			var l:int = s.length, h:uint = seed ^ l, i:uint = 0, k:uint;
			
			while (l >= 4)
			{
				k = ((s.charCodeAt(i) & 0xff)) | ((s.charCodeAt(++i) & 0xff) << 8) | ((s.charCodeAt(++i) & 0xff) << 16) | ((s.charCodeAt(++i) & 0xff) << 24);
				k = (((k & 0xffff) * 0x5BD1E995) + ((((k >>> 16) * 0x5BD1E995) & 0xffff) << 16));
				k ^= k >>> 24;
				k = (((k & 0xffff) * 0x5BD1E995) + ((((k >>> 16) * 0x5BD1E995) & 0xffff) << 16));
				h = (((h & 0xffff) * 0x5BD1E995) + ((((h >>> 16) * 0x5BD1E995) & 0xffff) << 16)) ^ k;
				l -= 4;
				++i;
			}
			
			switch (l)
			{
				case 3:
					h ^= (s.charCodeAt(i + 2) & 0xff) << 16;
				case 2:
					h ^= (s.charCodeAt(i + 1) & 0xff) << 8;
				case 1:
					h ^= (s.charCodeAt(i) & 0xff);
					h = (((h & 0xffff) * 0x5BD1E995) + ((((h >>> 16) * 0x5BD1E995) & 0xffff) << 16));
			}
			
			h ^= h >>> 13;
			h = (((h & 0xffff) * 0x5BD1E995) + ((((h >>> 16) * 0x5BD1E995) & 0xffff) << 16));
			h ^= h >>> 15;
			
			s = (h >>> 0).toString(16).toUpperCase();
			l = s.length;
			
			if (l == 8) return s;
			if (l == 7) return "0" + s;
			if (l == 6) return "00" + s;
			if (l == 5) return "000" + s;
			if (l == 4) return "0000" + s;
			if (l == 3) return "00000" + s;
			return s;
		}		
	
	}

}