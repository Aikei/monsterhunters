package eng.particle 
{
	import com.greensock.easing.Linear;
	import com.greensock.easing.RoughEase;
	import com.greensock.TweenLite;
	import com.greensock.TweenMax;
	import d_hentity.HEntity;
	import flash.utils.setTimeout;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class Particle extends HEntity 
	{
		private var _image: Image;
		private var _dieWithin: Number;
		private var _tweens: Vector.<TweenMax> = new Vector.<TweenMax>;
		
		public var launchTo: Vec2 = null;
		public var data: ParticleData;
		
		public function Particle(texture: Texture, data: ParticleData) 
		{
			super();
			_image = new Image(texture);
			_useWorldsPassedTime = true;
			this.data = data;
			_dieWithin = Misc.RandomNoFloor(data.minDieWithin, data.maxDieWithin);
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			setTimeout(Destroy, _dieWithin*1000);
			addChild(_image);
			_image.alignPivot();			
			Launch(launchTo);
			AddEffect();
		}
		
		override public function Destroy():void 
		{
			for (var i: int = 0; i < _tweens.length; i++)
			{
				_tweens[i].kill();
			}
			_tweens = null;
			super.Destroy();
		}
		
		private function RandomlyResize():void 
		{
			var by: Number = Misc.Random(data.resizeOnStartMin, data.resizeOnStartMax);
			_image.width *= by;
			_image.height *= by;
		}
		
		private function Launch(v: Vec2 = null):void 
		{
			if (data.resizeOnStart)
				RandomlyResize();
			if (v === null)
			{
				v = new Vec2();
				v.x = Misc.RandomNoFloor(0, data.maxDirX - data.minDirX) + data.minDirX;
				v.y = Misc.RandomNoFloor(0, data.maxDirY - data.minDirY) + data.minDirY;
			}
			ApplyRelativeVelocity(v, data.speed);
		}
		
		private function AddEffect():void 
		{
			if (data.effectRotate)
			{
				var r: Number = data.baseEffectRotateBy;
				if (data.randomEffectRotateWidth > 0)
					r += Misc.Random(0, data.randomEffectRotateWidth);
				ApplyRotation(r);
			}
			if (data.effectResize)
			{
				TweenLite.to(_image, 1, { width: _image.width*data.effectResizeBy, height: _image.height*data.effectResizeBy } );
			}
			if (data.effectQuietlyDisappear)
			{
				TweenLite.to(this, _dieWithin, { alpha: 0 } );	
			}
			if (data.effectShake)
			{
				_tweens.push(Tweens.TweenShake(this,data.shakeTime,-1,data.shakeDistance,data.shakeStrength));
				//var t: TweenMax = TweenMax.fromTo(this, 0.2, 
							//{ repeat: -1, x: this.x - 1 }, 
							//{ repeat: -1, x: this.x + 1, ease: RoughEase.ease.config( { strength: 2, points: 15, template: Linear.easeNone, randomize: false } ) } );
				//_tweens.push(t);
			}
		}		
	}		
		
}