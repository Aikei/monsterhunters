package eng.particle 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class ParticleData 
	{
		public var speed: Number = 300;
		public var minDirX: Number = -1;
		public var maxDirX: Number = 1;
		public var minDirY: Number = -1;
		public var maxDirY: Number = 1;
		
		public var resizeOnStart: Boolean = false;
		public var resizeOnStartMin: Number = 0.5;
		public var resizeOnStartMax: Number = 2;
		
		public var effectResize: Boolean = false;
		public var effectResizeBy: Number = 0.25;
		
		public var effectRotate: Boolean = false;		
		public var randomEffectRotateWidth: Number = 0;
		public var baseEffectRotateBy: Number = Misc.PI / 4;
		
		public var effectShake: Boolean = false;
		public var shakeTime: Number = 0.1;
		public var shakeStrength: Number = 6;
		public var shakeDistance: Number = 3;
		
		public var effectQuietlyDisappear: Boolean = false;
		
		public var minDieWithin: Number = 1.25;
		public var maxDieWithin: Number = 1.25;
		
		public function ParticleData() 
		{
			
		}
		
	}

}