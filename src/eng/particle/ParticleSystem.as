package eng.particle 
{
	import d_hentity.HEntity;
	import starling.events.EnterFrameEvent;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class ParticleSystem extends HEntity 
	{
		public static const TYPE_RANDOM: int = 0;
		public static const TYPE_RADIAL: int = 1;
		
		
		
		private var _inCycle: Boolean = false;
		private var _cycleNumPerStep: Number;
		private var _minCycleStepDelay: Number;
		private var _maxCycleStepDelay: Number;
		private var _allCyclesTime: Number;
		private var _currentCycleDelay: Number = 0;
		private var _lastCycleTime: Number = 0;
		private var _allCyclesStartTime: Number;
		private var _particlesPerCycle: Number;
		
		public var creationType: int = TYPE_RANDOM;
		public var radialVec2s: Vector.<Vec2> = new Vector.<Vec2>;
		public var particleData: ParticleData;
		public var texture: Texture;
		
		public var startSpreadX: Number = 100;
		public var startSpreadY: Number = 100;
		
		public function ParticleSystem(texture: Texture, data: ParticleData) 
		{
			super(false, false);
			this.touchable = false;
			this.particleData = data;
			this.texture = texture;
		}
		
		public function CreateParticles(n: int):void 
		{
			if (creationType === TYPE_RANDOM)
			{
				for (var i: int = 0; i < n; i++)
				{
					var particle: Particle = new Particle(texture, particleData);
					particle.x = Misc.Random(0, startSpreadX) - startSpreadX/2;
					particle.y = Misc.Random(0, startSpreadY) - startSpreadY/2;
					addChild(particle);
				}
			}
			else if (creationType === TYPE_RADIAL)
			{
				if (radialVec2s.length === 0)
				{
					var step: Number = (Misc.PI*2) / n;
					var currentDegree: Number = 0;					
					for (var j:int = 0; j < n; j++) 
					{
						var v: Vec2 = new Vec2(Math.cos(currentDegree), Math.sin(currentDegree));
						radialVec2s.push(v);
						currentDegree += step;
					}
				}
				for (i = 0; i < radialVec2s.length; i++)
				{
					particle = new Particle(texture, particleData);
					particle.x = Misc.Random(0, startSpreadX) - startSpreadX/2;
					particle.y = Misc.Random(0, startSpreadY) - startSpreadY/2;					
					particle.launchTo = radialVec2s[i];
					addChild(particle);
				}
			}
		}
		
		public function CycleCreateParticles(numPerStep: Number, minStepDelay: Number, maxStepDelay: Number, time: Number):void 
		{
			_inCycle = true;
			_minCycleStepDelay = minStepDelay;
			_maxCycleStepDelay = maxStepDelay;
			_allCyclesTime = time;
			_allCyclesStartTime = World.timeNow;
			_particlesPerCycle = numPerStep;
			_currentCycleDelay = 0;
		}
				
		public function OnGlobalEnterFrame(event: EnterFrameEvent):void 
		{
			if (_inCycle)			
			{				
				if (World.timeNow - _allCyclesStartTime >= _allCyclesTime)
				{
					_inCycle = false;
				}
				else 
				{
					if (World.timeNow - _lastCycleTime >= _currentCycleDelay)
					{						
						CreateParticles(_particlesPerCycle);
						_lastCycleTime = World.timeNow;
						_currentCycleDelay = Misc.RandomNoFloor(_minCycleStepDelay, _maxCycleStepDelay);
					}					
				}
			}
			for (var i: int = 0; i < this.numChildren; i++)
			{
				Particle(this.getChildAt(i)).OnEnterFrame(event);
			}
		}
		
		override public function Destroy():void 
		{
			for (var i: int = 0; i < numChildren; i++)
			{
				Particle(getChildAt(i)).Destroy();
			}
			super.Destroy();
		}
		
		public function Start():void 
		{
			
		}
		
	}

}