package ads 
{
	import com.vk.MainVKBanner;
	import com.vk.MainVKBannerEvent;
	import com.vk.vo.BannersPanelVO;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.URLRequest;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.system.SecurityDomain;
	import starling.core.Starling;
	/**
	 * ...
	 * @author Aikei
	 */
	public class VkAds extends Sprite
	{
		static public var AD_TEST_ID: String = "63035";
		static public var AD_MAIN_ID: String = "63034";
		static public var AD_1_ID: String = AD_MAIN_ID;		
		static public var AD_2_ID: String = AD_MAIN_ID;
		
		public function VkAds():void 
		{
			addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
		}
		
		private function OnAddedToStage(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			//addChild(_banner1)
		}		
		
		private function OnLoadComplete(e:Event):void 
		{
			var banner1: MainVKBanner = new MainVKBanner(AD_1_ID);	
			var bannersPanelVO: BannersPanelVO = new BannersPanelVO;
			bannersPanelVO.ad_width = Ads.BOTTOM_BLOCK_WIDTH;
			bannersPanelVO.ad_height = Ads.BOTTOM_BLOCK_HEIGHT;
			bannersPanelVO.ads_count = Ads.BOTTOM_BANNERS_PER_BLOCK;
			//bannersPanelVO.demo = "1";
			if (Ads.BOTTOM_BLOCK_TYPE === Ads.TYPE_HORIZONTAL)
				bannersPanelVO.ad_type = BannersPanelVO.AD_TYPE_HORIZONTAL;
			else
				bannersPanelVO.ad_type = BannersPanelVO.AD_TYPE_VERTICAL;
			if (Ads.BOTTOM_UNIT_TYPE === Ads.TYPE_HORIZONTAL)
				bannersPanelVO.ad_unit_type = BannersPanelVO.AD_UNIT_TYPE_HORIZONTAL;
			else
				bannersPanelVO.ad_unit_type = BannersPanelVO.AD_UNIT_TYPE_VERTICAL;
			bannersPanelVO.font_size = Ads.BOTTOM_BLOCK_FONT;
			//bannersPanelVO.bg_color = "0x"+Ads.BOTTOM_BLOCK_BG_COLOR.toString(16);
			banner1.initBanner(World.gWorld.flashVars, bannersPanelVO);
			banner1.x = Ads.BOTTOM_BLOCK_X;
			banner1.y = Ads.BOTTOM_BLOCK_Y;
			banner1.addEventListener(MainVKBannerEvent.LOAD_COMPLETE, banner_onLoad);
			banner1.addEventListener(MainVKBannerEvent.LOAD_IS_EMPTY, banner_onAdsEmpty);
			banner1.addEventListener(MainVKBannerEvent.LOAD_ERROR, banner_onError);
			Starling.current.nativeStage.addChild(banner1);
			
			if (Ads.ADD_RIGHT_BLOCK)
			{
				var banner2: MainVKBanner = new MainVKBanner(AD_2_ID);
				var bannersPanelVO2: BannersPanelVO = new BannersPanelVO;
				bannersPanelVO2.ad_width = Ads.RIGHT_BLOCK_WIDTH;
				bannersPanelVO2.ad_height = Ads.RIGHT_BLOCK_HEIGHT;
				bannersPanelVO2.ads_count = Ads.RIGHT_BANNERS_PER_BLOCK;
				if (Ads.RIGHT_BLOCK_TYPE === Ads.TYPE_HORIZONTAL)
					bannersPanelVO2.ad_type = BannersPanelVO.AD_TYPE_HORIZONTAL;
				else
					bannersPanelVO2.ad_type = BannersPanelVO.AD_TYPE_VERTICAL;
				if (Ads.RIGHT_UNIT_TYPE === Ads.TYPE_HORIZONTAL)
					bannersPanelVO2.ad_unit_type = BannersPanelVO.AD_UNIT_TYPE_HORIZONTAL;
				else
					bannersPanelVO2.ad_unit_type = BannersPanelVO.AD_UNIT_TYPE_VERTICAL;
				bannersPanelVO2.font_size = Ads.RIGHT_BLOCK_FONT;
				//bannersPanelVO2.bg_color = "0x"+Ads.RIGHT_BLOCK_BG_COLOR.toString(16);
				banner2.initBanner(World.gWorld.flashVars, bannersPanelVO2);
				banner2.x = Ads.RIGHT_BLOCK_X;
				banner2.y = Ads.RIGHT_BLOCK_Y;
				Starling.current.nativeStage.addChild(banner2);
			}
			
			World.gWorld.dispatchEventWith("adsInited");
		}
		
		private function banner_onError(e: Event):void 
		{
			MConsole.Write("_ii", "Banner Error: ",MainVKBannerEvent(e).errorMessage);
		}
		
		private function banner_onAdsEmpty(e: Event):void 
		{
			MConsole.Write("_ii", "Banner empty",MainVKBannerEvent(e).errorMessage);
		}
		
		private function banner_onLoad(e: Event):void 
		{
			MConsole.Write("_ii", "Banner loaded",MainVKBannerEvent(e).errorMessage);
		}
		
		public function InitAds() 
		{
			var loader: Loader = new Loader;
			var context: LoaderContext = new LoaderContext(false, ApplicationDomain.currentDomain);
			context.securityDomain = SecurityDomain.currentDomain;
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, OnLoadComplete);
			loader.load(new URLRequest("//api.vk.com/swf/vk_ads.swf"), context);
		}
		
	}

}