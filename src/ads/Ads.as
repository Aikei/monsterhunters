package ads 
{
	/**
	 * ...
	 * @author Aikei
	 */
	public class Ads 
	{
		static public const TYPE_HORIZONTAL: int = 0;
		static public const TYPE_VERTICAL: int = 1;
		
		static public var BOTTOM_BLOCK_HEIGHT: Number = 200;
		static public var BOTTOM_BLOCK_WIDTH: Number = 900;
		static public var BOTTOM_BLOCK_X: Number = 0;
		static public var BOTTOM_BLOCK_Y: Number = 0;
		static public var BOTTOM_BLOCK_TYPE: int = TYPE_HORIZONTAL;
		static public var BOTTOM_UNIT_TYPE: int = TYPE_VERTICAL;		
		static public var BOTTOM_BANNERS_PER_BLOCK: Number = 4;							
		static public var BOTTOM_BLOCK_FONT: String = "medium";
		static public var BOTTOM_BLOCK_BG_COLOR: uint = 0xffffff;		
		
		static public var ADD_RIGHT_BLOCK: Boolean = false;
		static public var RIGHT_BLOCK_WIDTH: Number = 0;
		static public var RIGHT_BLOCK_HEIGHT: Number = 0;
		static public var RIGHT_BLOCK_X: Number = 0;
		static public var RIGHT_BLOCK_Y: Number = 0;
		static public var RIGHT_BLOCK_TYPE: int = TYPE_HORIZONTAL;
		static public var RIGHT_UNIT_TYPE: int = TYPE_HORIZONTAL;			
		static public var RIGHT_BANNERS_PER_BLOCK: Number = 4;
		static public var RIGHT_BLOCK_FONT: String = "small";
		static public var RIGHT_BLOCK_BG_COLOR: uint = 0xffffff;		
		
		static public function InitAds() 
		{
			if (World.platform === 'vk')
			{
				var vkAds: VkAds = new VkAds;
				vkAds.InitAds();
			}
			else
			{
				World.gWorld.dispatchEventWith("adsInited");
			}
		}
		
	}

}