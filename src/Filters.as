package 
{
	import starling.filters.ColorMatrixFilter;
	/**
	 * ...
	 * @author Aikei
	 */
	public class Filters 
	{
		
		static public var highlightFilter: ColorMatrixFilter = new ColorMatrixFilter;
		static public var highlight2Filter: ColorMatrixFilter = new ColorMatrixFilter;
		static public var darkenM02Filter: ColorMatrixFilter = new ColorMatrixFilter;
		static public var darkenFilter: ColorMatrixFilter = new ColorMatrixFilter;
		static public var greyScaleFiler: ColorMatrixFilter = new ColorMatrixFilter;
		static public var greyScaleHighlightFiler: ColorMatrixFilter = new ColorMatrixFilter;
		
		static public function InitFilters() : void
		{
			highlightFilter.adjustBrightness(0.07);
			highlight2Filter.adjustBrightness(0.15);
			greyScaleFiler.adjustSaturation( -1);
			darkenFilter.adjustBrightness( -0.07);
			darkenM02Filter.adjustBrightness( -0.05);
			greyScaleHighlightFiler.adjustSaturation( -1);
			greyScaleHighlightFiler.adjustBrightness(0.07);
		}
		
	}

}