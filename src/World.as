package  
{
	/**
	 * ...
	 * @author Aikei
	 */
	
	import ads.Ads;
	import ads.VkAds;
	import controller.*;
	import controller.profiles.SocialProfile;
	import controller.remote.RemoteGameController;
	import flash.geom.Rectangle;
	import flash.system.Security;
	import flash.utils.setTimeout;
	import logic.ClientLogic;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.TouchEvent;
	import system.HSounds;
	import system.ProgramData;
	import system.ScriptsInit;
	import view.GameView;
	import logic.HeroData;
	import hevents.*;
	import view.LoadingView;
	import view.TransitionView;
	import logic.TalentEffects;
	//import logic.Logic;
	import system.Analytics;
	
	public class World extends Sprite 
	{
		public static const PHASE_CONNECTING_TO_SERVER: int = 0;
		public static const PHASE_IN_GAME: int = 1;
		public static const PHASE_IN_TRANSITION: int = 2;
		
		public const PHYSICS_DISCRETE: int = 0;
		public const PHYSICS_CONTINUOUS: int = 1;
		
		public static var data: ServerData;		
		public static var gWorld: World;
		public static var gameController: IGameController;
		public static var SHOW_TUTORIAL: Boolean = false;
		public static var REST_DOWNLOADED: Boolean = false;
		public static var DUNGEONS_LOADED: Boolean = false;
		
		public var flashVars: Object;
		public var socialProfile: SocialProfile = new SocialProfile;
		protected var _gameView: GameView;
		protected var _transitionView: TransitionView;
		protected var _loadingView: LoadingView;
		public var clientLogic: ClientLogic;
		public var physics: int = PHYSICS_CONTINUOUS;
		public var physSpeedMultiplier: Number = 1;
		public var firstFrameAfterDiscrete: Boolean = false;
		public var paused: Boolean = false;
		
		private var _phase: int = PHASE_CONNECTING_TO_SERVER;
		private var _dataObject: Object;
		
		private var _fps: Number = 0;
		private var _df: Number;
		private var _totalTime: Number = 0;
		private var _frameCount: int = 0;
		
		private const FPS_UPDATE_INTERVAL:Number = 0.5;
		private var _console: MConsole = new MConsole;
		private var _fpsSnapped: Boolean = false;
		private var _cont: Boolean = true;
		private var _inited: Boolean = false;
		private var _soundsInited: Boolean = false;
		private var _initAfterLoad: Boolean = true;
		private var _initRareAfterLoad: Boolean = true;
		
		public static var vkWindowIncreased: Boolean = false;
		public static var framePassedTime: Number = 0;
		public static var timeNow: Number = 0;
		
		public function set cont(c: Boolean):void 
		{			
			_cont = c;
			MaybeInitController();
		}
				
		static public var platform: String = "web";
		
		public function get phase(): int
		{
			return _phase;
		}
		
		public function get fps(): Number 
		{
			return _fps;
		}
		
		public function World() 
		{
			ProgramData.Init();	
			gWorld = this;
			FindPlatform();
			addEventListener("scriptsInitDone", OnScriptsInitDone);	
			ScriptsInit.Init();
		}
		
		private function FindPlatform():void 
		{
			flashVars = Starling.current.nativeStage.root.loaderInfo.parameters;
			if (flashVars.viewer_id != undefined)
				platform = "vk";			
		}
		
		private function OnScriptsInitDone(e:Event):void 
		{
			AfterScriptsInit();
		}
		
		private function AfterScriptsInit():void 
		{
			removeEventListener("scriptsInitDone", OnScriptsInitDone);
			addEventListener("adsInited", OnAdsInited);
			Ads.InitAds();				
		}
		
		private function OnAdsInited(e:Event):void 
		{
			removeEventListener("adsInited", OnAdsInited);
			HSounds.InitSounds();
			Language.Init();		//inits language strings
			Filters.InitFilters();									
			gameController = new RemoteGameController;	//controller - an object which controls connection with the server
			
			Assets.InitLocalAssets();
			_loadingView = new LoadingView;				//loading screen
			addChild(_loadingView);
			_transitionView = new TransitionView;		//dungeon transition screen (closing and opening doors)
			addChild(_transitionView);
			addChild(_console);				
			
				
			Misc.Init();			
			Tweens.Init();
			Assets.InitAssests();	//inits assets
			TalentEffects.Init();	//adds event listener to skillPicked
			//Assets.InitCursor();
						
			addEventListener("N_DataReceived", OnDataReceived);		//called when server data is received from the server
			addEventListener("doorsOpened", OnDoorsOpened);			//called when doors open after dungeon has been changed
			addEventListener("dungeonLoadDone", OnDungeonloadDone);
			addEventListener("soundsLoaded", OnSoundsLoaded);
			addEventListener("downloadDone", OnDownloadDone);
				//vars object containing, among other things, vk profile info
			//if (stage)
				//Init();		//init if the object already on stage ...
			//else
				//addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage); // ... or wait if it isn't already
			stage.addEventListener(Event.ENTER_FRAME, OnEnterFrame);
			stage.addEventListener(TouchEvent.TOUCH, OnGlobalTouch);
			addEventListener(Event.REMOVED, OnRemoved);					
			Init();	
		}		
		

		
		private function OnGlobalTouch(e:TouchEvent):void 
		{
			if (_gameView)
				_gameView.OnGlobalTouch(e);
		}
		
		private function OnRemoved(e:Event):void 
		{
			if (e.target is World)
				HSounds.StopTheme();	
		}
		
		private function OnSoundsLoaded(e:Event):void 
		{
			_soundsInited = true;
			MaybeInitController();
		}
		
		private function OnDungeonloadDone(e:Event):void 
		{
			if (_initAfterLoad)
			{
				gameController.StartGame();
				InitAfterLoading();
				_initAfterLoad = false;
				Assets.DownloadAssets(_dataObject);
			}
			//else if (_initRareAfterLoad)
			//{
				//removeEventListener("dungeonLoadDone", OnDungeonloadDone);
				//_initRareAfterLoad = false;
				//gameController.RareDownloaded();
				//Assets.DownloadAssets(_dataObject);
			//}
		}
		
		static public function SetViewPort(w: Number, h: Number): void 
		{
			Starling.current.viewPort = new Rectangle(0, 0, w, h);
			Starling.current.stage.stageWidth = w;
			Starling.current.stage.stageHeight = h;			
		}
		
		protected function OnEnterFrame(event: EnterFrameEvent): void 
		{
			framePassedTime = event.passedTime;
			timeNow = Misc.GetServerTime();
			if (_inited)
			{
				_gameView.OnGlobalEnterFrame(event);
			}
			//_totalTime += event.passedTime;
			//_frameCount++;
            //if (_totalTime > FPS_UPDATE_INTERVAL)
            //{
                //FpsUpdate();
                //_frameCount = _totalTime = 0;
            //}	
		}
		
		protected function FpsUpdate(): void
		{
			_fps = _totalTime > 0 ? _frameCount / _totalTime : 0;
			_df = 60 - _fps;
			if (_fps <= 30)
				physSpeedMultiplier = 1.0-0.004*_df;
		}
		
		protected function OnDoorsOpened(event: Event): void 
		{
			SetPhase(World.PHASE_IN_GAME);
		}
		
		//public function OnAddedToStage(ev: Event): void
		//{
			//removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			//Init();			
		//}
		
		public function Init(): void		//called by Assets when done loading
		{			
			//addChild(_keyboardHandler);
			//Starling.current.showStats = true;
			_gameView = new GameView;					//main game screen, containing all directly game-related graphics			
			addChild(_gameView);		

			removeChild(_loadingView);
			addChild(_loadingView);
			
			removeChild(_transitionView);
			addChild(_transitionView);				
			
			removeChild(_console);
			addChild(_console);	
			
			clientLogic = new ClientLogic;				//client-side logic object
			_inited = true;
			MaybeInitController();
		}
		
		public function MaybeInitController():void 
		{
			MConsole.Write("_ii", "MaybeInitController()","cont = ",_cont, "inited =",_inited, "sounds inited = ",_soundsInited);
			if (_cont && _inited && _soundsInited)
				gameController.Init();						//inits profile and connects to server, which sends server data back here (see OnDataReceived)			
		}
		
		public function SetPhase(newPhase: int): void
		{
			if (_phase === PHASE_CONNECTING_TO_SERVER && newPhase !== PHASE_CONNECTING_TO_SERVER)
			{
				setTimeout(_loadingView.OpenDoors,10);
			}
			_phase = newPhase;
		}
		
		public function OnDataReceived(event: Event): void 
		{
			MConsole.Write("_ii", "Server data received");
			_dataObject = event.data;
			socialProfile.InitProfileFromFullProfileData(_dataObject.profile);
			data = new ServerData;
			data.InitFromServerData(_dataObject);			
			InitAfterDataReceived();
		}		
		
		private function InitAfterDataReceived():void 
		{
			Assets.DownloadAssets(_dataObject);			
		}
		
		//private function OnDownloadDone(e:Event):void 
		//{
			//Assets.MaybeFlushSo();
			//if (uint(e.data) === 1)
			//{
				////gameController.StartGame();
				//Assets.LoadDungeonResources(_dataObject.dungeons[_dataObject.game.currentDungeon],Assets.RARE_MODE_WITHOUT_RARE);				
			//}
			//else if (uint(e.data) === 2)
			//{				
				//Assets.LoadDungeonResources(_dataObject.dungeons[_dataObject.game.currentDungeon],Assets.RARE_MODE_ONLY_RARE,true);
			//}
			//else if (uint(e.data) === 3)
			//{
				//dispatchEventWith("restDownloadDone");
				//REST_DOWNLOADED = true;
				//MConsole.Write("_ii", "Rest download done");
			//}
		//}		
		
		private function OnDownloadDone(e:Event):void 
		{
			Assets.MaybeFlushSo();
			if (uint(e.data) === 1)
			{
				//gameController.StartGame();
				Assets.LoadDungeonResources(_dataObject.dungeons[_dataObject.game.currentDungeon]);				
			}
			else if (uint(e.data) === 2)
			{
				DUNGEONS_LOADED = true;
				REST_DOWNLOADED = true;
				gameController.RareDownloaded();
				dispatchEventWith("restDownloadDone");
			}
		}
		
		protected function InitAfterLoading():void 
		{
			MConsole.Write("_ii", "After load init");
			//GoogleAnalytics.tracker.trackEvent("Screen", "Game Screen");
			Analytics.TrackPage("/game_screen");
			World.SHOW_TUTORIAL = _dataObject.game.tut;
			_gameView.Init(_dataObject);
			_transitionView.Init();
			clientLogic.Init(_dataObject);	
			gameController.Start();

			MConsole.Write("_ii","Backend: ",Starling.current.context.driverInfo);
		}
		
	}

}