package hevents 
{
	import logic.Dungeon;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventDungeonLevelSet extends Event 
	{
		public static const L_HEVENT_DUNGEON_LEVEL_SET: String = "L_DungeonLevelSet";
		public var dungeon: Dungeon;
		
		public function L_HEventDungeonLevelSet(dungeon: Dungeon, bubbles:Boolean=false) 
		{
			super(L_HEVENT_DUNGEON_LEVEL_SET, bubbles);
			this.dungeon = dungeon;
		}
		
	}

}