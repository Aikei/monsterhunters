package hevents 
{
	import logic.MonsterData;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HEventDungeonChanged extends Event 
	{
		public static const H_EVENT_DUNGEON_CHANGED: String = "H_DungeonChanged";
		
		public var dungeonType: int;
		public var monsterData: MonsterData;
		public var goldLimit: BigInt;
		
		public function HEventDungeonChanged(dungeonType: int, monsterData: MonsterData, goldLimit: BigInt, bubbles:Boolean=false) 
		{
			super(H_EVENT_DUNGEON_CHANGED, bubbles);
			this.dungeonType = dungeonType;
			this.monsterData = monsterData;
			this.goldLimit = goldLimit;
		}
		
	}

}