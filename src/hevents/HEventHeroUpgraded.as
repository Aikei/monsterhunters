package hevents 
{
	import logic.HeroData;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HEventHeroUpgraded extends Event 
	{
		public static const H_EVENT_HERO_UPGRADED: String = "HeroUpgraded";
		
		public var ah: Object;
		public var heroData: HeroData;
		public var startAnimation: Boolean;
		
		public function HEventHeroUpgraded(ah: Object, heroData: HeroData, startAnimation: Boolean = true, bubbles:Boolean=false) 
		{
			super(H_EVENT_HERO_UPGRADED, bubbles);
			this.ah = ah;
			this.heroData = heroData;
			this.startAnimation = startAnimation;
		}
		
	}

}