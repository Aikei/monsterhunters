package hevents 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventCoinsReceived extends Event 
	{
		public static const L_HEVENT_COINS_RECEIVED: String = "CoinsReceived";
		
		public var numberOfCoins: BigInt;
		
		public function L_HEventCoinsReceived(numberOfCoins: BigInt, bubbles:Boolean=false) 
		{
			super(L_HEVENT_COINS_RECEIVED, bubbles);
			this.numberOfCoins = numberOfCoins;			
		}
		
	}

}