package hevents 
{
	import logic.Dungeon;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventDungeonsListInitialized extends Event 
	{
		public static const L_HEVENT_DUNGEONS_LIST_INITIALIZED: String = "L_DungeonsListInitialized";
		public var dungeonsList: Vector.<Dungeon>;
		
		public function L_HEventDungeonsListInitialized(dungeonsList: Vector.<Dungeon>, bubbles:Boolean=false) 
		{
			super(L_HEVENT_DUNGEONS_LIST_INITIALIZED, bubbles);
			this.dungeonsList = dungeonsList;
		}
		
	}

}