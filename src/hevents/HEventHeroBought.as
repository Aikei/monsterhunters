package hevents 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HEventHeroBought extends Event 
	{
		public static var H_EVENT_HERO_BOUGHT: String = "HeroBought";
		
		public var heroType: int;
		public var heroName: String;
		public var heroId: int;
		
		public function HEventHeroBought(type: int, name: String, id: int, bubbles:Boolean=false) 
		{
			super(H_EVENT_HERO_BOUGHT, bubbles);	
			this.heroType = type;
			this.heroName = name;
			this.heroId = id;
		}
		
	}

}