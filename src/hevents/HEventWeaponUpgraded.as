package hevents 
{
	import logic.Weapon;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HEventWeaponUpgraded extends Event 
	{
		public static const EVENT_TYPE: String = "H_WeaponUpgraded";
		
		public var weapon: Weapon;
		
		public function HEventWeaponUpgraded(weapon: Weapon, bubbles:Boolean=false) 
		{
			super(EVENT_TYPE, bubbles);	
			this.weapon = weapon;					
		}
		
	}

}