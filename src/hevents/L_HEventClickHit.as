package hevents 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventClickHit extends Event 
	{
		public static const L_HEVENT_CLICK_HIT: String = "LocalClickHit";
		
		public function L_HEventClickHit(bubbles:Boolean=false) 
		{
			super(L_HEVENT_CLICK_HIT,bubbles);			
		}
		
	}

}