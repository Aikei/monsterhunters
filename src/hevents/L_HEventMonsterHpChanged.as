package hevents 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventMonsterHpChanged extends Event 
	{
		public static var H_EVENT_MONSTER_HP_CHANGED: String = "MonsterHpChanged";
		
		public function L_HEventMonsterHpChanged(hpNow: BigInt, bubbles:Boolean=false) 
		{
			super(H_EVENT_MONSTER_HP_CHANGED, bubbles, hpNow);			
		}
		
	}

}