package hevents 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventStartDungeonTransition extends Event 
	{
		public static const L_HEVENT_START_DUNGEON_TRANSITION: String = "L_StartDungeonTransition";
		
		public var dungeonType: int;
		
		public function L_HEventStartDungeonTransition(dungeonType: int, bubbles:Boolean=false) 
		{
			super(L_HEVENT_START_DUNGEON_TRANSITION, bubbles, data);
			this.dungeonType = dungeonType;
		}
		
	}

}