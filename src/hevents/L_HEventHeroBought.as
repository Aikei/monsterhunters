package hevents 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	import logic.local.ActualHero;
	
	public class L_HEventHeroBought extends Event 
	{
		public static var L_HEVENT_HERO_BOUGHT: String = "L_HeroBought";
		
		public var hero: ActualHero;
		public var startAnimation: Boolean;
		
		public function L_HEventHeroBought(hero: ActualHero, startAnimation: Boolean = true, bubbles:Boolean=false) 
		{
			super(L_HEVENT_HERO_BOUGHT, bubbles);	
			this.hero = hero;
			this.startAnimation = startAnimation;
		}
		
	}

}