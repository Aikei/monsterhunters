package hevents 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventCheater extends Event 
	{
		public static const H_EVENT_CHEATER: String = "Cheater";
		
		public function L_HEventCheater(bubbles:Boolean=false) 
		{
			super(H_EVENT_CHEATER, bubbles);			
		}
		
	}

}