package hevents 
{
	import logic.Weapon;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventWeaponSet extends Event 
	{
		public static const L_HEVENT_WEAPON_SET: String = "WeaponSet";
		public var weapon: Weapon;
		public var startAnimation: Boolean;
		
		public function L_HEventWeaponSet(weapon: Weapon, startAnimation: Boolean = true, bubbles:Boolean = false) 
		{
			super(L_HEVENT_WEAPON_SET, bubbles);
			this.weapon = weapon;
			this.startAnimation = startAnimation;
		}
		
	}

}