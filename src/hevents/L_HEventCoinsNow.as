package hevents 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventCoinsNow extends Event 
	{
		public static const L_HEVENT_COINS_NOW: String = "CoinsNow";
		
		public var coins: BigInt;
		
		public function L_HEventCoinsNow(coins: BigInt, bubbles:Boolean=false) 
		{
			super(L_HEVENT_COINS_NOW, bubbles);
			this.coins = coins;
		}
		
	}

}