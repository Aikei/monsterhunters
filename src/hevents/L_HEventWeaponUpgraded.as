package hevents 
{
	import logic.Weapon;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventWeaponUpgraded extends Event 
	{
		public static const L_HEVENT_WEAPON_UPGRADED: String = "L_WeaponUgraded";
		public var weapon: Weapon;
		public var startAnimation: Boolean;
		
		public function L_HEventWeaponUpgraded(weapon: Weapon, startAnimation: Boolean = true, bubbles:Boolean=false) 
		{
			super(L_HEVENT_WEAPON_UPGRADED, bubbles);
			this.weapon = weapon;
			this.startAnimation = startAnimation;
		}
		
	}

}