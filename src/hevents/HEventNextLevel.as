package hevents 
{
	import logic.Dungeon;
	import logic.MonsterData;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HEventNextLevel extends Event 
	{
		public static const HEVENT_NEXT_LEVEL: String = "H_NextLeveL";
		
		public var monsterData: MonsterData;
		public var level: uint;
		public var skillData: Object;
		public var goldLimit: BigInt;
		
		public function HEventNextLevel(monsterData: MonsterData, level: uint, goldLimit: BigInt, bubbles:Boolean=false) 
		{
			super(HEVENT_NEXT_LEVEL, bubbles);
			this.monsterData = monsterData;
			this.level = level;
			this.skillData = skillData;
			this.goldLimit = goldLimit;
		}
		
	}

}