package hevents {
	import logic.MonsterData;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HEventNewMonser extends Event 
	{
		public static const H_EVENT_NEW_MONSTER: String = "NewMonser";
		public var monstersKilled: uint;
		
		public function HEventNewMonser(monsterData: MonsterData, monstersKilled: uint, bubbles:Boolean=false) 
		{
			super(H_EVENT_NEW_MONSTER, bubbles, monsterData);
			this.monstersKilled = monstersKilled;
		}
		
	}

}