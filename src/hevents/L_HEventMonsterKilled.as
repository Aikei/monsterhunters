package hevents 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventMonsterKilled extends Event 
	{
		public static const H_EVENT_MONSTER_KILLED: String = "MonsterKilled";
		public var monstersKilledTotal: int;
		public var glory: BigInt;
		
		public function L_HEventMonsterKilled(monstersKilledTotal: int, glory: BigInt, bubbles:Boolean=false)
		{
			super(H_EVENT_MONSTER_KILLED, bubbles);
			this.monstersKilledTotal = monstersKilledTotal;
			this.glory = glory;
		}
		
	}

}