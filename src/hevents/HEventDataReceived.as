package hevents 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HEventDataReceived extends Event 
	{
		public static const H_EVENT_DATA_RECEIVED: String = "DataReceived";
		
		public function HEventDataReceived(data:Object, bubbles:Boolean=false) 
		{
			super(H_EVENT_DATA_RECEIVED, bubbles, data);
			
		}
		
	}

}