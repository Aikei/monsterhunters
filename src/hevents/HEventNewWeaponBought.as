package hevents 
{
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class HEventNewWeaponBought extends Event 
	{
		public static const H_EVENT_NEW_WEAPON_BOUGHT: String = "H_NewWeaponBought";
		
		public var dataObject: Object;
		
		public function HEventNewWeaponBought(dataObject: Object, bubbles:Boolean=false) 
		{
			super(H_EVENT_NEW_WEAPON_BOUGHT, bubbles);
			this.dataObject = dataObject;
		}
		
	}

}