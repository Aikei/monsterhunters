package hevents 
{
	import logic.HeroData;
	import starling.events.Event;
	
	/**
	 * ...
	 * @author Aikei
	 */
	public class L_HEventHeroUpgraded extends Event 
	{
		public static const L_HEVENT_HERO_UPGRADED: String = "L_HeroUpgraded";
		
		public var id: int;
		public var heroData: HeroData;
		
		public function L_HEventHeroUpgraded(id: int, heroData: HeroData, bubbles:Boolean=false) 
		{
			super(L_HEVENT_HERO_UPGRADED, bubbles);
			this.id = id;
			this.heroData = heroData;
		}
		
	}

}