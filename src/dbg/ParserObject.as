package dbg {
	/**
	 * ...
	 * @author Aikei
	 */
	public class ParserObject 
	{
		private var _host: Object;
		private var _staticHost: Object;
		private var _property: String;
						
		public function ParserObject(host: Object, property: String) 
		{
			_host = host;
			_property = property;
		}
		
		public function SetObject(to: Object): void 
		{
			_host[_property] = to;
		}
		
		public function GetObject(): Object 
		{
			return _host[_property];
		}			
		
		public function toString(): String 
		{
			return _host[_property];
		}
		
	}

}