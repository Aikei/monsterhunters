package dbg {
	/**
	 * ...
	 * @author Aikei
	 */
	import controller.remote.Connector;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	import flash.utils.getQualifiedClassName;
	import logic.ClientLogic;
	import starling.core.Starling;
	import starling.events.Event;
	import flash.display.BlendMode;
	import system.ProgramData;
	import view.GameView;
	import view.MonsterSprite;
	
	public class CommandParser 
	{
		static private const INFINITE: int = -1;
		static private var predicate: Function = null;
		static private var objects: Array;
		static private var objectsRequired: int = 0;
		static private var objectsPossible: int = INFINITE;
		static private var _listOfCommands: Dictionary = new Dictionary;
		static public var commandsVector: Vector.<String> = new Vector.<String>;		
		static private var _listOfVariables: Dictionary = new Dictionary;
		static public var variablesVector: Vector.<String> = new Vector.<String>;
		static private var _aliasStarted: Boolean = false;
		static private var _aliases: Dictionary = new Dictionary;
		static private var _recordedAliasName: String = "";
		static private var _recordedAlias: Vector.<String> = null;
		
		static private var _explain: Boolean = false;
		
		
		
		static public function Init():void 
		{
			//for (var command: String in _listOfCommands)
			
			_listOfCommands["show"] = "syntax: '<show> [variable1] [variable2] ...' - prints values of all [variable]s to console";
			_listOfCommands["c_show"] = "syntax: '<c_show> [object] [property] ...' - prints values of the [object]'s [property], example: 'c_show native_stage stageWidth' shows width of native flash stage";
			_listOfCommands["set"] = "syntax: '<set> [variable] [value]' - sets the [variable] to [value]";
			_listOfCommands["c_set"] = "syntax: '<c_set> [object] [property] ...' - sets the [property] of the specified [object]";
			_listOfCommands["explain"] = "syntax: '<explain> [command/variable]' - explains the specified [command] or [variable]";
			_listOfCommands["addMoney"] = "syntax: '<addMoney> [number]' - adds the specified [number] of money";
			_listOfCommands["help"] = "syntax: '<help>' display short console help";
			_listOfCommands["sendEvent"] = "syntax: '<sendEvent>/<se> [event name] [event.data object. sometimes may be in JSON]' sends an event";
			_listOfCommands["setModelScale"] = "syntax: '<setModelScale> [modelName] [scale]' sets the scale of the scpesified model. 1 means 100%.";
			_listOfCommands["addMoney"] = "syntax: '<addMoney> [number]' adds the specified [number] of money.";
			
			_listOfVariables["framerate"] = "currently set framerate, i.e. the framerate which the game is trying to achieve. actual framerate may be lower";
			_listOfVariables["showStats"] = "if set to true, displays a small window with statistics: fps, number of draw calls (drw) and memory used (mem)";
			_listOfVariables["true"] = "boolean true";
			_listOfVariables["false"] = "boolean false";
			_listOfVariables["physics"] = "current physics - discrete ("+String(World.gWorld.PHYSICS_DISCRETE)+") or continuous ("+String(World.gWorld.PHYSICS_CONTINUOUS)+")";
			_listOfVariables["commands"] = "list all possible console commands";
			_listOfVariables["errorStack"] = "contains stack trace of the latest error (if any)";
			_listOfVariables["variables"] = "list all possible console variables";
			_listOfVariables["blendMode"] = "current stage blend mode";
			_listOfVariables["currentLine"] = "stores the number of the current line in the console (the one you are writing to)";
			_listOfVariables["commonMonsterScale"] = "scale of common monsters. can be set. 1 means 100%";
			_listOfVariables["rareMonsterScale"] = "scale of rare monsters. can be set. 1 means 100%";
			_listOfVariables["eliteMonsterScale"] = "scale of elite monsters. can be set. 1 means 100%";
			_listOfVariables["socialid"] = "or 'id' social profile id. can be set.";
			_listOfVariables["version"] = "current version.";
			_listOfVariables["antialiasing"] = "or 'aas'. antialiasing. can be set.";
			_listOfVariables["native_stage"] = "native flash stage";
			_listOfVariables["native_overlay"] = "native flash overlay";
			
			//_listOfVariables["line"] = "syntax '[line] [number]' - value of the line number [number] in the console";
			
			for (var cmd: Object in _listOfCommands)
			{
				commandsVector.push(cmd);
			}
			
			for (var varr: Object in _listOfVariables)
			{
				variablesVector.push(varr);
			}
		}
		
		static public function AddAlias(alias: String, meaning: Vector.<String>)
		{
			_aliases[alias] = meaning;
		}
		
		static private function Set(setWhat: Object, toWhat: Object): void 
		{
			if (setWhat is ParserObject)
				ParserObject(setWhat).SetObject(toWhat);
			else
				MConsole.CmdWrite("can't set this object");
		}
		
		static public function HasSymbol(str: String, smb: String): Boolean 
		{
			for (var i: int = 0; i < str.length; i++)
			{
				if (str.charAt(i) == smb.charAt(0))
					return true;
			}
			return false;
		}
				
		static public function ParseCommand(command: String): void 
		{
			//if (command === "lucky strike")
			//{
				//MConsole.e = true;
				//return;
			//}
			var strings: Array = command.split(" ");
			if (strings.length == 0)
				return;
			if (strings[0] !== "alias" && _aliasStarted)
			{
				_recordedAlias.push(command);
				return;
			}
			if (String(strings[0]).charAt(0) == "@")
			{
				var firstData: String = "";
				var otherData: String = "";
				var aliasname: String = strings[0].slice(1);
				var a: Vector.<String> = _aliases[aliasname];	
				var dotPosition: int = aliasname.search(/\./);
				if (!a && dotPosition !== -1)
				{
					firstData += aliasname.slice(dotPosition);
					aliasname = aliasname.slice(0, dotPosition);					
					a = _aliases[aliasname];
				}											
				if (a)
				{
					if (strings.length > 1)
						strings.slice(1).join(" ");					
					for (var i: int = 0; i < a.length; i++)
					{
						var cmd: String = a[i];
						if (i == a.length - 1)
						{
							if (firstData.length > 0)
								cmd += firstData;
							if (otherData.length > 0)
								cmd += " " + otherData;
						}
						ParseCommand(cmd);
					}
				}
				return;
			}
			if (strings[0] === "show")
			{
				predicate = MConsole.CmdWrite;
				objectsRequired = 1;
				objectsPossible = INFINITE;
			}
			else if (strings[0] === "alias")
			{
				if (_aliasStarted)
				{
					_aliases[_recordedAliasName] = _recordedAlias;
					_aliasStarted = false;
				}
				else
				{
					if (strings.length > 1)
					{
						_recordedAliasName = strings[1];
						_recordedAlias = new Vector.<String>;
						_aliasStarted = true;
					}
				}
				return;
			}
			else if (strings[0] === "set")
			{
				predicate = Set;
				objectsRequired = 2;
				objectsPossible = 2;
			}
			else if (strings[0] === "set_view_port")
			{
				predicate = World.SetViewPort;
				objectsRequired = 2;
				objectsPossible = 2;				
			}
			else if (strings[0] === "explain")
			{
				_explain = true;
				predicate = MConsole.CmdWrite;
				objectsRequired = 1;
				objectsPossible = 1;
			}
			else if (strings[0] === "addMoney")
			{
				predicate = World.gameController.AddCoins;
				objectsRequired = 1;
				objectsPossible = 1;
			}
			else if (strings[0] === "addGlory")
			{
				predicate = World.gameController.AddGlory;
				objectsRequired = 1;
				objectsPossible = 1;
			}			
			else if (strings[0] === "c_show")
			{
				predicate = function (obj: Object, property: String): void 
				{
					MConsole.CmdWrite(obj[property]);
				}
				objectsRequired = 2;
				objectsPossible = 2;				
			}
			else if (strings[0] === "c_set")
			{
				predicate = function (obj: Object, property: String, value: Object): void 
				{
					obj[property] = value;
				}
				objectsRequired = 3;
				objectsPossible = 3;				
			}
			else if (strings[0] === "setModelScale")
			{
				predicate = GameView.gView.SetMonsterModelScale;
				objectsRequired = 2;
				objectsPossible = 2;			
			}
			else if (strings[0] === "help")
			{
				predicate = function () : void
				{ 
					MConsole.CmdWrite("NOTE: symbols \'[\' and \']\' signify objects, while < and > signify predicates, these symbols should not be entered.");
					MConsole.CmdWrite("enter 'explain [command/argument]' to get explanation of the command or argument.");	
					MConsole.CmdWrite("enter \'show commands\' to list all commands or 'show variables' to list all varibles to be used as command arguments.");					
				}
				
				objectsRequired = 0;
				objectsPossible = 0;				
			}
			else if (strings[0] === "assetsFunc")
			{
				predicate = function (f: String):void 
				{
					Assets[f]();
				}
				objectsRequired = 1;
				objectsPossible = 1;
			}
			else if (strings[0] === "set_vk_window_size")
			{
				predicate = Connector.SetVkWindowSize;
				objectsRequired = 2;
				objectsPossible = 2;					
			}
			else if (strings[0] === "sendEvent" || strings[0] === "se")
			{
				predicate = function() : void
				{
					if (objects.length > 1)
						World.gWorld.dispatchEventWith(objects[0], false, objects[1]);
					else
						World.gWorld.dispatchEventWith(objects[0]);
				}
				objectsRequired = 1;
				objectsPossible = 2;					
			}
			else if (strings[0] === "setMonsterFps")
			{
				predicate = function (fps) : void
				{ 
					World.gWorld.dispatchEvent(new Event("setMonsterFps", false, fps));
				}
				objectsRequired = 1;
				objectsPossible = 1;						
			}
			else
			{
				try 
				{
					var vRes: ParserObject = ParserObject(GetVar(strings[0]));
					var f: Object = vRes.GetObject();
					predicate = function (... args) : void { f.apply(null, args); };
					objectsRequired = 0;
					objectsPossible = INFINITE;									
				}
				catch (err:Error)
				{
					predicate = null;
				}					
			}
			
			if (predicate === null)
			{
				MConsole.Write("##", "predicate is null, predicate: ",predicate);
				return;
			}
			
			if (strings.length < objectsRequired + 1)
			{
				MConsole.Write("##", "not enough arguments, number of arguments passed:",strings.length,"number needed:",objectsRequired);
				return;
			}
			
			if (objectsPossible !== INFINITE && objectsPossible > strings.length)
			{
				MConsole.Write("##", "more objects passed than possible, objects possible:",objectsPossible,"objects passed:",strings.length);
				return;				
			}
			
			objects = new Array;
			if (strings.length > 1)
			{
				for (i = 1; i < strings.length; i++)
				{
					//strings[i] = String(strings[i]).toLowerCase();
					if (!_explain)
					{
						var obj: Object = GetVar(strings[i]);
						if (obj != null)
						{
							objects.push(obj);
							continue;
						}
						if (strings[i] === "framerate")
						{
							objects.push(new ParserObject(Starling.current.nativeStage, "frameRate"));
						}
						else if (strings[i] === "showstats")
						{
							objects.push(new ParserObject(Starling.current, "showStats"));
						}
						else if (strings[i] === "commands")
						{
							objects.push(_listOfCommands);
						}
						else if (strings[i] === "variables")
						{
							objects.push(_listOfVariables);
						}
						else if (strings[i] === "blendMode")
						{
							objects.push(new ParserObject(Starling.current.stage, "blendMode"));
							//Starling.current.stage.blendMode.
						}
						else if (strings[i] === "errorstack")
						{
							if (MConsole.lastError != null)
								objects.push(MConsole.lastError.getStackTrace());
						}
						else if (strings[i] === "currentline")
						{
							objects.push(MConsole.GetCurrentLineNumber());
						}
						else if (strings[i] === "line")
						{
							if (i + 1 >= strings.length)
								throw "No line number specified";
							objects.push(MConsole.GetLineValue(int(strings[i+1])));
						}						
						else if (strings[i] === "bm_add")
						{
							objects.push(BlendMode.ADD);
						}
						else if (strings[i] === "bm_alpha")
						{
							objects.push(BlendMode.ALPHA);
						}
						else if (strings[i] === "bm_multiply")
						{
							objects.push(BlendMode.MULTIPLY);
						}
						else if (strings[i] === "bm_darken")
						{
							objects.push(BlendMode.DARKEN);
						}
						else if (strings[i] === "bm_lighten")
						{
							objects.push(BlendMode.LIGHTEN);
						}					
						else if (strings[i] === "true")
						{
							objects.push(true);
						}
						else if (strings[i] === "false")
						{
							objects.push(false);
						}
						else if (strings[i] === "antialiasing" || strings[i].toLowerCase() === "aas")
						{
							objects.push(new ParserObject(Starling.current, "antiAliasing"));
						}
						else if (strings[i] === "cont")
						{
							objects.push(new ParserObject(World.gWorld, "cont"));
						}						
						//else if (strings[i] === "socialid" || strings[i] === "id")
						//{
							//objects.push(new ParserObject(World.gWorld.socialProfile, "id"));
						//}
						else if (strings[i] === "version")
						{
							objects.push(ProgramData.version);
						}
						else if (strings[i] === "commonMonsterScale")
						{
							objects.push(new ParserObject(GameView.gView, "commonMonsterScale"));
						}
						else if (strings[i] === "rareMonsterScale")
						{
							objects.push(new ParserObject(GameView.gView, "rareMonsterScale"));
						}	
						else if (strings[i] === "eliteMonsterScale")
						{
							objects.push(new ParserObject(GameView.gView, "eliteMonsterScale"));
						}							
						else if (strings[i] === "native_stage")
						{
							objects.push(Starling.current.nativeStage);
						}
						else if (strings[i] === "native_overlay")
						{
							objects.push(Starling.current.nativeOverlay);
						}						
						else if (strings[i] === "{")
						{
							var fullString: String = "";
							for (; i < strings.length; i++)
							{
								fullString += strings[i];
								if (i < strings.length - 1)
									fullString += " ";
								if (strings[i] === "}")
									break;
							}
							if (fullString.charAt(fullString.length - 1) !== "}")
							{
								MConsole.CmdWrite("Wrong JSON object specified");
							}
							try
							{
								var dataObject: Object = JSON.parse(fullString);
							}
							catch (err: Error)
							{
								MConsole.CmdWrite(err.message);
								return;
							}
							if (dataObject !== null)
							{
								objects.push(dataObject);
							}
							else
							{
								MConsole.CmdWrite("Wrong JSON object specified");
							}							
						}
						else if (String(strings[i]).charAt(0) === '"' && String(strings[i]).charAt(String(strings[i]).length - 1) === '"')
						{
							var s: String = String(strings[i]).substring(1, String(strings[i]).length - 1);
							objects.push(s[1]);
						}
						//else if (World.gWorld[strings[i]] !== undefined)
						//{
							//objects.push(new ParserObject(World.gWorld, strings[i]));
						//}
						else if (strings[i] === "physics")
						{
							objects.push(new ParserObject(World.gWorld, "physics"));
						}
						else if (strings[i] === "pause")
						{
							objects.push(new ParserObject(World.gWorld, "paused"));
						}
						else if (Misc.CheckIfStringIsAllNumbers(strings[i])) 
						{
							objects.push(Number(strings[i]));
						}					
						else if (Misc.CheckIfStringAllCaps(strings[i]))
						{
							if (World.gWorld[strings[i]] !== undefined)
								objects.push(new ParserObject(World.gWorld, strings[i]));
						}
						else
						{
							objects.push(String(strings[i]));
						}
					}
					else
					{
						_explain = false;
						if (_listOfCommands[strings[i]] !== undefined)
						{							
							objects.push(_listOfCommands[strings[i]]);
						}
						else if (_listOfVariables[strings[i]] !== undefined)
						{
							objects.push(_listOfVariables[strings[i]]);
						}
						else
						{							
							objects.push("wrong command or variable for explanation. enter 'show commands' or 'show variables' to get the corresponding list");
						}
					}
					
				}
			}
			try 
			{
				predicate.apply(null, objects);
				MConsole.Write("_ii", "success!")
			}
			catch (err:Error)
			{
				MConsole.Write("_ii", "failed");
			}
			//if (strings[0] === "show")
			//{
				//if (strings[1] === "framerate")
				//{
					//MConsole.Write("##", Starling.current.nativeStage.frameRate);
				//}
				//else if (strings[1] === "stats")
				//{
					//Starling.current.showStats = !Starling.current.showStats;
				//}
				//else if (strings[1] === "backend")
				//{
					//MConsole.Write("_ii","Backend: ",Starling.current.context.driverInfo);
				//}
				////setTimeout(
			//}
			//else if (strings[0] === "set")
			//{
				//if (strings[1] === "framerate")
				//{
					//if (strings.length > 2)
						//Starling.current.nativeStage.frameRate = Number(strings[2]);
					//else
						//MConsole.Write("##", "Specify framerate");
				//}				
			//}
			//else if (strings[0] === "print")
			//{
				//predicate = MConsole.Write;
			//}
		}
		
		static public function GetVar(s: String, host: Object = null): Object 
		{
			if (host != null)
			{
				try 
				{
					var obj: Object = host[s];
				}
				catch (err:Error)
				{
					try 
					{
						var c: Object = getDefinitionByName(getQualifiedClassName(host));
						obj = c[s];
						if (obj !== undefined)
							return new ParserObject(c, s);								
					}
					catch (err:Error)
					{
						return err.message;
					}							
				}
				if (obj != undefined)
					return new ParserObject(host, s);
			}
			else 
			{
				try 
				{
					c = getDefinitionByName(s);
					return c;
				}								
				catch (err:Error)
				{
					if (HasSymbol(s, '.'))
					{
						var a: Array = s.split('.');
						var b: String = a.slice(0, a.length-1).join(".");
						obj = GetVar(b);
						if (obj is ParserObject)
							obj = GetVar(a[a.length-1], ParserObject(obj).GetObject());
						else
							obj = GetVar(a[a.length-1], obj);					
						return obj;
					}
					else
					{
						if (s === "m_logic")
						{
							return ClientLogic.gLogic;
						}
						else if (s === "m_view")
						{
							return GameView.gView;
						}
						else if (s === "m_world")
						{
							return World.gWorld;
						}
						//else
						//{
							//c = getDefinitionByName(s);
							//if (c != undefined)
								//return c;
						//}				
					}
				}
			}
			return null;
		}		
		
		//static public function GetVar(s: String, host: Object = null): Object 
		//{
			//if (host != null)
			//{
				//try 
				//{
					//var obj: Object = host[s];
				//}
				//catch (err:Error)
				//{
					//try 
					//{
						//var c: Object = getDefinitionByName(getQualifiedClassName(host));
						//obj = c[s];
						//if (obj !== undefined)
							//return new ParserObject(c, s);								
					//}
					//catch (err:Error)
					//{
						//return err.message;
					//}							
				//}
				//if (obj)
					//return new ParserObject(host, s);
			//}
			//else 
			//{
				//c = getDefinitionByName(s);
				//if (c != undefined)
					//return c;				
				//if (HasSymbol(s, '.'))
				//{
					//var a: Array = s.split('.');
					//obj = null;
					//for (var i: int = 0; i < a.length; i++)
					//{
						//if (obj)
						//{
							//if (obj is ParserObject)
								//obj = GetVar(a[i], ParserObject(obj).GetObject());
							//else
								//obj = GetVar(a[i], obj);
						//}
						//else
						//{
							//obj = GetVar(a[i]);
						//}
						//if (obj == null)
							//return obj;
					//}
					//return obj;
				//}
				//else
				//{
					//if (s == "logic")
					//{
						//return ClientLogic.gLogic;
					//}
					//else if (s == "view")
					//{
						//return GameView.gView;
					//}
					//else
					//{
						//c = getDefinitionByName(s);
						//if (c != undefined)
							//return cc;
					//}				
				//}
			//}
			//return null;
		//}
		
		static public function IsCommand(s: String): void 
		{
			
		}
		
	}

}