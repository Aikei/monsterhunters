var fs = require('fs')
var path = require('path')
var zlib = require('zlib')
var walk = require('walk')
var mkpath = require('mkpath')
var childProcess = require('child_process')
'use strict';

var pathDivider = '\\'
var includeFolders = []
var excludeFolders = []
var includeExtensions = []
var includeFilenames = []

var dirToWalk = 'C:\\Users\\Aikei\\Documents\\flash\\MonsterHunters\\media'
var applyCommand = ""


var commandsList =
{
   convert_to_atf: 'C:\\Utilities\\atftools\\windows\\png2atf -c -i $f -o $F.atf',
   convert_to_atf_no_block: 'C:\\Utilities\\atftools\\windows\\png2atf -i $f -o $F.atf'
}

function CreateDeflated(fileName)
{
   var strm = fs.createReadStream(fileName)
   var deflate = zlib.createDeflate()
   var out = fileName.split(pathDivider)

   for (var i = 0; i < out.length; i++)
   {
      if (out[i] == 'media')
      {
         out[i] += 'd'
      }
   }
   var dirToCreate = out.slice(0,out.length-1).join(pathDivider)
   mkpath.sync(dirToCreate)
   out = out.join(pathDivider)+'d'
   console.log('out:',out)
   out = fs.createWriteStream(out)
   strm.pipe(deflate).pipe(out)   
}

console.log('ARGS:\n',process.argv)
args_cycle:
for (var i = 2; i < process.argv.length; i++)
{
	if (process.argv[i] == '-dir')
	{
		i++
		dirToWalk = process.argv[i]
	}
	else if (process.argv[i] == '-include_folders')
	{
      i++
      while(i < process.argv.length && process.argv[i].charAt(0) != '-')
      {       
         includeFolders.push(process.argv[i])
         i++
      }
      if (i < process.argv.length && process.argv[i].charAt(0) == '-')
         i--
	}
	else if (process.argv[i] == '-include_extensions')
	{
      i++
      while(i < process.argv.length && process.argv[i].charAt(0) != '-')
      {         
         includeExtensions.push(process.argv[i])
         i++
      }
      if (i < process.argv.length && process.argv[i].charAt(0) == '-')
         i--
	}
	else if (process.argv[i] == '-include_files')
	{
      i++
      while(i < process.argv.length && process.argv[i].charAt(0) != '-')
      {
         includeFilenames.push(process.argv[i])
         i++
      }
      if (i < process.argv.length && process.argv[i].charAt(0) == '-')
         i--
	}	   
	else if (process.argv[i] == '-exclude_folders')
	{
		i++
		excludeFolders.push(process.argv[i])
	}
	else if (process.argv[i] == '-apply_command')
	{
		i++
      applyCommand = process.argv[i]
	}   
}

console.log('applyCommand: ',applyCommand)
console.log('excludeFolders: ',excludeFolders)
console.log('includeFolders: ',includeFolders)
//console.log('excludeExtensions: ',excludeExtensions)
console.log('includeExtensions: ',includeExtensions)
console.log('includeFilenames: ',includeFilenames)
console.log('dir: ',dirToWalk)

for (var key in commandsList)
{
   if (key == applyCommand)
   {
      applyCommand = commandsList[key]
      break
   }
}

var walker = walk.walk(dirToWalk, { followLinks: true })

if (applyCommand == "")
{
   console.log('no command specified')
   return
}

walker.on('file', function (root, fileStat, next)
{
	var folders = root.split('\\')
	var foundInclude = false
	var foundExclude = false
	mainCycle:
	for (var i = 0; i < folders.length; i++)
	{
		for (var j = 0; j < excludeFolders.length; j++)
		{
			if (folders[i] == excludeFolders[j])
			{
				foundExclude = true
				break mainCycle
			}
		}
		if (includeFolders.length == 0)
			foundInclude = true
		if (!foundInclude)
		{
			for (var k = 0; k < includeFolders.length; k++)
			{
				if (folders[i] == includeFolders[k])
				{
					console.log('found include folder:',folders[i])
					foundInclude = true
					break mainCycle
				}			
			}
		}
	}
	//console.log('foundInclude:',foundInclude,'foundExclude:',foundExclude)
	if (!foundExclude && foundInclude)
	{
		//console.log('checking extensions')
		var fileName = path.resolve(root, fileStat.name)
      
		if (includeFilenames.length > 0)
		{
			//console.log('includeExtensions.length > 0')
			foundInclude = false
			for (var i = 0; i < includeFilenames.length; i++)
			{
				//console.log('checking extenstion',path.extname(fileStat.name),'in file',fileStat.name)
				if (fileStat.name == includeFilenames[i])
				{
					foundInclude = true
					//console.log('found included fileName:',includeExtensions[i])
					break
				}
			}
		}
      
		if (includeExtensions.length > 0)
		{
			//console.log('includeExtensions.length > 0')
			foundInclude = false
			for (var i = 0; i < includeExtensions.length; i++)
			{
				//console.log('checking extenstion',path.extname(fileStat.name),'in file',fileStat.name)
				if (path.extname(fileStat.name) == includeExtensions[i])
				{
					foundInclude = true
					//console.log('found included extension:',includeExtensions[i])
					break
				}
			}
		}
		if (foundInclude)
		{
         console.log('found fileName:',fileName)
         if (applyCommand == 'deflate')
         {            
            CreateDeflated(fileName)
         }
         else
         {
            var actualCommand = applyCommand;
            if (actualCommand.indexOf("$f") != -1)
            {
               //console.log('string contains $f, string: ',applyCommand)
               actualCommand = actualCommand.replace("$f",fileName)
               //console.log('string after: ',applyCommand)
            }
            if (actualCommand.indexOf("$F") != -1)
            {
               //console.log('string contains $F, string: ',applyCommand)
               actualCommand = actualCommand.replace("$F",fileName.slice(0,fileName.length-4))
               //console.log('string after: ',applyCommand)
            }            
            else
            {
               if (actualCommand.charAt(actualCommand.length-1) == ' ')
                  actualCommand += fileName
               else
                  actualCommand += " "+fileName
            }
            console.log('executing command: ',actualCommand)
            childProcess.execSync(actualCommand)
         }
		}
	}
	next()
})

walker.on('error', function(root, nodeStatsArray, next)
{
   nodeStatsArray.forEach(function(n)
   {
      console.error("[ERROR] "+n.name)
      console.error(n.error.message || (n.error.code + ": " + n.error.path))
   })
   next()
})
